import React, { Component } from 'react';
import { View, localStorage, Text, Picker, FlatList, Switch, NetInfo, BackHandler, CheckBox, RefreshControl, AsyncStorage, ToastAndroid, Modal, StatusBar, ActivityIndicator, ScrollView, SafeAreaView, Linking, StyleSheet, Alert, Image, Dimensions } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import IconEnt from 'react-native-vector-icons/Entypo';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Popover from 'react-native-popover-view';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, ViewPager, PagerDotIndicator } from 'rn-viewpager';
import I18n from '../i18';
import analytics from '@react-native-firebase/analytics';
import admob, { MaxAdContentRating } from '@react-native-firebase/admob';
import messaging from '@react-native-firebase/messaging';
import { InterstitialAd, RewardedAd, BannerAd, TestIds, BannerAdSize } from '@react-native-firebase/admob';
import Ripple from 'react-native-material-ripple';
import { connect } from 'react-redux';
import api from '../api.js';
import { NavigationEvents } from 'react-navigation';
import database from '@react-native-firebase/database';
import moment from 'moment';
const height = Dimensions.get('window').height;
const { width: screenWidth } = Dimensions.get('window')


class Home extends Component {
  currencyArray = [];
  activetab = 'home';
  sensexData = [];
  Data = '';
  code = [];
  watchlistData = [];
  watchdata = [];
  portdata = [];
  indices1 = [];
  indices2 = [];
  indices3 = [];
  Quantity = 0;
  Price = 0;
  Ltp = 0;
  OverAllgain = 0;
  overAllgainper = 0;
  totalInvPrice = 0;
  totalCurrvalue = 0;
  static navigationOptions = {

    header: null,
  };
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    this.state = {
      sidemenu: false,
      settingshow: false,
      ltp: '',
      chng: '',

      per: '',
      date: '',
      status: 'Close',
      indicesArray: [],
      currencyArray: [],
      commodityArray: [],
      loader: true,
      indicesloader: true,
      commodityloader: true,
      heatmaploader: true,
      currencyloader: true,
      sensexDetailloader: true,
      modvis: false,
      Warray: [],
      refreshing: false,
      watch: true,
      port: true,
      indices: true,
      commodity: true,
      heatmap: true,
      currency: true,
      sname1: '', ltp1: '', changeval1: '', changeper1: '', scode1: '',
      sname2: '', ltp2: '', changeval2: '', changeper2: '', scode2: '',
      sname3: '', ltp3: '', changeval3: '', changeper3: '', scode3: '',
      sname4: '', ltp4: '', changeval4: '', changeper4: '', scode4: '',
      sname5: '', ltp5: '', changeval5: '', changeper5: '', scode5: '',
      sname6: '', ltp6: '', changeval6: '', changeper6: '', scode6: '',
      one: '', two: '', three: '', four: '', five: '', six: '',
      onecurr: {}, twocurr: {}, threecurr: {}, fourcurr: {}, fivecurr: {}, sixcurr: {},
      selectlan: 'English',
      langauge: '',
      circular1: false,
      circular2: false,
      circular3: false,
      circular4: false,
      showedit: false,
      iconname: 'ios-arrow-down',
      iconnamesensex: 'ios-arrow-forward',
      iconnamesme: 'ios-arrow-forward',
      iconnamederivative: 'ios-arrow-forward',
      iconnamecurrency: 'ios-arrow-forward',
      iconnamecommodity: 'ios-arrow-forward',
      iconnameird: 'ios-arrow-forward',
      iconnameetf: 'ios-arrow-forward',
      iconnamedebt: 'ios-arrow-forward',
      iconnamecorporate: 'ios-arrow-forward',
      iconnameipf: 'ios-arrow-forward',
      ipf: false,
      equity: true,
      equitynew: false,
      sensex: false,
      sme: false,
      derivative: false,
      currencytab: false,
      commoditytab: false,
      ird: false,
      etf: false,
      debt: false,
      corporate: false,
      Indices1: [],
      Indices2: [],
      Indices3: [],
      overallgain: 0,
      overallgainper: '0.00%',
      Quant: 0,
      PortData: []

    }
    
    setTimeout(() => { SplashScreen.hide() }, 4000);
  }



  componentWillMount() {
    this.portfolioData();

    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
  }

  backPressed = () => {
    Alert.alert(
      'Exit App',
      'Do you want to exit?',
      [
        { text: 'No', onPress: () => '', style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false });
    return true;
  }



  _renderItemlight({ item, index }) {
    return (
      <View style={{}}>
        <View style={item.chgper > 0 ? styles.watchwhite : styles.watchwhitered} >
          <Text style={styles.watchnamelight} numberOfLines={1}>{item.sname}</Text>
          <Text style={styles.watchvallight} numberOfLines={1}>{item.ltp}<Text style={item.chgper > 0 ? styles.currencypos : styles.currencyred}>  {item.chgval}  {item.chgper}%</Text></Text>
          <Image style={{ width: 140, height: 33, marginLeft: '10%', marginTop: 20, bottom: 7 }} source={require('../images/pulse.jpg')} />

        </View>
        <Text></Text>
        <Text></Text>


      </View>
    )
  }
  _renderItemdark({ item, index }) {
    return (

      <View>
        <View style={item.chgper > 0 ? styles.watchdark : styles.watchdarkred} >
          <Text style={styles.watchnamedark} numberOfLines={1}>{item.sname}</Text>
          <Text style={styles.watchvaldark} numberOfLines={1}>{item.ltp} <Text style={item.chgper > 0 ? styles.currencypos : styles.currencyred}>  {item.chgval}%  {item.chgper}%</Text></Text>
          <Image style={{ width: 140, height: 33, marginLeft: '10%', marginTop: 20, bottom: 7 }} source={require('../images/rsz_new_design_hompage_black-theme_code.jpg')} />
        </View>
        <Text></Text>
        <Text></Text>

      </View>
    )
  }

  tab(val) {

    if (val == 'watch') { }
    else if (val == 'port') { }
    else if (val == 'home') { }
    else if (val == 'search') {
      this.props.navigation.navigate('search');

    }
    else if (val == 'more') {
      this.setState({ sidemenu: true })
    }

  }

  lanfun() {

    AsyncStorage.getItem('language').then((lan) => {
      // alert(lan);
      if (lan == null) {
        this.setState({ langauge: 'en', selectlan: 'English' });
        I18n.locale = "en"
      }
      else {

        lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
        lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
      }

      database().ref('watchlist').on('value', (snapshot) => {
        snapshot.forEach((child) => {
          let wjson = {
            sname: child.val().sname,
            scode: child.val().scode,
            ltp: child.val().ltp,
            chgval: child.val().chgval,
            chgper: child.val().chgper
          }

          this.watchdata.push(wjson);
          if (this.watchdata == null) { }
          else {
            AsyncStorage.setItem('watchval', "false");
            this.ok();
            this.setState({ watchloader: true });
            AsyncStorage.setItem('array', JSON.stringify(this.watchdata));
            this.watchListFun();
          }

        })
      });
      this.indicesFun(this.state.langauge);
      this.sensexHeatMapFun();

    });

  }
  language() {
    AsyncStorage.getItem('language').then((lan) => {
      if (lan == null) {
        this.setState({ langauge: 'en', selectlan: 'English' });
        I18n.locale = "en"
      }
      else {
        lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
        lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });

        database().ref('watchlist').on('value', (snapshot) => {
          snapshot.forEach((child) => {
            let wjson = {
              sname: child.val().sname,
              scode: child.val().scode,
              ltp: child.val().ltp,
              chgval: child.val().chgval,
              chgper: child.val().chgper
            }

            this.watchdata.push(wjson);
            if (this.watchdata == null) { }
            else {
              AsyncStorage.setItem('watchval', "false");
              this.ok();
              this.setState({ watchloader: true });
              AsyncStorage.setItem('array', JSON.stringify(this.watchdata));
              this.watchListFun();
            }
          })
        });
        this.indicesFun(this.state.langauge);
        this.sensexHeatMapFun();
      }
    });

  }

  componentDidMount() {
    this.lanfun();
    this.commodityFun();
    AsyncStorage.getItem('themecolor').then((dt) => {
      dt == null || dt == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

    });

    analytics().setCurrentScreen('Analytics'); //google Analytics
    admob().setRequestConfiguration({          //google Ads
      maxAdContentRating: MaxAdContentRating.PG,
      tagForChildDirectedTreatment: true,
      tagForUnderAgeOfConsent: true,
    })
      .then(() => { });

    //   messaging().getToken().then(token => { token });  google Cloud messaging token

    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    });

    //setTimeout(() => { this.watchListFun() }, 2000);
    // this.watchListFun()
    this.sensexFun();
    this.currencyFun();
    // this.portfolioData();

  }

  portfolioData = async () => {

    this.OverAllgain = 0;
    this.overAllgainper = 0;

    await AsyncStorage.getItem('portdata').then((data) => {

      let portdata = JSON.parse(data);
      this.portdata = portdata;

      console.log('portdata', this.portdata);
      if (portdata == null) { }
      else {
        this.Quantity = parseFloat(portdata[0].qty);
        this.Price = parseFloat(portdata[0].rate);
        this.Ltp = parseFloat(portdata[0].ltp);
       
        for (let i = 0; i < this.portdata.length; i++) {
          let overall = this.overallfun(this.portdata[i].qty, this.portdata[i].rate, this.portdata[i].ltp);
          console.log('overall', overall);
          this.OverAllgain = parseFloat(this.OverAllgain) + parseFloat(overall);

        }   
        let per = parseFloat(this.OverAllgain) - (parseFloat(this.Quantity) * parseFloat(this.Price));
        this.overAllgainper = per.toFixed(2);
        console.log(this.overAllgainper);


      }

    });

    console.log(this.Quantity);
    console.log('overall2', this.OverAllgain);
    console.log('port2', this.overAllgainper);
    console.log('port2', this.totalCurrvalue);
    console.log('port2', this.totalInvPrice);
  }
  overallfun(qty, rate, ltp) {
    this.totalCurrvalue = parseFloat(ltp * qty);
    this.totalInvPrice = qty * rate;
    let overall = this.totalCurrvalue - this.totalInvPrice;
    // let overper = (this.OverAllgain / this.totalInvPrice) * 100 ;
    //this.overAllgainper = overper.toFixed(2);

    return overall;
  }

  ok() {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.getItem('watchval').then((dt) => {
      if (dt == null) { this.setState({ watch: true }) }
      else { this.setState({ watch: false }); }
    });

    AsyncStorage.getItem('portval').then((dt) => {
      if (dt == null) { this.setState({ port: true }) }
      else { this.setState({ port: false }); }

    });

    AsyncStorage.getItem('indval').then((dt) => {
      if (dt == null) { this.setState({ indices: true }) }
      else { this.setState({ indices: false }); }

    });

    AsyncStorage.getItem('commodt').then((dt) => {
      if (dt == null) { this.setState({ commodity: true }) }
      else { this.setState({ commodity: false }); }

    });

    AsyncStorage.getItem('heatmap').then((dt) => {
      if (dt == null) { this.setState({ heatmap: true }) }
      else { this.setState({ heatmap: false }); }

    });

    AsyncStorage.getItem('curr').then((dt) => {
      if (dt == null) { this.setState({ currency: true }) }
      else { this.setState({ currency: false }); }

    });

    this.setState({ edit: false });
  }

  sensexFun() {
    this.setState({ loader: true });
    api.date().then(res => {
      this.sensexData = res;
      if (this.sensexData != '') {
        this.setState({ ltp: this.sensexData[0].ltp, chng: this.sensexData[0].chg, per: this.sensexData[0].perchg, date: this.sensexData[0].dttm, });
        this.sensexData[0].F == '0' ? this.setState({ status: 'Open' }) : this.sensexData[0].F == '1' ? this.setState({ status: 'Pre-Open' }) : this.sensexData[0].F == '2' ? this.setState({ status: 'Close' }) : this.sensexData[0].F == '3' ? this.setState({ status: 'Open' }) : '';

        this.setState({ loader: false });
      }
      else {
        this.setState({ ltp: '', chng: '', per: '', date: '', status: '', loader: false });

      }

    });

  }

  format(val) {
    var no = val.split('.');
    if (no[0].length == 5) {
      return val.substring(0, 2) + ',' + val.substring(2);
    }
    else if (no[0].length == 4) {
      return val.substring(0, 1) + ',' + val.substring(1);
    }
    else if (no[0].length == 3 || no[0].length < 3) {
      return val;
    }
    else if (no[0].length == 6) {
      return no.substring(0, 1) + ',' + no.substring(1, 3) + ',' + no.substring(3);
    }
    else if (no[0].length == 7) {
      return no.substring(0, 1) + ',' + no.substring(1, 4) + ',' + no.substring(4);
    }

  }

  indicesFun(lan) {
    this.indices1 = [];
    this.indices2 = [];
    this.indices3 = [];
    this.setState({ indicesloader: true });
    api.indices(lan).then(res => {
      console.log(res);
      if (res != '') {
        // res.map((dt) => {
        //   if (dt.chg.charAt(0) != '-') {
        //     dt.chg = '+' + dt.chg;
        //     dt.perchg = '+' + dt.perchg;
        //   }

        // })
        this.setState({ indicesArray: res, indicesloader: false });
        for (let i = 0; i < 5; i++) {
          if (i == 5) { break; }
          this.indices1.push({ 'indexname': res[i].indxnm, 'code': res[i].code, 'chng': res[i].chg, 'chngper': res[i].perchg, 'ltp': res[i].ltp });

        }
        for (let i = 5; i < 10; i++) {
          if (i == 10) { break; }
          this.indices2.push({ 'indexname': res[i].indxnm, 'code': res[i].code, 'chng': res[i].chg, 'chngper': res[i].perchg, 'ltp': res[i].ltp });

        }
        for (let i = 10; i < 15; i++) {
          if (i == 15) { break; }
          this.indices3.push({ 'indexname': res[i].indxnm, 'code': res[i].code, 'chng': res[i].chg, 'chngper': res[i].perchg, 'ltp': res[i].ltp });

        }
        console.log(this.indices1);
        console.log(this.indices2);
        console.log(this.indices3);
        this.setState({ Indices1: this.indices1, Indices2: this.indices2, Indices3: this.indices3 });

      }
      else {
        this.setState({ indicesArray: '', indicesloader: false });

      }

    });

  }

  currencyFun() {
    this.setState({ currencyloader: true });
    api.currency().then(res => {
      this.setState({ currencyArray: res });

      if (res != '') {
        this.state.currencyArray.map((dt) => {
          if (dt.CHANGEPERC.charAt(0) != '-') {
            dt.CHANGEPERC = '+' + dt.CHANGEPERC;
            dt.CHANGE = '+' + dt.CHANGE;

          }
        })
        this.setState({ onecurr: this.state.currencyArray[0], twocurr: this.state.currencyArray[1], threecurr: this.state.currencyArray[2], fourcurr: this.state.currencyArray[3], fivecurr: this.state.currencyArray[4], sixcurr: this.state.currencyArray[5] });
        this.setState({ currencyloader: false });
      }
      else {
        this.setState({ currencyArray: '', currencyloader: false });

      }
    });

  }

  sensexHeatMapFun() {
    try {

      this.setState({ heatmaploader: true });
      switch (this.state.langauge) {
        case 'en':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });
          break;
        case 'hn':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });

          break;
        case 'mr':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });
          break;
        case 'gj':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });
          break;
        case 'bn':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });
          break;
        case 'ml':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });
          break;
        case 'or':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });
          break;
        case 'tml':
          fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
            this.code = responsejson[0].scode.split('|');

            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
            });
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              responsejson.map((dt) => {
                if (dt.chgval.charAt(0) != '-') {
                  dt.chgval = '+' + dt.chgval;
                  dt.chgper = '+' + dt.chgper;

                }
              })
              this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
            });
            setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

          });

          break;
      }


    } catch (error) {
      this.setState({ heatmaploader: false });
    }

  }

  commodityFun() {
    this.setState({ commodityloader: true });
    api.commodity().then(res => {
      this.setState({ commodityArray: res });

      if (res != '') {
        this.state.commodityArray.map((dt) => {
          if (dt.CHANGEPERC.charAt(0) != '-') {
            dt.CHANGEPERC = '+' + dt.CHANGEPERC;
            dt.CHANGE = '+' + dt.CHANGE;

          }
        })

        this.setState({ one: this.state.commodityArray[0] == undefined ? '' : this.state.commodityArray[0], two: this.state.commodityArray[1] == undefined ? '' : this.state.commodityArray[1], three: this.state.commodityArray[2] == undefined ? '' : this.state.commodityArray[2], four: this.state.commodityArray[3] == undefined ? '' : this.state.commodityArray[3], five: this.state.commodityArray[4] == undefined ? '' : this.state.commodityArray[4], six: this.state.commodityArray[5] == undefined ? '' : this.state.commodityArray[5] });
        this.state.one != '' && this.state.two != '' && this.state.three != '' && this.state.four != '' && this.state.five != '' && this.state.six != '' ? this.setState({ circular1: true }) : this.state.three == '' && this.state.four == '' && this.state.five == '' && this.state.six == '' ? this.setState({ circular3: true }) : this.state.three == '' && this.state.four == '' && this.state.five == '' ? this.setState({ circular2: true }) : this.state.six == '' ? this.setState({ circular4: true }) : this.setState({ circular1: true });
        this.setState({ commodityloader: false });

      }
      else {
        this.setState({ commodityArray: '', commodityloader: false });

      }

    });

  }

  googleassistant() {
    // Linking.openURL('https://assistant.google.com/explore');

    AsyncStorage.clear();
  }

  refresh() {
    this.portfolioData();
    this.commodityFun();
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50)
    });
    this.sensexHeatMapFun();
    AsyncStorage.getItem('array').then((data) => {

      let dt = JSON.parse(data);
      if (dt == null) { }
      else {

        AsyncStorage.setItem('watchval', "false");
        this.ok();
        this.setState({ watchloader: true });
        this.watchListFun();

      }
    });
    this.sensexFun();
    this.indicesFun(this.state.langauge);
    this.currencyFun();

  }

  detailsensex(cd) {

    NetInfo.isConnected.fetch().then(isConnected => { isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50) });

    //this.setState({ modvis: true, sensexDetailloader: true });
    api.sensexdetail(cd, this.state.langauge).then(res => {
      if (res != '') {
        res.map((dt) => {
          if (dt.chgval > 0) {
            dt.chgper = '+' + dt.chgper;
            dt.chgval = '+' + dt.chgval;

          }
        });
        this.Data = res[0];
        this.props.navigation.navigate('getquote', { 'page': 'sensex', 'scripid': this.Data.scode, });

        this.setState({ sensexDetailloader: false });
      }
      else {
        this.setState({ sensexDetailloader: false });

      }

    });

  }

  add(dt) {

    AsyncStorage.getItem('array').then((data) => {
      const val = JSON.parse(data);

      if (val == null) {

        this.setState({ watchloader: true });

        AsyncStorage.getItem('watchdata').then((data) => {
          const watchdt = JSON.parse(data);
          console.log(watchdt);
          if (watchdt == null) {
            this.watchlistData.push(dt.scode);
            AsyncStorage.setItem('watchdata', JSON.stringify(this.watchlistData));
          }

        });
        database()
          .ref('/watchlist/' + dt.scode)
          .set({
            sname: dt.sname,
            scode: dt.scode,
            chgval: dt.chgval,
            chgper: dt.chgper,
            ltp: dt.ltp,
          })
          .then(() => console.log('Data set.')).catch((error) => {
            console.log(error);
          });

        database().ref('watchlist').on('value', (snapshot) => {
          snapshot.forEach((child) => {
            let wjson = {
              sname: child.val().sname,
              scode: child.val().scode,
              ltp: child.val().ltp,
              chgval: child.val().chgval,
              chgper: child.val().chgper
            }

            this.watchdata.push(wjson)
            AsyncStorage.setItem('array', JSON.stringify(this.watchdata));
            ToastAndroid.showWithGravityAndOffset("Security inserted successfully.", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
            this.watchListFun();
          })
        });


      }
      else if (val != '') {

        const found = val.some(el => el.scode === dt.scode)
        if (found) {
          ToastAndroid.showWithGravityAndOffset("Script code already exists in Watchlist.", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        }
        else {
          this.setState({ watchloader: true });
          AsyncStorage.getItem('watchdata').then((data) => {
            const watchdt = JSON.parse(data);
            watchdt.push(dt.scode);
            AsyncStorage.setItem('watchdata', JSON.stringify(watchdt));

          });

          database()
            .ref('/watchlist/' + dt.scode)
            .set({
              sname: dt.sname,
              scode: dt.scode,
              chgval: dt.chgval,
              chgper: dt.chgper,
              ltp: dt.ltp,
            })
            .then(() => console.log('Data set.')).catch((error) => {
              console.log(error);
            });

          database().ref('watchlist').on('value', (snapshot) => {
            snapshot.forEach((child) => {
              let wjson = {
                sname: child.val().sname,
                scode: child.val().scode,
                ltp: child.val().ltp,
                chgval: child.val().chgval,
                chgper: child.val().chgper
              }

              this.watchdata.push(wjson)

              AsyncStorage.setItem('array', JSON.stringify(this.watchdata));
              ToastAndroid.showWithGravityAndOffset("Security inserted successfully.", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
              this.watchListFun();
            })
          });

        }

      }

    })
    this.setState({ modvis: false });
  }

  watchListFun() {

    AsyncStorage.setItem('watchval', "false");
    this.ok();

    AsyncStorage.getItem('array').then((data) => {

      let watchdata = JSON.parse(data);
      console.log(watchdata);
      if (watchdata == null) {
        this.setState({ watchloader: false });

      }
      else {
        var result = watchdata.reduce((unique, o) => {
          if (!unique.some(obj => obj.sname === o.sname)) {
            unique.push(o);
          }
          return unique;
        }, []);
        console.log(result);
        result.map((dt) => {
          api.watchlist(dt.scode, this.state.langauge).then(res => {

            dt.sname = res[0].sname;
            dt.ltp = this.format(res[0].ltp);
            dt.chgval = res[0].chgval > 0 ? '+' + res[0].chgval : res[0].chgval;
            dt.chgper = res[0].chgper > 0 ? '+' + res[0].chgper : res[0].chgper;

            this.setState({ Warray: result });
            setTimeout(() => { this.setState({ watchloader: false }) }, 5000);

            if (this.state.indicesArray == '') {
              AsyncStorage.removeItem('watchval');
              this.ok();
            }
            else {
              AsyncStorage.removeItem('watchval');
              this.ok();
            }

          });

        });
      }


    })
  }

  onRefresh() {
    this.refresh();
  }

  oncheckWatch() {
    this.state.watch == true ? AsyncStorage.setItem('watchval', "false") : AsyncStorage.removeItem('watchval');
  }
  oncheckPort() {
    this.state.port == true ? AsyncStorage.setItem('portval', "false") : AsyncStorage.removeItem('portval');
  }
  oncheckIndices() {
    this.state.indices == true ? AsyncStorage.setItem('indval', "false") : AsyncStorage.removeItem('indval');
  }
  oncheckCommodit() {
    this.state.commodity == true ? AsyncStorage.setItem('commodt', "false") : AsyncStorage.removeItem('commodt');
  }
  oncheckSensex() {
    this.state.heatmap == true ? AsyncStorage.setItem('heatmap', "false") : AsyncStorage.removeItem('heatmap');
  }
  oncheckCurrency() {
    this.state.currency == true ? AsyncStorage.setItem('curr', "false") : AsyncStorage.removeItem('curr');
  }

  _renderDotIndicatorlight() {
    return <PagerDotIndicator pageCount={3} dotStyle={{ backgroundColor: '#d5d6d9', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#0089d0', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _renderDotIndicatorlighttwo() {
    return <PagerDotIndicator pageCount={2} dotStyle={{ backgroundColor: '#d5d6d9', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#0089d0', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _renderDotIndicatorlightthree() {
    return <PagerDotIndicator pageCount={1} dotStyle={{ backgroundColor: '#d5d6d9', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#0089d0', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _renderDotIndicatordark() {
    return <PagerDotIndicator pageCount={3} dotStyle={{ backgroundColor: '#2e414a', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: 'white', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _renderDotIndicatordarktwo() {
    return <PagerDotIndicator pageCount={2} dotStyle={{ backgroundColor: '#2e414a', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: 'white', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _renderDotIndicatordarkthree() {
    return <PagerDotIndicator pageCount={1} dotStyle={{ backgroundColor: '#2e414a', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: 'white', top: 28, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }

  close() {
    this.setState({ sidemenu: false });
  }
  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
    // this.props.navigation.navigate('setting');
  }

  indices() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('indices');
  }
  normal() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('getquote', { 'page': 'equity', 'scripid': '542727' });

  }
  newquote() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('newgetquote', { 'page': 'equity', 'scripid': '542727' });

  }
  newquoteagain() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('newget', { 'page': 'equity', 'scripid': '542727' });
  }
  graph() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('graph');
  }
  uniqueGraph(code) {
    this.setState({ modvis: false });
    this.props.navigation.navigate('detailgraph', { 'codevalue': code });
  }


  settingpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('setting', { 'current': val });
  }
  equitypage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('equity', { 'current': val });
  }
  equitypagenew(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('equitynew', { 'current': val });
  }
  derivativepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('derivativetab', { 'current': val });
  }
  currencypage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('currencytab', { 'current': val });
  }
  commoditypage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('commoditytab', { 'current': val });
  }
  irdpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ird', { 'current': val });
  }
  smepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('sme', { 'current': val });
  }
  etfpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('etf', { 'current': val });
  }
  debtpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('debt', { 'current': val });
  }
  corporatepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('corporate', { 'current': val });
  }
  marketstatic() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketstatic');
  }
  marketturn() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketturn');
  }
  listing() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('listing');
  }
  ipo() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipo');
  }
  watchlist() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('watch');
  }
  ipfpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipf', { 'current': val });
  }
  notices() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('notice');
  }

  editicon(val) {
    val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
  }

  changeTheme(thm) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem("themecolor", thm);
    thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();
    this.indicesFun(this.state.langauge);
  }
  chnglanguage(lan) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem('language', lan);
    this.lanfun();


  }
  detail(nameindices, id) {
    AsyncStorage.getItem('themecolor').then((dt) => {

      this.props.navigation.navigate('setting', { 'current': 0, 'theme': dt, 'name': nameindices, 'id': id });
    });
  }
  goIndices() {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.props.navigation.navigate('indices', { 'theme': dt });
    });
  }

  equityView() {
    this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });

  }
  equityViewNew() {
    this.state.equitynew == true ? this.setState({ equitynew: false, iconname: 'ios-arrow-forward' }) : this.setState({ equitynew: true, iconname: 'ios-arrow-down' });

  }
  sensexView() {
    this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
  }
  sensexViewnew() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('sensexagain', { 'current': 0 });
  }
  smeView() {
    this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
  }
  derivativeView() {
    this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
  }
  currencyView() {
    this.state.currencytab == false ? this.setState({ currencytab: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currencytab: false, iconnamecurrency: 'ios-arrow-forward' });
  }
  commadityView() {
    this.state.commoditytab == false ? this.setState({ commoditytab: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commoditytab: false, iconnamecommodity: 'ios-arrow-forward' });
  }
  irdView() {
    this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
  }
  etfView() {
    this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
  }
  debtView() {
    this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
  }
  corporateView() {
    this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
  }
  ipfView() {
    this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

  }
  getquoteCurrency(id, date) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('getquote', { 'page': 'currency', 'scripid': id, 'date': date });
  }
  languageandwatch() {
    this.setState({ watchloader: true });

    this.language();
    this.watchListFun();
   // this.portfolioData();
    this.refresh();
  }

  editwatchport() {
    this.setState({ sidemenu: false });

    this.props.navigation.navigate('editwatch');
  }

  // getquoteSensex(id, date) {
  //   this.setState({ sidemenu: false });
  //   this.props.navigation.navigate('getquote', { 'page': 'currency', 'scripid': id, 'date': date });
  // }

  render() {
    return (

      <View style={{ flex: 1 }} >
        <NavigationEvents
          onDidFocus={() => this.languageandwatch()}
        />
        <View style={this.props.theme == '0' ? styles.mainviewlight : styles.mainviewdark}>
          <StatusBar backgroundColor={this.props.theme == 0 ? '#f1f2f6' : '#0b0b0b'} />
          <ScrollView style={this.props.theme == '0' ? styles.scrollviewlight : styles.scrollviewdark}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
            showsVerticalScrollIndicator={false}

          >

            <View style={this.props.theme == 0 ? styles.light : styles.dark}>
              <Row style={styles.header}>
                <Col style={styles.header1}></Col>
                <Col style={styles.header2}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.image} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                <Col style={styles.header2}><TouchableOpacity activeOpacity={.5} ><IconEvil name="user" style={styles.user} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
              </Row>
            </View>

            <View style={this.props.theme == '0' ? styles.snpviewlight : styles.snpviewdark}>
              <View style={this.state.loader == false ? styles.show : styles.hide}>

                <Row>
                  <Col style={styles.snpheader1}></Col>
                  <Col style={styles.snpheader2}><Thumbnail square style={styles.image2} source={this.props.theme == '0' ? require('../images/sensex-removebg-preview.png') : require('../images/sen.jpg')} /></Col>
                  <Col style={styles.snpheader3}></Col>
                </Row>

                <Row>
                  <Col style={styles.ltp1}></Col>
                  <Col style={styles.ltp2}><Text style={this.props.theme == '0' ? styles.valuelight : styles.valuedark}>{this.state.ltp}</Text></Col>
                  <Col style={styles.ltp3}><Text style={this.state.chng > 0 ? styles.headervalpos : styles.headervalneg}>{this.state.chng}  {this.state.per}%</Text></Col>
                  <Col style={styles.ltp4}></Col>

                </Row>
                <Row>
                  <Col style={styles.datecol}><Text style={this.props.theme == '0' ? styles.datelight : styles.datedark}>{this.state.date} | {I18n.t(this.state.status)}</Text></Col>
                </Row>
              </View>
              <View style={this.state.loader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.ltploader} color={this.props.theme == '0' ? "#0000ff" : 'white'} />
              </View>
              <Row style={styles.bannerrow}>
                <Col style={styles.bannercol1}></Col>
                <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
              </Row>

            </View>

            <View style={this.state.watch == true ? styles.show : styles.hide}>

              <View style={this.state.Warray != null ? styles.show : styles.hide}>

                <View style={{}}>
                  <Row style={styles.watchheader}>
                    <Col style={this.props.theme == '0' ? styles.watchcollightup : styles.watchcoldark}><Text style={this.props.theme == '0' ? styles.watchlighttext : styles.watchdarktext} numberOfLines={1}>{I18n.t('watchlist')}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.watchcollightup : styles.watchcoldark}><Icon name="ios-more" style={styles.watchmore} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                  </Row>
                </View>

                <View style={styles.watchview}>
                  <Carousel
                    layout={'default'}
                    ref={ref => this.carousel = ref}
                    data={this.state.Warray}
                    sliderWidth={screenWidth}
                    itemWidth={185}
                    firstItem={1}
                    renderItem={this.props.theme == '0' ? this._renderItemlight : this._renderItemdark}
                    inactiveSlideOpacity={this.props.theme == '0' ? 1 : 0.9}
                    inactiveSlideScale={0.9}
                    inactiveSlideShift={4}

                  />

                </View>



              </View>

            </View>
            <View style={this.state.watchloader == false ? styles.hide : styles.show}>
              <ActivityIndicator size="large" color={this.props.theme == '0' ? "#0000ff" : 'white'} />

            </View>
            <View style={this.state.port == true ? styles.show : styles.hide}>

              <View style={this.portdata == null ? styles.hide : styles.show}>
                <View style={this.state.Warray != null && this.state.watch == true ? styles.withwatch : this.state.watch == false ? styles.wowatch : styles.wowatch}>
                  <Row style={styles.watchheader}>
                    <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.props.theme == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('portfolio')}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={styles.watchmore} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                  </Row>
                </View>
              </View>
              <View style={this.portdata == null ? styles.hide : styles.show}>
                <Row style={styles.portview1}>
                  <Col style={styles.portcol1}>
                    <View style={this.props.theme == '0' ? styles.portlightview : styles.portdarkview}>
                      <Text style={this.props.theme == '0' ? styles.lightgain : styles.darkgain}>{I18n.t('overallgain')}</Text>

                      <Row style={styles.portinnerrow}>
                        <Col style={styles.portinnercol}><Image style={styles.portimage} source={this.props.theme == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
                        <Col style={this.props.theme == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={styles.portinnercol}><Image style={styles.portimage} source={this.props.theme == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
                      </Row>
                      <Text></Text>
                      <Row style={{ bottom: 10 }}>
                        <Col style={styles.portinnercol}><Text style={this.props.theme == '0' ? styles.gainlightprice : styles.gaindarkprice}>{this.OverAllgain}</Text><Text style={this.props.theme == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inrupee')}</Text></Col>
                        <Col style={this.props.theme == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={styles.portinnercol}><Text style={this.props.theme == '0' ? styles.gainlightprice : styles.gaindarkprice}>{this.overAllgainper} % </Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inpercentage')}</Text></Col>
                      </Row>
                      <Text></Text>

                    </View>
                  </Col>
                  <Col style={styles.portcol2}></Col>
                  <Col style={styles.portcol1}>
                    <View style={this.props.theme == '0' ? styles.portlightview : styles.portdarkview}>
                      <Text style={this.props.theme == '0' ? styles.lightgain : styles.darkgain}>{I18n.t('todaygain')}</Text>
                      <Row style={styles.portinnerrow}>
                        <Col style={styles.portinnercol}><Image style={styles.portimage2} source={this.props.theme == '0' ? require('../images/down.jpg') : require('../images/rsz_1new_design_hompage_black-theme_code.jpg')} /></Col>
                        <Col style={this.props.theme == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={styles.portinnercol}><Image style={styles.portimage2} source={this.props.theme == '0' ? require('../images/down.jpg') : require('../images/rsz_1new_design_hompage_black-theme_code.jpg')} /></Col>
                      </Row>
                      <Text></Text>
                      <Row style={{ bottom: 10 }}>
                        <Col style={styles.portinnercol}><Text style={this.props.theme == '0' ? styles.gainlightprice : styles.gaindarkprice}>-6.00</Text><Text style={this.props.theme == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inrupee')}</Text></Col>
                        <Col style={this.props.theme == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={styles.portinnercol}><Text style={this.props.theme == '0' ? styles.gainlightprice : styles.gaindarkprice}>-4.55%</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inpercentage')}</Text></Col>
                      </Row>
                      <Text></Text>


                    </View>
                  </Col>
                </Row>
              </View>

            </View>

            {/* <View >
              <View >

                <Row style={styles.hide}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}>
                    <BannerAd unitId={TestIds.BANNER} style={{}} size={BannerAdSize.BANNER} />
                  </Col>
                </Row>
              </View>
            </View> */}

            <View style={this.state.indices == true ? styles.show : styles.hide}>

              <View style={styles.indicesheader}>
                <Row style={styles.watchheader}>
                  <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.props.theme == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('indices')}</Text></Col>
                  <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" onPress={() => this.goIndices()} style={styles.watchmore} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                </Row>
              </View>
              <View style={this.state.indicesloader == true ? styles.showindloader : styles.hide}>
                <ActivityIndicator size="large" color={this.props.theme == '0' ? "#0000ff" : 'white'} />
              </View>
              <View style={this.state.indicesloader == false ? styles.showindview : styles.hide}>
                <ScrollView horizontal showsVerticalScrollIndicator={false}>


                  <View style={styles.view}></View>
                  <View style={this.state.tabsValue == 0 ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}>
                    <FlatList
                      data={this.state.Indices1}
                      renderItem={({ item }) =>
                        <View>


                          <Ripple onPress={() => this.detail(item.indexname, item.code)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                            <Row style={this.props.theme == '0' && item.chng > 0 ? styles.showindiceslightpos : this.props.theme == '0' && item.chng < 0 ? styles.showindiceslight : this.props.theme == '1' && item.chng > 0 ? styles.showindicesdarkpos : this.props.theme == '1' && item.chng < 0 ? styles.showindicesdark : ''}>

                              <Col style={this.props.theme == '0' ? styles.indicescol3 : styles.indicescoldark3}><Text style={this.props.theme == '0' ? styles.lightindtext : styles.darkindtext} numberOfLines={1}>{item.indexname}</Text><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{moment().format('DD MMM YYYY')}</Text></Col>
                              <Col style={this.props.theme == '0' ? styles.indicescol4 : styles.indicescoldark4}><Text style={this.props.theme == '0' ? styles.lightvaluetext : styles.darkvaluetext}>{this.format(item.ltp)} </Text><Text style={item.chng.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chng}  {item.chngper}%</Text></Col>
                            </Row>



                          </Ripple>

                        </View>
                      }
                      keyExtractor={item => item.id}
                    />
                  </View>
                  <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>
                  <View style={this.state.tabsValue == 0 ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}>
                    <FlatList
                      data={this.state.Indices2}
                      renderItem={({ item }) =>
                        <View>


                          <Ripple onPress={() => this.detail(item.indexname, item.code)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                            <Row style={this.props.theme == '0' && item.chng > 0 ? styles.showindiceslightpos1 : this.props.theme == '0' && item.chng < 0 ? styles.showindiceslight1 : this.props.theme == '1' && item.chng > 0 ? styles.showindicesdarkpos1 : this.props.theme == '1' && item.chng < 0 ? styles.showindicesdark1 : ''}>

                              <Col style={this.props.theme == '0' ? styles.indicescol3 : styles.indicescoldark3}><Text style={this.props.theme == '0' ? styles.lightindtext : styles.darkindtext} numberOfLines={1}>{item.indexname}</Text><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{moment().format('DD MMM YYYY')}</Text></Col>
                              <Col style={this.props.theme == '0' ? styles.indicescol4 : styles.indicescoldark4}><Text style={this.props.theme == '0' ? styles.lightvaluetext : styles.darkvaluetext}>{this.format(item.ltp)} </Text><Text style={item.chng.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chng}  {item.chngper}%</Text></Col>
                            </Row>



                          </Ripple>

                        </View>
                      }
                      keyExtractor={item => item.id}
                    />
                  </View>
                  {/* <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View> */}
                  <View style={this.state.tabsValue == 0 ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}>
                    <FlatList
                      data={this.state.Indices3}
                      renderItem={({ item }) =>
                        <View>


                          <Ripple onPress={() => this.detail(item.indexname, item.code)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                            <Row style={this.props.theme == '0' && item.chng > 0 ? styles.showindiceslightpos2 : this.props.theme == '0' && item.chng < 0 ? styles.showindiceslight2 : this.props.theme == '1' && item.chng > 0 ? styles.showindicesdarkpos2 : this.props.theme == '1' && item.chng < 0 ? styles.showindicesdark2 : ''}>

                              <Col style={this.props.theme == '0' ? styles.indicescol3 : styles.indicescoldark3}><Text style={this.props.theme == '0' ? styles.lightindtext : styles.darkindtext} numberOfLines={1}>{item.indexname}</Text><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{moment().format('DD MMM YYYY')}</Text></Col>
                              <Col style={this.props.theme == '0' ? styles.indicescol4 : styles.indicescoldark4}><Text style={this.props.theme == '0' ? styles.lightvaluetext : styles.darkvaluetext}>{this.format(item.ltp)} </Text><Text style={item.chng.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chng}  {item.chngper}%</Text></Col>
                            </Row>



                          </Ripple>

                        </View>
                      }
                      keyExtractor={item => item.id}
                    />
                  </View>

                </ScrollView>


              </View>

            </View>

            <View style={this.state.commodity == true ? styles.show : styles.hide}>

              <View style={styles.commodityheader}>
                <Row style={styles.watchheader}>
                  <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.props.theme == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('commodity')}</Text></Col>
                  <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" onPress={() => this.commoditypage(0)} style={styles.watchmore} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                </Row>
              </View>

              <View style={this.state.commodityloader == true ? styles.showcommodityloader : styles.hide}>
                <ActivityIndicator size="large" color={this.props.theme == '0' ? "#0000ff" : 'white'} />
              </View>
              <View style={this.state.commodityloader == false ? styles.showcommodityview : styles.hide}>
                <View style={this.props.theme == '0' ? styles.show : styles.hide}>

                  <View style={this.state.circular1 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatorlight()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.one.EXPIRYDATE}</Text>
                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text> </Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>

                        <Row style={styles.commodityrow}>

                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.four == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.four.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.four.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.four.LASTTRADERATE)} <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE} {this.state.four.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.six == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.six.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.six.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.six.LASTTRADERATE)} <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE} {this.state.six.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>

                        </Row>


                      </View>

                    </IndicatorViewPager>
                  </View>

                  <View style={this.state.circular2 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatorlighttwo()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.one.EXPIRYDATE}</Text>
                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text> </Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                        </Row>

                      </View>

                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.six == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.six.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.six.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.six.LASTTRADERATE)} <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE} {this.state.six.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>

                        </Row>


                      </View>

                    </IndicatorViewPager>
                  </View>
                  <View style={this.state.circular3 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatorlightthree()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.one.EXPIRYDATE}</Text>
                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text> </Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                        </Row>

                      </View>



                    </IndicatorViewPager>
                  </View>
                  <View style={this.state.circular4 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatorlight()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.one.EXPIRYDATE}</Text>
                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text> </Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>

                        <Row style={styles.commodityrow}>

                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.four == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.four.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.four.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.four.LASTTRADERATE)} <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE} {this.state.four.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatelight}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>


                        </Row>


                      </View>

                    </IndicatorViewPager>
                  </View>

                </View>

                <View style={this.props.theme == '1' ? styles.show : styles.hide}>

                  <View style={this.state.circular1 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatordark()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.one.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>

                        <Row style={styles.commodityrow}>
                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.four == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.four.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.four.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.four.LASTTRADERATE)} <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE} {this.state.four.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={{ marginTop: 5 }}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.six == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.six.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.six.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.six.LASTTRADERATE)} <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE} {this.state.six.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>

                        </Row>

                      </View>

                    </IndicatorViewPager>
                  </View>

                  <View style={this.state.circular2 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={{ height: 107 }}
                      indicator={this._renderDotIndicatordarktwo()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.one.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                        </Row>

                      </View>

                      <View style={{}}>
                        <Row style={styles.commodityrow}>

                          <Col style={{ width: '4%', height: 90 }}></Col>
                          <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                            <Col style={this.state.six == '' ? styles.hide : styles.colnudefined}>
                              <View style={this.state.six.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.six.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.six.LASTTRADERATE)} <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE} {this.state.six.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </View>
                            </Col>
                          </Ripple>

                        </Row>

                      </View>

                    </IndicatorViewPager>
                  </View>

                  <View style={this.state.circular3 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={{ height: 107 }}
                      indicator={this._renderDotIndicatordarkthree()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.one.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                        </Row>

                      </View>

                    </IndicatorViewPager>
                  </View>

                  <View style={this.state.circular4 == true ? styles.show : styles.hide}>
                    <IndicatorViewPager
                      style={{ height: 107 }}
                      indicator={this._renderDotIndicatordark()}
                    >
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.one == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.one.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.one.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.two == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.two.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.two.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>

                        <Row style={styles.commodityrow}>
                          <Col style={this.state.three == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.three.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.three.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.four == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.four.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.four.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.four.LASTTRADERATE)} <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE} {this.state.four.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.five == '' ? styles.hide : styles.colnudefined}>
                            <View style={this.state.five.CHANGEPERC > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                              <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                                <Text style={styles.currdatedark}>{this.state.five.EXPIRYDATE}</Text>

                                <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>


                        </Row>

                      </View>

                    </IndicatorViewPager>
                  </View>

                </View>

              </View>

            </View>

            <Row style={styles.bannerrow2}>
              <Col style={styles.bannercol3}></Col>
              <Col style={styles.bannercol4}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.MEDIUM_RECTANGLE} /></Col>

            </Row>
            <View style={styles.addview}>

            </View>

            <View style={{}}>

              <View style={this.state.heatmap == true ? styles.show : styles.hide}>

                <View style={styles.commodityheader}>
                  <Row style={styles.watchheader}>
                    <Col style={styles.sensexheadercol1}><Text style={this.props.theme == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('sensexheat')}</Text></Col>
                    <Col style={styles.sensexheadercol3}><IconEnt name="line-graph" onPress={() => this.graph()} style={styles.grpicon} size={20} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                    <Col style={styles.sensexheadercol2}><Icon name="ios-more" onPress={() => this.settingpage(0)} style={styles.watchmore} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                  </Row>
                </View>
                <View style={this.state.heatmaploader == true ? styles.showheatloader : styles.hide}>
                  <ActivityIndicator size="large" color={this.props.theme == '0' ? "#0000ff" : 'white'} />
                </View>
                <View style={this.state.heatmaploader == false ? styles.showheatview : styles.hide}>
                  <View style={this.props.theme == '0' ? styles.show : styles.hide}>

                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatorlight()}
                    >
                      <View style={{}}>

                        <Row style={styles.commodityrow}>
                          <Col style={this.state.sname1 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval1 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode1)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname1}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp1)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval1 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval1}   {this.state.changeper1}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.sname2 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval2 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode2)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname2}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp2)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval2 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval2}   {this.state.changeper2}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.sname3 == undefined ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.changeval3 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode3)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname3}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp3)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>

                        <Row style={styles.commodityrow}>
                          <Col style={this.state.scode3 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval3 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode3)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname3}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp3)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.sname4 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval4 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode4)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname4}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp4)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval4 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval4}   {this.state.changeper4}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.sname5 == undefined ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.changeval5 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode5)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname5}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp5)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.scode5 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval5 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode5)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname5}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp5)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>

                          <Col style={this.state.scode6 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval6 > 0 ? styles.currlightviewgreen : styles.currlightviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode6)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname6}</Text>
                                <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp6)}</Text>

                                <Text style={styles.currvaluelight}><Text style={this.state.changeval6 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval6}   {this.state.changeper6}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>

                        </Row>

                      </View>

                    </IndicatorViewPager>

                  </View>

                  <View style={this.props.theme == '1' ? styles.show : styles.hide}>

                    <IndicatorViewPager
                      style={styles.commodityheight}
                      indicator={this._renderDotIndicatordark()}
                    >
                      <View style={{}}>

                        <Row style={styles.commodityrow}>
                          <Col style={this.state.sname1 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval1 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              {/* <TouchableOpacity onPress={() => this.detailsensex(this.state.scode1)}> */}
                              <Ripple onPress={() => this.detailsensex(this.state.scode1)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={700}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname1}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp1)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval1 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval1}   {this.state.changeper1}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                              {/* </TouchableOpacity> */}

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.sname2 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval2 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode2)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={700}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname2}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp2)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval2 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval2}   {this.state.changeper2}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>
                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.sname3 == undefined ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.changeval3 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode3)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={700}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname3}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp3)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.sname3 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval3 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode3)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname3}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp3)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.sname4 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval4 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode4)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname4}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp4)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval4 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval4}   {this.state.changeper4}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.scode5 == undefined ? styles.hide : styles.colnudefined2}>
                            <View style={this.state.changeval5 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode5)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname5}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp5)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                        </Row>

                      </View>
                      <View style={{}}>
                        <Row style={styles.commodityrow}>
                          <Col style={this.state.sname5 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval5 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode5)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname5}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp5)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>
                          <Col style={styles.commoditycol}></Col>
                          <Col style={this.state.sname6 == undefined ? styles.hide : styles.colnudefined}>
                            <View style={this.state.changeval6 > 0 ? styles.currdarkviewgreen : styles.currdarkviewred} >
                              <Ripple onPress={() => this.detailsensex(this.state.scode6)} rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000}>

                                <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname6}</Text>
                                <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp6)}</Text>

                                <Text style={styles.currvaluedark}><Text style={this.state.changeval6 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval6}   {this.state.changeper6}%</Text></Text>
                                <Text style={styles.commoditytext}></Text>
                              </Ripple>

                            </View>
                          </Col>

                        </Row>

                      </View>

                    </IndicatorViewPager>
                  </View>
                </View>
              </View>
            </View>

            <View style={this.state.currency == true ? styles.show : styles.hide}>

              <View style={styles.commodityheader}>
                <Row style={styles.watchheader}>
                  <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.props.theme == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('currency')}</Text></Col>
                  <Col style={this.props.theme == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" onPress={() => this.currencypage(0)} style={styles.watchmore} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                </Row>
              </View>
              <View style={this.state.currencyloader == true ? styles.showcurrloader : styles.hide}>
                <ActivityIndicator size="large" color={this.props.theme == '0' ? "#0000ff" : 'white'} />
              </View>
              <View style={this.state.currencyloader == false ? styles.showcurrview : styles.hide}>
                <View style={this.props.theme == '0' ? styles.show : styles.hide}>


                  <IndicatorViewPager
                    style={styles.commodityheight}
                    indicator={this._renderDotIndicatorlight()}
                  >
                    <View style={{}}>
                      <Row style={styles.commodityrow}>
                        <Col style={this.state.onecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.onecurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.onecurr.SCRIPCODE, this.state.onecurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.onecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.onecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.onecurr.LASTTRADERATE)} <Text style={this.state.onecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.onecurr.CHANGE} {this.state.onecurr.CHANGEPERC}%</Text> </Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>

                        <Col style={this.state.twocurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.twocurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.twocurr.SCRIPCODE, this.state.twocurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.twocurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.twocurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.twocurr.LASTTRADERATE)} <Text style={this.state.twocurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.twocurr.CHANGE} {this.state.twocurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>

                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={this.state.threecurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.threecurr.SCRIPCODE, this.state.threecurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.threecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE}   {this.state.threecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={styles.commodityrow}>
                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.threecurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.threecurr.SCRIPCODE, this.state.threecurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.threecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE}   {this.state.threecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>

                        <Col style={this.state.fourcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.fourcurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.fourcurr.SCRIPCODE, this.state.fourcurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fourcurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.fourcurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.fourcurr.LASTTRADERATE)} <Text style={this.state.fourcurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.fourcurr.CHANGE}   {this.state.fourcurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={this.state.fivecurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.fivecurr.SCRIPCODE, this.state.fivecurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.fivecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE}   {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.fivecurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.fivecurr.SCRIPCODE, this.state.fivecurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.fivecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE}   {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.sixcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.sixcurr.CHANGE > 0 ? styles.currlightviewgreen : styles.currlightviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.sixcurr.SCRIPCODE, this.state.sixcurr.DT_TM)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sixcurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatelight}>{this.state.sixcurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluelight} numberOfLines={1}>{() => this.format('' + this.state.sixcurr.LASTTRADERATE)} <Text style={this.state.sixcurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.sixcurr.CHANGE}   {this.state.sixcurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>

                      </Row>

                    </View>
                  </IndicatorViewPager>
                  <Text></Text>
                </View>
                <View style={this.props.theme == '1' ? styles.show : styles.hide}>

                  <IndicatorViewPager
                    style={styles.commodityheight}
                    indicator={this._renderDotIndicatordark()}
                  >
                    <View style={{}}>
                      <Row style={styles.commodityrow}>
                        <Col style={this.state.onecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.onecurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.onecurr.SCRIPCODE, this.state.onecurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.onecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.onecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.onecurr.LASTTRADERATE)} <Text numberOfLines={1} style={this.state.onecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.onecurr.CHANGE} {this.state.onecurr.CHANGEPERC} %</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.twocurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.twocurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.twocurr.SCRIPCODE, this.state.twocurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.twocurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.twocurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.twocurr.LASTTRADERATE)} <Text style={this.state.twocurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.twocurr.CHANGE} {this.state.twocurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={this.state.threecurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.threecurr.SCRIPCODE, this.state.threecurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.threecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE} {this.state.threecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={styles.commodityrow}>
                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.threecurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.threecurr.SCRIPCODE, this.state.threecurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.threecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE} {this.state.threecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.fourcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.fourcurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.fourcurr.SCRIPCODE, this.state.fourcurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.fourcurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.fourcurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.fourcurr.LASTTRADERATE)} <Text style={this.state.fourcurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.fourcurr.CHANGE} {this.state.fourcurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={this.state.fivecurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.fivecurr.SCRIPCODE, this.state.fivecurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.fivecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE} {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={styles.commodityrow}>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.fivecurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.fivecurr.SCRIPCODE, this.state.fivecurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.fivecurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE} {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>
                        <Col style={styles.commoditycol}></Col>
                        <Col style={this.state.sixcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={this.state.sixcurr.CHANGE > 0 ? styles.currdarkviewgreen : styles.currdarkviewred}>
                            <Ripple rippleColor="#2087c9" rippleOpacity={0.6} rippleDuration={1000} onPress={() => this.getquoteCurrency(this.state.sixcurr.SCRIPCODE, this.state.sixcurr.DT_TM)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sixcurr.INSTRUMENTNAME}</Text>
                              <Text style={styles.currdatedark}>{this.state.sixcurr.EXPIRYDATE}</Text>

                              <Text style={styles.currvaluedark} numberOfLines={1}>{() => this.format('' + this.state.sixcurr.LASTTRADERATE)} <Text style={this.state.sixcurr.CHANGE > 0 ? styles.currencypos : styles.currencyred}>{this.state.sixcurr.CHANGE} {this.state.fourcurr.CHANGEPERC}%</Text></Text>
                              <Text style={styles.commoditytext}></Text>
                            </Ripple>
                          </View>
                        </Col>

                      </Row>

                    </View>
                  </IndicatorViewPager>
                  <Text></Text>
                </View>
              </View>
            </View>


            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modvis}
              style={styles.modalheight}
              onRequestClose={() => {
                this.setState({ modvis: false });
              }}
            >
              <View style={styles.modalheight}>

                <View style={this.state.sensexDetailloader == true ? styles.show : styles.hide}>
                  <View style={styles.loaderheight}>
                    <ActivityIndicator size="large" color={'white'} />
                  </View>
                </View>

                <View style={this.state.sensexDetailloader == false ? styles.show : styles.hide}>
                  <View style={styles.modalsensexview}>
                    <Row style={styles.portview}>
                      <Col style={styles.popovercol2}><Icon style={styles.popovericon2} name="ios-add" onPress={() => this.add(this.Data)} size={30} /></Col>
                      <Col style={styles.popovercol1}><IconEnt style={styles.popovericon2} name="line-graph" onPress={() => this.uniqueGraph(this.Data.scode)} size={20} /></Col>
                    </Row>
                    <Row>
                      <Col style={styles.popovercol7}><Text style={styles.modalsensextext1}>{this.Data.sname}</Text></Col>
                      <Col style={styles.popovercol7}><Text style={styles.modalsensextext2}>Value:- {this.Data.ltp}</Text></Col>

                    </Row>
                    <Row>
                      <Col style={styles.popovercol7}><Text style={styles.modalsensextext2}>{this.Data.chgval} {this.Data.chgper}%</Text></Col>
                      <Col style={styles.popovercol7}><Text style={styles.modalsensextext2}>Scode:- {this.Data.scode}</Text></Col>

                    </Row>

                  </View>
                </View>
              </View>
            </Modal>
            <Modal
              animationType="slide"
              transparent={false}
              style={styles.modalheight}
              visible={this.state.sidemenu}
              onRequestClose={() => {
                this.setState({ sidemenu: false });
              }}
            >
              <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
                <Row style={styles.menurow}>
                  <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.close()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                  <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                  <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                </Row>

                <ScrollView style={styles.scrollview}>

                  <Text></Text>
                  <View style={{}}>
                    <Row style={styles.scrollrow}>
                      <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                      <Col style={styles.popovercol7}></Col>
                    </Row>


                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.equity == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.equitypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.equityViewNew()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>New Equity Tab</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.equityViewNew()}><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.equitynew == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.equitypagenew(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypagenew(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypagenew(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypagenew(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                      <Ripple onPress={() => this.equitypagenew(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                      <Text></Text>

                    </View>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                    </Row>


                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.sensex == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                      <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                      <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                      <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                      <Text></Text>

                    </View>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.sensexViewnew()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>New Sensex Tab</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.sensexViewnew()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.sme == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                      <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                      <Text></Text>
                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.derivative == false ? styles.hide : styles.show}>

                      <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                      <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.currencytab == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                      <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.commoditytab == false ? styles.hide : styles.show}>

                      <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                      <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.ird == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                      <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.etf == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                      <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.debt == false ? styles.hide : styles.show}>

                      <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                      <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                      <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                      <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                      <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                      <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                      <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.corporate == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                      <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                      <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                      <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                      <Text></Text>

                    </View>
                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                      <Col style={styles.menucol4}></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                      <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                    </Row>
                    <View style={this.state.ipf == false ? styles.hide : styles.show}>
                      <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                      <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                      <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                      <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                      <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                      <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                      <Text></Text>

                    </View>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.popovercol6} onPress={() => this.normal()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>Get quote Equity</Text></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.popovercol6} onPress={() => this.newquote()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>Get quote Equity New</Text></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.popovercol6} onPress={() => this.newquoteagain()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>Get quote Equity New with Box</Text></Col>
                    </Row>

                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                    <Row style={styles.scrollrow}>
                      <Col style={styles.popovercol6} onPress={() => this.editwatchport()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>Edit Watchlist and Portfolio</Text></Col>
                    </Row>

                    <Text></Text>
                    <Text></Text>
                    <Text></Text>
                    <Text></Text>
                    {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
                  </View>
                </ScrollView>
              </View>
            </Modal>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.settingshow}
              style={styles.modalheight}
              onRequestClose={() => {
                this.setState({ settingshow: false });
                this.setState({ sidemenu: false });

              }}
            >
              <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

                <ScrollView style={styles.scrollview1}>
                  <Row style={styles.settingrow}>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                    <Col style={styles.popovercol8}>
                      <Picker
                        selectedValue={this.state.selectlan}
                        style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                        onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                      >
                        <Picker.Item label="English" value="English" />
                        <Picker.Item label="Hindi" value="Hindi" />
                        <Picker.Item label="Marathi" value="Marathi" />
                        <Picker.Item label="Gujrati" value="Gujrati" />
                        <Picker.Item label="Bengali" value="Bengali" />
                        <Picker.Item label="Malayalam" value="Malayalam" />
                        <Picker.Item label="Oriya" value="Oriya" />
                        <Picker.Item label="Tamil" value="Tamil" />
                      </Picker>
                      <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                    </Col>
                  </Row>
                  <Text></Text>
                  <Row >
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                    <Col style={styles.popovercol8}>
                      <Picker
                        selectedValue={this.props.themename}
                        style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                        onValueChange={(itemValue) => this.changeTheme(itemValue)}
                      // onValueChange={(itemValue) => this.props.themechng()}
                      >
                        <Picker.Item label="Dark" value="dark" />
                        <Picker.Item label="Light" value="light" />

                      </Picker>
                      <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                    </Col>
                  </Row>
                  <Text></Text>
                  <Row>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}> {I18n.t('edithome')} </Text></Col>
                    <Col style={styles.popovercol8}><Icon name={this.state.iconname} onPress={() => this.editicon(this.state.showedit)} style={styles.popovericon3} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.showedit == true ? styles.show : styles.hide}>
                    <Text></Text>
                    <Row>
                      <Col style={styles.popovercol1}></Col>
                      <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lightedithometext : styles.darkedithometext}>{I18n.t('editwatch')}</Text></Col>
                      <Col style={styles.popovercol3}>
                        <CheckBox value={this.state.watch} onValueChange={() => this.oncheckWatch()} style={styles.popovericon1} />

                      </Col>
                    </Row>
                    <Row>
                      <Col style={styles.popovercol1}></Col>
                      <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lightedithometext : styles.darkedithometext}>{I18n.t('editport')}</Text></Col>
                      <Col style={styles.popovercol3}>
                        <CheckBox value={this.state.port} onValueChange={() => this.oncheckPort()} style={styles.popovericon1} />

                      </Col>
                    </Row>
                    <Row>
                      <Col style={styles.popovercol1}></Col>
                      <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lightedithometext : styles.darkedithometext}>{I18n.t('editindices')}</Text></Col>
                      <Col style={styles.popovercol3}>
                        <CheckBox value={this.state.indices} onValueChange={() => this.oncheckIndices()} style={styles.popovericon1} />

                      </Col>
                    </Row>
                    <Row>
                      <Col style={styles.popovercol1}></Col>
                      <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lightedithometext : styles.darkedithometext}>{I18n.t('editcommodity')}</Text></Col>
                      <Col style={styles.popovercol3}>
                        <CheckBox value={this.state.commodity} onValueChange={() => this.oncheckCommodit()} style={styles.popovericon1} />

                      </Col>
                    </Row>
                    <Row>
                      <Col style={styles.popovercol1}></Col>
                      <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lightedithometext : styles.darkedithometext}>{I18n.t('editsensex')}</Text></Col>
                      <Col style={styles.popovercol3}>
                        <CheckBox value={this.state.heatmap} onValueChange={() => this.oncheckSensex()} style={styles.popovericon1} />

                      </Col>
                    </Row>
                    <Row>
                      <Col style={styles.popovercol1}></Col>
                      <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lightedithometext : styles.darkedithometext}>{I18n.t('editcurrency')}</Text></Col>
                      <Col style={styles.popovercol3}>
                        <CheckBox value={this.state.currency} onValueChange={() => this.oncheckCurrency()} style={styles.popovericon1} />

                      </Col>
                    </Row>

                    <Row>
                      <Col style={styles.datecol}><Button style={styles.okbutton} onPress={() => this.ok()}><Text style={styles.oktext}>{I18n.t('ok')}</Text></Button></Col>
                    </Row>
                    <Text></Text>

                  </View>
                  <Text></Text>
                  <Row>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
                  </Row>
                  <Text></Text>
                  <Row>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
                  </Row>
                  <Text></Text>
                  <Row>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
                  </Row>
                  <Text></Text>
                  <Row>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
                  </Row>
                  <Text></Text>
                  <Row>
                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
                  </Row>
                </ScrollView>
              </View>
            </Modal>

          </ScrollView>

          <Footer>

            <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>
              <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('watchtab')} </Text>
              </Button>

              <Button style={styles.inactive} onPress={() => this.tab('port')}>
                <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />
                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('porttab')}</Text>
              </Button>

              <Button style={this.activetab == 'home' ? styles.active : styles.inactive} onPress={() => this.tab('home')}>
                <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
                <Text style={this.props.theme == '0' ? styles.tabnamelighthome : styles.tabnamedark} numberOfLines={1}>{I18n.t('home')}</Text>
              </Button>

              <Button style={styles.inactive} onPress={() => this.tab('search')}>
                <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('search')}</Text>
              </Button>

              <Button style={styles.inactive} onPress={() => this.tab('more')}>
                <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />
                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('menu')}</Text>
              </Button>

            </FooterTab>
          </Footer>
        </View >
      </View >
    );
  }
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
    themename: state.themename,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    themechnglight: () => dispatch({ type: 'light' }),
    themechngdark: () => dispatch({ type: 'dark' }),
  }
}

const styles = StyleSheet.create({
  iconpos: { textAlign: "center", color: '#19cf3e', top: 3, padding: 5, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },
  iconneg: { textAlign: "center", top: 3, color: '#f54845', padding: 5, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },
  iconposdark: { textAlign: "center", top: 3, color: '#19cf3e', padding: 5, backgroundColor: '#1a1f1f', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  iconnegdark: { textAlign: "center", top: 3, color: '#f54845', padding: 5, backgroundColor: '#1a1f1f', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  chnagepos: { alignSelf: 'flex-end', marginRight: 8, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13 },
  changeneg: { alignSelf: 'flex-end', marginRight: 8, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13 },
  lightdate: { marginLeft: 7, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13 },
  darkdate: { marginLeft: 7, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13 },
  currencypos: { fontSize: 12, color: '#19cf3e', fontFamily: 'Arial', },
  currencyred: { fontSize: 12, color: '#f54845', fontFamily: 'Arial', },
  light: { backgroundColor: '#f6f7f9' },
  dark: { backgroundColor: 'black' },
  mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
  mainviewdark: { backgroundColor: '#333333', height: '100%' },

  snpviewlight: { backgroundColor: '#f6f7f9', borderBottomRightRadius: 240, },
  snpviewdark: { backgroundColor: '#000000', borderBottomRightRadius: 240, },
  valuelight: { textAlign: 'center', marginLeft: '9%', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', },
  valuedark: { textAlign: 'center', marginLeft: '9%', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', color: 'white', },
  datelight: { textAlign: 'center', bottom: 31, fontSize: 16, color: '#aaafba', fontFamily: 'Arial', fontWeight: 'bold', },
  datedark: { textAlign: 'center', bottom: 31, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  scrollviewlight: { backgroundColor: '#f1f2f6', },
  scrollviewdark: { backgroundColor: '#0b0b0b', },
  watchcollight: { width: '50%', height: 30 },
  watchcollightup: { width: '50%', height: 30, top: 8 },
  watchcoldark: { width: '50%', height: 30, },
  sensexheadercol1: { width: '80%', height: 30 },
  sensexheadercol2: { width: '10%', height: 30, },
  sensexheadercol3: { width: '10%', height: 30, },

  watchlighttext: { fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', color: '#132144' },
  watchdarktext: { fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', color: 'white' },
  portlightview: { width: 160, margin: 5, backgroundColor: 'white', height: 110, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  portdarkview: { width: 160, margin: 5, backgroundColor: '#1a1f1f', height: 110, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  lightgain: { textAlign: 'center', fontSize: 16, top: 5, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkgain: { textAlign: 'center', fontSize: 16, top: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightcoloverall: { width: '1%', height: 40, backgroundColor: '#f4f4f4' },
  darkcoloverall: { width: '1%', height: 40, backgroundColor: '#353a3a' },
  gainlightprice: { textAlign: 'center', fontSize: 14.5, fontFamily: 'Arial', fontWeight: 'bold', color: "#132144" },
  gaindarkprice: { textAlign: 'center', fontSize: 14.5, fontFamily: 'Arial', fontWeight: 'bold', color: "white" },
  lightcurrency: { textAlign: 'center', color: '#0089d0', fontSize: 12, fontFamily: 'Arial', fontWeight: 'bold', },
  darkcurrency: { textAlign: 'center', color: 'white', fontSize: 12, fontFamily: 'Arial', fontWeight: 'bold', },
  indiceslight: { marginTop: 10, },
  indicesdark: { backgroundColor: '#0b0b0b', marginTop: 10, },
  lightindtext: { marginLeft: 7, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  darkindtext: { marginLeft: 7, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  lightvaluetext: { alignSelf: 'flex-end', marginTop: 9, fontFamily: 'Arial', fontWeight: 'bold', marginRight: 8, color: '#132144' },
  darkvaluetext: { alignSelf: 'flex-end', marginTop: 9, fontFamily: 'Arial', fontWeight: 'bold', marginRight: 8, color: 'white' },
  currlightview: { width: 145, backgroundColor: 'white', marginTop: 7, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
  currdarkview: { width: 145, backgroundColor: '#1a1f1f', marginTop: 7, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },
  tablight: { backgroundColor: 'white' },
  tabdark: { backgroundColor: '#1a1f1f' },
  tabnamelight: { color: '#7a878f', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold', },
  tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold', },
  tabnamedark: { color: '#f6f7f9', fontSize: 11, marginTop: 3, fontFamily: 'Arial', fontWeight: 'bold', },
  tabnamedarkdeactive: { color: 'white', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold', },
  show: { display: 'flex', },

  hide: { display: 'none' },
  showindices: { display: 'flex', marginLeft: 14, marginRight: 14 },
  showindview: { display: 'flex', bottom: '8%', backgroundColor: 'transparent' },
  showindloader: { display: 'flex', bottom: '4%' },
  showcommodityview: { display: 'flex', bottom: '14%' },
  showcommodityloader: { display: 'flex', bottom: '4%' },
  showheatloader: { display: 'flex', bottom: '4%' },
  showheatview: { display: 'flex', bottom: '14%' },
  showcurrloader: { display: 'flex', bottom: '4%' },
  showcurrview: { display: 'flex', bottom: '15%' },
  watchnamelight: { top: 9, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  watchnamedark: { top: 9, marginRight: 10, marginLeft: 10, color: 'white', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  watchvallight: { top: 15, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  watchvaldark: { top: 15, marginRight: 10, marginLeft: 10, color: 'white', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },

  currnamelight: { top: 5, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  currnamedark: { top: 5, marginRight: 10, marginLeft: 10, color: 'white', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  currdatelight: { top: 12, marginRight: 10, marginLeft: 10, color: '#aaafba', fontSize: 12, fontFamily: 'Arial', },
  currdatedark: { top: 12, marginRight: 10, marginLeft: 10, color: 'white', fontSize: 12, fontFamily: 'Arial', },
  currvaluelight: { top: 18, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  currvaluedark: { top: 18, marginRight: 10, marginLeft: 10, color: 'white', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  sensexltplight: { top: 12, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  sensexltpdark: { top: 12, marginRight: 10, marginLeft: 10, color: 'white', fontSize: 14, fontFamily: 'Arial', fontWeight: 'bold', },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
  inactive: { padding: 10, },
  withwatch: { bottom: '7%' },
  wowatch: { bottom: '2%' },
  withwatcport: { bottom: '20%' },
  wowatchport: { bottom: '15%' },
  wowatchimage: { bottom: 20 },
  withwatchimage: { bottom: 25 },
  watchportfalse: { bottom: 0 },
  headervalpos: { fontSize: 16, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 4 },
  headervalneg: { fontSize: 16, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 4 },
  colnudefined: { display: 'flex', width: '43%', height: 90, },
  colnudefined2: { display: 'flex', width: '8%', height: 90, },

  lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightedithometext: { textAlign: 'left', fontSize: 17, color: '#132144' },
  darkedithometext: { textAlign: 'left', fontSize: 17, color: 'white' },
  dropicon: { marginLeft: 88, bottom: 30 },
  lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
  darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
  lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  header: { marginBottom: 10, marginTop: 10 },
  header1: { width: '67%', height: 30, },
  header2: { width: '15%', height: 30, },
  image: { width: 23, height: 22, marginTop: 6, alignSelf: 'flex-end' },
  user: { marginTop: 3, textAlign: 'right' },
  snpheader1: { width: '17%', height: 65, },
  snpheader2: { width: '73%', height: 65, },
  snpheader3: { width: '10%', height: 65, },
  image2: { width: 200, height: 53, marginTop: 5, marginLeft: '7%', },
  ltp1: { width: '17%', height: 65, },
  ltp2: { width: '31%', height: 40, },
  ltp3: { width: '42%', height: 40, },
  ltp4: { width: '10%', height: 65, },
  datecol: { width: '100%', height: 40, },
  ltploader: { bottom: 20 },
  bannerrow: { bottom: 17 },
  bannercol1: { width: '6%', height: 53, },
  bannercol2: { width: '80%', height: 53, },
  watchheader: { margin: 15, },
  watchmore: { textAlign: "right", top: 3 },
  grpicon: { textAlign: "left", top: 3 },
  watchview: { flex: 1, flexDirection: 'row', justifyContent: 'center', height: 120, bottom: '3%' },
  portview: { margin: 10, },
  portview1: { margin: 10, bottom: 15 },
  portcol1: { width: '48%', height: 110 },
  portcol2: { width: '3%', height: 90 },
  portinnerrow: { marginTop: 10, },
  portinnercol: { width: '49.5%', height: 40 },
  portimage: { width: 55, height: 23, alignSelf: 'center' },
  portimage2: { width: 70, height: 23, alignSelf: 'center' },
  indicesheader: { bottom: '3%' },
  indicescol1: { width: '10%', height: 50, },
  indicescol2: { width: '4%', height: 50, },
  indicescol3: { width: '50%', height: 58, backgroundColor: 'white', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 5, },
  indicescol4: { width: '50%', height: 58, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 5, },

  indicescoldark3: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  indicescoldark4: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  commodityheader: { bottom: '4%' },
  commodityheight: { height: 107 },
  commodityrow: { marginLeft: 15 },
  commoditytext: { marginTop: 5 },
  commoditycol: { width: '4%', height: 90 },
  bannerrow2: { marginTop: 15, },
  bannercol3: { width: '10%', height: 53, },
  bannercol4: { width: '90%', height: 53, },
  addview: { marginTop: '60%', },
  popoverview: { backgroundColor: 'white', height: '100%' },
  popovercol1: { width: '20%', height: 50, },
  popovercol2: { width: '80%', height: 50, },
  popovercol3: { width: '40%', height: 50, },
  popovercol4: { width: '5%', height: 50, },
  popovercol5: { width: '35%', height: 50, },
  popovercol6: { width: '100%', height: 40, },
  popovercol7: { width: '50%', height: 40, },
  popovercol8: { width: '30%', height: 50, },
  pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
  pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },

  darkindicescol1: { width: '96.5%', height: 70, },
  darkindicescol2: { width: '0%', height: 70, marginLeft: -20, },
  darkindicescol3: { width: '100%', height: 70, },

  lightindicescol1: { width: '96.5%', height: 70, },
  lightindicescol2: { width: '0%', height: 70, marginLeft: -20, },
  lightindicescol3: { width: '100%', height: 70, },
  popovertext1: { textAlign: 'left', fontSize: 20, bottom: 3, color: 'black', },
  popovertext2: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' },
  popovericon1: { textAlign: 'center' },
  popovericon2: { textAlign: 'right' },
  popovericon3: { marginLeft: 50, textAlign: 'left' },
  switch: { bottom: 3, alignSelf: 'center' },
  edithometext: { textAlign: 'left', fontSize: 17, color: 'black' },
  okbutton: { marginLeft: 150, marginRight: 130, backgroundColor: '#2087c9', height: 35 },
  oktext: { marginLeft: 25, fontSize: 20 },
  modalheight: { height: '100%' },
  loaderheight: { marginTop: '30%', },
  modalsensexview: { borderColor: 'black', borderWidth: 1, marginTop: '10%', margin: 10, height: '40%' },
  modalsensextext1: { textAlign: 'center', fontSize: 19 },
  modalsensextext2: { textAlign: 'center', fontSize: 15 },
  menurow: { marginLeft: 20, marginTop: 10 },
  menucol1: { width: '67%', height: 40 },
  menucol2: { width: '15%', height: 40 },
  menucol3: { width: '75%', height: 40 },
  menucol4: { width: '25%', height: 40 },
  closeicon: { marginLeft: 10 },
  usericon: { marginTop: 3, textAlign: 'right' },
  settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
  scrollview: { top: '8%' },
  scrollview1: { bottom: 30 },
  scrollrow: { marginLeft: 16, marginRight: 16 },
  menuicon: { marginTop: 6 },
  settingrow: { marginTop: '25%' },
  footer1: { width: 26, height: 28, },
  footer2: { width: 25, height: 27, },
  footer3: { width: 23, height: 24, },
  darkview: { backgroundColor: '#0b0b0b', height: '100%' },
  lightview: { backgroundColor: 'red', height: '100%' },
  showindicesdark: { display: 'flex', marginLeft: 16, marginRight: 7, backgroundColor: '#1a1f1f', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5, },
  showindiceslight: { display: 'flex', marginLeft: 16, marginRight: 7, backgroundColor: 'white', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5, },

  showindicesdarkpos: { display: 'flex', marginLeft: 16, marginRight: 7, backgroundColor: '#1a1f1f', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, },
  showindiceslightpos: { display: 'flex', marginLeft: 16, marginRight: 7, backgroundColor: 'white', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, },

  showindicesdark1: { display: 'flex', marginRight: 16, backgroundColor: '#1a1f1f', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5, },
  showindiceslight1: { display: 'flex', marginRight: 16, backgroundColor: 'white', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5, },
  showindicesdarkpos1: { display: 'flex', marginRight: 16, backgroundColor: '#1a1f1f', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, },
  showindiceslightpos1: { display: 'flex', marginRight: 16, backgroundColor: 'white', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, },

  showindicesdark2: { display: 'flex', marginRight: 25, backgroundColor: '#1a1f1f', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5, },
  showindiceslight2: { display: 'flex', marginRight: 25, backgroundColor: 'white', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5, },
  showindicesdarkpos2: { display: 'flex', marginRight: 25, backgroundColor: '#1a1f1f', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, },
  showindiceslightpos2: { display: 'flex', marginRight: 25, backgroundColor: 'white', marginTop: 10, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, },
  currlightviewgreen: { width: 145, backgroundColor: 'white', marginTop: 7, borderRadius: 5, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 9, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
  currdarkviewgreen: { width: 145, backgroundColor: '#1a1f1f', marginTop: 7, borderRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
  currlightviewred: { width: 145, backgroundColor: 'white', marginTop: 7, borderRadius: 5, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 9, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  currdarkviewred: { width: 145, backgroundColor: '#1a1f1f', marginTop: 7, borderRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  watchwhite: { width: 160, height: 87, marginTop: 15, backgroundColor: 'white', left: 5, margin: 5, borderRadius: 7, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
  watchdark: { width: 160, height: 87, marginTop: 15, backgroundColor: '#1a1f1f', left: 5, margin: 5, borderRadius: 7, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
  watchwhitered: { width: 160, height: 87, marginTop: 15, backgroundColor: 'white', left: 5, margin: 5, borderRadius: 7, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  watchdarkred: { width: 160, height: 87, marginTop: 15, backgroundColor: '#1a1f1f', left: 5, margin: 5, borderRadius: 7, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },

  btnViewActive: { borderRadius: 5, width: 335, height: 355, marginTop: 5, },
  btnViewinactive: { borderRadius: 5, width: 335, height: 355, marginTop: 5, },
  btnViewinactiveLast: { borderRadius: 5, width: 335, height: 355, marginTop: 5, },
  btnViewinactivedark: { borderRadius: 5, width: 335, height: 355, marginTop: 5, },
  btnViewinactiveLastdark: { borderRadius: 5, width: 335, height: 355, marginTop: 5, },
  btnViewactiveLast: { borderRadius: 5, width: 335, height: 355, marginTop: 5, },
  view: { marginLeft: 0 },

  gaplight: { backgroundColor: 'transparent', width: 10, height: 70, bottom: 10 },
  gapdark: { backgroundColor: '#0b0b0b', width: 10, height: 70, bottom: 10 },



});

// export default Test;
export default connect(mapStateToProps, mapDispatchToProps)(Home);