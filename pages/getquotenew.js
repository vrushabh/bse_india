import React, { Component } from 'react';
import { View, ToastAndroid, Animated, Image, processColor, Easing, Modal, CheckBox, Linking, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Tab, Tabs, Container, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Iconoct from 'react-native-vector-icons/Octicons';
import moment from 'moment-timezone';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import { connect } from 'react-redux';
import { InterstitialAd, RewardedAd, BannerAd, TestIds, BannerAdSize } from '@react-native-firebase/admob';
import Ripple from 'react-native-material-ripple';
import api from '../api.js';
import { LineChart, CombinedChart } from 'react-native-charts-wrapper';
import Popover from 'react-native-popover-view'

import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class NewGetQuote extends Component {
  static navigationOptions = { header: null };
  pagename = '';
  scpid = '';
  scpdate = '';
  formatDate = '';
  marketdata = [{ 'buyquantity': 'Quantity', 'buyprice': 'Price', 'sellprice': 'Price', 'sellquantity': 'Quantity' }];
  constructor(props) {

    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    super(props);
    this.state = {
      pickername: '',
      fadeanim: new Animated.Value(0),
      xValue: new Animated.Value(0),
      xValue1: new Animated.Value(0),
      xValue2: new Animated.Value(0),
      xValue3: new Animated.Value(0),
      sidemenu: false,
      showedit: false,
      settingshow: false,
      y: 0,
      equity: true,
      sensex: false,
      sme: false,
      derivative: false,
      currency: false,
      commodity: false,
      ird: false,
      etf: false,
      debt: false,
      corporate: false,
      ipf: false,
      iconname: 'ios-arrow-down',
      iconnamesensex: 'ios-arrow-forward',
      iconnamesme: 'ios-arrow-forward',
      iconnamederivative: 'ios-arrow-forward',
      iconnamecurrency: 'ios-arrow-forward',
      iconnamecommodity: 'ios-arrow-forward',
      iconnameird: 'ios-arrow-forward',
      iconnameetf: 'ios-arrow-forward',
      iconnamedebt: 'ios-arrow-forward',
      iconnamecorporate: 'ios-arrow-forward',
      iconnameipf: 'ios-arrow-forward',
      pgname: '',
      seriesdata1: '',
      headername: '',
      headerltp: '',
      headerchng: '',
      headerchngper: '',
      date: '',
      id: '',
      loader: true,
      Data: [],
      note: '',
      tabsValue: 0,
      val: 0,
      isVisible: false,
      valuedata: [50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78, 50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78, 50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78, 50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78, 50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78, 50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78, 50, 20, 70, 10, 5, 35, 40, 26, 30, 50, 80, 20, 30, 50, 10, 24, 65, 78, 70, 60, 85, 80, 73, 60, 50, 10, 24, 65, 78, 85, 36, 50, 80, 20, 30, 50, 10, 24, 65, 78]

    };
    setTimeout(() => { SplashScreen.hide() }, 4000);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

  }

  componentDidMount() {
    this.setState({ tabsValue: 0 });

  }
  seriesData(id, dt) {
    api.getquoteseries(id, dt).then(res => {
      if (res != '') {
        console.log(res);
        this.setState({
          headername: res.split(',')[0], seriesid: res.split(',')[0], headerltp: res.split(',')[2], headerchng: res.split(',')[4],
          headerchngper: res.split(',')[5], date: res.split(',')[3], id: res.split(',')[1],
          prvclose: res.split(',')[8], open: res.split(',')[9], high: res.split(',')[10], low: res.split(',')[11],
          avgprice: res.split(',')[12], toValue: res.split(',')[13], oi: res.split(',')[15], prem: res.split(',')[20],
          mktlot: res.split(',')[18], ttq: res.split(',')[14], rbiref: res.split(',')[19], expiry: res.split(',')[7],
          allhigh: res.split(',')[16], alllow: res.split(',')[17]
        });


      }
      else {
        this.setState({ nodataturn: true, refreshing: false, loader: false, });

      }
    });
  }
  marketdepth(id, dt) {
    api.getquotemarket(id, dt).then(res => {
      if (res != '') {
        console.log(res);
        for (let i = 0; i < res.length; i++) {

          this.marketdata.push({ 'buyquantity': res[i].split(',')[0], 'buyprice': res[i].split(',')[1], 'sellprice': res[i].split(',')[2], 'sellquantity': res[i].split(',')[3] });



        }
        this.setState({ Data: this.marketdata });

        console.log(this.marketdata);
        // this.setState({
        //   loader:false,
        // });


      }
      else {
        this.setState({ nodataturn: true, refreshing: false, loader: false, });

      }
    });
  }
  opentextnote() {
    api.note().then(res => {
      if (res != '') {

        this.setState({ note: res.note, loader: false });
      }
      else {
        this.setState({ nodataturn: true, refreshing: false, loader: false, });

      }
    });

  }
  equitystockData() {
    api.getquoteEquity1().then(res => {
      if (res != '') {
        console.log(res);

        this.setState({
          headername: res[2], seriesid: res[1], id: res[0], headerltp: res[6], date: res[16],
          prvclose: res[7], open: res[8], high: res[9],
          low: res[10], avgprice: res[11], toValue: res[14],
          oi: res[12], prem: res[15], mktlot: res[13], ttq: res[17],
          rbiref: res[20], expiry: res[19],
          allhigh: res[3], alllow: res[22]
        });
        // this.setState({note:res.note,loader:false});
      }
      else {
        // this.setState({ nodataturn: true, refreshing: false, loader: false, });

      }
    });
  }
  equitystockData1() {
    api.getquoteEquity2().then(res => {
      if (res != '') {
        console.log(res);

        this.setState({
          headerchng: res[0].chgval, headerchngper: res[0].chgper,

        });
        // this.setState({note:res.note,loader:false});
      }
      else {
        // this.setState({ nodataturn: true, refreshing: false, loader: false, });

      }
    });
  }
  equitystockData2() {
    api.getquoteEquity3().then(res => {
      if (res != '') {
        console.log(res);
        for (let i = 0; i < res.length; i++) {
          if (res[i].split(',')[0] == '') { }
          else {
            this.marketdata.push({ 'buyquantity': res[i].split(',')[0], 'buyprice': res[i].split(',')[1], 'sellprice': res[i].split(',')[2], 'sellquantity': res[i].split(',')[3] });

          }

        }
        this.setState({ Data: this.marketdata, loader: false });

        console.log(this.marketdata);

      }
      else {
        this.setState({ nodataturn: true, refreshing: false, loader: false, });

      }
    });
  }
  componentWillMount() {
    this.pagename = this.props.navigation.getParam('page', '');

    this.scpid = this.props.navigation.getParam('scripid', '');
    this.scpdate = this.props.navigation.getParam('date', '');
    this.formatDate = moment(this.scpdate).format('YYYY-M-D');
    this.setState({ tabsValue: 0, pgname: this.pagename });
    if (this.pagename == 'currency') {
      this.seriesData(this.scpid, this.formatDate);
      this.marketdepth(this.scpid, this.formatDate);
      this.opentextnote();
    }
    else if (this.pagename == 'equity') {
      this.equitystockData();
      this.equitystockData1();
      this.equitystockData2();
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.refresh();
  }
  refresh() {
    this.seriesData(this.scpid, this.formatDate);
    this.marketdepth(this.scpid, this.formatDate);
  }
  chng(per1, per2) {

    if (per1 > 0) {
      return '+' + per1 + '  ' + '+' + per2 + ' %';
    }
    else if (per1 < 0) {
      return per1 + ' ' + per2 + ' %';
    }
    else if (per1 == '0.0000') {
      return '+' + per1 + '  ' + '+' + per2 + ' %';
    }


  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;

  }
  gotoEnd = () => {
    this.scrollView.scrollToEnd({ animated: true });

  }

  handleScroll1(event) {
    console.log(event.nativeEvent.contentOffset.x);
  }
  tab(val) {

    if (val == 'watch') { }
    else if (val == 'port') { }
    else if (val == 'home') {
      this.props.navigation.navigate('maindark');

    }
    else if (val == 'search') {
      AsyncStorage.getItem('themecolor').then((dt) => {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('search', { 'theme': dt });
      });
    }
    else if (val == 'more') {
      this.setState({ sidemenu: true })
    }

  }
  closesidemenu() {
    this.setState({ sidemenu: false });
  }

  indices() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('indices');
  }

  home() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('maindark');

  }

  settingpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('setting', { 'current': val });

  }

  derivativepage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('derivativetab', { 'current': val });

  }
  currencypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('currencytab', { 'current': val });

  }
  commoditypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('commoditytab', { 'current': val });

  }
  irdpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ird', { 'current': val });

  }
  smepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('sme', { 'current': val });
  }
  etfpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('etf', { 'current': val });
  }
  debtpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('debt', { 'current': val });
  }
  corporatepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('corporate', { 'current': val });
  }
  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
  }
  marketstatic() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketstatic');
  }
  marketturn() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketturn');
  }
  listing() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('listing');
  }
  ipo() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipo');
  }
  watchlist() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('watch');
  }
  ipfpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipf', { 'current': val });
  }
  notices() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('notice');
  }
  equityView() {
    // this.state.equity == false ? this.setState({ equity: true, iconname: 'ios-arrow-down' }) : this.setState({ equity: false, iconname: 'ios-arrow-forward' });
    this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });
  }
  sensexView() {
    this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
  }
  smeView() {
    this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
  }
  derivativeView() {
    this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
  }
  currencyView() {
    this.state.currency == false ? this.setState({ currency: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currency: false, iconnamecurrency: 'ios-arrow-forward' });
  }
  commadityView() {
    this.state.commodity == false ? this.setState({ commodity: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commodity: false, iconnamecommodity: 'ios-arrow-forward' });
  }
  irdView() {
    this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
  }
  etfView() {
    this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
  }
  debtView() {
    this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
  }
  corporateView() {
    this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
  }
  ipfView() {
    this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

  }
  changeTheme(thm) {

    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem("themecolor", thm);
    this.props.navigation.navigate('maindark');
    this.props.navigation.goBack(null);
    thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();
  }
  chnglanguage(lan) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem('language', lan);
    this.props.navigation.navigate('maindark');
  }
  pgscroll(i) {
    i == 3 ? this.scrollView1.scrollTo({ x: 1 * width, y: 0, animated: true }) : i == 6 ? this.scrollView1.scrollTo({ x: 2 * width, y: 0, animated: true }) : '';
    this.setState({ tabsValue: i });
  }
  stock() {
    this.viewPager1.setPage(0);

  }
  announcement() {
    this.viewPager1.setPage(1);

  }
  result() {
    this.viewPager1.setPage(2);

  }
  share() {
    this.viewPager1.setPage(3);

  }
  corp() {
    this.viewPager1.setPage(4);

  }
  voting() {
    this.viewPager1.setPage(5);

  }
  bulk() {
    this.viewPager1.setPage(6);

  }
  weeklow() {
    this.viewPager1.setPage(7);

  }
  _renderDotIndicatorlighttwo() {
    return <PagerDotIndicator pageCount={2} dotStyle={{ backgroundColor: '#d5d6d9', top: 10, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#0089d0', top: 10, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }

  _renderDotIndicatordarktwo() {
    return <PagerDotIndicator pageCount={2} dotStyle={{ backgroundColor: '#2e414a', top: 10, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: 'white', top: 10, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  render() {
    return (
      <Container style={this.props.theme == '0' ? styles.lightscroll : styles.darkscroll}>


        <Header hasTabs androidStatusBarColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} style={this.props.theme == '0' ? styles.headerLight : styles.headerLight} >

        </Header>

        <View style={{ bottom: 1 }}>
          <IndicatorViewPager
            style={{ height: 205 }}
            indicator={this._renderDotIndicatorlighttwo()}
          >
            <View>
              <View style={this.props.theme == '0' ? styles.viewLight : styles.viewDark}>

                <Row style={styles.mainrow1}>
                  <Col style={styles.maincol1}></Col>
                  <Col style={styles.maincol2}>

                  </Col>
                  <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.google} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                  <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={styles.popovericon1} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                </Row>
                <Row style={{ marginTop: 5, }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>

                <Row style={{ margin: 5, marginTop: 28, }}>
                  <Col style={styles.maincol20}><Text numberOfLines={2} style={this.props.theme == '0' ? styles.name : styles.namedark}>{this.state.headername}</Text></Col>

                  <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEnt name="pluscircleo" style={styles.popovericon} size={20} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                </Row>
                <View style={this.state.loader == true ? styles.show : styles.hide}>
                  <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                </View>

                <Row style={{ margin: 5, marginTop: 15, }}>
                  <Col style={styles.maincol15}>
                    <Text style={this.props.theme == '0' ? styles.value : styles.darkvalue}>{this.state.headerltp}</Text>
                  </Col>
                  <Col style={styles.maincol16}><TouchableOpacity activeOpacity={.5} onPress={() => this.setState({ isVisible: true })}><IconEnt name="infocirlce" style={styles.infoicon1} size={20} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>

                </Row>
                <Row style={{ margin: 5, bottom: 9 }}>
                  <Col style={styles.datecol1}>
                    <Text style={(this.state.headerchng > 0 || this.state.headerchng == '0.0000') ? styles.change : styles.negchange}>{this.chng(this.state.headerchng, this.state.headerchngper)}</Text>
                  </Col>
                  {/* <Col style={styles.datecol1}>
                    <Text style={styles.date}>{this.state.date}</Text>
                  </Col> */}

                </Row>
                <Row style={{ margin: 5, bottom: 15 }}>

                  <Col style={styles.datecol1}>
                    <Text style={styles.date}>{this.state.date}</Text>
                  </Col>

                </Row>

              </View>
            </View>
            <View>
              <LineChart
                style={this.props.theme == '0' ? styles.chart : styles.darkchart}
                data={{
                  dataSets: [{
                    label: "", values: this.state.valuedata, config: {
                      lineWidth: 3,
                      drawCircles: false,
                      drawFilled: true,
                      drawLabels: false,
                      drawValues: false,
                      fillColor: processColor('#ff0000'),
                      circleColor: processColor('red'),
                      drawCircleHole: false,
                      circleRadius: 5,
                      colors: [processColor('red')],
                      fillAlpha: 100,
                      valueTextSize: 10,

                    },

                  }

                  ],
                }}
                xAxis={{
                  position: 'BOTTOM',
                  //valueFormatter: this.state.tmdata,
                  granularityEnabled: true,
                  granularity: 1,
                  labelCount: 10,
                  drawAxisLine: true,
                  drawGridLines: true,
                  textSize: 13,
                  scaleXEnabled: true,
                  enabled: false,

                }}

                yAxis={{
                  right: { enabled: false },
                  left: {
                    enabled: true,
                    drawAxisLine: true,
                    drawGridLines: true,
                    textSize: 13
                  }
                }}
                legend={{
                  horizontalAlignment: "LEFT",
                  verticalAlignment: 'TOP',
                  enabled: false,
                  textSize: 15,
                  form: 'SQUARE',
                  formSize: 15,
                  xEntrySpace: 20,
                  yEntrySpace: 10,
                  formToTextSpace: 10,
                  wordWrapEnabled: true,
                  maxSizePercent: 0.5,
                }}
                animation={{ durationX: 2000 }}
                visibleRange={{ x: { min: 7, max: 7 } }}
                chartDescription={{ text: '' }}
                marker={{
                  enabled: true,
                  markerColor: processColor('#132144'), textSize: 15
                }}


                touchEnabled={true}
                dragEnabled={true}
                scaleEnabled={true}
                scaleXEnabled={false}
                scaleYEnabled={true}
                pinchZoom={true}
                zoom={this.state.chart1Zoom}

                ref="chart1"


              />
            </View>

          </IndicatorViewPager>
        </View>
        {/* <View style={styles.scroll1}>
          
        </View> */}


        <ScrollView ref={scrollView => this.scrollView = scrollView} style={this.props.theme == '0' ? styles.lightscroll : styles.darkscroll}>


          <Row>
            <Col style={styles.bannercol1}></Col>
            <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
          </Row>
          <View style={this.state.pgname == 'currency' ? styles.hide : styles.show}>
            <ScrollView horizontal ref={scrollView => this.scrollView1 = scrollView} onScroll={event => this.handleScroll1(event)} style={this.props.theme == '0' ? styles.buttonlight : styles.buttondark} showsHorizontalScrollIndicator={false} >

              <View style={styles.view}></View>
              <View style={this.state.tabsValue == 0 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.stock()}><Text style={this.state.tabsValue == 0 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Stock Trading </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>
              <View style={this.state.tabsValue == 1 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.announcement()}><Text style={this.state.tabsValue == 1 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Announcement </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

              <View style={this.state.tabsValue == 2 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.result()}><Text style={this.state.tabsValue == 2 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Result </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

              <View style={this.state.tabsValue == 3 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.share()}><Text style={this.state.tabsValue == 3 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Shareholding </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

              <View style={this.state.tabsValue == 4 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.corp()}><Text style={this.state.tabsValue == 4 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Corp Actions </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

              <View style={this.state.tabsValue == 5 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voting()}><Text style={this.state.tabsValue == 5 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Voting Result </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

              <View style={this.state.tabsValue == 6 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.bulk()}><Text style={this.state.tabsValue == 6 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Bulk/Block </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

              <View style={this.state.tabsValue == 7 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.weeklow()}><Text style={this.state.tabsValue == 7 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Technical </Text></TouchableOpacity></View>
              <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

            </ScrollView>

            <ViewPager
              style={{ height: height + 120 }}
              initialPage={this.state.tabsValue}
              ref={viewPager => { this.viewPager1 = viewPager; }}
              onPageScroll={(i) => this.pgscroll(i.position)}
            >
              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>

              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.prvcolequitylight : styles.prvcolequitydark}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 8 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>

                  <Row style={{ margin: 10, bottom: 10 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 12 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                  <Row style={{ margin: 10, bottom: 13 }}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                    <Col style={this.props.theme == '0' ? styles.midcollight : styles.midcoldark}></Col>

                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text><Text style={this.props.theme == '0' ? styles.lightborder : styles.darkborder}></Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.equityttitlelight : styles.equityttitledark}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.marketlight : styles.marketdark}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol10}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>


                <Row style={{ bottom: 30 }}>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>
            </ViewPager>
          </View>

          <View style={this.state.pgname == 'currency' ? styles.show : styles.hide}>
            <ViewPager
              style={{ height: height + 500 }}
              initialPage={this.state.tabsValue}
              ref={viewPager => { this.viewPager = viewPager; }}
              onPageScroll={(i) => this.pgscroll(i.position)}
            >
              <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>


                <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prvclose}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.open}</Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow6}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.high}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.low}</Text></Col>
                  </Row>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.avgprice}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'T/O (Cr.)' : 'Traded Value (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.toValue}</Text></Col>
                  </Row>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow7}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'OI Qty' : 'Upper Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.oi}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Prem (Cr.)' : 'TTQ (L)'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.prem}</Text></Col>
                  </Row>

                  <Row style={styles.mainrow8}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.mktlot}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>{this.state.pagename == 'currency' ? 'Mkt Lot' : 'Lower Ckt'}</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.ttq}</Text></Col>
                  </Row>
                  <Row style={styles.mainrow9}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>RBI Ref Rate</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.rbiref}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Expiry</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.expiry}</Text></Col>
                  </Row>
                  <Row style={styles.mainrow10}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.allhigh}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>All time Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>{this.state.alllow}</Text></Col>
                  </Row>
                </View>

                <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                  <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <Row style={styles.mainrow5}>
                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                    <Col style={styles.maincol10}></Col>
                  </Row>

                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                      />
                    }
                    data={this.state.Data}
                    //style={styles.flatlist1}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>

                      <View>
                        <Row style={styles.mainrow11}>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.buyquantity}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.buyprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>{item.sellprice}</Text></Col>
                          <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>{item.sellquantity}</Text></Col>
                        </Row>
                      </View>

                    } />


                </View>

                <View style={this.props.theme == '0' ? styles.lightopentext : styles.darkopentext}>
                  <Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext}>
                    {this.state.note}
                  </Text>
                </View>
                <Row>
                  <Col style={styles.bannercol1}></Col>
                  <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                </Row>
                <Text></Text>
              </View>
            </ViewPager>

          </View>





        </ScrollView>

        <Modal
          animationType="slide"
          transparent={false}
          style={styles.modalheight}
          visible={this.state.sidemenu}
          onRequestClose={() => {
            this.setState({ sidemenu: false });
          }}
        >
          <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
            <Row style={styles.menurow}>
              <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
            </Row>

            <ScrollView style={styles.scrollview}>

              <Text></Text>
              <View style={{}}>
                <Row style={styles.scrollrow}>
                  <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                  <Col style={styles.popovercol7}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.equity == false ? styles.hide : styles.show}>
                  <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                  <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                  <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                  <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                  <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.sensex == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.sme == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                  <Text></Text>
                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.derivative == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.currency == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.commodity == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.ird == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.etf == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.debt == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.corporate == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                  <Col style={styles.menucol4}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.ipf == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                  <Text></Text>

                </View>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>
                {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
              </View>
            </ScrollView>
          </View>
        </Modal>

        <Popover
          isVisible={this.state.isVisible}
          fromView={this.touchable}

          onRequestClose={() => this.setState({ isVisible: false })}>
          <View style={this.props.theme == '0' ? styles.lightrow : styles.darkrow}>
            <Row style={styles.menurow}>
              <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.setState({ isVisible: false })} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>

            </Row>
            <View style={styles.row1}>
              <View style={this.props.theme == '0' ? styles.maincol7 : styles.darkmaincol7}><Text style={this.props.theme == '0' ? styles.comname1 : styles.darkcomname1} numberOfLines={1}>{this.state.seriesid}</Text><Text style={styles.comname2}>Security ID</Text></View>
              <View style={this.props.theme == '0' ? styles.maincol8 : styles.darkmaincol8}><Text style={this.props.theme == '0' ? styles.comname1 : styles.darkcomname1}>{this.state.id}</Text><Text style={styles.comname2}>Security Code</Text></View>
              <View style={this.props.theme == '0' ? styles.maincol9 : styles.darkmaincol9}><Text style={this.props.theme == '0' ? styles.comname1 : styles.darkcomname1}>Listed</Text><Text style={styles.comname2}>Category</Text></View>
            </View>
          </View>
        </Popover>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.settingshow}
          style={styles.modalheight}
          onRequestClose={() => {
            this.setState({ settingshow: false });
            this.setState({ sidemenu: false });

          }}
        >
          <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

            <ScrollView style={styles.scrollview1}>
              <Row style={styles.settingrow}>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                <Col style={styles.popovercol8}>
                  <Picker
                    selectedValue={this.state.selectlan}
                    style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                    onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                  >
                    <Picker.Item label="English" value="English" />
                    <Picker.Item label="Hindi" value="Hindi" />
                    <Picker.Item label="Marathi" value="Marathi" />
                    <Picker.Item label="Gujrati" value="Gujrati" />
                    <Picker.Item label="Bengali" value="Bengali" />
                    <Picker.Item label="Malayalam" value="Malayalam" />
                    <Picker.Item label="Oriya" value="Oriya" />
                    <Picker.Item label="Tamil" value="Tamil" />
                  </Picker>
                  <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                </Col>
              </Row>
              <Text></Text>
              <Row >
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                <Col style={styles.popovercol8}>
                  <Picker
                    selectedValue={this.props.themename}
                    style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                    onValueChange={(itemValue) => this.changeTheme(itemValue)}
                  // onValueChange={(itemValue) => this.props.themechng()}
                  >
                    <Picker.Item label="Dark" value="dark" />
                    <Picker.Item label="Light" value="light" />

                  </Picker>
                  <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                </Col>
              </Row>

              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
              </Row>
            </ScrollView>
          </View>
        </Modal>
        <Footer>

          <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>

            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
              <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
            </Button>


            <Button style={styles.inactive} onPress={() => this.tab('port')}>
              <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
            </Button>

            <Button style={styles.active} onPress={() => this.tab('home')}>
              <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
              <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('search')}>
              <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('more')}>
              <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
            </Button>


          </FooterTab>
        </Footer>

      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
    themename: state.themename
  }
}

function mapDispatchToProps(dispatch) {
  return {
    themechnglight: () => dispatch({ type: 'light' }),
    themechngdark: () => dispatch({ type: 'dark' }),
  }
}
const styles = StyleSheet.create({
  headerDark: { backgroundColor: '#0b0b0b', height: 1 },
  headerLight: { backgroundColor: '#f6f7f9', height: 1 },
  lightscroll: { backgroundColor: '#f6f7f9', },
  darkscroll: { backgroundColor: '#000000', },
  viewDark: { backgroundColor: '#000000', height: 205 },
  viewLight: { backgroundColor: '#f6f7f9', height: 205 },
  scroll1: { height: 145, bottom: 1, },
  lightpicker: { width: '100%', color: '#132144', bottom: 8, fontFamily: 'Arial', fontWeight: 'bold', },
  darkpicker: { width: '100%', color: 'white', bottom: 8, fontFamily: 'Arial', fontWeight: 'bold', },

  mainrow1: { margin: 5 },
  mainrow2: { margin: 15, marginTop: '15%' },
  mainrow3: { marginTop: '8%' },
  mainrow5: { margin: 10, },
  mainrow11: { margin: 9, },
  mainrow6: { margin: 10, bottom: 2 },
  mainrow7: { margin: 10, bottom: 3 },
  mainrow8: { margin: 10, bottom: 10 },
  mainrow9: { margin: 10, bottom: 10 },
  mainrow10: { margin: 10, bottom: 5 },
  maincol1: { width: '15%', height: 30 },
  maincol2: { width: '55%', height: 30, },
  maincol3: { width: '20%', height: 30, },
  maincol10: { width: '30%', height: 30, },
  dropcol1: { width: '20%', height: 35, },
  dropcol2: { width: '60%', height: 35, backgroundColor: 'white', borderRadius: 5 },
  darkdropcol2: { width: '60%', height: 35, backgroundColor: '#1a1f1f', borderRadius: 5 },
  dropcol3: { width: '20%', height: 35, },
  maincol11: { width: '45%', height: 30, backgroundColor: 'white' },
  midcollight: { width: '10%', height: 30, backgroundColor: 'white' },
  midcoldark: { width: '10%', height: 30, backgroundColor: '#1a1f1f' },
  darkmaincol11: { width: '45%', height: 30, backgroundColor: '#1a1f1f' },
  maincol15: { width: '20%', height: 30, backgroundColor: 'white', },
  maincol16: { width: '25%', height: 30, backgroundColor: 'white', },
  maincol17: { width: '10%', height: 30, backgroundColor: 'white', },
  maincol15title: { width: '20%', height: 30, backgroundColor: 'white', borderBottomColor: '#132144', borderBottomWidth: 0.5 },
  maincol16title: { width: '25%', height: 30, backgroundColor: 'white', borderBottomColor: '#132144', borderBottomWidth: 0.5 },
  maincol17title: { width: '10%', height: 30, backgroundColor: 'white', borderBottomColor: '#132144', borderBottomWidth: 0.5 },

  darkmaincol15title: { width: '20%', height: 30, backgroundColor: '#1a1f1f', borderBottomColor: 'white', borderBottomWidth: 0.5 },
  darkmaincol16title: { width: '25%', height: 30, backgroundColor: '#1a1f1f', borderBottomColor: 'white', borderBottomWidth: 0.5 },
  darkmaincol17title: { width: '10%', height: 30, backgroundColor: '#1a1f1f', borderBottomColor: 'white', borderBottomWidth: 0.5 },
  maincol12: { height: 120, backgroundColor: 'white', margin: 10, borderRadius: 5 },
  darkmaincol12: { height: 120, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
  maincol13: { height: 420, backgroundColor: 'white', margin: 10, borderRadius: 5 },
  darkmaincol13: { height: 420, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
  maincol14: { height: 400, backgroundColor: 'white', margin: 10, borderRadius: 5 },
  darkmaincol14: { height: 400, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
  maincol4: { width: '10%', height: 30, },
  maincol5: { width: '100%', height: 50, },
  datecol1: { width: '100%', height: 30, },
  maincol15: { width: '90%', height: 50, },
  maincol16: { width: '10%', height: 50, },
  maincol6: { width: '80%', height: 40, },
  maincol20: { width: '90%', height: 40, },
  maincol7: { width: '35%', height: 70, backgroundColor: '#f6f7f9', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
  maincol8: { width: '35%', height: 70, backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  maincol9: { width: '30%', height: 70, backgroundColor: '#f6f7f9', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomRightRadius: 5, borderTopRightRadius: 5, },
  darkmaincol7: { width: '35%', height: 70, backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
  darkmaincol8: { width: '35%', height: 70, backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  darkmaincol9: { width: '30%', height: 70, backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomRightRadius: 5, borderTopRightRadius: 5, },
  google: { width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 },
  back: { textAlign: 'left', marginLeft: 13 },
  popovericon1: { textAlign: 'center' },
  popovericon: { textAlign: 'center', marginTop: 5, },

  infoicon: { textAlign: 'center', },
  infoicon1: { textAlign: 'center' },
  name: { fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 25, textAlign: 'center', fontSize: 19, color: '#132144' },
  namedark: { fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 25, textAlign: 'center', fontSize: 19, color: 'white' },
  value: { fontFamily: 'Arial', fontWeight: 'bold', textAlign: 'center', marginLeft: 25, bottom: 13, fontSize: 30, color: '#132144' },
  darkvalue: { fontFamily: 'Arial', fontWeight: 'bold', textAlign: 'center', marginLeft: 25, bottom: 13, fontSize: 30, color: 'white' },
  change: { fontFamily: 'Arial', fontWeight: 'bold', textAlign: 'center', fontSize: 17, color: '#19cf3e', },
  chart: { width: '100%', height: '100%', backgroundColor: '#f6f7f9' },
  darkchart: { width: '100%', height: '100%', backgroundColor: '#000000' },
  negchange: { fontFamily: 'Arial', fontWeight: 'bold', textAlign: 'center', fontSize: 17, color: '#f54845', },
  date: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, textAlign: 'center', color: '#aaafba' },
  row1: { flex: 1, flexDirection: 'row', margin: 10 },
  lightrow: { backgroundColor: '#f6f7f9' },
  darkrow: { backgroundColor: '#1a1f1f' },
  comname1: { textAlign: 'center', marginTop: 15, fontSize: 14, color: '#132144' },
  darkcomname1: { textAlign: 'center', marginTop: 15, fontSize: 14, color: 'white' },
  comname2: { textAlign: 'center', marginTop: 5, fontSize: 12, color: '#2087c9' },
  bannercol1: { width: '6%', height: 53, },
  bannercol2: { width: '80%', height: 53, },
  prv: { fontFamily: 'Arial', fontSize: 13, color: '#132144' },
  darkprv: { fontFamily: 'Arial', fontSize: 13, color: 'white' },
  buy: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 18, color: '#19cf3e' },
  sell: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 18, color: '#f54845', textAlign: 'center' },
  prvvalue: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, marginTop: 5, color: '#132144', },
  darkprvvalue: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, marginTop: 5, color: 'white', },
  lightborder: { borderBottomColor: '#132144', borderBottomWidth: 0.5, bottom: 15 },
  darkborder: { borderBottomColor: 'white', borderBottomWidth: 0.5, bottom: 15 },
  title: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 20, color: '#132144', margin: 10 },
  darktitle: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 20, color: 'white', margin: 10 },
  quantity: { fontFamily: 'Arial', bottom: 5, fontWeight: 'bold', fontSize: 13, color: '#132144', },
  darkquantity: { fontFamily: 'Arial', bottom: 5, fontWeight: 'bold', fontSize: 13, color: 'white', },
  price: { fontFamily: 'Arial', bottom: 5, fontWeight: 'bold', fontSize: 13, color: '#132144', textAlign: 'right' },
  darkprice: { fontFamily: 'Arial', bottom: 5, fontWeight: 'bold', fontSize: 13, color: 'white', textAlign: 'right' },
  tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold', },
  tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold', },
  tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold', },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
  inactive: { padding: 10, },
  footer1: { width: 26, height: 28, },
  footer2: { width: 26, height: 27, },
  footer3: { width: 23, height: 24, bottom: 2 },
  pickericon: { bottom: 50, marginLeft: 170 },
  modalheight: { height: '100%' },
  scrollview1: { bottom: 30 },
  settingrow: { marginTop: '25%' },
  popovercol1: { width: '20%', height: 50, },
  popovercol2: { width: '80%', height: 50, },
  popovercol3: { width: '40%', height: 50, },
  popovercol4: { width: '5%', height: 50, },
  popovercol5: { width: '35%', height: 50, },
  popovercol6: { width: '100%', height: 40, },
  popovercol7: { width: '50%', height: 40, },
  popovercol8: { width: '30%', height: 50, },
  pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
  pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },
  equityttitledark: { fontFamily: 'Arial', bottom: 20, fontWeight: 'bold', fontSize: 20, color: 'white', margin: 10 },
  equityttitlelight: { fontFamily: 'Arial', bottom: 20, fontWeight: 'bold', fontSize: 20, color: '#132144', margin: 10 },
  dropicon: { marginLeft: 88, bottom: 30 },

  closeicon: { marginLeft: 10 },
  usericon: { marginTop: 3, textAlign: 'right' },
  settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },

  buttonlight: { height: 50, marginLeft: 4, bottom: 4, backgroundColor: '#f6f7f9' },
  buttondark: { height: 50, marginLeft: 4, bottom: 4, backgroundColor: '#000000' },
  view: { marginLeft: 5 },
  btnViewActive: { borderRadius: 5, backgroundColor: '#2087c9', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 10, },
  btnViewinactive: { borderRadius: 5, backgroundColor: 'white', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 10, },
  btnViewinactiveLast: { borderRadius: 5, backgroundColor: 'white', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 10, },
  btnViewinactivedark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 10, },
  btnViewinactiveLastdark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 10, },
  btnViewactiveLast: { borderRadius: 5, backgroundColor: '#2087c9', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 10, },
  textActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textinActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
  textinActiveDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textinActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
  textinActivelasDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  gaplight: { backgroundColor: '#f6f7f9', width: 10, height: 70, bottom: 10 },
  gapdark: { backgroundColor: '#000000', width: 10, height: 70, bottom: 10 },
  viewrow: { marginLeft: 3, },
  lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
  darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
  lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  lightopentext: { backgroundColor: 'white', borderRadius: 5, margin: 10, height: 55 },
  darkopentext: { backgroundColor: '#1a1f1f', borderRadius: 5, margin: 10, height: 55 },
  prvcolequitylight: { height: 350, bottom: 5, backgroundColor: 'white', margin: 10, borderRadius: 5 },
  prvcolequitydark: { height: 350, bottom: 5, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
  maincol14: { height: 400, bottom: 20, backgroundColor: 'white', margin: 10, borderRadius: 5 },
  darkmaincol14: { height: 400, bottom: 20, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
  marketdark: { height: 400, bottom: 30, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
  marketlight: { height: 400, bottom: 30, backgroundColor: 'white', margin: 10, borderRadius: 5 },
  menucol1: { width: '67%', height: 40 },
  menucol2: { width: '15%', height: 40 },
  menucol3: { width: '75%', height: 40 },
  menucol4: { width: '25%', height: 40 },
  show: { display: 'flex' },
  hide: { display: 'none' },
  scrollview: { top: '8%' },
  scrollrow: { marginLeft: 16, marginRight: 16 },
  scrollrow1: { marginLeft: 17, marginRight: 17, marginTop: 12, },
  menurow: { marginLeft: 20, marginTop: 10 },
  menuicon: { marginTop: 6 },
  lighttext: { margin: 9, color: '#132144' },
  darktext: { margin: 9, color: 'white' },

  lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
});

export default connect(mapStateToProps, mapDispatchToProps)(NewGetQuote);