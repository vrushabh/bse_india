import React, { Component } from 'react';
import { View, ToastAndroid, Image, Modal, CheckBox, Linking, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Tab, Tabs, Container, ScrollableTab, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Iconoct from 'react-native-vector-icons/Octicons';
import moment from 'moment-timezone';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { InterstitialAd, RewardedAd, BannerAd, TestIds, BannerAdSize } from '@react-native-firebase/admob';
import * as Animatable from 'react-native-animatable';
import Ripple from 'react-native-material-ripple';
import { connect } from 'react-redux';
import api from '../api.js';
import CryptoJS from "react-native-crypto-js";

const keydt = 'BseIndiaApi@2020';
var tm = moment().tz("UTC").format('yyyy-MM-DD HH:mm:ss');
var data = tm;
console.log(tm);
var key = CryptoJS.enc.Latin1.parse(keydt);
var iv = CryptoJS.enc.Latin1.parse(keydt);
const encrypted = CryptoJS.AES.encrypt(
    data,
    key,
    {
        iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7
    });
import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
const height = Dimensions.get('window').height;

class NewEquity extends Component {
    static navigationOptions = { header: null };
    per = '';
    per1 = '';
    per2 = '';
    per3 = '';
    indicesId = '16';
    Sensex = [];
    TurnArray = [];
    turnlength = '';
    tabval = 0;
    themename = '';
    gain = [];
    lose = [];
    turn = [];
    high = [];
    low = [];
    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            date: '',
            status: '',
            value: 1,
            loader: false,
            gainerloader: false,
            loserloader: false,
            turnoverloader: false,
            highloader: false,
            lowloader: false,
            indicesLoader: false,
            isVisible: false,
            switchval: false,
            Gainer: [],
            Loser: [],
            Turnover: [],
            High: [],
            Low: [],
            Turn: [],
            contri: [],
            refreshing: false,
            sidemenu: false,
            showedit: false,
            settingshow: false,
            theme: 'dark',
            iconname: 'ios-arrow-down',
            iconnamesensex: 'ios-arrow-forward',
            iconnamesme: 'ios-arrow-forward',
            iconnamederivative: 'ios-arrow-forward',
            iconnamecurrency: 'ios-arrow-forward',
            iconnamecommodity: 'ios-arrow-forward',
            iconnameird: 'ios-arrow-forward',
            iconnameetf: 'ios-arrow-forward',
            iconnamedebt: 'ios-arrow-forward',
            iconnamecorporate: 'ios-arrow-forward',
            iconnameipf: 'ios-arrow-forward',
            ipf: false,
            selectlan: 'English',
            indicesdata: [],
            indicesName: 'S&P BSE SENSEX',
            langauge: '',

            ltp: '',
            change: '',
            changeper: '',
            indicesDropdown: false,
            tabsValue: 0,
            initial: '',
            equity: true,
            sensex: false,
            sme: false,
            derivative: false,
            currency: false,
            commodity: false,
            ird: false,
            etf: false,
            debt: false,
            corporate: false,
            nodatagainer: false,
            nodataloser: false,
            nodataturn: false,
            nodatahigh: false,
            nodatalow: false,

        };
        setTimeout(() => { SplashScreen.hide() }, 4000);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }
    componentDidMount() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            dt == null || dt == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        });
        this.sensexfun();

        AsyncStorage.getItem('language').then((lan) => {
            if (lan == null) {
                this.setState({ langauge: 'en', selectlan: 'English' });
                I18n.locale = "en";


                this.gainfun(this.state.langauge);
                this.losefun(this.state.langauge);
                this.turnoverfun(this.state.langauge);
                this.highfun(this.state.langauge);
                this.lowfun(this.state.langauge);
            }
            else {
                lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";


                this.gainfun(this.state.langauge);
                this.losefun(this.state.langauge);
                this.turnoverfun(this.state.langauge);
                this.highfun(this.state.langauge);
                this.lowfun(this.state.langauge);
            }
        });

    }

    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }
    componentWillMount() {
        this.tabval = this.props.navigation.getParam('current', '');

        this.setState({ tabsValue: this.tabval });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;

    }
    back() {
        this.props.navigation.goBack(null);
        return true;
    }
    sensexfun() {
        this.setState({ gainerloader: true });

        api.equityDate(encrypted).then(responsejson => {
            //responsejson.split('@')[6] == '0' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : responsejson.split('@')[6] == '1' ? this.setState({ date: responsejson.split('@')[5], status: 'Pre-Open' }) : responsejson.split('@')[6] == '2' ? this.setState({ date: responsejson.split('@')[5], status: 'Close' }) : responsejson.split('@')[6] == '3' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : '';
            if (res.status == 'success') {
                res.data[0].Flag == '0' ? this.setState({ date: res.data[0].Dt, status: 'Open' }) : res.data[0].Flag == '1' ? this.setState({ date: res.data[0].Dt, status: 'Pre-Open' }) : res.data[0].Flag == '2' ? this.setState({ date: res.data[0].Dt, status: 'Close' }) : res.data[0].Flag == '3' ? this.setState({ date: res.data[0].Dt, status: 'Open' }) : '';
                // responsejson.split('@')[6] == '0' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : responsejson.split('@')[6] == '1' ? this.setState({ date: responsejson.split('@')[5], status: 'Pre-Open' }) : responsejson.split('@')[6] == '2' ? this.setState({ date: responsejson.split('@')[5], status: 'Close' }) : responsejson.split('@')[6] == '3' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : '';
            }
            else{
                this.setState({ date: '', status: '',ltp:'' });
            }
        });


    }
    gainfun(lan) {
        this.gain = [];
        api.gainer(lan, encrypted).then(res => {
            console.log(res);
            if (res.status == 'success') {
                // for (let i = 0; i < res.data.length; i++) {
                //   this.gain.push({ 'name1': res.data[i].ScripName, 'ltp1': res.data[i].LTradeRt, 'change1': res.data[i].ChangeVal, 'per1': res.data[i].ChangePer, 'name2': this.state.Loser[i].ScripName, 'ltp2': this.state.Loser[i].LTradeRt, 'change2': this.state.Loser[i].ChangeVal, 'per2': this.state.Loser[i].ChangePer });

                // }
                // console.log('gain', this.gain);
                this.setState({ Gainer: res.data, refreshing: false, nodatagainer: false, gainerloader: false })

            }
            else {
                this.setState({ nodatagainer: true, refreshing: false, gainerloader: false });

            }
        });

    }
    losefun(lan) {
        this.setState({ loserloader: true });
        api.loser(lan, encrypted).then(res => {
            console.log(res.data);
            if (res.status == 'success') {

                this.setState({ Loser: res.data, refreshing: false, nodataloser: false, loserloader: false });

            }
            else {
                this.setState({ nodataloser: true, refreshing: false, loserloader: false });

            }
        });

    }

    turnoverfun(lan) {
        this.setState({ turnoverloader: true });
        api.turnover(lan, encrypted).then(res => {
            console.log(res.data);
            if (res.status == 'success') {
                this.setState({ Turnover: res.data, refreshing: false, nodataturn: false, turnoverloader: false })

            }
            else {
                this.setState({ nodataturn: true, refreshing: false, turnoverloader: false });

            }
        });

    }
    highfun(lan) {
        this.setState({ highloader: true });
        api.highapi(lan, encrypted).then(res => {
            console.log(res.data);
            if (res.status == 'success') {
                this.setState({ High: res.data, refreshing: false, nodatahigh: false, highloader: false })

            }
            else {
                this.setState({ nodatahigh: true, refreshing: false, highloader: false });

            }
        });

    }
    lowfun(lan) {
        this.setState({ lowloader: true });
        api.lowapi(lan, encrypted).then(res => {
            console.log(res.data);
            if (res.status == 'success') {
                this.setState({ Low: res.data, refreshing: false, nodatalow: false, lowloader: false })

            }
            else {
                this.setState({ nodatalow: true, refreshing: false, lowloader: false })

            }
        });

    }
    onRefresh() {
        this.setState({ refreshing: true });
        this.refresh();
    }
    refresh() {

        this.setState({ Gainer: [], Loser: [], Turnover: [], High: [], Low: [] })
        NetInfo.isConnected.fetch().then(isConnected => { isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50) });
        this.sensexfun();
        AsyncStorage.getItem('language').then((lan) => {
            if (lan == null) {
                this.setState({ langauge: 'en', selectlan: 'English' });
                I18n.locale = "en";
                this.gainfun(this.state.langauge);
                this.losefun(this.state.langauge);
                this.turnoverfun(this.state.langauge);
                this.highfun(this.state.langauge);
                this.lowfun(this.state.langauge);
            }
            else {
                lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";

                this.gainfun(this.state.langauge);
                this.losefun(this.state.langauge);
                this.turnoverfun(this.state.langauge);
                this.highfun(this.state.langauge);
                this.lowfun(this.state.langauge);
            }
        });

    }
    equityView() {
        // this.state.equity == false ? this.setState({ equity: true, iconname: 'ios-arrow-down' }) : this.setState({ equity: false, iconname: 'ios-arrow-forward' });
        this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });
    }
    sensexView() {
        this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
    }
    smeView() {
        this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
    }
    derivativeView() {
        this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
    }
    currencyView() {
        this.state.currency == false ? this.setState({ currency: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currency: false, iconnamecurrency: 'ios-arrow-forward' });
    }
    commadityView() {
        this.state.commodity == false ? this.setState({ commodity: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commodity: false, iconnamecommodity: 'ios-arrow-forward' });
    }
    irdView() {
        this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
    }
    etfView() {
        this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
    }
    debtView() {
        this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
    }
    corporateView() {
        this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
    }
    ipfView() {
        this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

    }
    googleassistant() {
        Linking.openURL('https://assistant.google.com/explore');

    }

    closesidemenu() {
        this.setState({ sidemenu: false });
    }

    indices() {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('indices');
    }

    home() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('maindark');

    }

    settingpage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('setting', { 'current': val });

    }

    derivativepage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('derivativetab', { 'current': val });

    }
    currencypage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('currencytab', { 'current': val });

    }
    commoditypage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('commoditytab', { 'current': val });

    }
    irdpage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('ird', { 'current': val });

    }
    smepage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('sme', { 'current': val });
    }
    etfpage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('etf', { 'current': val });
    }
    debtpage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('debt', { 'current': val });
    }
    corporatepage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('corporate', { 'current': val });
    }
    setting() {
        this.setState({ settingshow: true });
        this.setState({ sidemenu: false });
    }
    marketstatic() {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('marketstatic');
    }
    marketturn() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('marketturn');
    }
    listing() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('listing');
    }
    ipo() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('ipo');
    }
    watchlist() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('watch');
    }
    ipfpage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('ipf', { 'current': val });
    }
    notices() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('notice');
    }
    changeTheme(thm) {

        this.setState({ settingshow: false, sidemenu: false });
        AsyncStorage.setItem("themecolor", thm);
        this.props.navigation.navigate('maindark');
        this.props.navigation.goBack(null);
        thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();
    }
    chnglanguage(lan) {
        this.setState({ settingshow: false, sidemenu: false });
        AsyncStorage.setItem('language', lan);
        this.props.navigation.navigate('maindark');
    }
    weeklow() {
        // this.setState({ tabsValue: 4 });
        this.viewPager.setPage(4);

    }
    weekhigh() {
        // this.setState({ tabsValue: 3 });
        this.viewPager.setPage(3);

    }
    turnovr() {
        // this.setState({ tabsValue: 2 });
        this.viewPager.setPage(2);
        this.scrollView.scrollToEnd({ animated: true });
    }
    loser() {
        // this.setState({ tabsValue: 1 });
        this.viewPager.setPage(1);

    }
    gainer() {
        // this.setState({ tabsValue: 0 });
        this.viewPager.setPage(0);
    }
    pgscroll(i)
    {
        i == 3 ? this.scrollView.scrollToEnd({ animated: true }) : i == 2 ? this.scrollView.scrollTo({x: 0, y: 0, animated: true}) : i == 1 ? "" : '';
        this.setState({ tabsValue: i })
    }


    render() {
        return (
            <StyleProvider style={getTheme(material)}>

                <Container>


                    <Header hasTabs androidStatusBarColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} style={this.props.theme == '0' ? styles.headerLight : styles.headerDark} >

                    </Header>

                    <View style={this.props.theme == '0' ? styles.viewLight : styles.viewDark}>
                        <Row style={styles.mainrow1}>
                            <Col style={styles.maincol1}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={styles.back} size={27} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                            <Col style={styles.maincol2}>
                                <Text style={this.state.tabsValue == 0 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 0 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>{I18n.t('gainer')}</Text>
                                <Text style={this.state.tabsValue == 1 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 1 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>{I18n.t('loser')}</Text>
                                <Text style={this.state.tabsValue == 2 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 2 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>{I18n.t('topturn')}</Text>
                                <Text style={this.state.tabsValue == 3 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 3 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>{I18n.t('high')}</Text>
                                <Text style={this.state.tabsValue == 4 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 4 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>{I18n.t('low')}</Text>
                            </Col>
                            <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.google} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={styles.popovericon1} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                        </Row>
                        <Row style={styles.mainrow3}>
                            <Col style={styles.bannercol1}></Col>
                            <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                        </Row>
                        <Row style={styles.mainrow2}>

                            <Col style={styles.maincol5}><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.props.theme == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


                        </Row>

                        <ScrollView horizontal ref={scrollView => this.scrollView = scrollView} style={this.props.theme == '0' ? styles.buttonlight : styles.buttondark} showsHorizontalScrollIndicator={false} >

                            <View style={styles.view}></View>
                            <View style={this.state.tabsValue == 0 ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.gainer()}><Text style={this.state.tabsValue == 0 ? styles.textActive : this.props.theme == '0' ? styles.textinActive : styles.textinActiveDark}> Gainer </Text></TouchableOpacity></View>
                            <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>
                            <View style={this.state.tabsValue == 1 ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.loser()}><Text style={this.state.tabsValue == 1 ? styles.textActive : this.props.theme == '0' ? styles.textinActive : styles.textinActiveDark}> Loser </Text></TouchableOpacity></View>
                            <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                            <View style={this.state.tabsValue == 2 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.turnovr()}><Text style={this.state.tabsValue == 2 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Top Turnover </Text></TouchableOpacity></View>
                            <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                            <View style={this.state.tabsValue == 3 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.weekhigh()}><Text style={this.state.tabsValue == 3 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> 52 Week High </Text></TouchableOpacity></View>
                            <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                            <View style={this.state.tabsValue == 4 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.weeklow()}><Text style={this.state.tabsValue == 4 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> 52 Week Low </Text></TouchableOpacity></View>
                            <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        </ScrollView>

                    </View>





                    <ViewPager
                        style={{ height: height - 255 }}
                        initialPage={this.state.tabsValue}
                        ref={viewPager => { this.viewPager = viewPager; }}
                        onPageScroll={(i) => this.pgscroll(i.position)}
                    >
                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>

                            <View style={this.state.gainerloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>

                            <View style={this.state.nodatagainer == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>

                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Gainer}
                                style={styles.flatlistHeight}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500} >
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow1}>
                                                <Col style={styles.flatcol3}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.ScripName}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                        <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text numberOfLines={1} style={{ alignSelf: 'flex-end', marginTop: 8, fontFamily: 'Arial', fontWeight: 'bold', marginRight: 8, color: '#19cf3e' }}>{item.LTradeRt}</Text><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>+{item.ChangeVal} +{item.ChangePer}%</Text></Col>

                                                    </Row>
                                                </Col>

                                                <Col style={styles.flatcol2}></Col>

                                            </Row>
                                        </Ripple>

                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />
                            <Text style={{ marginTop: -15, }}></Text>


                        </View>

                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <View style={this.state.loserloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>
                            <View style={this.state.nodataloser == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Loser}
                                style={styles.flatlistHeight}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500} style={item.id == '' ? styles.hide : styles.show}>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow1}>
                                                <Col style={styles.flatcol3}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' ? styles.listlightcolloser1 : styles.listdarkcolloser1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.ScripName}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                        <Col style={this.props.theme == '0' ? styles.listlightcolloser2 : styles.listdarkcolloser2}><Text numberOfLines={1} style={{ alignSelf: 'flex-end', marginTop: 8, fontFamily: 'Arial', fontWeight: 'bold', marginRight: 8, color: '#f54845' }}>{item.LTradeRt}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{item.ChangeVal} {item.ChangePer}%</Text></Col>

                                                    </Row>
                                                </Col>

                                                <Col style={styles.flatcol2}></Col>

                                            </Row>
                                        </Ripple>

                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />
                            <Text style={{ marginTop: -15, }}></Text>

                        </View>

                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>


                            <Row style={styles.scrollrow}>
                                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('security')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('ltp')}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('to')}( <IconFonsito name="rupee" style={{}} color={this.props.theme == '0' ? 'black' : 'white'} size={13}></IconFonsito> Cr. )</Text></Col>
                            </Row>

                            <View style={this.state.turnoverloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>
                            <View style={this.state.nodataturn == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Turnover}
                                style={styles.flatlistHeight2}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500} >
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow1}>
                                                <Col style={styles.flatcol3}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' && item.ChangePer > 0 ? styles.listlightcol1 : this.props.theme == '0' && item.ChangePer < 0 ? styles.listlightcolloser1 : this.props.theme == '1' && item.ChangePer > 0 ? styles.listdarkcol1 : this.props.theme == '1' && item.ChangePer < 0 ? styles.listdarkcolloser1 : ''}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.ScripName}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                        <Col style={this.props.theme == '0' && item.ChangePer > 0 ? styles.listlightcol2 : this.props.theme == '0' && item.ChangePer < 0 ? styles.listlightcolloser2 : this.props.theme == '1' && item.ChangePer > 0 ? styles.listdarkcol2 : this.props.theme == '1' && item.ChangePer < 0 ? styles.listdarkcolloser2 : ''}><Text style={item.ChangePer > 0 ? styles.pos : styles.neg} numberOfLines={1} >{item.LTradeRt}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.turnltplightval : styles.turnltpdarkval}>{item.TradeVal}</Text></Col>

                                                    </Row>
                                                </Col>
                                                <Col style={styles.flatcol2}></Col>

                                            </Row>
                                        </Ripple>
                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />
                            <Text style={{ marginTop: -15, }}></Text>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <Row style={styles.scrollrow}>
                                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('security')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('high')}</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('ltp')}</Text></Col>
                            </Row>
                            <View style={this.state.highloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>
                            <View style={this.state.nodatahigh == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.High}
                                style={styles.flatlistHeight2}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500} >
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow1}>
                                                <Col style={styles.flatcol3}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' ? styles.highlightcol1 : styles.highdarkcol1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.ScripName}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                        <Col style={this.props.theme == '0' ? styles.highlightcol2 : styles.highdarkcol2}><Text style={styles.pos}>{item.curr52Val} </Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.perturnlight : styles.perturndark}>{item.Ltp}</Text></Col>

                                                    </Row>
                                                </Col>
                                                <Col style={styles.flatcol2}></Col>

                                            </Row>
                                        </Ripple>
                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />
                            <Text style={{ marginTop: -15, }}></Text>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <Row style={styles.scrollrow}>
                                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('security')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('low')}</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('ltp')}</Text></Col>
                            </Row>
                            <View style={this.state.lowloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={{ marginTop: 60 }} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>
                            <View style={this.state.nodatalow == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Low}
                                style={styles.flatlistHeight2}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500} >
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow1}>
                                                <Col style={{ width: '100%', height: 58, }}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' ? styles.lightcol1 : styles.darkcol1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.ScripName}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                        <Col style={this.props.theme == '0' ? styles.lightcol2 : styles.darkcol2}><Text style={styles.neg}>{item.curr52Val} </Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.perturnlight : styles.perturndark}>{item.Ltp}</Text></Col>

                                                    </Row>
                                                </Col>

                                            </Row>
                                        </Ripple>
                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />

                            <Text style={{ marginTop: -15, }}></Text>
                        </View>


                    </ViewPager>

                    <Modal
                        animationType="slide"
                        transparent={false}
                        style={styles.modalheight}
                        visible={this.state.sidemenu}
                        onRequestClose={() => {
                            this.setState({ sidemenu: false });
                        }}
                    >
                        <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
                            <Row style={styles.menurow}>
                                <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                                <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                                <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                            </Row>

                            <ScrollView style={styles.scrollview}>

                                <Text></Text>
                                <View style={{}}>
                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                                        <Col style={styles.popovercol7}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.equity == false ? styles.hide : styles.show}>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.sensex == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                                        <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                                        <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                                        <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.sme == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                                        <Text></Text>
                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.derivative == false ? styles.hide : styles.show}>

                                        <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.currency == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.commodity == false ? styles.hide : styles.show}>

                                        <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.ird == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.etf == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.debt == false ? styles.hide : styles.show}>

                                        <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.corporate == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                                        <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                                        <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                                        <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                                        <Col style={styles.menucol4}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.ipf == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                                        <Text></Text>

                                    </View>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                                    <Text></Text>
                                    <Text></Text>
                                    <Text></Text>
                                    <Text></Text>
                                    {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
                                </View>
                            </ScrollView>
                        </View>
                    </Modal>

                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.settingshow}
                        style={styles.modalheight}
                        onRequestClose={() => {
                            this.setState({ settingshow: false });
                            this.setState({ sidemenu: false });

                        }}
                    >
                        <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

                            <ScrollView style={styles.scrollview1}>
                                <Row style={styles.settingrow}>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                                    <Col style={styles.popovercol8}>
                                        <Picker
                                            selectedValue={this.state.selectlan}
                                            style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                                            onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                                        >
                                            <Picker.Item label="English" value="English" />
                                            <Picker.Item label="Hindi" value="Hindi" />
                                            <Picker.Item label="Marathi" value="Marathi" />
                                            <Picker.Item label="Gujrati" value="Gujrati" />
                                            <Picker.Item label="Bengali" value="Bengali" />
                                            <Picker.Item label="Malayalam" value="Malayalam" />
                                            <Picker.Item label="Oriya" value="Oriya" />
                                            <Picker.Item label="Tamil" value="Tamil" />
                                        </Picker>
                                        <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                                    </Col>
                                </Row>
                                <Text></Text>
                                <Row >
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                                    <Col style={styles.popovercol8}>
                                        <Picker
                                            selectedValue={this.props.themename}
                                            style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                                            onValueChange={(itemValue) => this.changeTheme(itemValue)}
                                        // onValueChange={(itemValue) => this.props.themechng()}
                                        >
                                            <Picker.Item label="Dark" value="dark" />
                                            <Picker.Item label="Light" value="light" />

                                        </Picker>
                                        <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                                    </Col>
                                </Row>

                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
                                </Row>
                            </ScrollView>
                        </View>
                    </Modal>


                    <Footer>

                        <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>

                            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                            </Button>


                            <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                            </Button>

                            <Button style={styles.active} onPress={() => this.tab('home')}>
                                <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
                                <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                            </Button>
                            <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                            </Button>
                            <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                            </Button>


                        </FooterTab>
                    </Footer>
                </Container>
            </StyleProvider>
        );
    }
}

function mapStateToProps(state) {
    return {
        theme: state.theme,
        themename: state.themename
    }
}

function mapDispatchToProps(dispatch) {
    return {
        themechnglight: () => dispatch({ type: 'light' }),
        themechngdark: () => dispatch({ type: 'dark' }),
    }
}


const styles = StyleSheet.create({
    viewDark: { backgroundColor: '#0b0b0b', height: 200 },
    viewLight: { backgroundColor: '#f6f7f9', height: 200 },
    darkgainertabs: { color: 'white', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold' },
    lightgainertabs: { color: '#132144', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold' },
    headerDark: { backgroundColor: '#0b0b0b', height: 1 },
    headerLight: { backgroundColor: '#f6f7f9', height: 1 },
    tabdarkmain: { backgroundColor: 'red', },
    tablightmain: { backgroundColor: '#f6f7f9', },
    lightdate: { color: '#132144', fontSize: 14, textAlign: 'right' },
    darkdate: { color: 'white', fontSize: 14, textAlign: 'right' },
    darkview: { backgroundColor: '#0b0b0b', height: '100%' },
    lightview: { backgroundColor: '#f6f7f9', height: '100%' },
    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
    inactive: { padding: 10, },
    titlelight: { color: '#132144', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 15 },
    titledark: { color: 'white', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 15 },
    pos: { alignSelf: 'flex-end', marginTop: 8, fontFamily: 'Arial', fontWeight: 'bold', marginRight: 8, color: '#19cf3e' },
    neg: { alignSelf: 'flex-end', marginTop: 8, fontFamily: 'Arial', fontWeight: 'bold', marginRight: 8, color: '#f54845' },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold', },
    tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold', },
    tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold', },
    statuslight: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: '#132144' },
    statusdark: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: 'white' },
    listlightcol1: { width: '50%', height: 58, backgroundColor: 'white', borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcol2: { width: '50%', height: 58, backgroundColor: 'white', borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcol1: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcol2: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, borderBottomRightRadius: 5, borderTopRightRadius: 5 },

    lightcol1: { width: '50%', height: 58, borderBottomColor: '#f54845', borderBottomWidth: 1.5, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    highlightcol1: { width: '50%', height: 58, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    lightcol2: { width: '50%', height: 58, borderBottomColor: '#f54845', borderBottomWidth: 1.5, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    highlightcol2: { width: '50%', height: 58, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    darkcol1: { width: '50%', height: 58, borderBottomColor: '#f54845', borderBottomWidth: 1.5, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    highdarkcol1: { width: '50%', height: 58, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    darkcol2: { width: '50%', height: 58, borderBottomColor: '#f54845', borderBottomWidth: 1.5, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    highdarkcol2: { width: '50%', height: 58, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },

    listlightcolloser1: { width: '50%', height: 58, backgroundColor: 'white', borderBottomColor: '#f54845', borderBottomWidth: 1.5, borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcolloser2: { width: '50%', height: 58, backgroundColor: 'white', borderBottomColor: '#f54845', borderBottomWidth: 1.5, borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolloser1: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#f54845', borderBottomWidth: 1.5, borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcolloser2: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#f54845', borderBottomWidth: 1.5, borderBottomRightRadius: 5, borderTopRightRadius: 5 },

    show: { display: 'flex' },
    hide: { display: 'none' },
    showNoData: { display: 'flex', marginTop: 70, },
    lighttext1: { marginLeft: 5, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darktext1: { marginLeft: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    perlight: { alignSelf: 'flex-end', marginRight: 8, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13 },
    perdark: { alignSelf: 'flex-end', marginRight: 8, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13 },
    perturnlight: { alignSelf: 'flex-end', marginRight: 12, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13, },
    perturndark: { alignSelf: 'flex-end', marginRight: 12, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13, },
    turnltpdarkval: { alignSelf: 'flex-end', marginRight: 8, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13, },
    turnltplightval: { alignSelf: 'flex-end', marginRight: 8, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13, },
    ltpviewpos: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    ltpviewneg: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#f54845', fontFamily: 'Arial', fontWeight: 'bold', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    lightheader1: { width: '32%', height: 40 },
    darkheader1: { width: '32%', height: 40, },
    lightheader2: { width: '30%', height: 40 },
    darkheader2: { width: '30%', height: 40, },
    lightheader3: { width: '33%', height: 40 },
    darkheader3: { width: '33%', height: 40, },
    lightheader4: { width: '50%', height: 40 },
    darkheader4: { width: '50%', height: 40 },
    lightheadertext: { fontSize: 15, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkheadertext: { fontSize: 15, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkheadertext1: { alignSelf: 'flex-end', color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    turnltplight: { alignSelf: 'center', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    turnltpdark: { alignSelf: 'center', color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    lighttext: { marginLeft: 7, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darktext: { marginLeft: 7, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    titlelight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    titledark: { marginLeft: 16, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    dropicon: { marginLeft: 88, bottom: 30 },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center' },
    nodatadark: { color: 'white', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center' },
    lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    footer1: { width: 26, height: 28, },
    footer2: { width: 26, height: 27, },
    footer3: { width: 23, height: 24, bottom: 2 },
    modalheight: { height: '100%' },
    scrollview1: { bottom: 30 },
    settingrow: { marginTop: '25%' },
    popovercol1: { width: '20%', height: 50, },
    popovercol2: { width: '80%', height: 50, },
    popovercol3: { width: '40%', height: 50, },
    popovercol4: { width: '5%', height: 50, },
    popovercol5: { width: '35%', height: 50, },
    popovercol6: { width: '100%', height: 40, },
    popovercol7: { width: '50%', height: 40, },
    popovercol8: { width: '30%', height: 50, },
    pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
    pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },
    popovericon1: { textAlign: 'center' },
    popovericon2: { textAlign: 'right' },
    popovericon3: { textAlign: 'left' },
    menucol1: { width: '67%', height: 40 },
    menucol2: { width: '15%', height: 40 },
    menucol3: { width: '75%', height: 40 },
    menucol4: { width: '25%', height: 40 },
    closeicon: { marginLeft: 10 },
    usericon: { marginTop: 3, textAlign: 'right' },
    settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
    scrollview: { top: '8%' },
    closeicon: { marginLeft: 10 },
    usericon: { marginTop: 3, textAlign: 'right' },
    settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
    scrollrow: { marginLeft: 16, marginRight: 16 },
    scrollrow1: { marginLeft: 17, marginRight: 17, marginTop: 12, },
    menurow: { marginLeft: 20, marginTop: 10 },
    menuicon: { marginTop: 6 },
    viewrow: { marginLeft: 3, },
    loader: { marginTop: 60 },
    flatlistHeight: { marginTop: 10, height: '100%' },
    flatlistHeight2: { marginTop: 40, height: '100%' },
    flatcol1: { width: '92%', height: 58, },
    flatcol2: { width: '6.5%', height: 58 },
    flatcol3: { width: '100%', height: 58 },
    mainrow1: { margin: 5 },
    mainrow2: { margin: 15, marginTop: '15%' },
    mainrow3: { marginTop: '8%' },
    maincol1: { width: '15%', height: 30 },
    maincol2: { width: '55%', height: 30, },
    maincol3: { width: '20%', height: 30, },
    maincol4: { width: '10%', height: 30, },
    maincol5: { width: '100%', height: 40, },
    google: { width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 },
    back: { textAlign: 'left', marginLeft: 13 },
    bannercol1: { width: '6%', height: 53, },
    bannercol2: { width: '80%', height: 53, },
    buttonlight: { height: 40, marginLeft: 4, backgroundColor: '#f6f7f9' },
    buttondark: { height: 40, marginLeft: 4, backgroundColor: '#0b0b0b' },
    view: { marginLeft: 15 },
    btnViewActive: { borderRadius: 5, backgroundColor: '#2087c9', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
    btnViewinactive: { borderRadius: 5, backgroundColor: 'white', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
    btnViewinactiveLast: { borderRadius: 5, backgroundColor: 'white', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
    btnViewinactivedark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
    btnViewinactiveLastdark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
    btnViewactiveLast: { borderRadius: 5, backgroundColor: '#2087c9', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
    textActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    textinActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
    textinActiveDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    textActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    textinActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
    textinActivelasDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    gaplight: { backgroundColor: '#f6f7f9', width: 10, height: 70, bottom: 10 },
    gapdark: { backgroundColor: '#0b0b0b', width: 10, height: 70, bottom: 10 },



})

// export default Equity;
export default connect(mapStateToProps, mapDispatchToProps)(NewEquity);