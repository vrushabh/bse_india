import React, { Component } from 'react';
import { View, ToastAndroid, Image, Modal, CheckBox, Linking, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Tab, Tabs, Container, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Iconoct from 'react-native-vector-icons/Octicons';
import * as Animatable from 'react-native-animatable';
import Ripple from 'react-native-material-ripple';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { connect } from 'react-redux';
import api from '../api.js';

import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
const height = Dimensions.get('window').height;

class DebtTabs extends Component {
  static navigationOptions = { header: null };
  per = '';
  per1 = '';
  per2 = '';
  per3 = '';
  indicesId = '16';
  Sensex = [];
  TurnArray = [];
  turnlength = '';
  tabval = 0;
  themename = '';
  summary = [];
  lose = [];
  turn = [];
  high = [];
  low = [];
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    this.state = {
      date: '',
      status: '',
      value: 1,
      summaryloader: false,
      corpbondingloader: false,
      retaildebtloader: false,
      gsecloader: false,
      retailgovloader: false,
      corpndsloader: false,
      ebploader: false,
      detailLoader: false,
      isVisible: false,
      isVisiblePop: false,
      switchval: false,
      Summary: [],
      Corpbond: [],
      retailcorp: [],
      gsec: [],
      retailgov: [],
      corpnds: [],
      ebp: [],
      detaildata: [],

      refreshing: false,
      sidemenu: false,
      settingshow: false,
      theme: 'dark',
      showedit: false,
      iconname: 'ios-arrow-down',
      iconnamesensex: 'ios-arrow-forward',
      iconnamesme: 'ios-arrow-forward',
      iconnamederivative: 'ios-arrow-forward',
      iconnamecurrency: 'ios-arrow-forward',
      iconnamecommodity: 'ios-arrow-forward',
      iconnameird: 'ios-arrow-forward',
      iconnameetf: 'ios-arrow-forward',
      iconnamedebt: 'ios-arrow-forward',
      iconnamecorporate: 'ios-arrow-forward',
      iconnameipf: 'ios-arrow-forward',
      ipf: false,
      selectlan: 'English',
      indicesdata: [],
      indicesName: 'S&P BSE SENSEX',

      ltp: '',
      change: '',
      changeper: '',
      indicesDropdown: false,
      tabsValue: 0,
      initial: '',
      equity: true,
      sensex: false,
      sme: false,
      derivative: false,
      currencytab: false,
      commoditytab: false,
      ird: false,
      etf: false,
      debt: false,
      corporate: false,
      detailModal: false,
      issue: '',
      isn: '', securityname: '', ltp: '', wap: '', way: '',
      coupon: '', maturity: '', rating: '', turnover: '',
      facevalue: '', couponfreq: '', lastinterest: '', securitycode: '',
      yield: '', nextinterest: '',
      detailname: '', traded: '',
      marketdata: ''
    };
    setTimeout(() => { SplashScreen.hide() }, 4000);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

  }
  componentDidMount() {
    AsyncStorage.getItem('themecolor').then((dt) => {
      dt == null || dt == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

    });

    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
        "Application requires Network to proceed",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    });

    this.sensexfun();
    this.summaryfun();
    this.corpbondfun();
    this.retailcorpfun();
    this.gsecfun();
    this.retailgovfun();
    this.corpbondndsfun();
    this.ebpfun();

  }

  tab(val) {

    if (val == 'watch') { }
    else if (val == 'port') { }
    else if (val == 'home') {
      this.props.navigation.navigate('maindark');

    }
    else if (val == 'search') {
      AsyncStorage.getItem('themecolor').then((dt) => {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('search', { 'theme': dt });
      });
    }
    else if (val == 'more') {
      this.setState({ sidemenu: true })
    }

  }
  componentWillMount() {
    this.tabval = this.props.navigation.getParam('current', '');

    this.setState({ tabsValue: this.tabval });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;

  }
  back() {
    this.props.navigation.goBack(null);
    return true;
  }
  sensexfun() {
    api.date().then(responsejson => {
      responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';

    });

  }
  summaryfun() {
    this.setState({ summaryloader: true, Summary: '' });

    api.debtSummary().then(res => {
      if (res != '') {
        this.setState({ Summary: res, refreshing: false, summaryloader: false })
      }
      else {
        this.setState({ Summary: '', refreshing: false, summaryloader: false })
      }
    });

  }
  corpbondfun() {
    this.setState({ corpbondingloader: true, Corpbond: '' });

    api.debtCorpbond().then(res => {
      if (res != '') {
        this.setState({ Corpbond: res, refreshing: false, corpbondingloader: false })
      }
      else {
        this.setState({ Corpbond: '', refreshing: false, corpbondingloader: false })
      }
    });

  }

  retailcorpfun() {
    this.setState({ retaildebtloader: true, retailcorp: '' });
    api.debtReailcorp().then(res => {
      if (res != '') {
        this.setState({ retailcorp: res, refreshing: false, retaildebtloader: false })
      }
      else {
        this.setState({ retailcorp: '', refreshing: false, retaildebtloader: false })
      }
    });
  }

  gsecfun() {
    this.setState({ gsecloader: true, gsec: '' });
    api.debtgsec().then(res => {
      if (res != '') {
        this.setState({ gsec: res, refreshing: false, gsecloader: false })
      }
      else {
        this.setState({ gsec: '', refreshing: false, gsecloader: false })
      }
    });

  }
  retailgovfun() {
    this.setState({ retailgovloader: true, retailgov: '' });
    api.debtRetailgov().then(res => {
      if (res != '') {
        this.setState({ retailgov: res, refreshing: false, retailgovloader: false })
      }
      else {
        this.setState({ retailgov: '', refreshing: false, retailgovloader: false })
      }
    });
  }

  corpbondndsfun() {
    this.setState({ corpndsloader: true, corpnds: '' });
    api.debtCorpbondnds().then(res => {
      if (res != '') {
        this.setState({ corpnds: res, refreshing: false, corpndsloader: false })
      }
      else {
        this.setState({ corpnds: '', refreshing: false, corpndsloader: false })
      }
    });
  }

  ebpfun() {
    this.setState({ ebploader: true, ebp: '' });
    api.debtebp().then(res => {
      if (res != '') {
        this.setState({ ebp: res, refreshing: false, ebploader: false })
      }
      else {
        this.setState({ ebp: '', refreshing: false, ebploader: false })
      }
    });
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.refresh();
  }
  refresh() {
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
        "Application requires Network to proceed",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    });
    this.sensexfun();
    this.summaryfun();
    this.corpbondfun();
    this.retailcorpfun();
    this.gsecfun();
    this.retailgovfun();
    this.corpbondndsfun();
    this.ebpfun();

  }
  equityView() {
    this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });
  }
  sensexView() {
    this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
  }
  smeView() {
    this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
  }
  derivativeView() {
    this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
  }
  currencyView() {
    this.state.currencytab == false ? this.setState({ currencytab: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currencytab: false, iconnamecurrency: 'ios-arrow-forward' });
  }
  commadityView() {
    this.state.commoditytab == false ? this.setState({ commoditytab: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commoditytab: false, iconnamecommodity: 'ios-arrow-forward' });
  }
  irdView() {
    this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
  }
  etfView() {
    this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
  }
  debtView() {
    this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
  }
  corporateView() {
    this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
  }
  ipfView() {
    this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

  }
  googleassistant() {
    Linking.openURL('https://assistant.google.com/explore');

  }

  closesidemenu() {
    this.setState({ sidemenu: false });
  }

  indices() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('indices');
  }

  home() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('maindark');

  }
  closePopover() {
    this.setState({ isVisible: false, isVisiblePop: false });
  }

  detailfun(data,namee) {
    this.setState({ marketdata: data,name:namee, isVisible: true });
  }

  detailFun(data1, data2, data3,name,tenor,rate) {
    this.setState({ marketdata1: data1, marketdata2: data2, marketdata3: data3,name:name,tenor:tenor,rate:rate,isVisiblePop: true });

  }
  settingpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('setting', { 'current': val });

  }
  equitypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('equity', { 'current': val });

  }
  derivativepage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('derivativetab', { 'current': val });

  }
  currencypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('currencytab', { 'current': val });

  }
  commoditypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('commoditytab', { 'current': val });

  }
  irdpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ird', { 'current': val });

  }
  smepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('sme', { 'current': val });
  }
  etfpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('etf', { 'current': val });
  }
  corporatepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('corporate', { 'current': val });
  }
  marketstatic() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketstatic');
  }

  marketturn() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketturn');
  }
  listing() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('listing');
  }
  ipo() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipo');
  }
  watchlist() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('watch');
  }
  ipfpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipf', { 'current': val });
  }
  notices() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('notice');
  }
  chng(per1, per2) {

    if (per1 > 0) {
      return '+' + per1 + ' ' + '+' + per2 + ' %';
    }
    else if (per1 < 0) {
      return per1 + ' ' + per2 + ' %';
    }
    else if (per1 == '0.00') {
      return per1 + ' ' + per2 + ' %';
    }
  }

  detail(id, name, issuename, oeid) {
    this.setState({ detailModal: true, detailLoader: true, detailname: name, issue: issuename });
    api.debtdetail(name, id, oeid, issuename).then(res => {
      if (res != '') {
        if (name == 'corp') {
          this.setState({ isn: res[0].ISSebiIsin, securityname: res[0].OESymbol, ltp: res[0].CMBPLastTradePrice, wap: res[0].CMBPLastTradePrice, way: res[0].CMBPAvgYield, coupon: res[0].OEIssueCouponRate, maturity: res[0].OEIssueMaturityDate, rating: res[0].ISCreditRating, turnover: res[0].CMBPLastTradeValue, facevalue: res[0].ISFaceValue, couponfreq: res[0].CouponFrequency, lastinterest: res[0].LastInterestPaymentDate, detailLoader: false });

        }
        else if (name == 'gsec') {
          this.setState({ isn: res[0].ISSebiIsin, securitycode: res[0].ISSecurityName, ltp: res[0].CMBPLastTradePrice, wap: res[0].CMBPAvgTradePrice, yield: res[0].Yield, coupon: res[0].ISCouponRate, maturity: res[0].ISMaturityDate, rating: res[0].ISCreditRating, turnover: res[0].CMBPTotTradeValue, facevalue: res[0].ISFaceValue, nextinterest: res[0].lastinterestpaymentdate, detailLoader: false });

        }
        else {
          this.setState({ isn: res.ISSebiIsin, securitycode: res.ISSecurityName, ltp: res._ScriptTradeData[0].CMBPLastTradePrice, wap: res._ScriptTradeData[0].CMBPAvgTradePrice, way: res._ScriptTradeData[0].CMBPAvgYield, coupon: res.ISCouponRate, maturity: res.ISMaturityDate, rating: res.ISCreditRating, turnover: res._ScriptTradeData[0].CMBPTotTradeValue, traded: res._ScriptTradeData[0].CMBPlastTradeQuantity, facevalue: res.ISFaceValue, couponfreq: res._ScriptTradeData[0].Coupon_Frequency, lastinterest: res._ScriptTradeData[0].Last_InterestPay_Date, detailLoader: false });

        }

      }
      else {
        this.setState({ ebploader: false })
      }
    });
    // if (name == 'corp') {
    //   fetch('https://api.bseindia.com/BseIndiaAPI/api/CorpBondScriptWiseTradeReport/w?Scrip_ID=' + id + '&OEID=&Flag=1&RCount=1&ln=en').then((response) => response.json()).then((responsejson) => {

    //     this.setState({ isn: responsejson._objTradeDetails[0].ISSebiIsin, securityname: responsejson._objTradeDetails[0].OESymbol, ltp: responsejson._objTradeDetails[0].CMBPLastTradePrice, wap: responsejson._objTradeDetails[0].CMBPLastTradePrice, way: responsejson._objTradeDetails[0].CMBPAvgYield, coupon: responsejson._objTradeDetails[0].OEIssueCouponRate, maturity: responsejson._objTradeDetails[0].OEIssueMaturityDate, rating: responsejson._objTradeDetails[0].ISCreditRating, turnover: responsejson._objTradeDetails[0].CMBPLastTradeValue, facevalue: responsejson._objTradeDetails[0].ISFaceValue, couponfreq: responsejson._objTradeDetails[0].CouponFrequency, lastinterest: responsejson._objTradeDetails[0].LastInterestPaymentDate, detailLoader: false });
    //   });
    // }
    // else if (name == 'gsec') {
    //   fetch('https://api.bseindia.com/BseIndiaAPI/api/GSecTradeReport/w?scripcode=' + id + '&RCount=1&ln=en').then((response) => response.json()).then((responsejson) => {

    //     this.setState({ isn: responsejson[0].ISSebiIsin, securitycode: responsejson[0].ISSecurityName, ltp: responsejson[0].CMBPLastTradePrice, wap: responsejson[0].CMBPAvgTradePrice, yield: responsejson[0].Yield, coupon: responsejson[0].ISCouponRate, maturity: responsejson[0].ISMaturityDate, rating: responsejson[0].ISCreditRating, turnover: responsejson[0].CMBPTotTradeValue, facevalue: responsejson[0].ISFaceValue, nextinterest: responsejson[0].lastinterestpaymentdate, detailLoader: false });
    //   });
    // }
    // else {
    //   fetch('https://api.bseindia.com/BseIndiaAPI/api/ScriptTradeReportNDS-RST/w?Scrip_ID=' + id + '&isin_no=' + issuename + '&cmpbid=' + oeid + '&RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {

    //     this.setState({ isn: responsejson.ISSebiIsin, securitycode: responsejson.ISSecurityName, ltp: responsejson._ScriptTradeData[0].CMBPLastTradePrice, wap: responsejson._ScriptTradeData[0].CMBPAvgTradePrice, way: responsejson._ScriptTradeData[0].CMBPAvgYield, coupon: responsejson.ISCouponRate, maturity: responsejson.ISMaturityDate, rating: responsejson.ISCreditRating, turnover: responsejson._ScriptTradeData[0].CMBPTotTradeValue, traded: responsejson._ScriptTradeData[0].CMBPlastTradeQuantity, facevalue: responsejson.ISFaceValue, couponfreq: responsejson._ScriptTradeData[0].Coupon_Frequency, lastinterest: responsejson._ScriptTradeData[0].Last_InterestPay_Date, detailLoader: false });
    //   });
    // }

  }
  closedetail() {
    this.setState({ detailModal: false });
  }
  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
  }

  changeTheme(thm) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem("themecolor", thm);
    this.props.navigation.navigate('maindark');
    this.props.navigation.goBack(null);
    thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

  }
  chnglanguage(lan) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem('language', lan);
    this.props.navigation.navigate('maindark');

  }



  render() {
    return (
      <StyleProvider style={getTheme(material)}>

        <Container>


          <Header hasTabs androidStatusBarColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} style={this.props.theme == '0' ? styles.headerLight : styles.headerDark} >

          </Header>

          <View style={this.props.theme == '0' ? styles.viewLight : styles.viewDark}>
            <Row style={styles.mainrow1}>
              <Col style={styles.maincol1}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={styles.back} size={27} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
              <Col style={styles.maincol2}>
                <Text style={this.state.tabsValue == 0 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 0 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Debt Market Summary</Text>
                <Text style={this.state.tabsValue == 1 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 1 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Corporate Bonds-OTC Trades</Text>
                <Text style={this.state.tabsValue == 2 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 2 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Retail Corp.Debt</Text>
                <Text style={this.state.tabsValue == 3 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 3 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>GSEC</Text>
                <Text style={this.state.tabsValue == 4 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 4 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Retail Gov Bonds</Text>
                <Text style={this.state.tabsValue == 5 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 5 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Corporate Bonds-NDS-RST</Text>
                <Text style={this.state.tabsValue == 6 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 6 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>EBP</Text>
              </Col>
              <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.google} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
              <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={styles.popovericon1} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
            </Row>
            <Row style={styles.mainrow2}>

              <Col style={styles.maincol5}><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.props.theme == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


            </Row>

          </View>



          <ViewPager
            style={{ height: height - 135 }}
            initialPage={this.tabval}
            onPageScroll={(i) => this.setState({ tabsValue: i.position })}
          >
            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Segment</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Turnover</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>(Rs.Lacs)</Text></Col>
              </Row>
              <View style={this.state.summaryloader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.Summary}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500} style={item.id == '' ? styles.hide : styles.show}>
                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} onPress={() => this.detailfun(item.Segment_Name,'Segment name')}>
                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row style={item.Segment_Name == 'Total' ? styles.hide : styles.show} >
                            <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>{item.Segment_Name}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{item.Turnover}</Text></Col>

                          </Row>
                          <Row style={item.Segment_Name == 'Total' ? styles.show : styles.hide} >
                            <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.totallight : styles.totaldark} numberOfLines={1}>{item.Segment_Name}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlighttotal : styles.perdarktotal}>{item.Turnover}</Text></Col>

                          </Row>
                        </Col>

                      </Row>
                    </Ripple>
                    <Text></Text>

                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />


            </View>

            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Issuer Name</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security Name</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTY</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Coupon(%)</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O (Rs.Lakhs)</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Maturity</Text></Col>
              </Row>
              <View style={this.state.corpbondingloader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.Corpbond}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500} style={item.Scripname == '' ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.detail(item.scrip_id, 'corp', item.Scripname, '')} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.Scripname}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.scrip_id}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue} numberOfLines={1}>{item.wtd_avg_Yield}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn}>{item.coupon}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.CMBPTotTradeValue}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.maturity_date}</Text></Col>

                          </Row>
                        </Col>

                      </Row>
                    </Ripple>
                    <Text></Text>

                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />
            </View>

            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Issuer Name</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security Name</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTY</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Coupon(%)</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O (Rs.Lakhs)</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Maturity</Text></Col>
              </Row>

              <View style={this.state.retaildebtloader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.retailcorp}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500}>
                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} onPress={() => this.detailfun(item.LONGNAME,'Issuer name')}>
                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.LONGNAME}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.ScripName}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue} numberOfLines={1}>{item.YTM}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn}>{item.ISCouponRate}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.TURNOVER}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.ISMaturityDate}</Text></Col>

                          </Row>
                        </Col>

                      </Row>
                    </Ripple>
                    <Text></Text>
                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />
            </View>

            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security Desc.</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security Code</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTY</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Coupon(%)</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O (Rs.Lakhs)</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Maturity</Text></Col>
              </Row>
              <View style={this.state.gsecloader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.gsec}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500} style={item.ISDescription == 'Total' ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.detail(item.ISBSEScriptCode, 'gsec', item.ISDescription, '')} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.ISDescription}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.ISSecurityName}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue} numberOfLines={1}>{item.CMBPAvgYield}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn}>{item.ISCouponRate}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.CMBPTotTradeValue}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.ISMaturityDate}</Text></Col>

                          </Row>
                        </Col>
                        <Col style={styles.flatcol2}></Col>

                      </Row>
                    </Ripple>
                    <Text></Text>
                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />
            </View>

            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security Name</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security ID</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Change(%)</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O (Rs.Lakhs)</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}></Text></Col>
              </Row>
              <View style={this.state.retailgovloader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.retailgov}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500} style={item.issuername == '' ? styles.hide : styles.show}>
                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} onPress={() => this.detailfun(item.issuername,'Security name')}>

                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov1 : styles.listdarkcolsensexgov1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.issuername}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.scrip_id}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov3 : styles.listdarkcolsensexgov3}><Text style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue} numberOfLines={1}>{item.close_rate}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn}>{this.chng(item.change_val, item.change_percent)}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov2 : styles.listdarkcolsensexgov2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.trd_val}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}></Text></Col>

                          </Row>
                        </Col>

                      </Row>
                    </Ripple>
                    <Text></Text>
                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />
            </View>

            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security Code</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Issuer Name</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>WAY</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Coupon %</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O (Rs.Lakhs)</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Maturity</Text></Col>
              </Row>
              <View style={this.state.corpndsloader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.corpnds}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500} style={item.Scripname == '' ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.detail(item.scrip_id, 'corpnds', item.Scripname, item.OEID)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov1 : styles.listdarkcolsensexgov1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1} >{item.scrip_id}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.Scripname}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov3 : styles.listdarkcolsensexgov3}><Text style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue} numberOfLines={1}>{item.wtd_avg_Yield}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn}>{item.coupon}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov2 : styles.listdarkcolsensexgov2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.CMBPTotTradeValue}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.maturity_date}</Text></Col>

                          </Row>
                        </Col>

                      </Row>
                    </Ripple>
                    <Text></Text>
                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />
            </View>

            <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
              <Row style={styles.scrollrow}>
                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Issuer Name</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Date of Issue</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Coupon %</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Tenor</Text></Col>
                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Amount Raised</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Rating</Text></Col>
              </Row>
              <View style={this.state.ebploader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

              </View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
                data={this.state.ebp}
                style={styles.flatlistHeight}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                  <Animatable.View animation="zoomIn" delay={500}>
                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} onPress={() => this.detailFun(item.COMPANY_NAME, item.TENOR, item.CREDIT_RATING,'Issuer name','Tenor','Rating')}>

                      <Row style={styles.scrollrow}>
                        <Col style={styles.flatcol1}>
                          <Row>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov1 : styles.listdarkcolsensexgov1}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.COMPANY_NAME}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.ENTRY_DATE}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov3 : styles.listdarkcolsensexgov3}><Text style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue} numberOfLines={1}>{item.COUPON_PRICE}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn}>{item.TENOR}</Text></Col>
                            <Col style={this.props.theme == '0' ? styles.listlightcolsensexgov2 : styles.listdarkcolsensexgov2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.AMOUNT_RAISED}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.CREDIT_RATING}</Text></Col>

                          </Row>
                        </Col>

                      </Row>
                    </Ripple>
                    <Text></Text>
                  </Animatable.View>

                }
                keyExtractor={item => item.id}
              />
            </View>


          </ViewPager>


          <Modal
            animationType="slide"
            transparent={false}
            style={styles.modalheight}
            visible={this.state.sidemenu}
            onRequestClose={() => {
              this.setState({ sidemenu: false });
            }}
          >
            <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
              <Row style={styles.menurow}>
                <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.close()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              </Row>

              <ScrollView style={styles.scrollview}>

                <Text></Text>
                <View style={{}}>
                  <Row style={styles.scrollrow}>
                    <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                    <Col style={styles.popovercol7}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.equity == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.equitypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                    <Ripple onPress={() => this.equitypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                    <Ripple onPress={() => this.equitypage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                    <Ripple onPress={() => this.equitypage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                    <Ripple onPress={() => this.equitypage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.sensex == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                    <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                    <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                    <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.sme == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                    <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                    <Text></Text>
                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.derivative == false ? styles.hide : styles.show}>

                    <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                    <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.currencytab == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                    <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.commoditytab == false ? styles.hide : styles.show}>

                    <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                    <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.ird == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                    <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.etf == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                    <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.debt == false ? styles.hide : styles.show}>

                    <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                    <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                    <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                    <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                    <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                    <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                    <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.corporate == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                    <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                    <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                    <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                    <Text></Text>

                  </View>
                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                    <Col style={styles.menucol4}></Col>
                  </Row>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                  <Row style={styles.scrollrow}>
                    <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                    <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                  </Row>
                  <View style={this.state.ipf == false ? styles.hide : styles.show}>
                    <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                    <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                    <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                    <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                    <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                    <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                    <Text></Text>

                  </View>

                  <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                  <Text></Text>
                  <Text></Text>
                  <Text></Text>
                  <Text></Text>
                  {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
                </View>
              </ScrollView>
            </View>
          </Modal>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.settingshow}
            style={styles.modalheight}
            onRequestClose={() => {
              this.setState({ settingshow: false });
              this.setState({ sidemenu: false });

            }}
          >
            <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

              <ScrollView style={styles.scrollview1}>
                <Row style={styles.settingrow}>
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                  <Col style={styles.popovercol8}>
                    <Picker
                      selectedValue={this.state.selectlan}
                      style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                      onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                    >
                      <Picker.Item label="English" value="English" />
                      <Picker.Item label="Hindi" value="Hindi" />
                      <Picker.Item label="Marathi" value="Marathi" />
                      <Picker.Item label="Gujrati" value="Gujrati" />
                      <Picker.Item label="Bengali" value="Bengali" />
                      <Picker.Item label="Malayalam" value="Malayalam" />
                      <Picker.Item label="Oriya" value="Oriya" />
                      <Picker.Item label="Tamil" value="Tamil" />
                    </Picker>
                    <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropiconsetting} size={22} color={'white'} />

                  </Col>
                </Row>
                <Text></Text>
                <Row >
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                  <Col style={styles.popovercol8}>
                    <Picker
                      selectedValue={this.props.themename}
                      style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                      onValueChange={(itemValue) => this.changeTheme(itemValue)}
                    // onValueChange={(itemValue) => this.props.themechng()}
                    >
                      <Picker.Item label="Dark" value="dark" />
                      <Picker.Item label="Light" value="light" />

                    </Picker>
                    <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropiconsetting} size={22} color={'white'} />

                  </Col>
                </Row>

                <Text></Text>
                <Row>
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
                </Row>
                <Text></Text>
                <Row>
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
                </Row>
                <Text></Text>
                <Row>
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
                </Row>
                <Text></Text>
                <Row>
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
                </Row>
                <Text></Text>
                <Row>
                  <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                  <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
                </Row>
              </ScrollView>
            </View>
          </Modal>


          <Modal
            animationType="slide"
            transparent={false}
            style={styles.modalheight}
            visible={this.state.detailModal}
            onRequestClose={() => {
              this.setState({ detailModal: false });
            }}
          >
            <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
              <Row style={styles.menurow}>
                <Col style={styles.maincol5}><Icon name="ios-close" onPress={() => this.closedetail()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>

              </Row>

              <View style={this.state.detailname == 'corp' ? styles.show : styles.hide}>
                <ScrollView style={styles.scrollmodel}>

                  <View style={this.state.detailLoader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={styles.scrollloader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>

                  <View>
                    <Row style={styles.modelrow1}>
                      <Col style={styles.maincol5}><Text style={this.props.theme == '0' ? styles.lightdetailname : styles.darkdetailname} >{this.state.issue}</Text></Col>

                    </Row>
                  </View>
                  <Text></Text>

                  <View>
                    <Row style={styles.modelrow2} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>ISN</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.isn}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Security Name</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.securityname}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>LTP</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.ltp}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>WAP</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.wap}</Text></Col>

                    </Row>
                  </View>


                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>WAY</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.way}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Coupon %</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.coupon}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Maturity</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.maturity}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Rating</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.rating}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Turnover (Rs. Lacs)</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.turnover}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Face Value (Rs.)</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.facevalue}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Coupon Frequency</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.couponfreq}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Last Interest Date</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.lastinterest}</Text></Col>

                    </Row>
                  </View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>


                </ScrollView>
              </View>

              <View style={this.state.detailname == 'gsec' ? styles.show : styles.hide}>
                <ScrollView style={styles.scrollmodel}>

                  <View style={this.state.detailLoader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={styles.scrollloader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <View>
                    <Row style={styles.modelrow1}>
                      <Col style={styles.maincol5}><Text style={this.props.theme == '0' ? styles.lightdetailname : styles.darkdetailname} >{this.state.issue}</Text></Col>

                    </Row>
                  </View>
                  <Text></Text>


                  <View>
                    <Row style={styles.modelrow2} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>ISN</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.isn}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Security Code</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.securitycode}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>LTP</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.ltp}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>WAP</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.wap}</Text></Col>

                    </Row>
                  </View>


                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Yield</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.yield}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Turnover (Rs. Lacs)</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.turnover}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Coupon %</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.coupon}</Text></Col>

                    </Row>
                  </View>


                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Maturity</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.maturity}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Rating</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.rating}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Face Value (Rs.)</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.facevalue}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Next Interest Date</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.nextinterest}</Text></Col>

                    </Row>
                  </View>

                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>

                </ScrollView>
              </View>

              <View style={this.state.detailname == 'corpnds' ? styles.show : styles.hide}>
                <ScrollView style={styles.scrollmodel}>

                  <View style={this.state.detailLoader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={styles.scrollloader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                  </View>
                  <View>
                    <Row style={styles.modelrow1}>
                      <Col style={styles.maincol5}><Text style={this.props.theme == '0' ? styles.lightdetailname : styles.darkdetailname} >{this.state.issue}</Text></Col>

                    </Row>
                  </View>
                  <Text></Text>


                  <View>
                    <Row style={styles.modelrow4} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>ISN</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.isn}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow5} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Security Code</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.securitycode}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>LTP</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.ltp}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>WAP</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.wap}</Text></Col>

                    </Row>
                  </View>


                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>WAY</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.way}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Coupon %</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.coupon}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Maturity</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.maturity}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Rating</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.rating}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Turnover (Rs. Lacs)</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.turnover}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow3} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Traded Qty</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.traded}</Text></Col>

                    </Row>
                  </View>

                  <View>
                    <Row style={styles.modelrow6} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Face Value (Rs.)</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.facevalue}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow6} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Coupon Frequency</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.couponfreq}</Text></Col>

                    </Row>
                  </View>
                  <View>
                    <Row style={styles.modelrow6} >
                      <Col style={this.props.theme == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.props.theme == '0' ? styles.lightdebtname : styles.darkdebtname} numberOfLines={1}>Last Interest Payment Date</Text></Col>
                      <Col style={this.props.theme == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={this.props.theme == '0' ? styles.perlight : styles.perdark}>{this.state.lastinterest}</Text></Col>

                    </Row>
                  </View>

                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>
                  <View><Text></Text></View>

                </ScrollView>
              </View>

            </View>
          </Modal>

          <Popover
            isVisible={this.state.isVisible}
            fromView={this.touchable}

            onRequestClose={() => this.closePopover()}>
            <View>
              <Icon name="ios-close" onPress={() => this.closePopover()} style={{ marginLeft: 20, }} size={40} color={'#132144'} />

              <Text style={{ margin: 20, fontSize: 17, fontWeight: 'bold', color: '#132144' }}>
                {this.state.name} :-
                            </Text>
              <Text style={{ margin: 15, }}>
                {this.state.marketdata}
              </Text>

            </View>
          </Popover>

          <Popover
            isVisible={this.state.isVisiblePop}
            fromView={this.touchable}

            onRequestClose={() => this.closePopover()}>
            <View>
              <Icon name="ios-close" onPress={() => this.closePopover()} style={{ marginLeft: 20, }} size={40} color={'#132144'} />

              <Text style={{ margin: 20, }}>
              {this.state.name} :- {this.state.marketdata1}
              </Text>
              <Text style={{ margin: 20, }}>
              {this.state.tenor} :-  {this.state.marketdata2}
              </Text>
              <Text style={{ margin: 20, }}>
              {this.state.rate} :- {this.state.marketdata3}
              </Text>


            </View>
          </Popover>

          <Footer>

            <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>
              <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
              </Button>


              <Button style={styles.inactive} onPress={() => this.tab('port')}>
                <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
              </Button>

              <Button style={styles.active} onPress={() => this.tab('home')}>
                <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
                <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
              </Button>
              <Button style={styles.inactive} onPress={() => this.tab('search')}>
                <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
              </Button>
              <Button style={styles.inactive} onPress={() => this.tab('more')}>
                <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
              </Button>


            </FooterTab>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
    themename: state.themename
  }
}

function mapDispatchToProps(dispatch) {
  return {
    themechnglight: () => dispatch({ type: 'light' }),
    themechngdark: () => dispatch({ type: 'dark' }),
  }
}


const styles = StyleSheet.create({
  viewDark: { backgroundColor: '#0b0b0b', height: 80 },
  viewLight: { backgroundColor: '#f6f7f9', height: 80 },
  darkgainertabs: { color: 'white', fontSize: 17, marginTop: 2, fontFamily: 'SegoePro-Bold' },
  lightgainertabs: { color: '#132144', fontSize: 17, marginTop: 2, fontFamily: 'SegoePro-Bold' },
  headerDark: { backgroundColor: '#0b0b0b', height: 1 },
  headerLight: { backgroundColor: '#f6f7f9', height: 1 },
  lightdate: { color: '#132144', marginTop: 4, fontSize: 14, textAlign: 'right' },
  darkdate: { color: 'white', marginTop: 4, fontSize: 14, textAlign: 'right' },
  darkview: { backgroundColor: '#0b0b0b', height: '100%' },
  lightview: { backgroundColor: '#f6f7f9', height: '100%' },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
  inactive: { padding: 10, },
  tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold' },
  tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold' },
  tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold' },
  statuslight: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: '#132144' },
  statusdark: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: 'white' },
  listlightcol1: { width: '70%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listlightcol2: { width: '30%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listdarkcol1: { width: '70%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listdarkcol2: { width: '30%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listdarkcolsensex1: { width: '43%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listdarkcolsensex3: { width: '25%', height: 58, backgroundColor: '#1a1f1f', },
  listlightcolsensex1: { width: '43%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listlightcolsensex3: { width: '25%', height: 58, backgroundColor: 'white', },
  listdarkcolsensexgov1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listdarkcolsensexgov2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listdarkcolsensexgov3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', },
  listlightcolsensexgov1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listlightcolsensexgov2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listlightcolsensexgov3: { width: '33%', height: 58, backgroundColor: 'white', },
  show: { display: 'flex' },
  hide: { display: 'none' },
  lighttext1: { marginLeft: 7, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  lightsensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  lightsensexturn: { alignSelf: 'flex-end', marginRight: 13, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  lightsensex1: { alignSelf: 'flex-end', marginRight: 7, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksensex: { alignSelf: 'flex-end', marginRight: 5, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  darksensexturn: { alignSelf: 'flex-end', marginRight: 13, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightdetailname: { marginLeft: 5, alignSelf: 'flex-start', marginTop: 9, color: '#132144', fontSize: 16, fontFamily: 'Arial', fontWeight: 'bold' },
  darkdetailname: { marginLeft: 5, alignSelf: 'flex-start', marginTop: 9, color: 'white', fontSize: 16, fontFamily: 'Arial', fontWeight: 'bold' },
  darksensex1: { alignSelf: 'flex-end', marginRight: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightsensexvalue: { marginTop: 8, marginRight: 13, alignSelf: 'flex-end', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksensexvalue: { marginTop: 8, marginRight: 13, alignSelf: 'flex-end', color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  darktext1: { marginLeft: 7, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  perlight: { alignSelf: 'flex-end', marginTop: 17, marginRight: 8, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', fontSize: 13 },
  perdark: { alignSelf: 'flex-end', marginTop: 17, marginRight: 8, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', fontSize: 13 },
  perlighttotal: { alignSelf: 'flex-end', marginTop: 17, fontSize: 16, marginRight: 8, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  perdarktotal: { alignSelf: 'flex-end', marginTop: 17, fontSize: 16, marginRight: 8, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightheader1: { width: '32%', height: 40 },
  darkheader1: { width: '32%', height: 40, },
  lightheader2: { width: '30%', height: 40 },
  darkheader2: { width: '30%', height: 40, },
  lightheader3: { width: '38%', height: 40 },
  darkheader3: { width: '38%', height: 40, },
  lightheader4: { width: '50%', height: 40 },
  darkheader4: { width: '50%', height: 40 },
  lightheadertext: { fontSize: 15, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  darkheadertext: { fontSize: 15, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  darkheadertext1: { alignSelf: 'flex-end', color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  lighttext: { marginLeft: 7, marginTop: 8, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darktext: { marginLeft: 7, marginTop: 8, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightdebtname: { marginLeft: 7, marginTop: 17, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkdebtname: { marginLeft: 7, marginTop: 17, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
  darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
  lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  totallight: { marginLeft: 7, fontSize: 18, marginTop: 17, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  totaldark: { marginLeft: 7, fontSize: 18, marginTop: 17, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
  darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
  pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
  pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },
  dropiconsetting: { marginLeft: 88, bottom: 30 },
  footer1: { width: 26, height: 28, },
  footer2: { width: 26, height: 27, },
  footer3: { width: 23, height: 24, bottom: 2 },
  modalheight: { height: '100%' },
  scrollview1: { bottom: 30 },
  settingrow: { marginTop: '25%' },
  popovercol1: { width: '20%', height: 50, },
  popovercol2: { width: '80%', height: 50, },
  popovercol3: { width: '40%', height: 50, },
  popovercol4: { width: '5%', height: 50, },
  popovercol5: { width: '35%', height: 50, },
  popovercol6: { width: '100%', height: 40, },
  popovercol7: { width: '50%', height: 40, },
  popovercol8: { width: '30%', height: 50, },
  picker: { height: 30, bottom: 5, width: '100%', },
  popovericon1: { textAlign: 'center' },
  popovericon2: { textAlign: 'right' },
  popovericon3: { textAlign: 'left' },
  menucol1: { width: '67%', height: 40 },
  menucol2: { width: '15%', height: 40 },
  menucol3: { width: '75%', height: 40 },
  menucol4: { width: '25%', height: 40 },
  closeicon: { marginLeft: 10 },
  usericon: { marginTop: 3, textAlign: 'right' },
  settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
  scrollview: { top: '8%' },
  scrollrow: { marginLeft: 16, marginRight: 16 },
  menurow: { marginLeft: 20, marginTop: 10 },
  menuicon: { marginTop: 6 },
  viewrow: { marginLeft: 3, },
  loader: { marginTop: 60 },
  scrollloader: { marginTop: 20 },
  flatlistHeight: { marginTop: 55, height: '100%' },
  flatcol1: { width: '100%', height: 58 },
  flatcol2: { width: '6.5%', height: 58 },
  mainrow1: { margin: 5 },
  mainrow2: { margin: 15, marginTop: '3%' },
  maincol1: { width: '15%', height: 30 },
  maincol2: { width: '55%', height: 30, },
  maincol3: { width: '20%', height: 30, },
  maincol4: { width: '10%', height: 30, },
  maincol5: { width: '100%', height: 40, },
  google: { width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 },
  back: { textAlign: 'left', marginLeft: 13 },
  scrollmodel: { top: '10%', height: '100%' },
  modelrow1: { marginLeft: 20, marginRight: 20, },
  modelrow2: { marginLeft: 20, marginRight: 15, },
  modelrow3: { marginLeft: 20, marginRight: 15, marginTop: 20, },
  modelrow4: { marginLeft: 15, marginRight: 15, },
  modelrow5: { marginLeft: 15, marginRight: 15, marginTop: 20 },
  modelrow6: { marginLeft: 20, marginRight: 15, marginTop: 20 },
})

// export default DebtTabs;
export default connect(mapStateToProps, mapDispatchToProps)(DebtTabs);