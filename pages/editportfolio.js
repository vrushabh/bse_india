import React, { Component } from 'react';
import { NetInfo, View, KeyboardAvoidingView, Image, Text, Linking, CheckBox, ToastAndroid, Modal, Switch, Picker, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEvil from 'react-native-vector-icons/EvilIcons';

import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import * as Animatable from 'react-native-animatable';
import Voice from '@react-native-community/voice';
import I18n from '../i18';
import Popover from 'react-native-popover-view'
import Ripple from 'react-native-material-ripple';
import { connect } from 'react-redux';
import database from '@react-native-firebase/database';

class EditPort extends Component {

  static navigationOptions = {

    header: null,
  };
  themename = '';
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps.allowFontScaling = false;
    i = 0

    this.state = {
      date: '',
      status: '',
      Equity: '',
      Derivative: '',
      mf: '',
      debt: '',
      showloader: false,
      showloaderderivative: false,
      showloadermf: false,
      showloaderdebt: false,
      val: '',
      valderivative: '',
      valmf: '',
      valdebt: '',
      closeicon: false,
      closeiconderivative: false,
      closeiconmf: false,
      closeicondebt: false,
      name: 'equity',
      value: 1,
      isVisible: false,
      switchval: false,
      sidemenu: false,
      settingshow: false,
      theme: 'dark',
      showedit: false,
      selectlan: 'English',
      iconname: 'ios-arrow-down',
      iconnamesensex: 'ios-arrow-forward',
      iconnamesme: 'ios-arrow-forward',
      iconnamederivative: 'ios-arrow-forward',
      iconnamecurrency: 'ios-arrow-forward',
      iconnamecommodity: 'ios-arrow-forward',
      iconnameird: 'ios-arrow-forward',
      iconnameetf: 'ios-arrow-forward',
      iconnamedebt: 'ios-arrow-forward',
      iconnamecorporate: 'ios-arrow-forward',
      iconnameipf: 'ios-arrow-forward',
      ipf: false,
      equity: true,
      sensex: false,
      sme: false,
      derivative: false,
      currencytab: false,
      commoditytab: false,
      ird: false,
      etf: false,
      debt: false,
      corporate: false,
      id:0
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    // setTimeout(() => { SplashScreen.hide() }, 1000);

    Voice.onSpeechStart = this.onSpeechStart.bind(this);
    Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
  }

  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
    // this.props.navigation.navigate('setting');
  }
  sensexfun() {
    fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

      try {
        responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';

      } catch (error) {
      }
    })

  }

  editicon(val) {
    val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
    // this.setState({showedit:!val});
  }



  chnglanguage(lan) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem('language', lan);
    this.props.navigation.navigate('maindark');

  }
  componentDidMount() {
    AsyncStorage.getItem('themecolor').then((dt) => {
      // dt == null || dt == 'dark' ? this.setState({ value: '1', theme: 'dark' }) : this.setState({ value: '0', theme: 'light' });
      dt == null || dt == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

      AsyncStorage.getItem('language').then((lan) => {
        if (lan == null) {
          this.setState({ langauge: 'en', selectlan: 'English' });
          I18n.locale = "en"
        }
        else {

          lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
          lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
        }
      });
    });
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    });

    this.sensexfun();
    // I18n.locale = "en";
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    Voice.destroy().then(Voice.removeAllListeners);
  }

  onSpeechStart(e) {
  };

  onSpeechRecognized(e) {
  };

  onSpeechResults(e) {
    let res = JSON.stringify(e.value[0]).slice(1, -1);
    this.setState({ val: res, valderivative: res, valmf: res, valdebt: res });
    this.chng(res);
    this.chngderivative(res);
    this.chngmf(res);
    this.chngdebt(res);
  }

  voice() {
    Voice.start('en-US');
  }

  changeTheme(thm) {

    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem("themecolor", thm);
    this.props.navigation.navigate('maindark');
    this.props.navigation.goBack(null);
    thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  back() {
    this.props.navigation.goBack(null);
    return true;
  }
  tab(val) {

    if (val == 'watch') { }
    else if (val == 'port') { }
    else if (val == 'home') {
      this.props.navigation.navigate('maindark');
    }
    else if (val == 'search') { }
    else if (val == 'more') {
      this.setState({ sidemenu: true })
    }

  }

  chng(event) {
    if (event != '') {
      if (event.length < 3) {
        this.setState({ val: event, valderivative: event, valmf: event, valdebt: event, closeicon: true });
      }
      else {
        this.setState({ showloader: true, val: event, valderivative: event, valmf: event, valdebt: event, closeicon: true });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Equity: responsejson.split('#'), showloader: false });
        });
        this.chngderivative(event);
        this.chngmf(event);
        this.chngdebt(event);
      }

    }
    else { this.setState({ val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false }); }
  }

  close() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
  }

  chngderivative(event) {

    if (event != '') {
      if (event.length < 3) {
        this.setState({ valderivative: event, val: event, valmf: event, valdebt: event, closeiconderivative: true, closeicon: true, closeiconmf: true, closeicondebt: true });
      }
      else {
        this.setState({ showloaderderivative: true, showloader: true, showloadermf: true, showloaderdebt: true, valderivative: event, val: event, valmf: event, valdebt: event, closeiconderivative: true, closeicon: true, closeiconmf: true, closeicondebt: true });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Derivative: responsejson.split('#'), showloaderderivative: false });

        });

        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Equity: responsejson.split('#'), showloader: false });

        });

        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ mf: responsejson.split('#'), showloadermf: false });

        });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ debt: responsejson.split('#'), showloaderdebt: false });

        });

      }

    }
    else {

      this.setState({ valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    }

  }
  closederivative() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
  }

  chngmf(event) {
    if (event != '') {
      if (event.length < 3) {
        this.setState({ valmf: event, val: event, valderivative: event, valdebt: event, closeiconmf: true, closeicon: true, closeiconderivative: true, closeicondebt: true });
      }
      else {
        this.setState({ showloadermf: true, showloaderderivative: true, showloader: true, showloaderdebt: true, valmf: event, val: event, valderivative: event, valdebt: event, closeiconmf: true, closeicon: true, closeiconderivative: true, closeicondebt: true });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ mf: responsejson.split('#'), showloadermf: false });

        });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Derivative: responsejson.split('#'), showloaderderivative: false });

        });

        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Equity: responsejson.split('#'), showloader: false });

        });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ debt: responsejson.split('#'), showloaderdebt: false });

        });

      }

    }
    else {

      this.setState({ valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    }
  }
  closemf() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
  }

  chngdebt(event) {
    if (event != '') {

      if (event != '') {
        if (event.length < 3) {
          this.setState({ valdebt: event, val: event, valderivative: event, valmf: event, closeicondebt: true, closeicon: true, closeiconderivative: true, closeiconmf: true });
        }
        else {
          this.setState({ showloaderdebt: true, showloadermf: true, showloaderderivative: true, showloader: true, valdebt: event, val: event, valderivative: event, valmf: event, closeicondebt: true, closeicon: true, closeiconderivative: true, closeiconmf: true });
          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ debt: responsejson.split('#'), showloaderdebt: false });

          });
          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ Derivative: responsejson.split('#'), showloaderderivative: false });

          });

          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ Equity: responsejson.split('#'), showloader: false });

          });
          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ mf: responsejson.split('#'), showloadermf: false });

          });

        }

      }

    }
    else {

      this.setState({ valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    }
  }
  closedebt() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
  }
  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
  }

  equity() { this.setState({ name: 'equity' }) }
  derivative() { this.setState({ name: 'derivative' }) }
  mf() { this.setState({ name: 'mf' }) }
  debt() { this.setState({ name: 'debt' }) }

  more() {
    this.setState({ isVisible: true });
  }

  closePopover() {
    this.setState({ isVisible: false });
  }
  closesidemenu() {
    this.setState({ sidemenu: false });
  }

  indices() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('indices');
  }

  home() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('maindark');

  }

  settingpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('setting', { 'current': val });

  }
  equitypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('equity', { 'current': val });

  }
  derivativepage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('derivativetab', { 'current': val });

  }
  currencypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('currencytab', { 'current': val });

  }
  commoditypage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('commoditytab', { 'current': val });

  }
  irdpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ird', { 'current': val });

  }
  smepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('sme', { 'current': val });
  }
  etfpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('etf', { 'current': val });
  }
  debtpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('debt', { 'current': val });
  }
  corporatepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('corporate', { 'current': val });
  }
  marketstatic() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketstatic');
  }

  marketturn() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketturn');
  }
  listing() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('listing');
  }
  ipo() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipo');
  }
  watchlist() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('watch');
  }
  ipfpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipf', { 'current': val });
  }
  notices() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('notice');
  }
  googleassistant() {
    Linking.openURL('https://assistant.google.com/explore');

  }
  equityView() {
    this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });
  }
  sensexView() {
    this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
  }
  smeView() {
    this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
  }
  derivativeView() {
    this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
  }
  currencyView() {
    this.state.currencytab == false ? this.setState({ currencytab: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currencytab: false, iconnamecurrency: 'ios-arrow-forward' });
  }
  commadityView() {
    this.state.commoditytab == false ? this.setState({ commoditytab: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commoditytab: false, iconnamecommodity: 'ios-arrow-forward' });
  }
  irdView() {
    this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
  }
  etfView() {
    this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
  }
  debtView() {
    this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
  }
  corporateView() {
    this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
  }
  ipfView() {
    this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

  }

  watchlistadd(id, name) {
    this.setState({id:parseInt(this.state.id) + 1});
    fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + id + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
      console.log(responsejson);
    responsejson.map((dt) => {
        if (dt.chgval > 0) {
          dt.chgval = '+' + dt.chgval;
          dt.chgper = '+' + dt.chgper;
          dt.sname = name;

        }

        // this.setState({ sname: responsejson[0].sname, ltp: responsejson[0].ltp, changeval: responsejson[0].chgval, changeper: responsejson[0].chgper, scode: responsejson[0].scode });
        database()
          .ref('/watchlist/' + responsejson[0].scode)
          .set({
            sname: name,
            scode: responsejson[0].scode,
            chgval: responsejson[0].chgval,
            chgper: responsejson[0].chgper,
            ltp: responsejson[0].ltp,
            page:this.state.id,
            prvclose:responsejson[0].PrevClose
          })
          .then(() => console.log('Data set.')).catch((error) => {
            console.log(error);
          });
        AsyncStorage.setItem('watchdata', JSON.stringify(responsejson[0]));
        this.back();
      })
    });



  }

  render() {

    return (
      <View style={this.props.theme == '0' ? styles.mainviewlight : styles.mainviewdark}>
        <StatusBar backgroundColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} />

        <View style={styles.headerView}>
          <Row style={styles.row1}>
            {/* <Col style={styles.col1}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={styles.back} size={27} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col> */}
            <Col style={styles.col2}><Text style={this.props.theme == '0' ? styles.titlelight : styles.titledark}>Portfolio</Text></Col>
          </Row>

          <Row style={styles.row2}>
            <Col style={styles.col3}><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.props.theme == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

          </Row>

          <ScrollView horizontal style={this.props.theme == '0' ? styles.buttonlight : styles.buttondark} showsHorizontalScrollIndicator={false}>

            <View style={styles.view}></View>
            <View style={this.state.name == 'equity' ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.equity()}><Text style={this.state.name == 'equity' ? styles.textActive : this.props.theme == '0' ? styles.textinActive : styles.textinActiveDark}> Equity </Text></TouchableOpacity></View>
            <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>
            <View style={this.state.name == 'derivative' ? styles.btnViewActive : this.props.theme == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.derivative()}><Text style={this.state.name == 'derivative' ? styles.textActive : this.props.theme == '0' ? styles.textinActive : styles.textinActiveDark}> Derivatives </Text></TouchableOpacity></View>


          </ScrollView>

          <View style={this.state.name == 'equity' ? styles.showequity : styles.hide} >
            <Row style={styles.scrollrow}>
              <Col style={this.props.theme == '0' ? styles.lightsearch : styles.darksearch}><Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.equatyimage} /></Col>
              <Col style={this.props.theme == '0' ? styles.lightsearch1 : styles.darksearch1}><TextInput placeholder="Security Name / Id / Code" value={this.state.val} placeholderTextColor={this.props.theme == '0' ? "black" : "white"} onChangeText={(e) => this.chng(e)} style={this.props.theme == '0' ? styles.lightsearchtext : styles.darksearchtext} /></Col>
              <Col style={this.state.closeicon == false ? this.props.theme == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voice()}><Icon name={'ios-mic'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
              <Col style={this.state.closeicon == true ? this.props.theme == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.close()}><Icon name={'ios-close'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>

            </Row>
          </View>
          <View style={this.state.name == 'derivative' ? styles.showequity : styles.hide} >
            <Row style={styles.scrollrow}>
              <Col style={this.props.theme == '0' ? styles.lightsearch : styles.darksearch}><Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.equatyimage} /></Col>
              <Col style={this.props.theme == '0' ? styles.lightsearch1 : styles.darksearch1}><TextInput placeholder="Enter Underlying Index Name" value={this.state.valderivative} placeholderTextColor={this.props.theme == '0' ? "black" : "white"} style={this.props.theme == '0' ? styles.lightsearchtext : styles.darksearchtext} onChangeText={(e) => this.chngderivative(e)} /></Col>
              <Col style={this.state.closeiconderivative == false ? this.props.theme == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voice()}><Icon name={'ios-mic'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
              <Col style={this.state.closeiconderivative == true ? this.props.theme == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.closederivative()}><Icon name={'ios-close'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
            </Row>

          </View>

        </View>

        <ScrollView style={this.state.name == 'equity' ? styles.show : styles.hide}>

          <View style={this.state.showloader == true ? styles.show : styles.hide}>
            <ActivityIndicator size="large" style={styles.scrollloader} color={this.props.theme == '0' ? "#0000ff" : "white"} />
          </View>

          <View style={this.state.Equity == '' ? styles.hide : styles.show}>

            <FlatList
              data={this.state.Equity}
              style={styles.flatlist}
              renderItem={({ item }) =>
                <Animatable.View animation="fadeInUp">
                  <Row style={styles.flatlistrow} onPress={() => this.watchlistadd(item.split(',')[0], item.split(',')[2])}>
                    <Col style={item.split(',')[0] != '' ? this.props.theme == '0' ? styles.list : styles.listdark : styles.hide}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split(',')[2]}</Text><Text style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{item.split(',')[0]} | {item.split(',')[1]}</Text></Col>
                  </Row>
                </Animatable.View>

              }
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>

        <ScrollView style={this.state.name == 'derivative' ? styles.show : styles.hide}>

          <View style={this.state.showloaderderivative == true ? styles.show : styles.hide}>
            <ActivityIndicator size="large" style={styles.scrollloader} color={this.props.theme == '0' ? "#0000ff" : "white"} />
          </View>


          <View style={this.state.Derivative == '' ? styles.hide : styles.show}>
            <FlatList
              data={this.state.Derivative}
              style={styles.flatlist}
              renderItem={({ item }) =>
                <Animatable.View animation="fadeInUp">
                  <Row style={styles.flatlistrow} onPress={() => this.watchlistadd(item.split(',')[0], item.split(',')[2])}>
                    <Col style={item.split(',')[0] != '' ? this.props.theme == '0' ? styles.list : styles.listdark : styles.hide}><Text style={this.props.theme == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split(',')[2]}</Text><Text style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1} >{item.split(',')[0]} | {item.split(',')[1]}</Text></Col>
                  </Row>
                </Animatable.View>

              }
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>




        <Modal
          animationType="slide"
          transparent={false}
          style={styles.modalheight}
          visible={this.state.sidemenu}
          onRequestClose={() => {
            this.setState({ sidemenu: false });
          }}
        >
          <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
            <Row style={styles.menurow}>
              <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.close()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
            </Row>

            <ScrollView style={styles.scrollview}>

              <Text></Text>
              <View style={{}}>
                <Row style={styles.scrollrow}>
                  <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                  <Col style={styles.popovercol7}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.equity == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.equitypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.sensex == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.sme == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                  <Text></Text>
                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.derivative == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.currencytab == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.commoditytab == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.ird == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.etf == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.debt == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.corporate == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                  <Col style={styles.menucol4}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.ipf == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                  <Text></Text>

                </View>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>
                {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
              </View>
            </ScrollView>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.settingshow}
          style={styles.modalheight}
          onRequestClose={() => {
            this.setState({ settingshow: false });
            this.setState({ sidemenu: false });

          }}
        >
          <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

            <ScrollView style={styles.scrollview1}>
              <Row style={styles.settingrow}>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                <Col style={styles.popovercol8}>
                  <Picker
                    selectedValue={this.state.selectlan}
                    style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                    onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                  >
                    <Picker.Item label="English" value="English" />
                    <Picker.Item label="Hindi" value="Hindi" />
                    <Picker.Item label="Marathi" value="Marathi" />
                    <Picker.Item label="Gujrati" value="Gujrati" />
                    <Picker.Item label="Bengali" value="Bengali" />
                    <Picker.Item label="Malayalam" value="Malayalam" />
                    <Picker.Item label="Oriya" value="Oriya" />
                    <Picker.Item label="Tamil" value="Tamil" />
                  </Picker>
                  <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                </Col>
              </Row>
              <Text></Text>
              <Row >
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                <Col style={styles.popovercol8}>
                  <Picker
                    selectedValue={this.props.themename}
                    style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                    onValueChange={(itemValue) => this.changeTheme(itemValue)}
                  // onValueChange={(itemValue) => this.props.themechng()}
                  >
                    <Picker.Item label="Dark" value="dark" />
                    <Picker.Item label="Light" value="light" />

                  </Picker>
                  <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                </Col>
              </Row>

              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
              </Row>
            </ScrollView>
          </View>
        </Modal>


        <Footer>

          <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>
            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
              <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
            </Button>


            <Button style={styles.inactive} onPress={() => this.tab('port')}>
              <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
            </Button>

            <Button style={styles.active} onPress={() => this.tab('home')}>
              <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
              <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('search')}>
              <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('more')}>
              <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
            </Button>


          </FooterTab>
        </Footer>
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    theme: state.theme,
    themename: state.themename
  }
}

function mapDispatchToProps(dispatch) {
  return {
    themechnglight: () => dispatch({ type: 'light' }),
    themechngdark: () => dispatch({ type: 'dark' }),
  }
}

const styles = StyleSheet.create({
  lightdate: { bottom: 28, color: '#132144', fontSize: 14, textAlign: 'right', marginRight: 5, },
  darkdate: { bottom: 28, color: 'white', fontSize: 14, textAlign: 'right', marginRight: 5, },
  statuslight: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: '#132144' },
  statusdark: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: 'white' },
  mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
  mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
  btnViewActive: { borderRadius: 5, backgroundColor: '#2087c9', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactive: { borderRadius: 5, backgroundColor: 'white', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactiveLast: { borderRadius: 5, backgroundColor: 'white', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactivedark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactiveLastdark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewactiveLast: { borderRadius: 5, backgroundColor: '#2087c9', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  textActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textinActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
  textinActiveDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textinActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
  textinActivelasDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  show: { display: 'flex' },
  showequity: { display: 'flex', bottom: 60 },
  hide: { display: 'none' },
  dropicon: { marginLeft: 38, bottom: 50 },

  lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  list: { width: '100%', height: 58, backgroundColor: 'white', borderRadius: 5, },
  listdark: { width: '100%', height: 58, backgroundColor: '#1a1f1f', borderRadius: 5, },
  closeiconclass: { textAlign: 'center', marginTop: 9 },
  close: { width: '15%', height: 45, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  closedark: { width: '15%', height: 45, backgroundColor: '#1a1f1f', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
  inactive: { padding: 10, },
  tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
  buttonlight: { bottom: 74, height: 0, marginLeft: 4, backgroundColor: '#f6f7f9' },
  buttondark: { bottom: 74, height: 0, marginLeft: 4, backgroundColor: '#0b0b0b' },
  gaplight: { backgroundColor: '#f6f7f9', width: 10, height: 70, bottom: 10 },
  gapdark: { backgroundColor: '#0b0b0b', width: 10, height: 70, bottom: 10 },
  titlelight: { color: '#132144', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 15 },
  titledark: { color: 'white', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 15 },
  lightsearch: { width: '15%', height: 45, backgroundColor: 'white', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  darksearch: { width: '15%', height: 45, backgroundColor: '#1a1f1f', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  lightsearch1: { width: '70%', height: 45, backgroundColor: 'white', },
  darksearch1: { width: '70%', height: 45, backgroundColor: '#1a1f1f', },
  lightsearchtext: { fontSize: 15, top: 2, fontFamily: 'SegoeProDisplay-Regular', color: '#132144' },
  darksearchtext: { fontSize: 15, top: 2, fontFamily: 'SegoeProDisplay-Regular', color: 'white' },
  lighttext: { marginLeft: 9, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular' },
  darktext: { marginLeft: 9, marginTop: 9, color: 'white', fontFamily: 'SegoeProDisplay-Regular' },
  lighttext1: { marginLeft: 9, marginTop: 1, color: '#aaafba', fontFamily: 'SegoeProDisplay-Regular' },
  darktext1: { marginLeft: 9, marginTop: 1, color: 'white', fontFamily: 'SegoeProDisplay-Regular' },
  lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
  darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
  lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
  darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'SegoeProDisplay-Regular', },
  lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  modalheight: { height: '100%' },

  menurow: { marginLeft: 20, marginTop: 10 },
  menucol1: { width: '67%', height: 40 },
  menucol2: { width: '15%', height: 40 },
  menucol3: { width: '75%', height: 40 },
  menucol4: { width: '25%', height: 40 },
  closeicon: { marginLeft: 10 },
  usericon: { marginTop: 3, textAlign: 'right' },
  settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
  scrollview: { top: '8%' },
  scrollview1: { bottom: 30 },
  scrollrow: { marginLeft: 16, marginRight: 16 },
  menuicon: { marginTop: 6 },
  settingrow: { marginTop: '25%' },
  footer1: { width: 26, height: 28, },
  footer2: { width: 26, height: 27, },
  footer3: { width: 23, height: 24, bottom: 2 },
  popoverview: { backgroundColor: 'white', height: '100%' },
  popovercol1: { width: '20%', height: 50, },
  popovercol2: { width: '80%', height: 50, },
  popovercol3: { width: '40%', height: 50, },
  popovercol4: { width: '5%', height: 50, },
  popovercol5: { width: '35%', height: 50, },
  popovercol6: { width: '100%', height: 40, },
  popovercol7: { width: '50%', height: 40, },
  popovercol8: { width: '30%', height: 50, },
  popovericon1: { textAlign: 'center' },
  popovericon2: { textAlign: 'right' },
  popovericon3: { textAlign: 'left' },
  picker: { height: 30, bottom: 5, width: '100%', },
  headerView: { height: '32%', },
  row1: { margin: 5, marginTop: 10, },
  col1: { width: '15%', height: 30, },
  col2: { width: '100%', height: 30, },
  back: { textAlign: 'left', marginLeft: 13 },
  image: { width: 23, height: 22, marginTop: 3, alignSelf: 'flex-end' },
  user: { marginTop: 1, textAlign: 'right' },
  row2: { margin: 5, marginRight: 19, },
  col3: { width: '100%', height: 40, },
  view: { marginLeft: 15 },
  scrollrow: { marginLeft: 16, marginRight: 16 },
  equatyimage: { width: 22, height: 23, margin: 12 },
  scrollloader: { marginTop: 20 },
  flatlist: { height: '100%', bottom: 12 },
  flatlistrow: { marginLeft: 10, marginRight: 10, marginTop: 7, },

})

// export default Search;
export default connect(mapStateToProps, mapDispatchToProps)(EditPort);