import React, { Component } from 'react';
import { View, Text, Dimensions, BackHandler, processColor, ScrollView, StyleSheet, ActivityIndicator } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Row } from 'react-native-easy-grid';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import moment from 'moment';
import { LineChart, CombinedChart } from 'react-native-charts-wrapper';

import api from '../api.js';

import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
import { log } from 'react-native-reanimated';
const height = Dimensions.get('window').height;
class Graph extends Component {
    timedata = [];
    predata = [];
    opendata = [];
    static navigationOptions = {

        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            time: '',
            title: '',
            value: '',
            data: [],
            tmdata: [],
            valuedata: [1, 2, 3, 4, 5],
            loading: false,
            opndata: [1, 2, 3, 4, 5],
            data: {},
            chart1Zoom: { scaleX: 0, scaleY:0, xValue:0, yValue: 0},
            circle:false
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setTimeout(() => { SplashScreen.hide() }, 4000);

    }
    componentDidMount() {
        this.setState({ loading: true });

        api.grp().then(res => {
            this.setState({ data: res.split('@')[1].split('#'), value: res.split('@')[0].split(',')[5], time: res.split('@')[0].split(',')[6].split('#')[0], title: res.split('@')[0].split(',')[4] });
            console.log(this.state.data);


            for (let i = 1; i < 16; i++) {
                // if (i == 2 || i == 7 || i == 10 || i == 14) {
                   
                //     this.setState({circle:false});
                // }
                this.predata.push(parseInt(this.state.data[i].split(',')[1]));
                this.opendata.push(parseInt(this.state.data[i].split(',')[1]));
                this.timedata.push(moment(this.state.data[i].split(',')[0]).format('HH:mm'));

            }
            for (let i = 16; i < this.state.data.length - 1; i++) {
                this.setState({circle:true});
                this.timedata.push(moment(this.state.data[i].split(',')[0]).format('HH:mm'));
                this.opendata.push(parseInt(this.state.data[i].split(',')[2]));


                // if (i == 4 || i == 7 || i == 12) {
                //     this.predata.push(parseInt(this.state.data[i].split(',')[1]));
                // }
                // else if (i == 16 || i == 41 || i == 70 || i == 83 || i == 100 || i == 150 || i == 250 || i == 300 || i == 350 || i == 380 || i == 420) {
                //     this.opendata.push(parseInt(this.state.data[i].split(',')[2]));
                // }

            }
            // this.timedata.push(res.split('@')[0].split(',')[6].split('#')[0]);
            console.log(res.split('@')[0].split(',')[6].split('#')[0]);
            console.log(this.timedata);
            this.setState({ loading: false, tmdata: this.timedata, valuedata: this.predata, opndata: this.opendata, })

        });
    }
    syncToChart2(event) {
        if (event.action == 'chartScaled' || event.action == 'chartTranslated') {
            let { scaleX, scaleY, centerX, centerY } = event
            this.setState({ ...this.state, chart2Zoom: { scaleX: scaleX, scaleY: scaleY, xValue: centerX, yValue: centerY } })
            console.log("sync chart2" +
                " to " + centerX + " " + centerY)
        }

        console.log(event)
    }

    componentWillMount() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;

    }

    render() {
        return (
            <View>

                <Row style={{ margin: 5, marginTop: 30, }}>
                    <Col style={{ width: '40%', height: 40, }}><Text style={{ fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 16, bottom: 3 }}>{this.state.title}</Text></Col>
                    <Col style={{ width: '30%', height: 40, }}><Text style={{ textAlign: 'center', color: '#132144', }}>Time : {this.state.time}</Text></Col>
                    <Col style={{ width: '30%', height: 40, }}><Text style={{ textAlign: 'right', color: '#132144', }}>Value : {this.state.value}</Text></Col>
                </Row>
                <Text></Text>
                <View style={this.state.loading == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{}} color={'black'} />

                </View>
                <Text></Text>
               
                <LineChart
                        style={this.state.loading == true ? styles.hide : styles.chart}
                        data={{
                            dataSets: [{
                                label: "Pre-Open", values: this.state.valuedata, config: {
                                    lineWidth: 1,
                                    drawCircles: false,
                                    drawFilled: true,
                                    drawLabels: true,
                                    drawValues: false,
                                    fillColor: processColor('#ff0000'),
                                    circleColor: processColor('red'),
                                    drawCircleHole: false,
                                    circleRadius: 5,
                                    colors: [processColor('#e0a3a3')],
                                    fillAlpha: 100,
                                    valueTextSize: 10,
    
                                },
    
                            },
                            {
                                label: "Open", values: this.state.opndata, config: {
                                    lineWidth: 1,
                                    drawCircles: false,
                                    drawFilled: true,
                                    drawLabels: true,
                                    drawValues: false,
                                    fillColor: processColor('#7bd6ed'),
                                    circleColor:processColor('red'),
                                    drawCircleHole: false,
                                    circleRadius: 5,
                                    colors: [processColor('#7bd6ed')],
                                    fillAlpha: 100,
                                    valueTextSize: 10,
    
                                },
    
                            }
                            ],
                        }}
                        xAxis={{
                            position: 'BOTTOM',
                            valueFormatter: this.state.tmdata,
                            granularityEnabled: true,
                            granularity: 1,
                            labelCount: 10,
                            drawAxisLine: true,
                            drawGridLines: true,
                            textSize: 13,
                            scaleXEnabled: true
    
                        }}
    
                        yAxis={{
                            right: { enabled: false },
                            left: {
                                enabled: true,
                                drawAxisLine: true,
                                drawGridLines: true,
                                textSize: 13
                            }
                        }}
                        legend={{
                            horizontalAlignment: "LEFT",
                            verticalAlignment: 'TOP',
                            enabled: true,
                            textSize: 15,
                            form: 'SQUARE',
                            formSize: 15,
                            xEntrySpace: 20,
                            yEntrySpace: 10,
                            formToTextSpace: 10,
                            wordWrapEnabled: true,
                            maxSizePercent: 0.5,
                        }}
                        animation={{ durationX: 2000 }}
                        visibleRange={{ x: { min: 7, max: 7 } }}
                        chartDescription={{ text: '' }}
                        marker={{
                            enabled: true,
                            markerColor: processColor('#132144'), textSize: 15
                        }}
                        

                        touchEnabled={true}
                        dragEnabled={true}
                        scaleEnabled={true}
                        scaleXEnabled={false}
                        scaleYEnabled={true}
                        pinchZoom={true}
                        zoom={this.state.chart1Zoom}

                        ref="chart1"

                        onChange={(event) => this.syncToChart2(event.nativeEvent)}
                    />

               



            </View>

        );
    }
}
const styles = StyleSheet.create({
    chart: {
        width: '100%',
        height: '85%'
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF'
    },

    show: { display: 'flex', },

    hide: { display: 'none' },
});

export default Graph;
