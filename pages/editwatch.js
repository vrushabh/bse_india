import React, { Component } from 'react';

import { View, Image, Alert, Modal, Linking, ToastAndroid, CheckBox, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Ripple from 'react-native-material-ripple';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';
import api from '../api.js';
import { NavigationEvents } from 'react-navigation';
import database from '@react-native-firebase/database';
import DatePicker from 'react-native-datepicker'
import moment from 'moment';


class EditWatch extends Component {
  static navigationOptions = {

    header: null,
  };
  Data = [];
  themename = '';
  watchlistData = [];
  qtyPricedata = [];
  ratePricedata = [];
  datePricedata = [];
  editwatchData = [];
  finalquantity = '0';
  finalrate = '0';
  finalltp = '0';

  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    this.state = {
      date: '',
      status: '',
      value: 1,
      isVisible: false,
      switchval: false,
      indicesdata: [],
      loader: false,
      refreshing: false,
      sidemenu: false,
      settingshow: false,
      theme: 'dark',
      showedit: false,
      iconname: 'ios-arrow-down',
      selectlan: 'English',
      nodata: false,
      iconname: 'ios-arrow-down',
      iconnamesensex: 'ios-arrow-forward',
      iconnamesme: 'ios-arrow-forward',
      iconnamederivative: 'ios-arrow-forward',
      iconnamecurrency: 'ios-arrow-forward',
      iconnamecommodity: 'ios-arrow-forward',
      iconnameird: 'ios-arrow-forward',
      iconnameetf: 'ios-arrow-forward',
      iconnamedebt: 'ios-arrow-forward',
      iconnamecorporate: 'ios-arrow-forward',
      iconnameipf: 'ios-arrow-forward',
      ipf: false,
      equity: true,
      sensex: false,
      sme: false,
      derivative: false,
      currencytab: false,
      commoditytab: false,
      ird: false,
      etf: false,
      debt: false,
      corporate: false,
      equityadd: false,
      val: "",
      rate: '',
      noticeDate: moment().format("DD/MM/YY"),
      qty: '',
      watchname: ''

    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    setTimeout(() => { SplashScreen.hide() }, 3000);
    // setInterval(() => { this.main() }, 500);

  }

  componentDidMount() {
    AsyncStorage.getItem('themecolor').then((dt) => {
      dt == null || dt == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

    });

    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    });


    this.sensexdatefun();
    AsyncStorage.getItem('language').then((lan) => {
      if (lan == null) {
        this.setState({ langauge: 'en', selectlan: 'English' });
        I18n.locale = "en";
        this.watchdatafun(this.state.langauge);
      }
      else {
        lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tm", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
        lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
        this.watchdatafun(this.state.langauge);
      }
    });
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }


  add() {
    this.props.navigation.navigate('editport');

  }
  closeport() {
    this.setState({ equityadd: false });

  }

  sensexdatefun() {
    api.date().then(responsejson => {
      responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';

    });
  }

  watchdatafun(lan) {
    this.setState({ loader: true });

    this.watchlistData = [];

    AsyncStorage.getItem('array').then((data) => {

      let watchdata = JSON.parse(data);
      console.log(watchdata);
      if (watchdata == null) {
        console.log('1');
        this.setState({ nodata: true, loader: false, refreshing: false, });

      }
      else {
        console.log('2');

        database().ref('/watchlist/').on('value', (snapshot) => {
          snapshot.forEach((child) => {
            console.log(child);

            let wjson = {
              sname: child.val().sname,
              scode: child.val().scode,
              ltp: child.val().ltp,
              chgval: child.val().chgval,
              chgper: child.val().chgper,
              prvclose: child.val().prvclose
            }
            console.log(wjson);


            this.watchlistData.push(wjson);
            var result = this.watchlistData.reduce((unique, o) => {
              if (!unique.some(obj => obj.sname === o.sname)) {
                unique.push(o);
              }
              return unique;
            }, []);
            console.log(result);
            this.setState({ indicesdata: result, nodata: false, refreshing: false, loader: false });
            AsyncStorage.setItem('array', JSON.stringify(this.watchlistData));




          })
        });
      }

    });



  }

  refresh() {
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
        "Application requires Network to proceed",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    });
    this.sensexdatefun();
    AsyncStorage.getItem('language').then((lan) => {
      if (lan == null) {
        this.setState({ langauge: 'en', selectlan: 'English' });
        I18n.locale = "en";
        this.watchdatafun(this.state.langauge);
      }
      else {
        lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tm", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
        lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
        this.watchdatafun(this.state.langauge);
      }
    });
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.refresh();
  }

  tab(val) {

    if (val == 'watch') { }
    else if (val == 'port') { }
    else if (val == 'home') {
      this.props.navigation.navigate('maindark');

    }
    else if (val == 'search') {
      AsyncStorage.getItem('themecolor').then((dt) => {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('search', { 'theme': dt });
      });
    }
    else if (val == 'more') {
      this.setState({ sidemenu: true })
    }

  }


  toggleSwitch(val) {
    if (val == true) {
      this.setState({ value: 1, isVisible: false, themename: 'Dark', switchval: val });
      AsyncStorage.setItem("themecolor", "dark");
    }
    else {
      this.setState({ value: 0, isVisible: false, themename: 'Light', switchval: val });
      AsyncStorage.setItem("themecolor", "light");
    }

  }
  closePopover() {
    this.setState({ isVisible: false });
  }

  handleBackButtonClick() {

    this.props.navigation.goBack(null);
    return true;
  }
  back() {
    this.qtyPricedata = [];
    this.props.navigation.goBack(null);
    return true;
  }

  closesidemenu() {
    this.setState({ sidemenu: false });
  }
  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
  }

  changeTheme(thm) {

    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem("themecolor", thm);
    this.props.navigation.navigate('maindark');
    this.props.navigation.goBack(null);
    thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();
  }
  chnglanguage(lan) {
    this.setState({ settingshow: false, sidemenu: false });
    AsyncStorage.setItem('language', lan);
    this.props.navigation.navigate('maindark');

  }


  home() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('maindark');

  }

  settingpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('setting', { 'current': val });

  }
  equitypage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('equity', { 'current': val });
  }
  derivativepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('derivativetab', { 'current': val });
  }
  currencypage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('currencytab', { 'current': val });
  }
  commoditypage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('commoditytab', { 'current': val });
  }
  irdpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ird', { 'current': val });
  }
  smepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('sme', { 'current': val });
  }
  etfpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('etf', { 'current': val });
  }
  debtpage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('debt', { 'current': val });
  }
  corporatepage(val) {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('corporate', { 'current': val });
  }
  marketstatic() {

    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketstatic');
  }
  marketturn() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('marketturn');
  }
  listing() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('listing');
  }
  ipo() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipo');
  }
  watchlist() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('watch');
  }
  ipfpage(val) {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('ipf', { 'current': val });
  }
  notices() {
    this.setState({ sidemenu: false });
    this.props.navigation.navigate('notice');
  }

  editicon(val) {
    val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
    // this.setState({showedit:!val});
  }



  googleassistant() {
    Linking.openURL('https://assistant.google.com/explore');

  }
  detail(nameindices, id) {
    AsyncStorage.getItem('themecolor').then((dt) => {

      this.props.navigation.navigate('setting', { 'current': 0, 'theme': dt, 'name': nameindices, 'id': id });
    });
  }
  chngper(per1, per2) {
    if (per1 > 0) {
      return '+' + per1 + ' ' + '+' + per2 + '%';
    }
    else if (per1 < 0) {
      return per1 + ' ' + per2 + '%';
    }

  }
  value(val) {
    var no = '' + val;
    if (no.length == 5) {
      return no.substring(0, 2) + ',' + no.substring(2);
    }
    else if (no.length == 4) {
      return no.substring(0, 1) + ',' + no.substring(1);
    }
    else if (no.length == 3) {
      return no;
    }
    else if (no.length == 2) {
      return no;
    } else if (no.length == 1) {
      return no;
    }
    else if (no.length == 6) {
      return no.substring(0, 1) + ',' + no.substring(1, 3) + ',' + no.substring(3);
    }
    else if (no.length == 7) {
      return no.substring(0, 1) + ',' + no.substring(1, 4) + ',' + no.substring(4);
    }
  }
  equityView() {
    this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });

  }
  sensexView() {
    this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
  }
  smeView() {
    this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
  }
  derivativeView() {
    this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
  }
  currencyView() {
    this.state.currencytab == false ? this.setState({ currencytab: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currencytab: false, iconnamecurrency: 'ios-arrow-forward' });
  }
  commadityView() {
    this.state.commoditytab == false ? this.setState({ commoditytab: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commoditytab: false, iconnamecommodity: 'ios-arrow-forward' });
  }
  irdView() {
    this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
  }
  etfView() {
    this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
  }
  debtView() {
    this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
  }
  corporateView() {
    this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
  }
  ipfView() {
    this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

  }

  save = async () => {
    this.qtyPricedata = this.qtyPricedata.reduce((unique, o) => {
      if (!unique.some(obj => obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);
    this.ratePricedata = this.ratePricedata.reduce((unique, o) => {
      if (!unique.some(obj => obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);

    var result2 = this.datePricedata.reduce((unique, o) => {
      if (!unique.some(obj => obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);

    console.log(this.qtyPricedata);
    console.log(this.ratePricedata);
    for (let i = 0; i < this.qtyPricedata.length; i++) {
      let data = { 'qty': this.qtyPricedata[i].qty, 'rate': this.ratePricedata[i].rate, 'ltp': this.qtyPricedata[i].ltp, 'name': this.qtyPricedata[i].name, 'prvclose': this.qtyPricedata[i].prv };
      this.editwatchData.push(data);
    }
    


    console.log(this.editwatchData);
    await AsyncStorage.setItem('portdata', JSON.stringify(this.editwatchData));


    database()
      .ref('/portfolio/' + this.editwatchData[0].qty)
      .set({
        qty: this.editwatchData[0].qty,
        rate: this.editwatchData[0].rate,
        ltp: this.editwatchData[0].ltp,
      })
      .then(() => console.log('Data set.')).catch((error) => {
        console.log(error);
      });

    this.editwatchData = '';
    this.back();

  }

  chngqty(event, name, ltp, prv) {

    let data = { 'qty': event, 'name': name, 'ltp': ltp, 'prv': prv };
    this.qtyPricedata.push(data);

    this.qtyPricedata.map((data) => {
      if (event != data.qty && name == data.name) {
        data.qty = event;
      }
    })


  }

  chngrate(event, name, ltp, prv) {

    let data = { 'rate': event, 'name': name, 'ltp': ltp, 'prv': prv };
    this.ratePricedata.push(data);

    this.ratePricedata.map((data) => {
      if (event != data.rate && name == data.name) {
        data.rate = event;
      }
    })


  }

  chngdate(event, name) {
    this.setState({ noticeDate: event });
    let data = { 'date': event, 'name': name };
    this.datePricedata.push(data);

    this.datePricedata.map((data) => {
      if (event != data.date && name == data.name) {
        data.date = event;
      }
    })
  }
  close(code) {

    Alert.alert(
      "Save",
      "Do you want to delete and save changes ?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "YES", onPress: () =>
            this.deletefun(code)
        }
      ],
      { cancelable: false }
    );

  }
  deletefun(code) {
    database()
      .ref('/watchlist/' + code).remove()
      .then(() => console.log('Data delete.')).catch((error) => {
        console.log(error);
      });
    this.watchdatafun(this.state.langauge);
  }


  render() {

    return (
      <View style={this.props.theme == '0' ? styles.mainviewlight : styles.mainviewdark}

      >
        <NavigationEvents
          onDidFocus={() => this.watchdatafun(this.state.langauge)}
        />
        <StatusBar backgroundColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} />

        <Row style={styles.mainrow1}>
          <Col style={styles.maincol1}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={styles.back} size={27} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
          <Col style={styles.maincol2}><Text style={this.props.theme == '0' ? styles.titlelight : styles.titledark}>Edit Watchlist / Portfolio</Text></Col>
          <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.google} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
          <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={styles.popovericon1} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
        </Row>
        <Row style={styles.saveIconRow}>
          <Col style={styles.maincol1}></Col>
          <Col style={styles.maincol2}></Col>
          <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.save()}><IconFontAwesome name="folder" style={styles.popovericon1} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
          <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => this.add()}><IconFontAwesome name="plus" style={styles.popovericon1} size={25} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
        </Row>

        <Row style={styles.mainrow2}>
          <Col style={styles.maincol5}><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.props.theme == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

        </Row>

        <Row style={styles.viewrow}>
          <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>Qty</Text></Col>
          <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Rate</Text></Col>
          <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext2 : styles.darkheadertext2}>Date</Text></Col>
        </Row>
        <View style={this.state.loader == true ? styles.show : styles.hide}>
          <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

        </View>
        <View style={this.state.nodata == true ? styles.showNoData : styles.hide}>
          <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
        </View>



        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          data={this.state.indicesdata}
          style={styles.flatlistHeight}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) =>
            <Animatable.View animation="zoomIn">

              <Row style={styles.scrollrow}>
                <Col style={styles.flatcol1}>

                  <Row>
                    <Col style={this.props.theme == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.props.theme == '0' ? styles.namepos : styles.nameneg} numberOfLines={1}>{item.sname}</Text></Col>
                    <Col style={this.props.theme == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Icon name="ios-close" onPress={() => this.close(item.scode)} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>

                  </Row>
                </Col>

              </Row>
              <Row style={{ marginLeft: 18, bottom: 20 }}>
                <Col style={{ width: '25%', height: 40, }}><TextInput placeholder="Qty" keyboardType="number-pad" value={this.state.indicesdata[index]} onChangeText={(e) => this.chngqty(e, item.sname, item.ltp, item.prvclose)} placeholderTextColor={this.props.theme == '0' ? "black" : "white"} style={this.props.theme == '0' ? styles.lightsearchtext : styles.darksearchtext} /></Col>
                <Col style={{ width: '3%', height: 40, }}></Col>
                <Col style={{ width: '25%', height: 40, }}><TextInput placeholder="Rate" keyboardType="number-pad" value={this.state.indicesdata[index]} onChangeText={(e) => this.chngrate(e, item.sname, item.ltp, item.prvclose)} placeholderTextColor={this.props.theme == '0' ? "black" : "white"} style={this.props.theme == '0' ? styles.lightsearchtext : styles.darksearchtext} /></Col>
                <Col style={{ width: '3%', height: 40, }}></Col>
                <Col style={{ width: '25%', height: 40, }}>
                  <DatePicker
                    style={{ width: 100, margin: 5, marginLeft: 25, color: 'white' }}
                    date={this.state.noticeDate}
                    mode="date"
                    placeholder="Select Date"

                    format="DD/MM/YY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 80,
                        top: 4,
                        // marginLeft: 0,
                        // right:30
                      },
                      dateInput: {
                        marginLeft: 20,
                        color: 'white'
                      },
                      placeholderText: {
                        fontSize: 16,
                        color: 'white'
                      }
                    }}
                    onDateChange={(date) => { this.chngdate(date, item.sname) }}

                  />
                </Col>
              </Row>

              <Text></Text>
            </Animatable.View>

          }
          keyExtractor={item => item.id}
        />


        <Modal
          animationType="slide"
          transparent={false}
          style={styles.modalheight}
          visible={this.state.sidemenu}
          onRequestClose={() => {
            this.setState({ sidemenu: false });
          }}
        >
          <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
            <Row style={styles.menurow}>
              <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
              <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
            </Row>

            <ScrollView style={styles.scrollview}>

              <Text></Text>
              <View style={{}}>
                <Row style={styles.scrollrow}>
                  <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                  <Col style={styles.popovercol7}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.equity == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.equitypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                  <Ripple onPress={() => this.equitypage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.sensex == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                  <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.sme == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                  <Text></Text>
                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.derivative == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.currencytab == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.commoditytab == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.ird == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.etf == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                  <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.debt == false ? styles.hide : styles.show}>

                  <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                  <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.corporate == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                  <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                  <Text></Text>

                </View>
                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                  <Col style={styles.menucol4}></Col>
                </Row>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={styles.scrollrow}>
                  <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                  <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                </Row>
                <View style={this.state.ipf == false ? styles.hide : styles.show}>
                  <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                  <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                  <Text></Text>

                </View>

                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>
              </View>
            </ScrollView>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.settingshow}
          style={styles.modalheight}
          onRequestClose={() => {
            this.setState({ settingshow: false });
            this.setState({ sidemenu: false });

          }}
        >
          <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

            <ScrollView style={styles.scrollview1}>
              <Row style={styles.settingrow}>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                <Col style={styles.popovercol8}>
                  <Picker
                    selectedValue={this.state.selectlan}
                    style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                    onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                  >
                    <Picker.Item label="English" value="English" />
                    <Picker.Item label="Hindi" value="Hindi" />
                    <Picker.Item label="Marathi" value="Marathi" />
                    <Picker.Item label="Gujrati" value="Gujrati" />
                    <Picker.Item label="Bengali" value="Bengali" />
                    <Picker.Item label="Malayalam" value="Malayalam" />
                    <Picker.Item label="Oriya" value="Oriya" />
                    <Picker.Item label="Tamil" value="Tamil" />
                  </Picker>
                  <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                </Col>
              </Row>
              <Text></Text>
              <Row >
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                <Col style={styles.popovercol8}>
                  <Picker
                    selectedValue={this.props.themename}
                    style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                    onValueChange={(itemValue) => this.changeTheme(itemValue)}
                  // onValueChange={(itemValue) => this.props.themechng()}
                  >
                    <Picker.Item label="Dark" value="dark" />
                    <Picker.Item label="Light" value="light" />

                  </Picker>
                  <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                </Col>
              </Row>

              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
              </Row>
              <Text></Text>
              <Row>
                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
              </Row>
            </ScrollView>
          </View>
        </Modal>


        <Footer>

          <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>
            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
              <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
            </Button>


            <Button style={styles.inactive} onPress={() => this.tab('port')}>
              <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
            </Button>

            <Button style={styles.active} onPress={() => this.tab('home')}>
              <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
              <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('search')}>
              <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('more')}>
              <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

              <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
            </Button>


          </FooterTab>
        </Footer>

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
    themename: state.themename
  }
}

function mapDispatchToProps(dispatch) {
  return {
    themechnglight: () => dispatch({ type: 'light' }),
    themechngdark: () => dispatch({ type: 'dark' }),
  }
}

const styles = StyleSheet.create({
  showNoData: { display: 'flex', marginTop: 80, },
  nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center' },
  nodatadark: { color: 'white', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center' },
  mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
  mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
  lightdate: { color: '#132144', textAlign: 'right', marginTop: 10, fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, marginRight: 5 },
  darkdate: { color: 'white', textAlign: 'right', marginTop: 10, fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, marginRight: 5 },
  statuslight: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: '#132144' },
  statusdark: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: 'white' },
  lightheadertext: { marginLeft: 15, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkheadertext: { marginLeft: 15, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkheadertext1: { alignSelf: 'flex-end', color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightheadertext2: { alignSelf: 'flex-end', marginRight: 15, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkheadertext2: { alignSelf: 'flex-end', marginRight: 15, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  show: { display: 'flex' },
  hide: { display: 'none' },
  namepos: { marginLeft: 5, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  nameneg: { marginLeft: 5, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },

  lightheader4: { width: '57%', height: 40 },
  darkheader4: { width: '57%', height: 40 },

  listdarkcolsensex1: { width: '90%', height: 88, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
  listdarkcolsensex2: { width: '10%', height: 88, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5, },
  listdarkcolsensex3: { width: '33%', height: 88, backgroundColor: '#1a1f1f', },
  listlightcolsensex1: { width: '90%', height: 88, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
  listlightcolsensex2: { width: '10%', height: 88, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5, },
  listlightcolsensex3: { width: '33%', height: 88, backgroundColor: 'white', },


  neglistdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  neglistdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  neglistdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#f54845', borderBottomWidth: 1.5 },

  neglistlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  neglistlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
  neglistlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', borderBottomColor: '#f54845', borderBottomWidth: 1.5 },

  perturnlight: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13, },
  perturndark: { alignSelf: 'flex-end', marginRight: 10, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', marginTop: 2, fontSize: 13, },
  valuepos: { marginRight: 10, marginTop: 9, fontFamily: 'Arial', fontWeight: 'bold', borderRadius: 4, alignSelf: 'flex-end', color: 'white' },
  valueneg: { marginRight: 10, marginTop: 9, fontFamily: 'Arial', fontWeight: 'bold', borderRadius: 4, alignSelf: 'flex-end', color: '#f54845' },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
  inactive: { padding: 10, },
  tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold' },
  tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold' },
  tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold' },
  titlelight: { color: '#132144', fontSize: 17, fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 10, marginTop: 3 },
  titledark: { color: 'white', fontSize: 17, fontFamily: 'Arial', fontWeight: 'bold', marginLeft: 10, marginTop: 3 },
  lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
  darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
  lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
  pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },
  dropicon: { marginLeft: 88, bottom: 30 },
  footer1: { width: 26, height: 28, },
  footer2: { width: 26, height: 27, },
  footer3: { width: 23, height: 24, bottom: 2 },
  modalheight: { height: '100%' },
  scrollview1: { bottom: 30 },
  settingrow: { marginTop: '25%' },
  popovercol1: { width: '20%', height: 50, },
  popovercol2: { width: '80%', height: 50, },
  popovercol3: { width: '40%', height: 50, },
  popovercol4: { width: '5%', height: 50, },
  popovercol5: { width: '35%', height: 50, },
  popovercol6: { width: '100%', height: 40, },
  popovercol7: { width: '50%', height: 40, },
  popovercol8: { width: '30%', height: 50, },
  picker: { height: 30, bottom: 5, width: '100%', },
  popovericon1: { textAlign: 'center' },
  popovericon2: { textAlign: 'right' },
  popovericon3: { textAlign: 'left' },
  menucol1: { width: '67%', height: 40 },
  menucol2: { width: '15%', height: 40 },
  menucol3: { width: '75%', height: 40 },
  menucol4: { width: '25%', height: 40 },
  closeicon: { marginLeft: 10 },
  usericon: { marginTop: 3, textAlign: 'right' },
  settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
  scrollview: { top: '8%' },
  showequity: { display: 'flex', marginTop: 20 },
  equatyimage: { width: 22, height: 23, margin: 12 },
  list: { width: '100%', height: 58, backgroundColor: 'white', borderRadius: 5, },
  listdark: { width: '100%', height: 58, backgroundColor: '#1a1f1f', borderRadius: 5, },
  scrollloader: { marginTop: 20 },
  lighttext: { marginLeft: 9, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular' },
  darktext: { marginLeft: 9, marginTop: 9, color: 'white', fontFamily: 'SegoeProDisplay-Regular' },
  lighttextt1: { marginLeft: 9, marginTop: 1, color: '#aaafba', fontFamily: 'SegoeProDisplay-Regular' },
  darktextt1: { marginLeft: 9, marginTop: 1, color: 'white', fontFamily: 'SegoeProDisplay-Regular' },
  flatlist: { height: '100%', bottom: 12 },
  flatlistrow: { marginLeft: 16, marginRight: 16, marginTop: 20, },
  buttonlight: { marginTop: 30, height: 0, marginLeft: 4, backgroundColor: '#f6f7f9' },
  buttondark: { marginTop: 30, height: 0, marginLeft: 4, backgroundColor: '#0b0b0b' },
  gaplight: { backgroundColor: '#f6f7f9', width: 10, height: 70, bottom: 10 },
  gapdark: { backgroundColor: '#0b0b0b', width: 10, height: 70, bottom: 10 },
  btnViewActive: { borderRadius: 5, backgroundColor: '#2087c9', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactive: { borderRadius: 5, backgroundColor: 'white', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactiveLast: { borderRadius: 5, backgroundColor: 'white', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactivedark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactiveLastdark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewactiveLast: { borderRadius: 5, backgroundColor: '#2087c9', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  textActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textinActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
  textinActiveDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
  textinActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
  textinActivelasDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },


  lightsearch: { width: '15%', height: 45, backgroundColor: 'white', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  darksearch: { width: '15%', height: 45, backgroundColor: '#1a1f1f', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  lightsearch1: { width: '70%', height: 45, backgroundColor: 'white', },
  darksearch1: { width: '70%', height: 45, backgroundColor: '#1a1f1f', },
  lightsearchtext: { fontSize: 15, top: 2, fontFamily: 'SegoeProDisplay-Regular', color: '#132144', borderBottomColor: 'grey', borderBottomWidth: 1 },
  darksearchtext: { fontSize: 15, top: 2, fontFamily: 'SegoeProDisplay-Regular', color: 'white', borderBottomColor: 'white', borderBottomWidth: 1 },
  closeiconclass: { textAlign: 'center', marginTop: 9 },
  close: { width: '15%', height: 45, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  closedark: { width: '15%', height: 45, backgroundColor: '#1a1f1f', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  scrollrow: { marginLeft: 16, marginRight: 16 },
  menurow: { marginLeft: 20, marginTop: 10 },
  menuicon: { marginTop: 6 },
  viewrow: { marginLeft: 3, marginTop: 45, },
  loader: { marginTop: 60 },
  flatlistHeight: { marginTop: 60, height: '100%' },
  flatcol1: { width: '100%', height: 58 },
  flatcol2: { width: '6.5%', height: 58 },
  mainrow1: { margin: 5 },
  saveIconRow: { margin: 5, marginTop: 40 },
  mainrow2: { marginLeft: 16, marginRight: 16, marginTop: '12%' },
  maincol1: { width: '15%', height: 30 },
  maincol2: { width: '55%', height: 30, },
  maincol3: { width: '20%', height: 30, },
  maincol4: { width: '10%', height: 30, },
  maincol5: { width: '100%', height: 40, },
  google: { width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 },
  back: { textAlign: 'left', marginLeft: 13 },
  lightheader1: { width: '30%', height: 40 },
  darkheader1: { width: '30%', height: 40 },
  lightheader2: { width: '25%', height: 40 },
  darkheader2: { width: '25%', height: 40 },
  lightheader3: { width: '45%', height: 40 },
  darkheader3: { width: '45%', height: 40 },
  lighttext1: { marginLeft: 5, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darktext1: { marginLeft: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  portwhitename: { margin: 10, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  portdarkname: { margin: 10, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightsensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 10, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 10, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  lightsensexsell: { alignSelf: 'flex-end', marginRight: 7, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
  darksensexsell: { alignSelf: 'flex-end', marginRight: 7, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
  posltp: { alignSelf: 'flex-end', marginRight: 7, marginTop: 10, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold', },
  negkltp: { alignSelf: 'flex-end', marginRight: 7, marginTop: 10, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold', }
})

// export default Indices;
export default connect(mapStateToProps, mapDispatchToProps)(EditWatch);