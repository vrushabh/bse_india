import React, { Component } from 'react';
import { View, ToastAndroid, Animated, Image, Easing, Modal, CheckBox, Linking, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Iconoct from 'react-native-vector-icons/Octicons';
import moment from 'moment-timezone';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import { connect } from 'react-redux';
import { InterstitialAd, RewardedAd, BannerAd, TestIds, BannerAdSize } from '@react-native-firebase/admob';
import Ripple from 'react-native-material-ripple';
import { Tab, Tabs, Container, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
const height = Dimensions.get('window').height;
HEADER_MAX_HEIGHT = 200;
HEADER_MIN_HEIGHT = 150;
TEXT_MAX_HEIGHT = 40;
TEXT_MIN_HEIGHT = 25;
class Demo extends Component {

    static navigationOptions = { header: null };

    constructor(props) {

        super(props);
        this.state = {
            scrolly: new Animated.Value(0),

            sidemenu: false,
            showedit: false,
            settingshow: false,

            equity: true,
            sensex: false,
            sme: false,
            derivative: false,
            currency: false,
            commodity: false,
            ird: false,
            etf: false,
            debt: false,
            corporate: false,
            ipf: false,
            iconname: 'ios-arrow-down',
            iconnamesensex: 'ios-arrow-forward',
            iconnamesme: 'ios-arrow-forward',
            iconnamederivative: 'ios-arrow-forward',
            iconnamecurrency: 'ios-arrow-forward',
            iconnamecommodity: 'ios-arrow-forward',
            iconnameird: 'ios-arrow-forward',
            iconnameetf: 'ios-arrow-forward',
            iconnamedebt: 'ios-arrow-forward',
            iconnamecorporate: 'ios-arrow-forward',
            iconnameipf: 'ios-arrow-forward',
            tabsValue: 0,
            val: 0
        };
        setTimeout(() => { SplashScreen.hide() }, 4000);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }
    componentWillMount() {
        // this.tabval = this.props.navigation.getParam('current', '');

        // this.setState({ tabsValue: this.tabval });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;

    }
    gotoEnd = () => {
        this.scrollView.scrollToEnd({ animated: true });

    }
    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            //this.setState({ sidemenu: true })
        }

    }
    closesidemenu() {
        this.setState({ sidemenu: false });
    }
    pgscroll(i) {
        this.setState({ tabsValue: i })
    }

    stock() {
        this.viewPager.setPage(0);

    }
    announcement() {
        this.viewPager.setPage(1);

    }
    result() {
        this.viewPager.setPage(2);

    }
    share() {
        this.viewPager.setPage(3);

    }
    corp() {
        this.viewPager.setPage(4);

    }
    voting() {
        this.viewPager.setPage(5);

    }
    bulk() {
        this.viewPager.setPage(6);

    }
    weeklow() {
        this.viewPager.setPage(7);

    }

    render() {
        const headerheight = this.state.scrolly.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp'
        })

        const textheight = this.state.scrolly.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [TEXT_MAX_HEIGHT, TEXT_MIN_HEIGHT],
            extrapolate: 'clamp'
        })

        const headerzindex = this.state.scrolly.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        })



        return (
            <View style={this.props.theme == '0' ? { flex: 1, backgroundColor: '#f6f7f9' } : { flex: 1, backgroundColor: '#0b0b0b' }}>
                <Header hasTabs androidStatusBarColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} style={this.props.theme == '0' ? styles.headerLight : styles.headerLight} >

                </Header>
                <Animated.View style={this.props.theme == '0' ? { position: "absolute", top: 0, left: 0, right: 0, backgroundColor: '#f6f7f9', height: headerheight, zIndex: headerzindex } : { position: "absolute", top: 0, left: 0, right: 0, backgroundColor: '#0b0b0b', height: headerheight, zIndex: headerzindex }}>

                    <Row style={styles.mainrow1}>
                        <Col style={styles.maincol1}></Col>
                        <Col style={styles.maincol2}>

                        </Col>
                        <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.google} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                        <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={styles.popovericon1} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                    </Row>

                    <Row style={styles.mainrow1}>
                        <Col style={styles.maincol4}></Col>
                        <Col style={styles.maincol6}><Text style={this.props.theme == '0' ?  styles.namelight : styles.namedark}>Bajaj Finance Limited</Text></Col>
                        <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} ><IconEnt name="pluscircleo" style={styles.popovericonnew} size={20} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                    </Row>
                    <Row style={styles.mainrow1}>
                        <Col style={styles.maincol5}>
                            <Animated.Text style={this.props.theme == '0' ? { fontFamily: 'Arial', fontWeight: 'bold', bottom: 5, fontSize: textheight, textAlign: 'center', color: '#132144', } : { fontFamily: 'Arial', fontWeight: 'bold', bottom: 5, fontSize: textheight, textAlign: 'center', color: 'white', }}>4,985.05</Animated.Text>
                        </Col>

                    </Row>
                    <Row style={styles.mainrow1}>
                        <Col style={styles.maincol5}>
                            <Animated.Text style={{ fontFamily: 'Arial', fontWeight: 'bold', fontSize: 18, textAlign: 'center', color: '#19cf3e', marginTop: 2, }}>+ 392.24 +8.90</Animated.Text>
                        </Col>

                    </Row>
                    <Row style={styles.mainrow1}>
                        <Col style={styles.maincol5}>
                            <Text style={styles.date}>26 Feb 2020 | 16.00 | Close</Text>
                        </Col>

                    </Row>
                </Animated.View>


                {/* <ScrollView style={{ flex: 1 }} scrollEventThrottle={16} onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrolly } } }])}>
                    <View style={{ marginTop: HEADER_MAX_HEIGHT + 20, height: 2000, backgroundColor: 'yellow' }}>
                        <Text>Hiii</Text>

                    </View>



                </ScrollView> */}
                <ScrollView ref={scrollView => this.scrollView = scrollView} style={this.props.theme == '0' ? { backgroundColor: '#f6f7f9', marginTop: 130 } : { backgroundColor: '#0b0b0b', marginTop: 130 }} scrollEventThrottle={16} onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrolly } } }])}>
                    <View style={styles.row1}>
                        <View style={this.props.theme == '0' ? styles.maincol7 : styles.darkmaincol7}><Text style={this.props.theme == '0' ? styles.comname1 : styles.darkcomname1}>BAJAJFINANCE</Text><Text style={styles.comname2}>Security ID</Text></View>
                        <View style={this.props.theme == '0' ? styles.maincol8 : styles.darkmaincol8}><Text style={this.props.theme == '0' ? styles.comname1 : styles.darkcomname1}>500034</Text><Text style={styles.comname2}>Security Code</Text></View>
                        <View style={this.props.theme == '0' ? styles.maincol9 : styles.darkmaincol9}><Text style={this.props.theme == '0' ? styles.comname1 : styles.darkcomname1}>Listed</Text><Text style={styles.comname2}>Category</Text></View>
                    </View>
                    <View>
                        <TouchableOpacity activeOpacity={.5} onPress={this.gotoEnd}><IconEvil name="chevron-up" style={styles.popovericon1} size={50} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity>
                    </View>

                    <Row>
                        <Col style={styles.bannercol1}></Col>
                        <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                    </Row>

                    <ScrollView horizontal ref={scrollView => this.scrollView = scrollView} style={this.props.theme == '0' ? styles.buttonlight : styles.buttondark} showsHorizontalScrollIndicator={false} >

                        <View style={styles.view}></View>
                        <View style={this.state.tabsValue == 0 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.stock()}><Text style={this.state.tabsValue == 0 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Stock Trading </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>
                        <View style={this.state.tabsValue == 1 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.announcement()}><Text style={this.state.tabsValue == 1 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Announcement </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        <View style={this.state.tabsValue == 2 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.result()}><Text style={this.state.tabsValue == 2 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Result </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        <View style={this.state.tabsValue == 3 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.share()}><Text style={this.state.tabsValue == 3 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Shareholding </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        <View style={this.state.tabsValue == 4 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.corp()}><Text style={this.state.tabsValue == 4 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Corp Actions </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        <View style={this.state.tabsValue == 5 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voting()}><Text style={this.state.tabsValue == 5 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Voting Result </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        <View style={this.state.tabsValue == 6 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.bulk()}><Text style={this.state.tabsValue == 6 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Bulk/Block </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                        <View style={this.state.tabsValue == 7 ? styles.btnViewactiveLast : this.props.theme == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.weeklow()}><Text style={this.state.tabsValue == 7 ? styles.textActivelast : this.props.theme == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Technical </Text></TouchableOpacity></View>
                        <View style={this.props.theme == '0' ? styles.gaplight : styles.gapdark}></View>

                    </ScrollView>


                    <ViewPager
                        style={{ height: height - 255 }}
                        initialPage={this.state.tabsValue}
                        ref={viewPager => { this.viewPager = viewPager; }}
                        onPageScroll={(i) => this.pgscroll(i.position)}
                    >
                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                        <View style={this.props.theme == 0 ? styles.lightscroll : styles.darkscroll}>

                            <View style={this.props.theme == '0' ? styles.maincol12 : styles.darkmaincol12}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Previous Close</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4898.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Open</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>4894.40</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>High</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Low</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5789.40</Text></Col>
                                </Row>
                            </View>

                            <View style={this.props.theme == '0' ? styles.maincol13 : styles.darkmaincol13}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Wtd.Avg Price</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,009.58</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Traded Value(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>9,373.00</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Upper Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,9047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>TTQ(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.94</Text></Col>
                                </Row>

                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Lower Ckt</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>5,8047.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>2W Avg Q(L)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>1.14</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap Full (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>308,901.25</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>MCap FF (Cr.)</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>135,916.59</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>Face Value</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>2.00</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={this.props.theme == '0' ? styles.prv : styles.darkprv}>ISIN</Text><Text style={this.props.theme == '0' ? styles.prvvalue : styles.darkprvvalue}>INE296A01024</Text></Col>
                                </Row>
                            </View>

                            <Text style={this.props.theme == '0' ? styles.title : styles.darktitle}>Market Depth</Text>

                            <View style={this.props.theme == '0' ? styles.maincol14 : styles.darkmaincol14}>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol11 : styles.darkmaincol11}><Text style={styles.buy}>Buy</Text></Col>
                                    <Col style={styles.maincol3}><Text style={styles.sell}>Sell</Text></Col>
                                    <Col style={styles.maincol10}></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Quantity</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Price</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.15</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>15</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>100</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5043.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>100</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>23</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.30</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>23</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>103</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.20</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>103</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>3</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>5042.10</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>3</Text></Col>
                                </Row>
                                <Row style={styles.mainrow5}>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>183659</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol17title : styles.darkmaincol17title}></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol15title : styles.darkmaincol15title}><Text style={this.props.theme == '0' ? styles.quantity : styles.darkquantity}>Total</Text></Col>
                                    <Col style={this.props.theme == '0' ? styles.maincol16title : styles.darkmaincol16title}><Text style={this.props.theme == '0' ? styles.price : styles.darkprice}>183659</Text></Col>
                                </Row>

                            </View>
                        </View>

                    </ViewPager>
                    <Row>
                        <Col style={styles.bannercol1}></Col>
                        <Col style={styles.bannercol2}><BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER} /></Col>
                    </Row>
                    <Text></Text>


                </ScrollView>

                <Modal
                    animationType="slide"
                    transparent={false}
                    style={styles.modalheight}
                    visible={this.state.sidemenu}
                    onRequestClose={() => {
                        this.setState({ sidemenu: false });
                    }}
                >
                    <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
                        <Row style={styles.menurow}>
                            <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                            <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                            <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                        </Row>

                        <ScrollView style={styles.scrollview}>

                            <Text></Text>
                            <View style={{}}>
                                <Row style={styles.scrollrow}>
                                    <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                                    <Col style={styles.popovercol7}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.equity == false ? styles.hide : styles.show}>
                                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                                    <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.sensex == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.settingpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                                    <Ripple onPress={() => this.settingpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                                    <Ripple onPress={() => this.settingpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                                    <Ripple onPress={() => this.settingpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.sme == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                    <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                                    <Text></Text>
                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.derivative == false ? styles.hide : styles.show}>

                                    <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                    <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.currency == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                    <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.commodity == false ? styles.hide : styles.show}>

                                    <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                    <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.ird == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                    <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.etf == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                                    <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.debt == false ? styles.hide : styles.show}>

                                    <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                                    <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                                    <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                                    <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                                    <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                                    <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                                    <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.corporate == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                                    <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                                    <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                                    <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                                    <Text></Text>

                                </View>
                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                                    <Col style={styles.menucol4}></Col>
                                </Row>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={styles.scrollrow}>
                                    <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                                    <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                </Row>
                                <View style={this.state.ipf == false ? styles.hide : styles.show}>
                                    <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                                    <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                                    <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                                    <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                                    <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                                    <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                                    <Text></Text>

                                </View>

                                <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                                <Text></Text>
                                <Text></Text>
                                <Text></Text>
                                <Text></Text>
                                {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
                            </View>
                        </ScrollView>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.settingshow}
                    style={styles.modalheight}
                    onRequestClose={() => {
                        this.setState({ settingshow: false });
                        this.setState({ sidemenu: false });

                    }}
                >
                    <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

                        <ScrollView style={styles.scrollview1}>
                            <Row style={styles.settingrow}>
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                                <Col style={styles.popovercol8}>
                                    <Picker
                                        selectedValue={this.state.selectlan}
                                        style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                                        onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                                    >
                                        <Picker.Item label="English" value="English" />
                                        <Picker.Item label="Hindi" value="Hindi" />
                                        <Picker.Item label="Marathi" value="Marathi" />
                                        <Picker.Item label="Gujrati" value="Gujrati" />
                                        <Picker.Item label="Bengali" value="Bengali" />
                                        <Picker.Item label="Malayalam" value="Malayalam" />
                                        <Picker.Item label="Oriya" value="Oriya" />
                                        <Picker.Item label="Tamil" value="Tamil" />
                                    </Picker>
                                    <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                                </Col>
                            </Row>
                            <Text></Text>
                            <Row >
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                                <Col style={styles.popovercol8}>
                                    <Picker
                                        selectedValue={this.props.themename}
                                        style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                                        onValueChange={(itemValue) => this.changeTheme(itemValue)}
                                    // onValueChange={(itemValue) => this.props.themechng()}
                                    >
                                        <Picker.Item label="Dark" value="dark" />
                                        <Picker.Item label="Light" value="light" />

                                    </Picker>
                                    <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropicon} size={22} color={'white'} />

                                </Col>
                            </Row>

                            <Text></Text>
                            <Row>
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
                            </Row>
                            <Text></Text>
                            <Row>
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
                            </Row>
                            <Text></Text>
                            <Row>
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
                            </Row>
                            <Text></Text>
                            <Row>
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
                            </Row>
                            <Text></Text>
                            <Row>
                                <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
                            </Row>
                        </ScrollView>
                    </View>
                </Modal>
                <Footer>

                    <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>

                        <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                            <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
                            <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                        </Button>


                        <Button style={styles.inactive} onPress={() => this.tab('port')}>
                            <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

                            <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                        </Button>

                        <Button style={styles.active} onPress={() => this.tab('home')}>
                            <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
                            <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                        </Button>
                        <Button style={styles.inactive} onPress={() => this.tab('search')}>
                            <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
                            <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                        </Button>
                        <Button style={styles.inactive} onPress={() => this.tab('more')}>
                            <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

                            <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                        </Button>


                    </FooterTab>
                </Footer>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        theme: state.theme,
        themename: state.themename
    }
}

function mapDispatchToProps(dispatch) {
    return {
        themechnglight: () => dispatch({ type: 'light' }),
        themechngdark: () => dispatch({ type: 'dark' }),
    }
}
const styles = StyleSheet.create({
    viewDark: { backgroundColor: '#0b0b0b', },
    viewLight: { backgroundColor: '#f6f7f9', },
    mainrow1: { margin: 5 },
    maincol1: { width: '15%', height: 30 },
    maincol2: { width: '55%', height: 30, },
    maincol3: { width: '20%', height: 30, },
    maincol10: { width: '30%', height: 30, },
    maincol4: { width: '10%', height: 30, },
    maincol5: { width: '100%', height: 50, },
    maincol6: { width: '80%', height: 40, },
    maincol7: { width: '35%', height: 70, backgroundColor: '#f6f7f9', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
    maincol8: { width: '35%', height: 70, backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
    maincol9: { width: '30%', height: 70, backgroundColor: '#f6f7f9', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomRightRadius: 5, borderTopRightRadius: 5, },
    popovericon1: { textAlign: 'center' },
    popovericonnew: { textAlign: 'center', marginTop: 5, },
    popovericon: { textAlign: 'center', },
    namedark: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 19, textAlign: 'center', color: 'white' },
    namelight: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 19, textAlign: 'center', color: '#132144' },
    darkvalue: { fontFamily: 'Arial', fontWeight: 'bold', bottom: 10, fontSize: 40, textAlign: 'center', color: 'white' },
    change: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 18, textAlign: 'center', color: '#19cf3e', bottom: 5, },
    date: { fontFamily: 'Arial', fontWeight: 'bold', bottom: 5, fontSize: 14, textAlign: 'center', color: '#aaafba' },
    google: { width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 },
    show: { display: 'flex' },
    hide: { display: 'none' },





    lightscroll: { backgroundColor: '#f6f7f9' },
    darkscroll: { backgroundColor: '#0b0b0b', },

    mainrow5: { margin: 10, },
    maincol11: { width: '50%', height: 30, backgroundColor: 'white' },
    darkmaincol11: { width: '50%', height: 30, backgroundColor: '#1a1f1f' },
    maincol15title: { width: '20%', height: 30, backgroundColor: 'white', borderBottomColor: 'grey', borderBottomWidth: 1.5 },
    maincol16title: { width: '25%', height: 30, backgroundColor: 'white', borderBottomColor: 'grey', borderBottomWidth: 1.5 },
    maincol17title: { width: '10%', height: 30, backgroundColor: 'white', borderBottomColor: 'grey', borderBottomWidth: 1.5 },

    darkmaincol15title: { width: '20%', height: 30, backgroundColor: '#1a1f1f', borderBottomColor: 'grey', borderBottomWidth: 1.5 },
    darkmaincol16title: { width: '25%', height: 30, backgroundColor: '#1a1f1f', borderBottomColor: 'grey', borderBottomWidth: 1.5 },
    darkmaincol17title: { width: '10%', height: 30, backgroundColor: '#1a1f1f', borderBottomColor: 'grey', borderBottomWidth: 1.5 },
    maincol12: { height: 130, backgroundColor: 'white', margin: 10, borderRadius: 5 },
    darkmaincol12: { height: 130, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
    maincol13: { height: 350, backgroundColor: 'white', margin: 10, borderRadius: 5 },
    darkmaincol13: { height: 350, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
    maincol14: { height: 400, backgroundColor: 'white', margin: 10, borderRadius: 5 },
    darkmaincol14: { height: 400, backgroundColor: '#1a1f1f', margin: 10, borderRadius: 5 },
    darkmaincol7: { width: '35%', height: 70, backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
    darkmaincol8: { width: '35%', height: 70, backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
    darkmaincol9: { width: '30%', height: 70, backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14, borderBottomRightRadius: 5, borderTopRightRadius: 5, },

    row1: { flex: 1, flexDirection: 'row', margin: 10 },
    comname1: { textAlign: 'center', marginTop: 15, fontSize: 14, color: '#132144' },
    darkcomname1: { textAlign: 'center', marginTop: 15, fontSize: 14, color: 'white' },
    comname2: { textAlign: 'center', marginTop: 5, fontSize: 12, color: '#2087c9' },
    bannercol1: { width: '6%', height: 53, },
    bannercol2: { width: '80%', height: 53, },
    prv: { fontFamily: 'Arial', fontSize: 16, color: '#132144' },
    darkprv: { fontFamily: 'Arial', fontSize: 16, color: 'white' },
    buy: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 18, color: '#19cf3e' },
    sell: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 18, color: '#f54845', textAlign: 'center' },
    prvvalue: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, marginTop: 5, color: '#132144' },
    darkprvvalue: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, marginTop: 5, color: 'white' },
    title: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 20, color: '#132144', margin: 10 },
    darktitle: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 20, color: 'white', margin: 10 },
    quantity: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: '#132144', },
    darkquantity: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: 'white', },
    price: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: '#132144', textAlign: 'right' },
    darkprice: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 15, color: 'white', textAlign: 'right' },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold', },
    tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'Arial', fontWeight: 'bold', },
    tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'Arial', fontWeight: 'bold', },
    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
    inactive: { padding: 10, },
    footer1: { width: 26, height: 28, },
    footer2: { width: 26, height: 27, },
    footer3: { width: 23, height: 24, bottom: 2 },
    pickericon: { bottom: 50, marginLeft: 170 },
    modalheight: { height: '100%' },
    scrollview1: { bottom: 30 },
    settingrow: { marginTop: '25%' },
    popovercol1: { width: '20%', height: 50, },
    popovercol2: { width: '80%', height: 50, },
    popovercol3: { width: '40%', height: 50, },
    popovercol4: { width: '5%', height: 50, },
    popovercol5: { width: '35%', height: 50, },
    popovercol6: { width: '100%', height: 40, },
    popovercol7: { width: '50%', height: 40, },
    popovercol8: { width: '30%', height: 50, },
    pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
    pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },

    dropicon: { marginLeft: 88, bottom: 30 },

    closeicon: { marginLeft: 10 },
    usericon: { marginTop: 3, textAlign: 'right' },
    settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },


    viewrow: { marginLeft: 3, },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },


    menucol1: { width: '67%', height: 40 },
    menucol2: { width: '15%', height: 40 },
    menucol3: { width: '75%', height: 40 },
    menucol4: { width: '25%', height: 40 },
    show: { display: 'flex' },
    hide: { display: 'none' },
    scrollview: { top: '8%' },
    scrollrow: { marginLeft: 16, marginRight: 16 },
    scrollrow1: { marginLeft: 17, marginRight: 17, marginTop: 12, },
    menurow: { marginLeft: 20, marginTop: 10 },
    menuicon: { marginTop: 6 },
    lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    buttonlight: { height: 70, marginLeft: 4, backgroundColor: '#f6f7f9' },
    buttondark: { height: 70, marginLeft: 4, backgroundColor: '#0b0b0b' },
    view: { marginLeft: 15 },
    btnViewActive: { borderRadius: 5, backgroundColor: '#2087c9', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 20, },
    btnViewinactive: { borderRadius: 5, backgroundColor: 'white', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 20, },
    btnViewinactiveLast: { borderRadius: 5, backgroundColor: 'white', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 20, },
    btnViewinactivedark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 20, },
    btnViewinactiveLastdark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 20, },
    btnViewactiveLast: { borderRadius: 5, backgroundColor: '#2087c9', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 20, },
    textActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    textinActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
    textinActiveDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    textActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    textinActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 13 },
    textinActivelasDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'Arial', fontWeight: 'bold', color: 'white', fontSize: 13 },
    gaplight: { backgroundColor: '#f6f7f9', width: 10, height: 70, bottom: 10 },
    gapdark: { backgroundColor: '#0b0b0b', width: 10, height: 70, bottom: 10 },

});
export default connect(mapStateToProps, mapDispatchToProps)(Demo);