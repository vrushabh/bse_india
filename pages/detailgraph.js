import React, { Component } from 'react';
import { View, Text, BackHandler,StyleSheet,ActivityIndicator,processColor } from 'react-native';
import api from '../api.js';
import SplashScreen from 'react-native-splash-screen';
import { Row } from 'react-native-easy-grid';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import moment from 'moment';
import { LineChart } from 'react-native-charts-wrapper';
class DetailGraph extends Component {
    value = '';
    timedata = [];
    opendata = [];
    static navigationOptions = {

        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            time: '',
            title: '',
            value: '',
            data: [],
            loading: false,
            tmdata: ["1", "2", "3", "4", "5"],
            valuedata: [1, 2, 3, 4, 5],
            loading: false,
            chart1Zoom: { scaleX: 0, scaleY:0, xValue:0, yValue: 0},

        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setTimeout(() => { SplashScreen.hide() }, 4000);
    }
    componentWillMount() {
        this.setState({ loading: true });
        this.value = this.props.navigation.getParam('codevalue', '');
        api.detailgrp(this.value).then(res => {
            this.setState({ data: res.split('@')[1].split('#'), value: res.split('@')[0].split(',')[5], time: res.split('@')[0].split(',')[6], title: res.split('@')[0].split(',')[4] });
            console.log(this.state.data);
            for (let i = 1; i < this.state.data.length-1; i++) {
                this.opendata.push(parseInt(this.state.data[i].split(',')[2]));
                this.timedata.push(moment(this.state.data[i].split(',')[0]).format('hh:mm'));

                // if (i == 367 || i == 310 || i == 235 || i == 175 || i == 130 || i == 89 || i == 53 || i == 12 || i == 4 ) {
                //     this.opendata.push(parseInt(this.state.data[i].split(',')[2]));
                // }
                // else if (i == 365 || i == 309 || i == 233 || i == 174 || i == 88 || i == 20 || i == 3) {
                //     this.timedata.push(moment(this.state.data[i].split(',')[0]).format('hh:mm'));
                   
                // }

            }
            console.log(this.timedata);

            this.setState({ loading: false, tmdata: this.timedata.reverse(), valuedata: this.opendata.reverse(),  })
            
        });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;

    }
    back() {
        this.props.navigation.goBack(null);
        return true;
    }
    syncToChart2(event) {
        if (event.action == 'chartScaled' || event.action == 'chartTranslated') {
            let { scaleX, scaleY, centerX, centerY } = event
            this.setState({ ...this.state, chart2Zoom: { scaleX: scaleX, scaleY: scaleY, xValue: centerX, yValue: centerY } })
            console.log("sync chart2" +
                " to " + centerX + " " + centerY)
        }

        console.log(event)
    }
    render() {
        return (
            <View>

            <Row style={{ margin: 5, marginTop: 30, }}>
                <Col style={{ width: '40%', height: 40, }}><Text style={{ fontFamily: 'Arial', fontWeight: 'bold', color: '#132144', fontSize: 16, bottom: 3 }}>{this.state.title}</Text></Col>
                <Col style={{ width: '30%', height: 40, }}><Text style={{ textAlign: 'center', color: '#132144', }}>Time : {this.state.time}</Text></Col>
                <Col style={{ width: '30%', height: 40, }}><Text style={{ textAlign: 'right', color: '#132144', }}>Value : {this.state.value}</Text></Col>
            </Row>
            <Text></Text>
            <View style={this.state.loading == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={{}} color={'black'} />

            </View>
            <Text></Text>
            <LineChart style={this.state.loading == true ? styles.hide : styles.chart}
                data={{
                    dataSets: [{
                        label: "Open", values: this.state.valuedata, config: {
                            lineWidth: 1,
                            drawCircles: false,
                            drawFilled: true,
                            drawLabels: true,
                            drawValues: false,
                            fillColor: processColor('#0ee2ed'),
                            circleColor: processColor('blue'),
                            drawCircleHole: false,
                            circleRadius: 5,
                            colors: [processColor('#0ee2ed')],
                            fillAlpha: 100,
                            valueTextSize: 10,

                        },

                    }],
                }}
                xAxis={{
                    position: 'BOTTOM',
                    valueFormatter: this.state.tmdata,
                    granularityEnabled: true,
                    granularity: 1,
                    labelCount: 10,
                    drawAxisLine: true,
                    drawGridLines: true,
                    textSize: 13
                }}
                yAxis={{
                    right: { enabled: false },
                    left: {
                        enabled: true,
                        drawAxisLine: true,
                        drawGridLines: true,
                        textSize: 13
                    }
                }}
                legend={{
                    horizontalAlignment: "LEFT",
                    verticalAlignment: 'TOP',
                    enabled: false,
                    textSize: 15,
                    form: 'SQUARE',
                    formSize: 15,
                    xEntrySpace: 20,
                    yEntrySpace: 10,
                    formToTextSpace: 10,
                    wordWrapEnabled: true,
                    maxSizePercent: 0.5,
                }}
                animation={{ durationX: 2000 }}
                visibleRange={{ x: { min: 7, max: 7 } }}
                chartDescription={{ text: '' }}
                marker={{
                    enabled: true,
                    markerColor: processColor('#132144'), textSize: 15
                }}
               
                touchEnabled={true}
                dragEnabled={true}
                scaleEnabled={true}
                scaleXEnabled={false}
                scaleYEnabled={true}
                pinchZoom={true}
                zoom={this.state.chart1Zoom}

                ref="chart1"

                onChange={(event) => this.syncToChart2(event.nativeEvent)}
            />

        </View>
        );
    }
}
const styles = StyleSheet.create({
    chart: {
        width: '100%',
        height: '85%'
    },
    show: { display: 'flex', },

    hide: { display: 'none' },
});
export default DetailGraph;
