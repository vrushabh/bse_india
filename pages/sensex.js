import React, { Component } from 'react';
import { View, Image, Modal, ToastAndroid, CheckBox, Linking, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Tab, Tabs, Container, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Iconoct from 'react-native-vector-icons/Octicons';
import * as Animatable from 'react-native-animatable';
import Ripple from 'react-native-material-ripple';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { connect } from 'react-redux';
import api from '../api.js';
import CryptoJS from "react-native-crypto-js";

const keydt = 'BseIndiaApi@2020';
var tm = moment().tz("UTC").format('yyyy-MM-DD HH:mm:ss');
var data = tm;
console.log(tm);
var key = CryptoJS.enc.Latin1.parse(keydt);
var iv = CryptoJS.enc.Latin1.parse(keydt);
const encrypted = CryptoJS.AES.encrypt(
    data,
    key,
    {
        iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7
    });
import moment from 'moment-timezone';
import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
const height = Dimensions.get('window').height;

class Setting extends Component {
    static navigationOptions = { header: null };
    per = '';
    per1 = '';
    per2 = '';
    per3 = '';
    indicesId = '16';
    Sensex = [];
    TurnArray = [];
    turnlength = '';
    tabval = 0;
    themename = '';
    IndName = '';
    IndId = '';
    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            date: '',
            status: '',
            value: 1,
            loader: false,
            securityloader: false,
            overviewloader: false,
            turnoverloader: false,
            contriloader: false,
            isVisible: false,
            switchval: false,
            security: [],
            overview: [],
            Turn: [],
            contri: [],
            refreshing: false,
            sidemenu: false,
            settingshow: false,
            theme: 'dark',
            showedit: false,
            selectlan: 'English',
            indicesdata: [],
            indicesName: 'S&P BSE SENSEX',
            ltp: '',
            change: '',
            changeper: '',
            indicesDropdown: false,
            tabsValue: 0,
            initial: '',
            iconname: 'ios-arrow-down',
            iconnamesensex: 'ios-arrow-forward',
            iconnamesme: 'ios-arrow-forward',
            iconnamederivative: 'ios-arrow-forward',
            iconnamecurrency: 'ios-arrow-forward',
            iconnamecommodity: 'ios-arrow-forward',
            iconnameird: 'ios-arrow-forward',
            iconnameetf: 'ios-arrow-forward',
            iconnamedebt: 'ios-arrow-forward',
            iconnamecorporate: 'ios-arrow-forward',
            iconnameipf: 'ios-arrow-forward',
            ipf: false,
            equity: true,
            sensex: false,
            sme: false,
            derivative: false,
            currencytab: false,
            commoditytab: false,
            ird: false,
            etf: false,
            debt: false,
            corporate: false,
            nodatasensex: false,
            nodataoverview: false,
            nodataturn: false,
            nodatacontri: false,
            scrollleft: false,
            scrollicon: '',
            scrollmsg: ''

        };
        setTimeout(() => { SplashScreen.hide() }, 4000);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }
    componentDidMount() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            dt == null || dt == 'dark' ? this.props.themechngdark() : this.props.themechnglight();

        });

        AsyncStorage.getItem('scrollvalue').then((dt) => {
            console.log(dt);
            dt == null ? this.setState({ scrollleft: true }) : this.setState({ scrollleft: false });

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50)
        });

        this.IndName = this.props.navigation.getParam('name', '');
        this.IndId = this.props.navigation.getParam('id', '');
        if (this.IndName == null || this.IndId == null || this.IndName == '' || this.IndId == '') {
            this.sensexfun();
            AsyncStorage.getItem('language').then((lan) => {
                if (lan == null) {
                    this.setState({ langauge: 'en', selectlan: 'English' });
                    I18n.locale = "en";
                    this.indicesfun(this.state.langauge);
                    this.securitydatafun(this.indicesId, this.state.langauge);
                    this.overviewdatafun(this.indicesId, this.state.langauge);
                    this.turndatafun(this.indicesId, this.state.langauge);
                    this.contridatafun(this.indicesId, this.state.langauge);
                }
                else {
                    lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                    lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";

                    this.indicesfun(this.state.langauge);
                    this.securitydatafun(this.indicesId, this.state.langauge);
                    this.overviewdatafun(this.indicesId, this.state.langauge);
                    this.turndatafun(this.indicesId, this.state.langauge);
                    this.contridatafun(this.indicesId, this.state.langauge);
                }
            });


        }
        else {
            AsyncStorage.setItem('indicesname', this.IndName);
            AsyncStorage.setItem('indicesid', this.IndId);
            this.setState({ indicesName: this.IndName });
            this.sensexfun();
            AsyncStorage.getItem('language').then((lan) => {
                if (lan == null) {
                    this.setState({ langauge: 'en', selectlan: 'English' });
                    I18n.locale = "en";
                    this.indicesfun(this.state.langauge);
                    this.securitydatafun(this.IndId, this.state.langauge);
                    this.overviewdatafun(this.IndId, this.state.langauge);
                    this.turndatafun(this.IndId, this.state.langauge);
                    this.contridatafun(this.IndId, this.state.langauge);
                }
                else {
                    lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                    lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";

                    this.indicesfun(this.state.langauge);
                    this.securitydatafun(this.IndId, this.state.langauge);
                    this.overviewdatafun(this.IndId, this.state.langauge);
                    this.turndatafun(this.IndId, this.state.langauge);
                    this.contridatafun(this.IndId, this.state.langauge);
                }
            });

        }

    }

    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }
    componentWillMount() {
        this.tabval = this.props.navigation.getParam('current', '');
        // this.tabval = 3;
        // if(this.tabval == 3)
        // {


        // }
        this.tabval == 3 ? this.setState({ scrollicon: 'hand-o-left', scrollmsg: 'Scroll left to get more Data.', tabsValue: this.tabval }) : this.setState({ scrollicon: 'hand-o-right', scrollmsg: 'Scroll right to get more Data.', tabsValue: this.tabval });

        // this.setState({ tabsValue: this.tabval });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        AsyncStorage.removeItem('indicesname');
        AsyncStorage.removeItem('indicesId')

        this.props.navigation.goBack(null);
        return true;

    }
    back() {
        AsyncStorage.removeItem('indicesname');
        AsyncStorage.removeItem('indicesId')

        this.props.navigation.goBack(null);
        return true;
    }
    sensexfun() {
        api.equityDate(encrypted).then(res => {

            if (res.status == 'success') {
                res.data[0].Flag == '0' ? this.setState({ date: res.data[0].Dt, status: 'Open' }) : res.data[0].Flag == '1' ? this.setState({ date: res.data[0].Dt, status: 'Pre-Open' }) : res.data[0].Flag == '2' ? this.setState({ date: res.data[0].Dt, status: 'Close' }) : res.data[0].Flag == '3' ? this.setState({ date: res.data[0].Dt, status: 'Open' }) : '';
                // responsejson.split('@')[6] == '0' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : responsejson.split('@')[6] == '1' ? this.setState({ date: responsejson.split('@')[5], status: 'Pre-Open' }) : responsejson.split('@')[6] == '2' ? this.setState({ date: responsejson.split('@')[5], status: 'Close' }) : responsejson.split('@')[6] == '3' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : '';
                this.setState({ ltp: res.data[0].LTP });
                this.setState({ change: res.data[0].Change, changeper: res.data[0].ChangePer });
            }
            else{
                this.setState({ date: '', status: '',ltp:'',change:'',changeper:'' });
            }


            // responsejson.split('@')[6] == '0' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : responsejson.split('@')[6] == '1' ? this.setState({ date: responsejson.split('@')[5], status: 'Pre-Open' }) : responsejson.split('@')[6] == '2' ? this.setState({ date: responsejson.split('@')[5], status: 'Close' }) : responsejson.split('@')[6] == '3' ? this.setState({ date: responsejson.split('@')[5], status: 'Open' }) : '';
            // this.setState({ ltp: responsejson.split('@')[1] });
            // this.setState({ change: responsejson.split('@')[2], changeper: responsejson.split('@')[3] });
        });
    }
    indicesfun(lan) {
        api.indicesDropDown(lan).then(res => {
            this.setState({ indicesdata: res });

        });
    }
    securitydatafun(id, lan) {
        this.setState({ securityloader: true, security: '' });

        api.security(id, lan, encrypted).then(res => {
            console.log(res);
            if (res.status == "success") {
                this.setState({ security: res.data, refreshing: false, securityloader: false })
            }
            else {
                this.setState({ nodatasensex: true, refreshing: false, securityloader: false });

            }
        });

    }
    overviewdatafun(id, lan) {
        this.setState({ overviewloader: true, overview: '' });
        api.overview1(id, encrypted).then(res1 => {
            api.overview2(id, lan, encrypted).then(res2 => {
                console.log(res1);
                console.log(res2);
                if (res1 != null && res2 != null) {

                    this.Sensex = [
                        { 'no': '1', 'prv': res1[1], 'open': res1[3], 'high': res1[5], 'low': res1[7], },
                        { 'no': '2', 'full': res1[9], 'free': res1[11], },
                        { 'no': '3', 'advance': res2[0].split('@')[1], 'advanceTO': res2[0].split('@')[2], 'decline': res2[1].split('@')[1], 'declineTO': res2[1].split('@')[2], 'unchanged': res2[2].split('@')[1], 'unchangedTO': res2[2].split('@')[2], 'nottraded': res2[3].split('@')[1], 'nottradedTO': res2[3].split('@')[2], 'total': res2[4].split('@')[1], 'totalTO': res2[4].split('@')[2] },
                        { 'no': '4', 'high': res1[13], 'highdate': res1[14], 'low': res1[16], 'lowdate': res1[17], 'alltimehigh': res1[19], 'alltimehighdate': res1[20], 'alltimelow': res1[22], 'alltimelowdate': res1[23], },
                        { 'no': '5', 'pe': res1[25], 'pb': res1[27], 'divided': res1[29].substring(0, 4), 'value': '57.45 +64.34', 'ltp': '2345.54', 'buy': '6545.00(4)', 'sell': '2131.00(4)', 'trd': '40,337', 'status': 'pos', 'name': 'INDUSDINBANK', },
                    ]
                    this.setState({ overview: this.Sensex, refreshing: false, overviewloader: false })
                }
                else {
                    this.setState({ nodataoverview: true, refreshing: false, overviewloader: false });

                }
            });
        });
    }
    turndatafun(id, lan) {
        this.setState({ turnoverloader: true, Turn: '' });
        api.sensexTurnover(id, lan, encrypted).then(res => {
            console.log(res);
            if (res != null) {
                // this.turnlength = res.length;

                this.setState({ Turn: res, refreshing: false, turnoverloader: false })
            }
            else {
                this.setState({ nodataturn: true, refreshing: false, turnoverloader: false });

            }

        });

    }
    contridatafun(id, lan) {
        this.setState({ contriloader: true, contri: '' });
        api.contribution(id, lan, encrypted).then(res => {
            console.log(res);
            if (res.status == "success") {

                this.setState({ contri: res.ContrbutionwiseData, refreshing: false, contriloader: false })
            }
            else {
                this.setState({ nodatacontri: true, refreshing: false, contriloader: false });

            }
        });

    }
    onRefresh() {
        this.setState({ refreshing: true });
        this.refresh();
    }
    refresh() {
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
                "Application requires Network to proceed",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50
            );
        });
        this.sensexfun();

        AsyncStorage.getItem('indicesname').then((dt) => {
            AsyncStorage.getItem('indicesid').then((dtid) => {
                if (dt == 'S&P BSE SENSEX' || dt == null) {

                    AsyncStorage.getItem('language').then((lan) => {
                        if (lan == null) {
                            this.setState({ langauge: 'en', selectlan: 'English' });
                            I18n.locale = "en";
                            this.securitydatafun(this.indicesId, this.state.langauge);
                            this.overviewdatafun(this.indicesId, this.state.langauge);
                            this.turndatafun(this.indicesId, this.state.langauge);
                            this.contridatafun(this.indicesId, this.state.langauge);
                        }
                        else {
                            lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                            lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
                            this.securitydatafun(this.indicesId, this.state.langauge);
                            this.overviewdatafun(this.indicesId, this.state.langauge);
                            this.turndatafun(this.indicesId, this.state.langauge);
                            this.contridatafun(this.indicesId, this.state.langauge);
                        }
                    });
                }
                else {
                    this.setState({ indicesName: dt });

                    AsyncStorage.getItem('language').then((lan) => {
                        if (lan == null) {
                            this.setState({ langauge: 'en', selectlan: 'English' });
                            I18n.locale = "en";
                            this.securitydatafun(dtid, this.state.langauge);
                            this.overviewdatafun(dtid, this.state.langauge);
                            this.turndatafun(dtid, this.state.langauge);
                            this.contridatafun(dtid, this.state.langauge);
                        }
                        else {
                            lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                            lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
                            this.securitydatafun(dtid, this.state.langauge);
                            this.overviewdatafun(dtid, this.state.langauge);
                            this.turndatafun(dtid, this.state.langauge);
                            this.contridatafun(dtid, this.state.langauge);
                        }
                    });


                }

            });
        });

    }
    value(val) {
        var no = '' + val;
        if (no.length == 5) {
            return no.substring(0, 2) + ',' + no.substring(2);
        }
        else if (no.length == 4) {
            return no.substring(0, 1) + ',' + no.substring(1);
        }
        else if (no.length == 3) {
            return no;
        }
        else if (no.length == 6) {
            return no.substring(0, 3) + ',' + no.substring(3);
        }
        else if (no.length == 7) {
            return no.substring(0, 3) + ',' + no.substring(3, 5) + ',' + no.substring(5);
        }
    }
    chng(per1, per2, per3) {

        if (per1 > 0) {
            return '+' + per1 + '  ' + '+' + per2 + ' %';
        }
        else if (per1 < 0) {
            return per1 + ' ' + per2 + ' %';
        }
        else if (per3 > 0) {
            return '+' + per3;
        }
        else if (per3 < 0) {
            return per3;
        }
        else if (per3 == 0.00) {
            return per3;
        }

    }
    googleassistant() {
        // Linking.openURL('https://assistant.google.com/explore');
        AsyncStorage.clear();

    }
    selectedValue(name, id) {
        this.TurnArray = [];
        this.setState({ loader: true, Turn: [], indicesDropdown: false });

        AsyncStorage.setItem('indicesname', name);
        AsyncStorage.setItem('indicesid', id);
        this.setState({ indicesName: name });
        AsyncStorage.getItem('language').then((lan) => {
            if (lan == null) {
                this.setState({ langauge: 'en', selectlan: 'English' });
                I18n.locale = "en";
                this.overviewdatafun(id, this.state.langauge);
                this.turndatafun(id, this.state.langauge);
                this.securitydatafun(id, this.state.langauge);
                this.contridatafun(id, this.state.langauge);
            }
            else {
                lan == 'English' ? this.setState({ langauge: "en", selectlan: 'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn", selectlan: 'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr", selectlan: 'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj", selectlan: 'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn", selectlan: 'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml", selectlan: 'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or", selectlan: 'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml", selectlan: 'Tamil' }) : this.setState({ langauge: "en", selectlan: 'English' });
                lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
                this.overviewdatafun(id, this.state.langauge);
                this.turndatafun(id, this.state.langauge);
                this.securitydatafun(id, this.state.langauge);
                this.contridatafun(id, this.state.langauge);
            }
        });


    }

    closesidemenu() {
        this.setState({ sidemenu: false });
    }

    indices() {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('indices');
    }

    home() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('maindark');

    }

    equitypage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('equity', { 'current': val });

    }
    derivativepage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('derivativetab', { 'current': val });

    }
    currencypage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('currencytab', { 'current': val });

    }
    commoditypage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('commoditytab', { 'current': val });

    }
    irdpage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('ird', { 'current': val });

    }
    smepage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('sme', { 'current': val });
    }
    etfpage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('etf', { 'current': val });
    }
    debtpage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('debt', { 'current': val });
    }

    corporatepage(val) {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('corporate', { 'current': val });
    }
    marketstatic() {

        this.setState({ sidemenu: false });
        this.props.navigation.navigate('marketstatic');
    }

    marketturn() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('marketturn');
    }
    listing() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('listing');
    }
    ipo() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('ipo');
    }
    watchlist() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('watch');
    }
    ipfpage(val) {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('ipf', { 'current': val });
    }
    notices() {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('notice');
    }

    equityView() {
        this.state.equity == true ? this.setState({ equity: false, iconname: 'ios-arrow-forward' }) : this.setState({ equity: true, iconname: 'ios-arrow-down' });
    }
    sensexView() {
        this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-down' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
    }
    smeView() {
        this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-down' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
    }
    derivativeView() {
        this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-down' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
    }
    currencyView() {
        this.state.currencytab == false ? this.setState({ currencytab: true, iconnamecurrency: 'ios-arrow-down' }) : this.setState({ currencytab: false, iconnamecurrency: 'ios-arrow-forward' });
    }
    commadityView() {
        this.state.commoditytab == false ? this.setState({ commoditytab: true, iconnamecommodity: 'ios-arrow-down' }) : this.setState({ commoditytab: false, iconnamecommodity: 'ios-arrow-forward' });
    }
    irdView() {
        this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-down' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
    }
    etfView() {
        this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-down' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
    }
    debtView() {
        this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-down' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
    }
    corporateView() {
        this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-down' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
    }
    ipfView() {
        this.state.ipf == false ? this.setState({ ipf: true, iconnameipf: 'ios-arrow-down' }) : this.setState({ ipf: false, iconnameipf: 'ios-arrow-forward' });

    }
    setting() {
        this.setState({ settingshow: true });
        this.setState({ sidemenu: false });
    }

    changeTheme(thm) {


        this.setState({ settingshow: false, sidemenu: false });
        AsyncStorage.setItem("themecolor", thm);
        this.props.navigation.navigate('maindark');
        this.props.navigation.goBack(null);
        thm == 'dark' ? this.props.themechngdark() : this.props.themechnglight();
    }

    chnglanguage(lan) {
        this.setState({ settingshow: false, sidemenu: false });
        AsyncStorage.setItem('language', lan);

        this.props.navigation.navigate('maindark');

    }

    popscroll() {
        // this.tabval = 2;
        AsyncStorage.setItem('scrollvalue', "true");
        this.state.tabsValue == 3 ? this.setState({ scrollleft: false, tabsValue: this.state.tabsValue - 1 }) : this.setState({ scrollleft: false, tabsValue: parseInt(this.state.tabsValue) + 1 });
        this.state.tabsValue == 0 ? this.viewPager.setPage(1) : this.state.tabsValue == 1 ? this.viewPager.setPage(2) : this.state.tabsValue == 2 ? this.viewPager.setPage(3) : this.viewPager.setPage(2);

    }

    render() {
        return (
            <StyleProvider style={getTheme(material)}>

                <Container>


                    <Header hasTabs androidStatusBarColor={this.props.theme == '0' ? '#f6f7f9' : '#0b0b0b'} style={this.props.theme == '0' ? styles.headerLight : styles.headerDark} >


                    </Header>
                    <View style={this.props.theme == '0' ? styles.viewLight : styles.viewDark}>
                        <Row style={styles.mainrow1}>
                            <Col style={styles.maincol1}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={styles.back} size={27} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                            <Col style={styles.maincol2}>
                                <Text style={this.state.tabsValue == 0 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 0 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Security</Text>
                                <Text style={this.state.tabsValue == 1 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 1 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Overview</Text>
                                <Text style={this.state.tabsValue == 2 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 2 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Turnover</Text>
                                <Text style={this.state.tabsValue == 3 && this.props.theme == 0 ? styles.lightgainertabs : this.state.tabsValue == 3 && this.props.theme == 1 ? styles.darkgainertabs : styles.hide}>Contribution</Text>
                            </Col>
                            <Col style={styles.maincol3}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={styles.google} source={this.props.theme == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            <Col style={styles.maincol4}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={styles.popovericon1} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></TouchableOpacity></Col>
                        </Row>
                        <Row style={styles.mainrow2}>
                            <Col style={styles.maincol5}>

                                <Text numberOfLines={1} onPress={() => this.setState({ indicesDropdown: true })} style={this.props.theme == '0' ? styles.lightselectedText : styles.darkselectedText}>{this.state.indicesName}</Text>
                            </Col>
                            <Col style={styles.maincol6} >
                                <TouchableOpacity activeOpacity={.5} onPress={() => this.setState({ indicesDropdown: true })} ><Icon name="ios-arrow-dropdown" style={styles.dropicon} size={22} color={this.props.theme == '0' ? "#132144" : "white"} /></TouchableOpacity>

                            </Col>
                            <Col style={styles.maincol7}><Text style={this.props.theme == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.props.theme == '0' ? styles.statuslight : styles.statusdark}>  {this.state.status}</Text></Text></Col>


                        </Row>

                        <Row style={styles.mainrow3}>
                            <Col style={styles.maincol8}><Text style={this.props.theme == '0' ? styles.ltplight : styles.ltpdark}>{this.state.ltp} <Text style={this.state.change > 0 ? styles.possensex : styles.negsensex}> {this.state.change} {this.state.changeper}%</Text></Text></Col>
                            <Col style={styles.maincol9}><TouchableOpacity activeOpacity={.5}  ><Iconoct name="graph" style={styles.popovericon2} size={22} color={this.props.theme == '0' ? "#132144" : "white"} /></TouchableOpacity></Col>

                        </Row>
                    </View>

                    <ViewPager
                        style={{ height: height - 175 }}
                        initialPage={this.state.tabsValue}
                        ref={viewPager => { this.viewPager = viewPager; }}
                        onPageScroll={(i) => this.setState({ tabsValue: i.position })}
                    >
                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <Row style={styles.scrollrow1}>
                                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('security')}</Text><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('Trd Qty')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('ltp')}</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('Chnage')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('Buy Price')}</Text><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('Sell Price')}</Text></Col>
                            </Row>
                            <View style={this.state.securityloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>

                            <View style={this.state.nodatasensex == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>

                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.security}
                                style={styles.flatlist}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>

                                    <Animatable.View animation="zoomIn" delay={500}>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow}>
                                                <Col style={styles.flatcol1}>
                                                    <Row style={item.Scrip_ID == '' ? styles.hide : styles.show}>
                                                        <Col style={this.props.theme == '0' && item.Change_Per > 0 ? styles.listlightcolsensex1 : this.props.theme == '0' && item.Change_Per < 0 ? styles.neglistlightcolsensex1 : this.props.theme == '1' && item.Change_Per > 0 ? styles.listdarkcolsensex1 : styles.neglistdarkcolsensex1}><Text style={item.Change_Per > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.ScripName}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lighttext1 : styles.darktext1}>{this.value(item.Volume)}</Text></Col>
                                                        <Col style={this.props.theme == '0' && item.Change_Per > 0 ? styles.listlightcolsensex3 : this.props.theme == '0' && item.Change_Per < 0 ? styles.neglistlightcolsensex3 : this.props.theme == '1' && item.Change_Per > 0 ? styles.listdarkcolsensex3 : styles.neglistdarkcolsensex3}><View style={item.Change_Per > 0 ? styles.show : styles.hide}><Text style={item.Change_Per > 0 ? styles.lightltp : styles.darkltp} numberOfLines={1}>{item.LTP}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>+{item.ChangeVal} +{item.Change_Per}%</Text></View><View style={item.Change_Per < 0 || item.Change_Per == 0.00 ? styles.show : styles.hide}><Text style={item.Change_Per > 0 ? styles.lightltp : styles.darkltp} numberOfLines={1}>{item.LTP}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>{item.ChangeVal} {item.Change_Per}%</Text></View></Col>
                                                        <Col style={this.props.theme == '0' && item.Change_Per > 0 ? styles.listlightcolsensex2 : this.props.theme == '0' && item.Change_Per < 0 ? styles.neglistlightcolsensex2 : this.props.theme == '1' && item.Change_Per > 0 ? styles.listdarkcolsensex2 : styles.neglistdarkcolsensex2}><Text style={this.props.theme == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.BestBuyPrice} {'(' + item.BestQty + ')'}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.SellPrice} {'(' + item.SellQty + ')'}</Text></Col>

                                                    </Row>
                                                </Col>
                                                <Col style={styles.flatcol2}></Col>

                                            </Row>
                                        </Ripple>
                                        <Text></Text>

                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />

                            {/* <View style={this.state.security.length == 1 ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View> */}
                        </View>
                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <View style={this.state.overviewloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>

                            <View style={this.state.nodataoverview == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.overview}
                                style={{}}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500}>
                                        <Row style={styles.scrollrow}>
                                            <Col style={item.no == '1' && this.props.theme == '0' ? styles.firstcol : item.no == '1' && this.props.theme == '1' ? styles.firstcoldark : styles.hide}>
                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol1}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Prv Close</Text></Col>
                                                    <Col style={styles.overviewcol2}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol3}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark} numberOfLines={1}>{item.prv}</Text></Col>
                                                    <Col style={styles.overviewcol4}><Text style={this.props.theme == '0' ? styles.openlight : styles.opendark} numberOfLines={1}>Open</Text></Col>
                                                    <Col style={styles.overviewcol2}><Text style={this.props.theme == '0' ? styles.openlight : styles.opendark}>:</Text></Col>
                                                    <Col style={styles.overviewcol5}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.open}</Text></Col>
                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol1}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>High</Text></Col>
                                                    <Col style={styles.overviewcol2}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol3}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark} numberOfLines={1}>{item.high}</Text></Col>
                                                    <Col style={styles.overviewcol4}><Text style={this.props.theme == '0' ? styles.openlight : styles.opendark} numberOfLines={1}>Low</Text></Col>
                                                    <Col style={styles.overviewcol2}><Text style={this.props.theme == '0' ? styles.openlight : styles.opendark}>:</Text></Col>
                                                    <Col style={styles.overviewcol5}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.low}</Text></Col>
                                                </Row>
                                            </Col>
                                            <Col style={item.no == '2' && this.props.theme == '0' ? styles.secondcol : item.no == '2' && this.props.theme == '1' ? styles.secondcoldark : styles.hide}>
                                                <Text style={this.props.theme == '0' ? styles.titlelight : styles.titledark} >Market Capitalisation</Text>

                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Full(Cr)</Text></Col>
                                                    <Col style={styles.overviewcol7}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.full}</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Free Float(Cr)</Text></Col>
                                                    <Col style={styles.overviewcol7}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.free}</Text></Col>

                                                </Row>
                                            </Col>
                                            <Col style={item.no == '3' && this.props.theme == '0' ? styles.thirdcol : item.no == '3' && this.props.theme == '1' ? styles.thirdcoldark : styles.hide}>
                                                <Text style={this.props.theme == '0' ? styles.titlelight : styles.titledark} >Ups & Downs</Text>

                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol8}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}></Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>Scrips</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>T/O Cr</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol8}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Advances</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>{item.advance}</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.advanceTO}</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol8}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Declines</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>{item.decline}</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.declineTO}</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol8}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Unchanged</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>{item.unchanged}</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.unchangedTO}</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol8}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Not Traded</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>{item.nottraded}</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.nottradedTO}</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrowtotal}>
                                                    <Col style={styles.overviewcol8}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Total</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>{item.total}</Text></Col>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.totalTO}</Text></Col>

                                                </Row>



                                            </Col>
                                            <Col style={item.no == '4' && this.props.theme == '0' ? styles.fourthcol : item.no == '4' && this.props.theme == '1' ? styles.fourthcoldark : styles.hide}>
                                                <Text style={this.props.theme == '0' ? styles.titlelight : styles.titledark} >High  Lows</Text>

                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>52  Wk  High</Text></Col>
                                                    <Col style={styles.overviewcol10}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol11}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.high}  {item.highdate}</Text></Col>

                                                </Row>
                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>52  Wk   Low</Text></Col>
                                                    <Col style={styles.overviewcol10}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol11}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.low}  {item.lowdate}</Text></Col>

                                                </Row>
                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>All time high</Text></Col>
                                                    <Col style={styles.overviewcol10}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol11}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.alltimehigh}  {item.alltimehighdate}</Text></Col>

                                                </Row>
                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol9}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>All time low</Text></Col>
                                                    <Col style={styles.overviewcol10}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol11}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.alltimelow}  {item.alltimelowdate}</Text></Col>

                                                </Row>

                                            </Col>
                                            <Col style={item.no == '5' && this.props.theme == '0' ? styles.fivecol : item.no == '5' && this.props.theme == '1' ? styles.fivecoldark : styles.hide}>
                                                <Text style={this.props.theme == '0' ? styles.titlelight : styles.titledark} >Fundamental Data</Text>

                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>PE</Text></Col>
                                                    <Col style={styles.overviewcol7}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.pe}</Text></Col>

                                                </Row>
                                                <Row style={styles.mainrow4}>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>PB</Text></Col>
                                                    <Col style={styles.overviewcol7}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.pb}</Text></Col>

                                                </Row>
                                                <Row style={styles.overviewrow1}>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.closelightname : styles.closedarkname} numberOfLines={1}>Dividend Yield</Text></Col>
                                                    <Col style={styles.overviewcol7}><Text style={this.props.theme == '0' ? styles.closelight : styles.closedark}>:</Text></Col>
                                                    <Col style={styles.overviewcol6}><Text style={this.props.theme == '0' ? styles.lightlow : styles.darklow} numberOfLines={1}>{item.divided}</Text></Col>

                                                </Row>

                                            </Col>

                                        </Row>
                                        <Text></Text>

                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />

                        </View>
                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <Row style={styles.scrollrow1}>
                                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('security')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader2 : styles.darkheader2}><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('Trd Qty')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('to')}( <IconFonsito name="rupee" style={{}} color={this.props.theme == '0' ? 'black' : 'white'} size={13}></IconFonsito> Lacs )</Text></Col>
                            </Row>
                            <View style={this.state.turnoverloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>
                            <View style={this.state.nodataturn == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Turn}
                                style={styles.flatlist1}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <Animatable.View animation="zoomIn" delay={500}>
                                        <View style={item.id != 0 ? styles.show : styles.hide}>
                                            <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                                <Row style={styles.scrollrow}>
                                                    <Col style={styles.flatcol1}>
                                                        <Row>
                                                            <Col style={this.props.theme == '0' && item.status > 0 ? styles.listlightcolsensex1 : this.props.theme == '0' && item.status < 0 ? styles.neglistlightcolsensex1 : this.props.theme == '1' && item.status > 0 ? styles.listdarkcolsensex1 : styles.neglistdarkcolsensex1}><Text style={item.status > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.name1}</Text></Col>
                                                            <Col style={this.props.theme == '0' && item.status > 0 ? styles.listlightcolsensex3 : this.props.theme == '0' && item.status < 0 ? styles.neglistlightcolsensex3 : this.props.theme == '1' && item.status > 0 ? styles.listdarkcolsensex3 : styles.neglistdarkcolsensex3}><Text style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn} numberOfLines={1}>{item.trd}</Text></Col>
                                                            <Col style={this.props.theme == '0' && item.status > 0 ? styles.listlightcolsensex2 : this.props.theme == '0' && item.status < 0 ? styles.neglistlightcolsensex2 : this.props.theme == '1' && item.status > 0 ? styles.listdarkcolsensex2 : styles.neglistdarkcolsensex2}><Text style={this.props.theme == '0' ? styles.lightsensexturn : styles.darksensexturn} numberOfLines={1}>{item.to}</Text></Col>

                                                        </Row>
                                                    </Col>

                                                </Row>
                                            </Ripple>
                                            <Text></Text>

                                        </View>
                                        <View style={item.name1 == 'Total' ? styles.show : styles.hide}>
                                            <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                                <Row style={styles.scrollrow}>
                                                    <Col style={styles.flatcol1}>
                                                        <Row>
                                                            <Col style={this.props.theme == '0' ? styles.totallistlightcolsensex1 : styles.totallistdarkcolsensex1}><Text style={this.props.theme == '0' ? styles.totallight : styles.totaldark} numberOfLines={1}>{item.name1}</Text></Col>
                                                            <Col style={this.props.theme == '0' ? styles.totallistlightcolsensex3 : styles.totallistdarkcolsensex3}><Text style={this.props.theme == '0' ? styles.lightsensextotal : styles.darksensextotal} numberOfLines={1}>{item.trd}</Text></Col>
                                                            <Col style={this.props.theme == '0' ? styles.totallistlightcolsensex2 : styles.totallistdarkcolsensex2}><Text style={this.props.theme == '0' ? styles.lightsensextotal : styles.darksensextotal} numberOfLines={1}>{item.to}</Text></Col>

                                                        </Row>
                                                    </Col>
                                                    <Col style={styles.flatcol2}></Col>

                                                </Row>
                                            </Ripple>
                                            <Text></Text>

                                        </View>
                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}
                            />

                            {/* <View style={this.turnlength == 1 ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View> */}
                        </View>
                        <View style={this.props.theme == 0 ? styles.lightview : styles.darkview}>
                            <Row style={styles.scrollrow1}>
                                <Col style={this.props.theme == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.props.theme == '0' ? styles.lightheadertext : styles.darkheadertext}>{I18n.t('security')}</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('ltp')}( <IconFonsito name="rupee" style={{}} color={this.props.theme == '0' ? 'black' : 'white'} size={13}></IconFonsito>)</Text></Col>
                                <Col style={this.props.theme == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.props.theme == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>{I18n.t('Contribution')}</Text></Col>
                            </Row>
                            <View style={this.state.contriloader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={styles.loader} color={this.props.theme == '0' ? "#0000ff" : "white"} />

                            </View>
                            <View style={this.state.nodatacontri == true ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.contri}
                                style={styles.flatlist1}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>

                                    <Animatable.View animation="zoomIn" delay={500} style={item.ScripID == '' ? styles.hide : styles.show}>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}>

                                            <Row style={styles.scrollrow}>
                                                <Col style={item.ChangeVal == 0.00 || item.ChangePer == 0.00 ? styles.hide : styles.contricol}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listlightcolsensex1 : this.props.theme == '0' && item.ChangeVal < 0 ? styles.neglistlightcolsensex1 : this.props.theme == '1' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listdarkcolsensex1 : styles.neglistdarkcolsensex1}><Text style={item.ChangeVal > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.ScripName}</Text></Col>
                                                        <Col style={this.props.theme == '0' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listlightcolsensex3 : this.props.theme == '0' && item.ChangeVal < 0 ? styles.neglistlightcolsensex3 : this.props.theme == '1' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listdarkcolsensex3 : styles.neglistdarkcolsensex3}><Text style={item.ChangeVal > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{item.LTP}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>{this.chng(item.ChangeVal, item.ChangePer, '')}</Text></Col>
                                                        <Col style={this.props.theme == '0' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listlightcolsensex2 : this.props.theme == '0' && item.ChangeVal < 0 ? styles.neglistlightcolsensex2 : this.props.theme == '1' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listdarkcolsensex2 : styles.neglistdarkcolsensex2}><Text style={item.ChangeVal > 0 ? styles.lastlightcontri : styles.lastdarkcontri} numberOfLines={1}>{this.chng('', '', item.PiontsContrbution)}</Text></Col>

                                                    </Row>
                                                </Col>
                                                <Col style={item.ChangeVal == 0.00 || item.ChangePer == 0.00 ? styles.contricol : styles.hide}>
                                                    <Row>
                                                        <Col style={this.props.theme == '0' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listlightcolsensex1 : this.props.theme == '0' && item.ChangeVal < 0 ? styles.neglistlightcolsensex1 : this.props.theme == '1' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listdarkcolsensex1 : styles.neglistdarkcolsensex1}><Text style={item.ChangeVal > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.ScripName}</Text></Col>
                                                        <Col style={this.props.theme == '0' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listlightcolsensex3 : this.props.theme == '0' && item.ChangeVal < 0 ? styles.neglistlightcolsensex3 : this.props.theme == '1' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listdarkcolsensex3 : styles.neglistdarkcolsensex3}><Text style={item.ChangeVal > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{item.LTP}</Text><Text numberOfLines={1} style={this.props.theme == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>{item.ChangeVal}  {item.ChangePer} %</Text></Col>
                                                        <Col style={this.props.theme == '0' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listlightcolsensex2 : this.props.theme == '0' && item.ChangeVal < 0 ? styles.neglistlightcolsensex2 : this.props.theme == '1' && (item.ChangeVal > 0 || item.ChangeVal == 0.00) ? styles.listdarkcolsensex2 : styles.neglistdarkcolsensex2}><Text style={item.ChangeVal > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{item.PiontsContrbution}</Text></Col>

                                                    </Row>
                                                </Col>
                                                <Col style={styles.flatcol2}></Col>

                                            </Row>
                                        </Ripple>
                                        <Text></Text>

                                    </Animatable.View>

                                }
                                keyExtractor={item => item.id}

                            />

                            {/* <View style={this.state.contri.length == 1 ? styles.showNoData : styles.hide}>
                                <Text style={this.props.theme == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View> */}
                        </View>
                    </ViewPager>


                    <Popover
                        isVisible={this.state.indicesDropdown}
                        fromView={this.touchable}
                        onRequestClose={() => this.setState({ indicesDropdown: false })} >
                        <View style={this.props.theme == '0' ? styles.mainviewlight : styles.mainviewdark}>
                            <FlatList
                                data={this.state.indicesdata}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <View style={this.props.theme == '0' ? styles.lightdropdown : styles.darkdropdown}>
                                        <Text onPress={() => this.selectedValue(item.split(',')[0], item.split(',')[5])} style={this.props.theme == '0' ? styles.lightdroptext : styles.darkdroptext}>{item.split(',')[0]}</Text>
                                    </View>
                                }

                            />
                            <Text></Text>
                            <Text></Text>


                        </View>
                    </Popover>

                    <Popover
                        isVisible={this.state.scrollleft}
                        fromView={this.touchable}
                        onRequestClose={() => console.log('close')}
                    >
                        <View style={styles.mainviewlight}>



                            <Animatable.Text animation="pulse" iterationCount="infinite" style={{ textAlign: 'center', margin: 20, }} direction="alternate"><IconFont name={this.state.scrollicon} size={70} color='black' /></Animatable.Text>


                            <Text style={{ margin: 20, bottom: 20, color: 'black', fontSize: 18, fontWeight: 'bold' }}>{this.state.scrollmsg}</Text>

                            <View style={{ flex: 1, flexDirection: 'row', margin: 10, bottom: 20, }}>
                                <View style={{ width: '30%', height: 50, backgroundColor: '#f6f7f9' }} />
                                <View style={{ width: '30%', height: 50, backgroundColor: '#f6f7f9' }} />
                                <View style={{ width: '40%', height: 50, backgroundColor: '#2087c9', borderRadius: 5 }} ><Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center', margin: 10, }} onPress={() => this.popscroll()}>Scroll Now</Text></View>
                            </View>

                        </View>
                    </Popover>



                    <Modal
                        animationType="slide"
                        transparent={false}
                        style={styles.modalheight}
                        visible={this.state.sidemenu}
                        onRequestClose={() => {
                            this.setState({ sidemenu: false });
                        }}
                    >
                        <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>
                            <Row style={styles.menurow}>
                                <Col style={styles.menucol1}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={styles.closeicon} size={40} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                                <Col style={styles.menucol2}><IconEvil name="user" style={styles.usericon} size={35} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                                <Col style={styles.menucol2}><Icon name="ios-settings" onPress={() => this.setting()} style={styles.settingicon} size={30} color={this.props.theme == '0' ? '#132144' : 'white'} /></Col>
                            </Row>

                            <ScrollView style={styles.scrollview}>

                                <Text></Text>
                                <View style={{}}>
                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.popovercol7}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('home')}</Text></Col>
                                        <Col style={styles.popovercol7}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.equityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Equity')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.equityView()} ><Icon name={this.state.iconname} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.equity == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.equitypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GAINERS')}</Text></Ripple>
                                        <Ripple onPress={() => this.equitypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('LOSERS')}</Text></Ripple>
                                        <Ripple onPress={() => this.equitypage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TOP TURNOVER')}</Text></Ripple>
                                        <Ripple onPress={() => this.equitypage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK HIGH')}</Text></Ripple>
                                        <Ripple onPress={() => this.equitypage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('52 WK LOW')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Indices')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.indices()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.sensexView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Sensex')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.sensexView()} ><Icon name={this.state.iconnamesensex} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.sensex == false ? styles.hide : styles.show}>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('SECURITY')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('OVERVIEW')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('TURNOVER')}</Text></Ripple>
                                        <Ripple rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CONTRIBUTION')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.smeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('SME')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.smeView()} ><Icon name={this.state.iconnamesme} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.sme == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.smepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.smepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET STATISTICS')}</Text></Ripple>
                                        <Text></Text>
                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.derivativeView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext} >{I18n.t('Derivatives')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.derivativeView()} ><Icon name={this.state.iconnamederivative} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.derivative == false ? styles.hide : styles.show}>

                                        <Ripple onPress={() => this.derivativepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.derivativepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000}><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.currencyView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Currency')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.currencyView()} ><Icon name={this.state.iconnamecurrency} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.currencytab == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.currencypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.currencypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.commadityView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Commodity')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.commadityView()} ><Icon name={this.state.iconnamecommodity} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.commoditytab == false ? styles.hide : styles.show}>

                                        <Ripple onPress={() => this.commoditypage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.commoditypage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.irdView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IRD')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.irdView()} ><Icon name={this.state.iconnameird} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.ird == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.irdpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.irdpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.etfView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('ETF')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.etfView()} ><Icon name={this.state.iconnameetf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.etf == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.etfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ETF WATCH')}</Text></Ripple>
                                        <Ripple onPress={() => this.etfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('MARKET SUMMARY')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.debtView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Debt')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.debtView()} ><Icon name={this.state.iconnamedebt} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.debt == false ? styles.hide : styles.show}>

                                        <Ripple onPress={() => this.debtpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('DEBT MARKET SUMMARY')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('CORPORATE BONDS-OTC TRADES')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL CORP')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('GSEC')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RETAIL GOV BONDS')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Corporate Bonds-NDS-RST')}</Text></Ripple>
                                        <Ripple onPress={() => this.debtpage(6)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('EBP')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.corporateView()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Corporates')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.corporateView()} ><Icon name={this.state.iconnamecorporate} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.corporate == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.corporatepage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ANNOUNCEMENTS')}</Text></Ripple>
                                        <Ripple onPress={() => this.corporatepage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('ACTIONS')}</Text></Ripple>
                                        <Ripple onPress={() => this.corporatepage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('RESULTS CALENDAR')}</Text></Ripple>
                                        <Ripple onPress={() => this.corporatepage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('BOARD MEETINGS')}</Text></Ripple>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.marketstatic()} ><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Statistics')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.marketstatic()} ></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.marketturn()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Market Turnover')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.marketturn()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.ipo()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPO/OFS')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.ipo()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.listing()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Listings')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.listing()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.notices()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('Notices')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.notices()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.watchlist()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('watchlist')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.watchlist()}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('portfolio')}</Text></Col>
                                        <Col style={styles.menucol4}></Col>
                                    </Row>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={styles.scrollrow}>
                                        <Col style={styles.menucol3} onPress={() => this.ipfView()}><Text style={this.props.theme == '0' ? styles.lightmenutext : styles.darkmenutext}>{I18n.t('IPF')}</Text></Col>
                                        <Col style={styles.menucol4} onPress={() => this.ipfView()}><Icon name={this.state.iconnameipf} style={styles.menuicon} color={this.props.theme == '0' ? '#132144' : 'white'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.ipf == false ? styles.hide : styles.show}>
                                        <Ripple onPress={() => this.ipfpage(0)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Service')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(1)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Guide')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(2)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Investor Complaints')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(3)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Arbitration')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(4)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Regulatory')}</Text></Ripple>
                                        <Ripple onPress={() => this.ipfpage(5)} rippleColor="#2087c9" rippleOpacity={0.8} rippleDuration={1000} ><Text style={this.props.theme == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>{I18n.t('Dissemination Board')}</Text></Ripple>
                                        <Text></Text>

                                    </View>

                                    <Text style={this.props.theme == '0' ? styles.lightline : styles.darkline}></Text>
                                    <Text></Text>
                                    <Text></Text>
                                    <Text></Text>
                                    <Text></Text>
                                    {/* <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Semibold', }}></Text> */}
                                </View>
                            </ScrollView>
                        </View>
                    </Modal>

                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.settingshow}
                        style={{ height: '100%' }}
                        onRequestClose={() => {
                            this.setState({ settingshow: false });
                            this.setState({ sidemenu: false });

                        }}
                    >
                        <View style={this.props.theme == '0' ? styles.lightmenu : styles.darkmenu}>

                            <ScrollView style={styles.scrollview1}>
                                <Row style={styles.settingrow}>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('changelanguage')}</Text></Col>
                                    <Col style={styles.popovercol8}>
                                        <Picker
                                            selectedValue={this.state.selectlan}
                                            style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                                            onValueChange={(itemValue) => this.chnglanguage(itemValue)}
                                        >
                                            <Picker.Item label="English" value="English" />
                                            <Picker.Item label="Hindi" value="Hindi" />
                                            <Picker.Item label="Marathi" value="Marathi" />
                                            <Picker.Item label="Gujrati" value="Gujrati" />
                                            <Picker.Item label="Bengali" value="Bengali" />
                                            <Picker.Item label="Malayalam" value="Malayalam" />
                                            <Picker.Item label="Oriya" value="Oriya" />
                                            <Picker.Item label="Tamil" value="Tamil" />
                                        </Picker>
                                        <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropiconsetting} size={22} color={'white'} />

                                    </Col>
                                </Row>
                                <Text></Text>
                                <Row >
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol8}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('theme')} </Text></Col>
                                    <Col style={styles.popovercol8}>
                                        <Picker
                                            selectedValue={this.props.themename}
                                            style={this.props.theme == '0' ? styles.pickerlight : styles.pickerdark}
                                            onValueChange={(itemValue) => this.changeTheme(itemValue)}
                                        // onValueChange={(itemValue) => this.props.themechng()}
                                        >
                                            <Picker.Item label="Dark" value="dark" />
                                            <Picker.Item label="Light" value="light" />

                                        </Picker>
                                        <Icon name="ios-arrow-down" style={this.props.theme == '0' ? styles.hide : styles.dropiconsetting} size={22} color={'white'} />

                                    </Col>
                                </Row>

                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('App Info')} </Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Privacy Policy')}</Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('About Us')}</Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Notification')}</Text></Col>
                                </Row>
                                <Text></Text>
                                <Row>
                                    <Col style={styles.popovercol1}><Icon name="ios-arrow-forward" style={styles.popovericon1} color={this.props.theme == '0' ? '#132144' : 'white'} size={20} ></Icon></Col>
                                    <Col style={styles.popovercol3}><Text style={this.props.theme == '0' ? styles.lighsettingtext : styles.darksettingtext}>{I18n.t('Help')}</Text></Col>
                                </Row>
                            </ScrollView>
                        </View>
                    </Modal>

                    <Footer>

                        <FooterTab style={this.props.theme == '0' ? styles.tablight : styles.tabdark}>
                            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                <Image source={this.props.theme == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={styles.footer1} />
                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                            </Button>


                            <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                <Image source={this.props.theme == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={styles.footer1} />

                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                            </Button>

                            <Button style={styles.active} onPress={() => this.tab('home')}>
                                <Image source={require('../images/bsemenuicon.png')} style={styles.footer3} />
                                <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                            </Button>
                            <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                <Image source={this.props.theme == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={styles.footer2} />
                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                            </Button>
                            <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                <Image source={this.props.theme == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={styles.footer1} />

                                <Text style={this.props.theme == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                            </Button>


                        </FooterTab>
                    </Footer>
                </Container>
            </StyleProvider>
        );
    }
}

function mapStateToProps(state) {
    return {
        theme: state.theme,
        themename: state.themename
    }
}

function mapDispatchToProps(dispatch) {
    return {
        themechnglight: () => dispatch({ type: 'light' }),
        themechngdark: () => dispatch({ type: 'dark' }),
    }
}

const styles = StyleSheet.create({

    darkgainertabs: { color: 'white', fontSize: 19, marginLeft: 10, fontFamily: 'Arial', fontWeight: 'bold' },
    lightgainertabs: { color: '#132144', fontSize: 19, marginLeft: 10, fontFamily: 'Arial', fontWeight: 'bold' },
    mainviewlight: { backgroundColor: '#f6f7f9', height: '100%', },
    mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
    headerDark: { backgroundColor: '#0b0b0b', height: 1 },
    headerLight: { backgroundColor: '#f6f7f9', height: 1 },
    viewDark: { backgroundColor: '#0b0b0b', height: 120 },
    viewLight: { backgroundColor: '#f6f7f9', height: 120 },
    lightdate: { color: '#132144', fontSize: 14, marginTop: 12, textAlign: 'right' },
    darkdate: { color: 'white', fontSize: 14, marginTop: 12, textAlign: 'right' },
    darkview: { backgroundColor: '#0b0b0b', height: '100%' },
    lightview: { backgroundColor: '#f6f7f9', height: '100%' },

    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 14, marginRight: 14, borderRadius: 3 },
    inactive: { padding: 10, },

    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamedark: { color: 'white', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamelighthome: { color: 'white', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },

    statuslight: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, color: '#132144' },
    statusdark: { fontFamily: 'Arial', fontWeight: 'bold', fontSize: 14, color: 'white' },
    listdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
    listdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
    listdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },

    listlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
    listlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },
    listlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', borderBottomColor: '#19cf3e', borderBottomWidth: 1.5 },

    neglistdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
    neglistdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
    neglistdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', borderBottomColor: '#f54845', borderBottomWidth: 1.5 },

    neglistlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
    neglistlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5, borderBottomColor: '#f54845', borderBottomWidth: 1.5 },
    neglistlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', borderBottomColor: '#f54845', borderBottomWidth: 1.5 },


    totallistlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
    totallistlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5, },
    totallistlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', },

    totallistdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, },
    totallistdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5, },
    totallistdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', },


    show: { display: 'flex' },
    hide: { display: 'none' },
    showNoData: { display: 'flex', marginTop: 40 },
    possensex: { marginLeft: 5, marginTop: 9, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold' },
    lighttext1: { marginLeft: 5, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    lightsensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    lightsensexturn: { alignSelf: 'flex-end', marginRight: 7, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    lightsensex1: { alignSelf: 'flex-end', marginRight: 7, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darksensex: { alignSelf: 'flex-end', marginRight: 5, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    darksensexturn: { alignSelf: 'flex-end', marginRight: 7, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightsensextotal: { alignSelf: 'flex-end', fontSize: 16, marginRight: 7, marginTop: 10, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darksensextotal: { alignSelf: 'flex-end', fontSize: 16, marginRight: 5, marginTop: 10, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },


    darksensex1: { alignSelf: 'flex-end', marginRight: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightsensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darksensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    negsensex: { marginLeft: 5, marginTop: 9, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold' },
    lightltp: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold' },
    darkltp: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold' },
    lightltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold' },
    darkltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold' },
    lastlightcontri: { alignSelf: 'flex-end', marginRight: 6, marginTop: 9, color: '#19cf3e', fontFamily: 'Arial', fontWeight: 'bold' },
    lastdarkcontri: { alignSelf: 'flex-end', marginRight: 6, marginTop: 9, color: '#f54845', fontFamily: 'Arial', fontWeight: 'bold' },
    darktext1: { marginLeft: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightheader1: { width: '32%', height: 40 },
    darkheader1: { width: '32%', height: 40 },
    lightheader2: { width: '30%', height: 40 },
    darkheader2: { width: '30%', height: 40 },
    lightheader3: { width: '33%', height: 40 },
    darkheader3: { width: '33%', height: 40 },
    lightheadertext: { marginLeft: 15, fontSize: 15, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkheadertext: { marginLeft: 15, fontSize: 15, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkheadertext1: { alignSelf: 'flex-end', color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    firstcol: { borderRadius: 5, backgroundColor: 'white', width: '100%', height: 90, },
    firstcoldark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: '100%', height: 90, },
    secondcol: { borderRadius: 5, backgroundColor: 'white', width: '100%', height: 120, },
    secondcoldark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: '100%', height: 120, },
    thirdcol: { borderRadius: 5, backgroundColor: 'white', width: '100%', height: 260, },
    thirdcoldark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: '100%', height: 260, },
    fourthcol: { borderRadius: 5, backgroundColor: 'white', width: '100%', height: 220, },
    fourthcoldark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: '100%', height: 220, },
    fivecol: { borderRadius: 5, backgroundColor: 'white', width: '100%', height: 160, },
    fivecoldark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: '100%', height: 160, },

    titlelight: { margin: 10, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    titledark: { margin: 10, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    dropicon: { textAlign: 'center', marginTop: 10 },
    pickerdark: { height: 30, bottom: 5, width: '100%', color: 'white' },
    pickerlight: { height: 30, bottom: 5, width: '100%', color: '#132144' },
    dropiconsetting: { marginLeft: 88, bottom: 30 },
    ltplight: { marginLeft: 5, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    ltpdark: { marginLeft: 5, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darkmenutext: { marginLeft: 15, fontSize: 20, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold' },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: 'white', fontFamily: 'Arial', fontWeight: 'bold' },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: 'white', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    contricol: { width: '100%', height: 58 },
    lightdropdown: { margin: 10, backgroundColor: '#f6f7f9' },
    darkdropdown: { margin: 10, backgroundColor: '#0b0b0b' },
    lightdroptext: { color: '#132144' },
    darkdroptext: { color: 'white' },
    lightselectedText: { color: '#132144', marginTop: 13, marginLeft: 5 },
    darkselectedText: { color: 'white', marginTop: 13, marginLeft: 5 },
    totallight: { marginLeft: 5, fontSize: 18, marginTop: 9, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    totaldark: { marginLeft: 5, fontSize: 18, marginTop: 9, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },

    nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center' },
    nodatadark: { color: 'white', fontSize: 19, fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center' },

    closedarkname: { color: 'white', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'flex-start', },
    closelightname: { color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'flex-start', },
    closelight: { color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center', },
    closedark: { color: 'white', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center', },
    opendark: { color: 'white', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center', },
    openlight: { color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'center', },
    lightlow: { color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'flex-end', },
    darklow: { color: 'white', fontFamily: 'Arial', fontWeight: 'bold', alignSelf: 'flex-end', },
    lighsettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: '#132144', fontFamily: 'Arial', fontWeight: 'bold', },
    darksettingtext: { textAlign: 'left', fontSize: 16, bottom: 2, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', },

    modalheight: { height: '100%' },
    menurow: { marginLeft: 20, marginTop: 10 },
    menucol1: { width: '67%', height: 40 },
    menucol2: { width: '15%', height: 40 },
    menucol3: { width: '75%', height: 40 },
    menucol4: { width: '25%', height: 40 },
    closeicon: { marginLeft: 10 },
    usericon: { marginTop: 3, textAlign: 'right' },
    settingicon: { marginRight: 5, marginTop: 2, textAlign: 'right' },
    scrollview: { top: '8%' },
    scrollview1: { bottom: 30 },
    scrollrow: { marginLeft: 16, marginRight: 16 },
    menuicon: { marginTop: 6 },
    settingrow: { marginTop: '25%' },
    footer1: { width: 26, height: 28, },
    footer2: { width: 26, height: 27, },
    footer3: { width: 23, height: 24, bottom: 2 },
    popoverview: { backgroundColor: 'white', height: '100%' },
    popovercol1: { width: '20%', height: 50, },
    popovercol2: { width: '80%', height: 50, },
    popovercol3: { width: '40%', height: 50, },
    popovercol4: { width: '5%', height: 50, },
    popovercol5: { width: '35%', height: 50, },
    popovercol6: { width: '100%', height: 40, },
    popovercol7: { width: '50%', height: 40, },
    popovercol8: { width: '30%', height: 50, },
    popovericon1: { textAlign: 'center' },
    popovericon2: { textAlign: 'right' },
    popovericon3: { textAlign: 'left' },
    picker: { height: 30, bottom: 5, width: '100%', },
    headerView: { height: '32%', },
    row1: { margin: 5, marginTop: 10, },
    maincol1: { width: '15%', height: 30 },
    maincol2: { width: '55%', height: 30, },
    maincol3: { width: '20%', height: 30, },
    maincol4: { width: '10%', height: 30, },
    maincol5: { width: '37%', height: 40, },
    maincol6: { width: '7%', height: 40, },
    maincol7: { width: '56%', height: 40, },
    maincol8: { width: '70%', height: 40, },
    maincol9: { width: '30%', height: 40, },
    back: { textAlign: 'left', marginLeft: 13 },
    google: { width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 },
    user: { marginTop: 1, textAlign: 'right' },
    row2: { margin: 5, marginRight: 19, },
    col3: { width: '100%', height: 40, },
    view: { marginLeft: 15 },
    scrollrow1: { marginLeft: 3, },
    equatyimage: { width: 22, height: 23, margin: 12 },
    scrollloader: { marginTop: 20 },
    flatlist: { height: '100%', marginTop: 60 },
    flatlist1: { height: '100%', marginTop: 50 },
    flatlistrow: { marginLeft: 16, marginRight: 16, marginTop: 20, },
    mainrow1: { margin: 5 },
    mainrow2: { margin: 15, marginTop: '3%' },
    mainrow3: { margin: 15, },
    mainrow4: { margin: 10, },
    loader: { marginTop: 60 },
    flatcol1: { width: '100%', height: 58, },
    flatcol2: { width: '6.5%', height: 58 },
    flatcol3: { width: '100%', height: 58 },
    overviewcol1: { width: '23%', height: 20, },
    overviewcol2: { width: '2%', height: 20, },
    overviewcol3: { width: '25%', height: 20, },
    overviewcol4: { width: '20%', height: 20, },
    overviewcol5: { width: '28%', height: 20, },
    overviewcol6: { width: '45%', height: 20, },
    overviewcol7: { width: '10%', height: 20, },
    overviewcol8: { width: '40%', height: 20, },
    overviewcol9: { width: '30%', height: 20, },
    overviewcol10: { width: '15%', height: 20, },
    overviewcol11: { width: '55%', height: 20, },
    overviewrow1: { margin: 10, },
    overviewrowtotal: { margin: 10, bottom: 3 },

})

// export default Setting;
export default connect(mapStateToProps, mapDispatchToProps)(Setting);