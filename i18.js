import I18n from 'react-native-i18n';
// // import en from '../localization/en.json';
// // import hi from '../localization/en.json';

I18n.fallbacks = true;
I18n.translations = {
    'en': require('./translations/en'),
    'hi': require('./translations/hi'),
    'mr': require('./translations/mr'),
    'bn': require('./translations/bn'),
    'gj': require('./translations/gj'),
    'ml': require('./translations/ml'),
    'or': require('./translations/or'),
    'tml': require('./translations/tml')
    // // mr,hn,bn,gj,ml,or,tml,en
  };
  export default I18n;