import React, { Component } from 'react';
import { View, Image, Modal, ToastAndroid, Linking, CheckBox, Dimensions, Switch, Picker, Text, BackHandler, RefreshControl, NetInfo, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view';
import IconEvil from 'react-native-vector-icons/EvilIcons';

import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';

const height = Dimensions.get('window').height;

class Gainer extends Component {
    static navigationOptions = {

        header: null,
    };

    gain = [];
    lose = [];
    turn = [];
    high = [];
    low = [];
    Sensex = [];
    higharray = [];
    lowarray = [];
    themename = '';
    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            date: '',
            status: '',
            value: 1,
            loader: false,
            isVisible: false,
            switchval: false,

            Gainer: [],
            Loser: [],
            Turnover: [],
            High: [],
            Low: [],
            refreshing: false,
            sidemenu: false,
            settingshow: false,
            theme: '',
            showedit: false,
            iconname: 'ios-arrow-forward',
            iconnamesensex: 'ios-arrow-forward',
            iconnamesme: 'ios-arrow-forward',
            iconnamederivative: 'ios-arrow-forward',
            iconnamecurrency: 'ios-arrow-forward',
            iconnamecommodity: 'ios-arrow-forward',
            iconnameird: 'ios-arrow-forward',
            iconnameetf: 'ios-arrow-forward',
            iconnamedebt: 'ios-arrow-forward',
            iconnamecorporate: 'ios-arrow-forward',
            selectlan: 'English',
            equity:false,
            sensex:false,
            sme:false,
            derivative:false,
            currency:false,
            commodity:false,
            ird:false,
            etf:false,
            debt:false,
            corporate:false
            // page: ''

        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setTimeout(() => { SplashScreen.hide() }, 3000);

    }
    componentDidMount() {

        I18n.locale = "en";
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        });
        this.themename = this.props.navigation.getParam('theme', '');
        if (this.themename == null || this.themename == 'dark') {
            this.setState({ value: '1' });
        }
        else {
            this.setState({ value: '1' });
        }
        this.highfun();
        this.sensexfun();
        this.gainfun();
        this.losefun();
        this.turnoverfun();
       
        this.lowfun();

    }

    sensexfun() {
        this.setState({ loader: true });
        fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

            responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';
        })
    }
    gainfun() {
        fetch('https://api.bseindia.com/msource/gainersAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson) => {

            fetch('https://api.bseindia.com/msource/losersAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson1) => {
                for (let i = 0; i < responsejson.split('#').length; i++) {
                    this.gain.push({ 'id': responsejson.split('#')[i].split(',')[0], 'change': responsejson.split('#')[i].split(',')[1], 'value1': responsejson.split('#')[i].split(',')[2], 'value2': responsejson.split('#')[i].split(',')[3], 'name1': responsejson.split('#')[i].split(',')[4], 'name2': responsejson1.split('#')[i].split(',')[4] });
                }
            });
            this.setState({ Gainer: this.gain, });

        });

    }
    losefun() {
        fetch('https://api.bseindia.com/msource/losersAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson) => {

            fetch('https://api.bseindia.com/msource/turnoverAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson1) => {
                for (let i = 0; i < responsejson.split('#').length; i++) {
                    this.lose.push({ 'id': responsejson.split('#')[i].split(',')[0], 'change': responsejson.split('#')[i].split(',')[1], 'value1': responsejson.split('#')[i].split(',')[2], 'value2': responsejson.split('#')[i].split(',')[3], 'name1': responsejson.split('#')[i].split(',')[4], 'name2': responsejson1.split('#')[i].split(',')[6] });
                }
            });
            this.setState({ Loser: this.lose, });
        });
    }
    turnoverfun() {
        fetch('https://api.bseindia.com/msource/turnoverAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson) => {

            fetch('https://api.bseindia.com/msource/get52highlow.aspx?flag=0&ln=en').then((response) => response.text()).then((responsejson1) => {
                for (let i = 0; i < responsejson.split('#').length; i++) {
                    this.turn.push({ 'per': responsejson.split('#')[i].split(',')[2], 'id': responsejson.split('#')[i].split(',')[0], 'change': responsejson.split('#')[i].split(',')[1], 'value': responsejson.split('#')[i].split(',')[5], 'name1': responsejson.split('#')[i].split(',')[6], 'name2': responsejson1.split('#')[i].split('@')[1] });
                }
            });
            this.setState({ Turnover: this.turn });
        });

    }

    highfun() {
        fetch('https://api.bseindia.com/msource/get52highlow.aspx?flag=0&ln=en').then((response) => response.text()).then((responsejson) => {
            this.setState({ High: responsejson.split('#')});
            console.log(this.state.High);
         
        });

    }
    lowfun() {
        this.lowarray = [];
        this.low=[];
        fetch('https://api.bseindia.com/msource/get52highlow.aspx?flag=1&ln=en').then((response) => response.text()).then((responsejson) => {

            for (let i = 0; i < responsejson.split('#').length; i++) {
                this.low.push({ 'id': responsejson.split('#')[i].split('@')[0], 'change': responsejson.split('#')[i].split('@')[2], 'ltp': responsejson.split('#')[i].split('@')[3], 'name': responsejson.split('#')[i].split('@')[1], });
            }
            
            setTimeout(() => { this.setState({ refreshing: false, loader: false }) }, 4000);
        });
    }


    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {

        this.props.navigation.goBack(null);
        return true;
    }
    back() {
        this.props.navigation.goBack(null);
        return true;
    }
    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }

    more() {
        this.setState({ isVisible: true });
    }

    closePopover() {
        this.setState({ isVisible: false });
    }
    refresh() {
        this.gain = [];
        this.lose = [];
        this.turn = [];
        this.high = [];
        this.low = [];
        this.setState({ Gainer: [], Loser: [], Turnover: [], High: [], Low: [] })
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
                "Application requires Network to proceed",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50
            );
        });
        this.sensexfun();
        this.gainfun();
        this.losefun();
        this.turnoverfun();
        this.highfun();
        this.lowfun();
    }
    onRefresh() {
        this.setState({ refreshing: true });

        this.refresh();
    }
    closesidemenu() {
        this.setState({ sidemenu: false });
    }
    equity() {
        this.setState({ sidemenu: false });
    }
    indices() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('indices', { 'theme': dt });
        });
    }
    sensex() {
        // AsyncStorage.getItem('themecolor').then((dt) => {
        //     this.setState({ sidemenu: false });
        //     this.props.navigation.navigate('sensex', { 'theme': dt });
        // });
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('setting', { 'theme': dt,'current': 0 });
      
          });

    }
    home() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('maindark', { 'theme': dt });
        });

    }
    derivative() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('derivative', { 'theme': dt });
        });

    }
    currency() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('currency', { 'theme': dt });
        });

    }
    commodity() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('commodity', { 'theme': dt });
        });

    }
    settingpage(val)
  {
    AsyncStorage.getItem('themecolor').then((dt) => {
        this.setState({ sidemenu: false });
        this.props.navigation.navigate('setting', { 'theme': dt,'current': val });
  
      });
  }

    setting() {
        this.setState({ settingshow: true });
        this.setState({ sidemenu: false });
        // this.props.navigation.navigate('setting');
    }

    editicon(val) {
        val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
        // this.setState({showedit:!val});
    }

    changeTheme(thm) {
        this.setState({ theme: thm, settingshow: false, sidemenu: false });
        AsyncStorage.setItem("themecolor", thm);
    }
    chnglanguage(lan) {
        this.setState({ settingshow: false, sidemenu: false });

        AsyncStorage.setItem('language', lan);
    }
    googleassistant() {
        Linking.openURL('https://assistant.google.com/explore');

    }
    equityView()
    {
        this.state.equity == false ? this.setState({equity:true,iconname:'ios-arrow-dropdown'}) : this.setState({equity:false,iconname:'ios-arrow-forward'}) ;
    }
    sensexView()
    {
        this.state.sensex == false ? this.setState({sensex:true,iconnamesensex:'ios-arrow-dropdown'}) : this.setState({sensex:false,iconnamesensex:'ios-arrow-forward'}) ;
    }
    smeView()
    {
        this.state.sme == false ? this.setState({sme:true,iconnamesme:'ios-arrow-dropdown'}) : this.setState({sme:false,iconnamesme:'ios-arrow-forward'}) ;
    }
    derivativeView()
    {
        this.state.derivative == false ? this.setState({derivative:true,iconnamederivative:'ios-arrow-dropdown'}) : this.setState({derivative:false,iconnamederivative:'ios-arrow-forward'}) ;
    }
    currencyView()
    {
        this.state.currency == false ? this.setState({currency:true,iconnamecurrency:'ios-arrow-dropdown'}) : this.setState({currency:false,iconnamecurrency:'ios-arrow-forward'}) ;
    }
    commadityView()
    {
        this.state.commodity == false ? this.setState({commodity:true,iconnamecommodity:'ios-arrow-dropdown'}) : this.setState({commodity:false,iconnamecommodity:'ios-arrow-forward'}) ;
    }
    irdView()
    {
        this.state.ird == false ? this.setState({ird:true,iconnameird:'ios-arrow-dropdown'}) : this.setState({ird:false,iconnameird:'ios-arrow-forward'}) ;
    }
    etfView()
    {
        this.state.etf == false ? this.setState({etf:true,iconnameetf:'ios-arrow-dropdown'}) : this.setState({etf:false,iconnameetf:'ios-arrow-forward'}) ;
    }
    debtView()
    {
        this.state.debt == false ? this.setState({debt:true,iconnamedebt:'ios-arrow-dropdown'}) : this.setState({debt:false,iconnamedebt:'ios-arrow-forward'}) ;
    }
    corporateView()
    {
        this.state.corporate == false ? this.setState({corporate:true,iconnamecorporate:'ios-arrow-dropdown'}) : this.setState({corporate:false,iconnamecorporate:'ios-arrow-forward'}) ;
    }
    pagechange(obj)
    {
        var page = obj;
       // alert(page.position);
    }


    render() {


        return (
            <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
                <StatusBar backgroundColor={this.state.value == '0' ? '#f6f7f9' : '#0b0b0b'} />
                <ViewPager 
                style={{ height: height }} 
                initialPage={1} 
                onPageScroll={(i) => this.pagechange(i)} 
                >
                    <View>
                        <Row style={{ margin: 5 }} >
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>Gainer</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.Gainer}
                            style={{ marginTop: 50 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={item.id == '' ? styles.hide : styles.show}>
                                    <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                        <Col style={{ width: '90%', height: 58 }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name1}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><View style={{ width: 60, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 }} ><Text numberOfLines={1} style={{ alignSelf: 'flex-end', marginRight: 5, color: 'white' }}>{item.change}</Text></View><Text style={this.state.value == '0' ? styles.perlight : styles.perdark}>+{item.value1} +{item.value2}%</Text></Col>

                                            </Row>
                                        </Col>

                                        <Col style={{ width: '6.5%', height: 58 }}></Col>
                                        <Col style={{ width: '100%', height: 58, }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name2}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={{ marginLeft: 42, marginRight: 8, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', textAlign: 'center', borderRadius: 4, }}></Text><Text numberOfLines={1} style={styles.perlight}>{item.value1}%</Text></Col>

                                            </Row>
                                        </Col>
                                    </Row>
                                    <Text></Text>

                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>

                            </FooterTab>
                        </Footer>

                    </View>
                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>Loser</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.Loser}
                            style={{ marginTop: 50 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={item.id == '' ? styles.hide : styles.show}>
                                    <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                        <Col style={{ width: '90%', height: 58 }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name1}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><View style={{ width: 60, marginTop: 8, color: 'white', backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 }} ><Text numberOfLines={1} style={{ alignSelf: 'flex-end', marginRight: 5, color: 'white' }}>{item.change}</Text></View><Text numberOfLines={1} style={this.state.value == '0' ? styles.perlight : styles.perdark}>{item.value1} {item.value2}%</Text></Col>

                                            </Row>
                                        </Col>

                                        <Col style={{ width: '6.5%', height: 58 }}></Col>
                                        <Col style={{ width: '100%', height: 58, }}>
                                            <Row style={item.name2 == undefined ? styles.hide : styles.show}>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name2}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={{ marginLeft: 42, marginRight: 8, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', textAlign: 'center', borderRadius: 4, }}></Text><Text numberOfLines={1} style={styles.perlight}>{item.value1}%</Text></Col>

                                            </Row>
                                        </Col>
                                    </Row>
                                    <Text></Text>

                                </View>

                            }
                            keyExtractor={item => item.id}
                        />

                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>Top Turnover</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O( <IconFonsito name="rupee" style={{}} color={this.state.value == '0' ? 'black' : '#72a3bf'} size={13}></IconFonsito> Cr. )</Text></Col>
                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.Turnover}
                            style={{ marginTop: 50 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={item.name1 == undefined ? styles.hide : styles.show}>
                                    <Row style={{ marginLeft: 17, marginRight: 17 }}>
                                        <Col style={{ width: '90%', height: 58, }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name1}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><View style={item.per > 0 ? styles.ltpviewpos : styles.ltpviewneg} ><Text style={item.per > 0 ? styles.pos : styles.neg}>{item.change}</Text></View><Text numberOfLines={1} style={this.state.value == '0' ? styles.perturnlight : styles.perturndark}>{item.value}</Text></Col>

                                            </Row>
                                        </Col>
                                        <Col style={{ width: '6.5%', height: 58 }}></Col>
                                        <Col style={{ width: '100%', height: 58, }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name2}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={{ marginLeft: 42, marginRight: 8, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', textAlign: 'center', borderRadius: 4, }}>{item.change} </Text><Text numberOfLines={1} style={{ color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, marginLeft: 9, fontSize: 13 }}>{item.value}</Text></Col>

                                            </Row>
                                        </Col>
                                    </Row>
                                    <Text></Text>
                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>52 week High</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>

                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>
                        {/* <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>52 week High</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text></Col>
                        </Row> */}
                         <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={this.state.value == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>52 week High</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text></Col>
                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.High}
                            style={{ marginTop: 50 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={item.split('@')[0] == '' ? styles.hide : styles.show}>
                                    <Row style={{ marginLeft: 17, marginRight: 17 }}>
                                        <Col style={{ width: '100%', height: 58, }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split('@')[1]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><View style={styles.ltpviewpos} ><Text style={styles.pos}>{item.split('@')[2]} </Text></View><Text numberOfLines={1} style={this.state.value == '0' ? styles.perturnlight : styles.perturndark}>{item.split('@')[3]}</Text></Col>

                                            </Row>
                                        </Col>
                                        <Col style={{ width: '6.5%', height: 58 }}></Col>
                                        {/* <Col style={{ width: '100%', height: 58, }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name2}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text style={{ marginLeft: 42, marginRight: 8, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', textAlign: 'center', borderRadius: 4, }}>{item.change} </Text><Text numberOfLines={1} style={{ color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, marginLeft: 9, fontSize: 13 }}>{item.ltp}</Text></Col>

                                            </Row>
                                        </Col> */}
                                    </Row>
                                    <Text></Text>
                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>52 week Low</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={this.state.value == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>52 week Low</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text></Col>
                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.Low}
                            style={{ marginTop: 50 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <Row style={{ marginLeft: 17, marginRight: 17 }}>
                                        <Col style={{ width: '100%', height: 58, }}>
                                            <Row>
                                                <Col style={item.name == undefined && this.state.value == '0' ? styles.und : item.name == undefined && this.state.value == '1' ? styles.unddark : this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.name}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                                <Col style={item.name == undefined && this.state.value == '0' ? styles.und1 : item.name == undefined && this.state.value == '1' ? styles.unddark1 : this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><View style={styles.ltpviewneg} ><Text style={item.change == undefined ? styles.hide : styles.neg}>{item.change} </Text></View><Text numberOfLines={1} style={this.state.value == '0' ? styles.perturnlight : styles.perturndark}>{item.ltp}</Text></Col>

                                            </Row>
                                        </Col>
                                        
                                    </Row>
                                    <Text></Text>
                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                </ViewPager>

                <Modal
                    animationType="slide"
                    transparent={false}
                    style={{ height: '100%' }}
                    visible={this.state.sidemenu}
                    onRequestClose={() => {
                        this.setState({ sidemenu: false });
                    }}
                >
                    <View style={this.state.value == '0' ? styles.lightmenu : styles.darkmenu}>
                        <Row style={{ marginLeft: 20, marginTop: 10 }}>
                            <Col style={{ width: '50%', height: 40 }}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={{ marginLeft: 10 }} size={40} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>

                        </Row>

                        <ScrollView style={{ top: '8%', }}>
                            <Image style={{ width: 150, height: 150, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/lightprofile.png') : require('../images/darkprofile.png')} />
                            {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
                            <Text></Text>
                            <View style={{}}>
                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Home</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>

                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.equityView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Equity</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconname} onPress={() => this.equityView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.equity == false ? styles.hide : styles.show}>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GAINERS</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>LOSERS</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TOP TURNOVER</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK HIGH</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK LOW</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Indices</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>

                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.sensexView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Sensex</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamesensex} onPress={() => this.sensexView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.sensex == false ? styles.hide : styles.show}>
                                    <Text onPress={() => this.settingpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>SECURITY</Text>
                                    <Text onPress={() => this.settingpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>OVERVIEW</Text>
                                    <Text onPress={() => this.settingpage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TURNOVER</Text>
                                    <Text onPress={() => this.settingpage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CONTRIBUTION</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.smeView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >SME</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamesme} onPress={() => this.smeView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.sme == false ? styles.hide : styles.show}>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET STATISTICS</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.derivative()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Derivatives</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamederivative} onPress={() => this.derivativeView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.derivative == false ? styles.hide : styles.show}>

                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.currency()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Currency</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecurrency} onPress={() => this.currencyView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.currency == false ? styles.hide : styles.show}>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.commodity()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Commadity</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecommodity} onPress={() => this.commadityView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.commodity == false ? styles.hide : styles.show}>

                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IRD</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnameird} onPress={() => this.irdView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.ird == false ? styles.hide : styles.show}>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                    <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>ETF</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnameetf} onPress={() => this.etfView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.etf == false ? styles.hide : styles.show}>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ETF WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Debt</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamedebt} onPress={() => this.debtView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.debt == false ? styles.hide : styles.show}>
                                
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>DEBT MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BONDS-OTC TRADES</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL CORP. DEBT</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GSEC</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL GOV BONDS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BOND-NDS-RST</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>EBP</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Corporates</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecorporate} onPress={() => this.corporateView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                </Row>
                                <View style={this.state.corporate == false ? styles.hide : styles.show}>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ANNOUNCEMENTS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ACTIONS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RESULTS CALENDAR</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>BOARD MEETINGS</Text>
                                </View>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '70%', height: 40,}}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Statistics</Text></Col>
                                    <Col style={{ width: '30%', height: 40}}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Turnover</Text></Col>
                                    <Col style={{ width: '30%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPO/OFS</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Listings</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Notices</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Watchlist</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Portfolio</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                    <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPF</Text></Col>
                                    <Col style={{ width: '50%', height: 40, }}></Col>
                                </Row>
                                
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>
                                <Text></Text>
                                <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', }}></Text>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>




            </View>
        );
    }
}

const styles = StyleSheet.create({

    mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
    mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
    inactive: { padding: 12, },
    titlelight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    titledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    pos: { backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 5, color: 'white' },
    neg: { backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 5, color: 'white' },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamedark: { color: '#79868e', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
    lightdate: { color: '#132144', textAlign: 'right', marginTop: 10, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    darkdate: { color: '#72a3bf', textAlign: 'right', marginTop: 10, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    statuslight: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
    statusdark: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
    listlightcol1: { width: '50%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    und: { width: '50%', height: 58, backgroundColor: '#f6f7f9', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    unddark: { width: '50%', height: 58, backgroundColor: '#0b0b0b', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    und1: { width: '50%', height: 58, backgroundColor: '#f6f7f9', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    unddark1: { width: '50%', height: 58, backgroundColor: '#0b0b0b', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listlightcol2: { width: '50%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcol1: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcol2: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', },
    listlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', },
    show: { display: 'flex' },
    hide: { display: 'none' },
    possensex: { marginLeft: 15, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    lighttext1: { marginLeft: 15, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex1: { alignSelf: 'flex-end', marginRight: 7, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex: { alignSelf: 'center', marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex1: { alignSelf: 'center', color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexvalue: { marginRight: 6, fontSize: 12.5, alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexvalue: { marginRight: 6, fontSize: 12.5, alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    negsensex: { marginLeft: 15, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightltp: { alignSelf: 'flex-end', marginRight: 6, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    darkltp: { alignSelf: 'flex-end', marginRight: 6, marginTop: 9, color: '#2087c9', fontFamily: 'SegoeProDisplay-Regular', },
    darktext1: { marginLeft: 15, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    perlight: { alignSelf: 'flex-end', marginRight: 8, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perdark: { alignSelf: 'flex-end', marginRight: 8, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perturnlight: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    perturndark: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    ltpviewpos: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    ltpviewneg: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    lightheader1: { width: '32%', height: 40 },
    darkheader1: { width: '32%', height: 40 },
    lightheader2: { width: '30%', height: 40 },
    darkheader2: { width: '30%', height: 40 },
    lightheader3: { width: '33%', height: 40 },
    darkheader3: { width: '33%', height: 40 },
    lightheader4: { width: '57%', height: 40 },
    darkheader4: { width: '57%', height: 40 },

    lightheadertext: { marginLeft: 15, fontSize: 15, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext: { marginLeft: 15, fontSize: 15, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightheadertext1: { alignSelf: 'center', color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext1: { alignSelf: 'center', color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lighttext: { marginLeft: 15, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darktext: { marginLeft: 15, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkmenutext: { marginLeft: 15, fontSize: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 16 },
    darkline: { marginLeft: 30, borderBottomColor: '#72a3bf', borderBottomWidth: 1, marginRight: 25, bottom: 16 },

})

export default Gainer;
