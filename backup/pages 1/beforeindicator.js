// import React, { Component } from 'react';
// import { View, Text, FlatList, Switch, BackHandler, CheckBox, RefreshControl, AsyncStorage, ToastAndroid, Modal, StatusBar, ActivityIndicator, ScrollView, SafeAreaView, Linking, StyleSheet, Alert, Image, Dimensions } from 'react-native';
// import SplashScreen from 'react-native-splash-screen';
// import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
// import { Row } from 'react-native-easy-grid';
// import Icon from 'react-native-vector-icons/Ionicons';
// import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
// import IconEnt from 'react-native-vector-icons/AntDesign';
// import IconFonsito from 'react-native-vector-icons/FontAwesome';
// import { LineChart } from "react-native-chart-kit";
// import Carousel from 'react-native-snap-carousel';
// import Popover from 'react-native-popover-view'
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';

// // import Carousell from 'react-native-carousel-view';

// const { width: screenWidth } = Dimensions.get('window')

// class Test extends Component {
//   watchArray = [];
//   // indicesArray = [];
//   currencyArray = [];
//   radio_props = [];
//   color;
//   activetab = 'home';
//   ltp = '';
//   chng = '';
//   per = '';
//   date = '';
//   status = 'Close';
//   sensexData = [];
//   scodeArray = [];
//   watch = [];
//   Data = '';
//   code = [];
//   watchval = '';
//   portval = '';
//   indval = '';
//   commoval = '';
//   heatmapval = '';
//   currval = '';

//   // watcharray=[];
//   static navigationOptions = {

//     header: null,
//   };
//   constructor(props) {
//     super(props);
//     this.state = {
//       isVisible: false,
//       theme: 'dark',
//       value: 0,
//       sensexData: [],
//       ltp: '',
//       chng: '',

//       per: '',
//       date: '',
//       status: 'Close',
//       indicesArray: [],
//       currencyArray: [],
//       sensexHeatArray: [],
//       commodityArray: [],
//       switchval: false,
//       themename: 'Light',
//       loader: true,
//       indicesloader: true,
//       commodityloader: true,
//       heatmaploader: true,
//       currencyloader: true,
//       sensexDetailloader: true,
//       modvis: false,
//       Warray: [],
//       editIcon: 'ios-arrow-dropright-circle',
//       edit: false,
//       refreshing: false,
//       watch: true,
//       port: true,
//       indices: true,
//       commodity: true,
//       heatmap: true,
//       currency: true,
//       one: {}, two: {}, three: {}, four: {}, five: {}, six: {},
//       onecurr: {}, twocurr: {}, threecurr: {}, fourcurr: {}, fivecurr: {}, sixcurr: {},
//     }
//     setTimeout(() => { SplashScreen.hide() }, 4000);


//     this.radio_props = [
//       { label: 'Light Mode Theme', value: 0 },
//       { label: 'Dark Mode Theme', value: 1 }
//     ];


//   }

//   showview(val, name) {
//     alert(val + '  ' + name);
//   }

//   componentWillMount() {
//     BackHandler.addEventListener('hardwareBackPress', this.backPressed);
//   }

//   componentWillUnmount() {
//     BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
//   }

//   backPressed = () => {
//     Alert.alert(
//       'Exit App',
//       'Do you want to exit?',
//       [
//         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
//         { text: 'Yes', onPress: () => BackHandler.exitApp() },
//       ],
//       { cancelable: false });
//     return true;
//   }

//   more() {
//     this.setState({ isVisible: true });
//   }
//   edit() {
//     this.setState({ isVisible: false });

//     this.setState({ edit: true });
//     //this.state.edit == 'false' ? this.setState({ edit: 'true', editIcon: 'ios-arrow-up' }) : this.setState({ edit: 'false', editIcon: 'ios-arrow-down' });
//   }

//   toggleSwitch(val) {
//     //alert(''+val);

//     if (val == true) {
//       this.setState({ value: 1 });
//       this.setState({ isVisible: false });
//       this.setState({ themename: 'Dark' });

//     }
//     else {
//       this.setState({ value: 0 });
//       this.setState({ isVisible: false });
//       this.setState({ themename: 'Light' });

//     }
//     this.setState({ switchval: val });
//   }

//   closePopover() {
//     this.setState({ isVisible: false });
//   }

//   changetheme(thmval) {
//     // Alert.alert('change');
//     this.setState({ value: thmval })
//     this.setState({ isVisible: false });
//     console.log(this.state.value);

//   }

//   _renderItemlight({ item, index }) {
//     return (
//       <View>
//         <View style={styles.watchwhite} >
//           <Text style={styles.watchnamelight}>{item.sname}</Text>
//           <Text style={styles.watchvallight}>{item.ltp} <Text style={item.chgper.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>  {item.chgval}  {item.chgper}%</Text></Text>
//           <Image style={{ width: 140, height: 33, marginLeft: '10%', marginTop: 20, bottom: 5 }} source={require('../images/pulse.jpg')} />
//         </View>
//         <Text></Text>
//       </View>
//     )
//   }
//   _renderItemdark({ item, index }) {
//     return (

//       <View>
//         <View style={styles.watchdark} >
//           <Text style={styles.watchnamedark}>{item.sname}</Text>
//           <Text style={styles.watchvaldark}>{item.ltp} <Text style={item.chgper.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>  {item.chgval}%  {item.chgper}%</Text></Text>
//           <Image style={{ width: 140, height: 33, marginLeft: '10%', marginTop: 20, bottom: 5 }} source={require('../images/rsz_new_design_hompage_black-theme_code.jpg')} />
//         </View>
//         <Text></Text>
//       </View>
//     )
//   }

//   tab(val) {
//     if (val == 'watch') { }
//     if (val == 'port') { }
//     if (val == 'home') { }
//     if (val == 'search') { }
//     if (val == 'more') { }
//   }

//   componentDidMount() {
//     this.sensexHeatMapFun();
//     this.indicesFun();
//     this.edithome();
//     this.watchListFun();
//     this.sensexFun();
//     this.commodityFun();
//     this.currencyFun();
//   }
//   edithome() {
//     AsyncStorage.getItem('watchval').then((dt) => {
//       if (dt == null || dt == undefined) {
//         this.setState({ watch: true });
//       }
//       else {
//         this.setState({ watch: false });
//       }
//     });
//     AsyncStorage.getItem('portval').then((dt) => {

//       if (dt == null || dt == undefined) {
//         this.setState({ port: true });
//       }
//       else {
//         this.setState({ port: false });
//       }

//     });
//     AsyncStorage.getItem('indval').then((dt) => {

//       if (dt == null || dt == undefined) {
//         this.setState({ indices: true });
//       }
//       else {
//         this.setState({ indices: false });
//       }

//     });
//     AsyncStorage.getItem('commodt').then((dt) => {

//       if (dt == null || dt == undefined) {
//         this.setState({ commodity: true });
//       }
//       else {
//         this.setState({ commodity: false });
//       }

//     });
//     AsyncStorage.getItem('heatmap').then((dt) => {

//       if (dt == null || dt == undefined) {
//         this.setState({ heatmap: true });
//       }
//       else {
//         this.setState({ heatmap: false });
//       }

//     });
//     AsyncStorage.getItem('curr').then((dt) => {

//       if (dt == null || dt == undefined) {
//         this.setState({ currency: true });
//       }
//       else {
//         this.setState({ currency: false });
//       }

//     });
//   }
//   ok() {
//     AsyncStorage.getItem('watchval').then((dt) => {
//       if (dt == null) { this.setState({ watch: true }) }
//       else { this.setState({ watch: false }); }
//     });
//     AsyncStorage.getItem('portval').then((dt) => {
//       if (dt == null) { this.setState({ port: true }) }
//       else { this.setState({ port: false }); }

//       // this.portval = dt;
//       // console.log('portval', this.portval);
//     });
//     AsyncStorage.getItem('indval').then((dt) => {
//       if (dt == null) { this.setState({ indices: true }) }
//       else { this.setState({ indices: false }); }
//       // console.log(dt);

//       // this.indval = dt;
//       // console.log('watchval', this.indval);
//     });
//     AsyncStorage.getItem('commodt').then((dt) => {
//       if (dt == null) { this.setState({ commodity: true }) }
//       else { this.setState({ commodity: false }); }
//       // console.log(dt);

//       // this.commoval = dt;
//       // console.log('watchval', this.commoval);
//     });
//     AsyncStorage.getItem('heatmap').then((dt) => {
//       if (dt == null) { this.setState({ heatmap: true }) }
//       else { this.setState({ heatmap: false }); }
//       // console.log(dt);

//       // this.heatmapval = dt;
//       // console.log('watchval', this.heatmapval);
//     });
//     AsyncStorage.getItem('curr').then((dt) => {
//       if (dt == null) { this.setState({ currency: true }) }
//       else { this.setState({ currency: false }); }
//       // console.log(dt);

//       // this.currval = dt;
//       // console.log('watchval', this.currval);
//     });
//     this.setState({ edit: false });
//   }
//   sensexFun() {
//     this.setState({ loader: true });
//     fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

//       this.sensexData = responsejson;
//       console.log(this.sensexData);

//       this.setState({ ltp: this.sensexData[0].ltp, chng: this.sensexData[0].chg, per: this.sensexData[0].perchg, date: this.sensexData[0].dttm, });
//       switch (this.sensexData[0].F) {
//         case '0':
//           this.setState({ status: 'Close' });
//           break;
//         case '1':
//           this.setState({ status: 'Pre-Open' });

//           break;
//         case '2':
//           this.setState({ status: 'Close' });

//           break;
//         case '3':
//           this.setState({ status: 'Open' });

//           break;
//       }
//       this.setState({ loader: false });

//     })
//   }
//   indicesFun() {
//     this.setState({ indicesloader: true });
//     fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"en","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
//       this.setState({ indicesArray: responsejson });
//       this.setState({ indicesloader: false });
//     });
//   }
//   currencyFun() {
//     this.setState({ currencyloader: true });

//     fetch('https://api.bseindia.com/BseIndiaAPI/api/CurrHeatMap/m?Flag=9&Records=1000&scripcode=0&pg=2&cnt=5&field=CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM&ln=en').then((response) => response.json()).then((responsejson) => {
//       this.setState({ currencyArray: responsejson });
//       this.state.currencyArray.map((dt) => {
//         if (dt.CHANGEPERC.charAt(0) != '-') {
//           dt.CHANGEPERC = '+' + dt.CHANGEPERC;
//           dt.CHANGE = '+' + dt.CHANGE;

//         }
//       })
//       this.setState({ onecurr: this.state.currencyArray[0], twocurr: this.state.currencyArray[1], threecurr: this.state.currencyArray[2], fourcurr: this.state.currencyArray[3], fivecurr: this.state.currencyArray[4], sixcurr: this.state.currencyArray[5] });
//       this.setState({ currencyloader: false });

//     });

//   }
//   sensexHeatMapFun() {
//     this.setState({ heatmaploader: true });

//     // fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
//     //   this.code = responsejson[0].scode.split('|');
//     //   console.log('code', this.code);

//     //   // this.setState({ sensexHeatArray: [] });
//     //   this.scodeArray = [];


//     //   for (let i of this.code) {
//     //     console.log(i);

//     //     fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + i + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
//     //       // console.log(responsejson);
//     //       // console.log(responsejson[0].scode);
//     //       if (responsejson[0].scode == undefined || responsejson[0].scode == null || responsejson[0].scode == '') { }
//     //       else {
//     //         this.scodeArray.push(responsejson[0]);

//     //       }

//     //     });
//     //   }
//     //   console.log(this.scodeArray);

//     //   this.scodeArray.map((dt) => {
//     //     if (dt.chgval.charAt(0) != '-') {
//     //       dt.chgval = '+' + dt.chgval;
//     //       dt.chgper = '+' + dt.chgper;

//     //     }
//     //   })
//     //   // console.log('scode',this.scodeArray);
//     //   setInterval(() => this.setState({ sensexHeatArray: this.scodeArray }), 1000);
//     // console.log(this.scodeArray);

//     // this.setState({ sensexHeatArray: this.scodeArray });
//     // console.log('sen', this.state.sensexHeatArray);
//     this.scodeArray = [
//       { scode: "532215", sname: "AXISBANK", ltp: "431.55", PrevClose: "406.50", chgval: "25.05", chgper: "6.16" },
//       { scode: "532978", sname: "BAJAJFINSV", ltp: "6155.10", PrevClose: "5840.30", chgval: "314.80", chgper: "5.39" },
//       { scode: "500820", sname: "ASIANPAINT", ltp: "1689.20", PrevClose: "1687.35", chgval: "1.85", chgper: "0.11" },
//       { scode: "532977", sname: "BAJAJ-AUTO", ltp: "2841.65", PrevClose: "2825.60", chgval: "16.05", chgper: "0.57" },
//       { scode: "532454", sname: "BHARTIARTL", ltp: "560.90", PrevClose: "559.65", chgval: "1.25", chgper: "6.16" },
//       { scode: "532977", sname: "BAJAJ-AUTO", ltp: "2841.65", PrevClose: "2825.60", chgval: "16.05", chgper: "0.57" },

//     ];

//     // this.setState({ sensexHeatArray: this.scodeArray });
//     console.log('sensex', this.scodeArray);
    
    
//     this.scodeArray.map((dt) => {
//       if (dt.chgval.charAt(0) != '-') {
//         dt.chgval = '+' + dt.chgval;
//         dt.chgper = '+' + dt.chgper;

//       }
//     })
//     console.log(JSON.parse(this.scodeArray[0]));
//     console.log(this.scodeArray[1]);
//     console.log(this.scodeArray[2]);
    
//     this.setState({ sensexHeatArray: this.scodeArray });
//     console.log(this.state.sensexHeatArray);

//     this.setState({ one: this.scodeArray[0], two: this.scodeArray[1], three: this.scodeArray[2], four: this.scodeArray[3], five: this.scodeArray[4], six: this.scodeArray[5] });
//     console.log(this.state.one);
//     console.log(this.state.two);
//     console.log(this.state.three);
//     console.log(this.state.four);
//     console.log(this.state.five);
//     console.log(this.state.six);
//     this.setState({ heatmaploader: false });
//     // });


//   }
//   commodityFun() {
//     this.setState({ commodityloader: true });

//     fetch('https://api.bseindia.com/BseIndiaAPI/api/CommHeatMap/m?Flag=8&Records=1000&scripcode=0&pg=1&cnt=6&field=CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM&ln=en').then((response) => response.json()).then((responsejson) => {
//       this.setState({ commodityArray: responsejson });
//       this.state.commodityArray.map((dt) => {
//         if (dt.CHANGEPERC.charAt(0) != '-') {
//           dt.CHANGEPERC = '+' + dt.CHANGEPERC;
//           dt.CHANGE = '+' + dt.CHANGE;

//         }
//       })
//       console.log(this.state.commodityArray);

//       this.setState({ one: this.state.commodityArray[0], two: this.state.commodityArray[1], three: this.state.commodityArray[2], four: this.state.commodityArray[3], five: this.state.commodityArray[4], six: this.state.commodityArray[5] });

//       this.setState({ commodityloader: false });

//     });
//   }

//   firstFun() {

//   }
//   watchfun() {

//   }

//   googleassistant() {

//     // Linking.openURL('https://assistant.google.com/explore');
//     AsyncStorage.clear();
//   }
//   refresh() {

//     this.sensexHeatMapFun();
//     this.sensexFun();
//     this.indicesFun();

//     this.commodityFun();
//     this.currencyFun();
//     // AsyncStorage.clear();


//   }

//   detailsensex(cd) {
//     //alert(''+data.sname);
//     //this.props.navigation.navigate('snsxdtl',{'detail':data});
//     this.setState({ modvis: true });
//     this.setState({ sensexDetailloader: true });
//     fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {

//       this.Data = responsejson[0];

//       this.setState({ sensexDetailloader: false });

//     });

//   }

//   add(dt) {

//     AsyncStorage.getItem('array').then((data) => {
//       const val = JSON.parse(data);
//       console.log('val', val);
//       if (val == null) {
//         console.log('1');

//         this.watchArray.push(dt);
//         console.log(this.watchArray);


//         console.log('val', this.watchArray);
//         var ary = this.watchArray.filter((obj, pos, arr) => {
//           return arr.map(mapObj =>
//             mapObj.scode).indexOf(obj.scode) == pos;
//         });
//         console.log('ary', ary);

//         this.setState({ Warray: ary });

//         AsyncStorage.setItem('array', JSON.stringify(ary));
//         ToastAndroid.showWithGravityAndOffset(
//           "Added to Watchlist Successfully.",
//           ToastAndroid.LONG,
//           ToastAndroid.BOTTOM,
//           25,
//           50
//         );
//       }
//       else if (val != '') {
//         console.log('2');
//         val.push(dt);
//         console.log('val', val);
//         var ary = val.filter((obj, pos, arr) => {
//           return arr.map(mapObj =>
//             mapObj.scode).indexOf(obj.scode) == pos;
//         });
//         console.log('ary', ary);

//         this.setState({ Warray: ary });

//         AsyncStorage.setItem('array', JSON.stringify(ary));
//         ToastAndroid.showWithGravityAndOffset(
//           "Added to Watchlist Successfully.",
//           ToastAndroid.LONG,
//           ToastAndroid.BOTTOM,
//           25,
//           50
//         );
//       }


//     })


//     this.setState({ modvis: false });

//   }
//   watchListFun() {
//     AsyncStorage.getItem('array').then((data) => {
//       console.log('data', JSON.parse(data));
//       this.setState({ Warray: JSON.parse(data) });
//       console.log(this.state.Warray);
//       this.state.Warray.map((dt) => {
//         if (dt.chgper.charAt(0) != '-') {
//           dt.chgper = '+' + dt.chgper;
//           dt.chgval = '+' + dt.chgval;

//         }
//       });

//     })
//   }

//   onRefresh() {
//     this.refresh();
//   }

//   oncheckWatch() {
//     //this.setState({ watch: !val });
//     this.state.watch == true ? AsyncStorage.setItem('watchval', "false") : AsyncStorage.removeItem('watchval');
//   }
//   oncheckPort() {
//     // this.setState({ port: !val });
//     this.state.port == true ? AsyncStorage.setItem('portval', "false") : AsyncStorage.removeItem('portval');
//   }
//   oncheckIndices() {
//     // this.setState({ indices: !val });
//     this.state.indices == true ? AsyncStorage.setItem('indval', "false") : AsyncStorage.removeItem('indval');
//   }
//   oncheckCommodit() {
//     // this.setState({ commodity: !val });
//     this.state.commodity == true ? AsyncStorage.setItem('commodt', "false") : AsyncStorage.removeItem('commodt');
//   }
//   oncheckSensex() {
//     // this.setState({ heatmap: !val });
//     this.state.heatmap == true ? AsyncStorage.setItem('heatmap', "false") : AsyncStorage.removeItem('heatmap');
//   }
//   oncheckCurrency() {
//     // this.setState({ currency: !val });
//     this.state.currency == true ? AsyncStorage.setItem('curr', "false") : AsyncStorage.removeItem('curr');
//   }

//   _renderDotIndicator() {
//     return <PagerDotIndicator pageCount={3} dotStyle={{ backgroundColor: '#b7b3b3', top: 30, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#2087c9', top: 30, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
//   }


//   render() {

//     return (
//       <View>

//         <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
//           <StatusBar hidden={true} />
//           <ScrollView style={this.state.value == '0' ? styles.scrollviewlight : styles.scrollviewdark}
//             refreshControl={
//               <RefreshControl
//                 refreshing={this.state.refreshing}
//                 onRefresh={this.onRefresh.bind(this)}
//               />
//             }
//             showsVerticalScrollIndicator={false}

//           >

//             <Header style={this.state.value == '0' ? styles.light : styles.dark}>

//               {/* <Row style={{bottom:15}}>
//                 <Col style={this.state.value == '0' ? styles.headermaxcollight : styles.headermaxcoldark}><TouchableOpacity></TouchableOpacity></Col>
//                 <Col style={this.state.value == '0' ? styles.headermincollight : styles.headermincoldark}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 4, }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
//                 <Col style={this.state.value == '0' ? styles.headermincollight : styles.headermincoldark}><TouchableOpacity activeOpacity={.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'right',marginRight:15 }} size={30} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></TouchableOpacity></Col>
//               </Row> */}
//               <Left>
//                 <TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, marginLeft: 270 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity>

//               </Left>
//               <Right>
//                 <TouchableOpacity activeOpacity={.5} onPress={() => this.more()}><Icon name="md-more" style={{ marginTop: 5, marginRight: 13 }} size={30} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></TouchableOpacity>

//               </Right>

//             </Header>

//             <View style={this.state.value == '0' ? styles.snpviewlight : styles.snpviewdark}>
//               <View style={this.state.loader == false ? styles.show : styles.hide}>
//                 {/* <Text></Text> */}
//                 <Row>
//                   <Col style={{ width: '17%', height: 65, }}></Col>
//                   <Col style={{ width: '73%', height: 65, }}><Thumbnail square style={{ width: 200, height: 53, marginTop: 5, marginLeft: '7%', }} source={this.state.value == '0' ? require('../images/sensex-removebg-preview.png') : require('../images/sen.jpg')} /></Col>
//                   <Col style={{ width: '10%', height: 65, }}></Col>
//                 </Row>



//                 <Row>
//                   <Col style={{ width: '17%', height: 65, }}></Col>
//                   <Col style={{ width: '31%', height: 40, }}><Text style={this.state.value == '0' ? styles.valuelight : styles.valuedark}>{this.state.ltp}</Text></Col>
//                   <Col style={{ width: '42%', height: 40, }}><Text style={{ fontSize: 15, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', marginTop: 5 }}>{this.state.chng}  {this.state.per}%</Text></Col>
//                   <Col style={{ width: '10%', height: 65, }}></Col>

//                 </Row>
//                 <Row>
//                   <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.datelight : styles.datedark}>{this.state.date} | {this.state.status}</Text></Col>
//                 </Row>
//               </View>
//               <View style={this.state.loader == true ? styles.show : styles.hide}>
//                 <ActivityIndicator size="large" style={{ bottom: 20 }} color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
//               </View>
//               <Row style={{ bottom: 35 }}>
//                 <Col style={{ width: '100%', height: 53, }}><Image
//                   style={{ width: Dimensions.get('window').width - 30, height: 53, margin: 15, }}
//                   source={require('../images/ad1.jpg')}
//                 /></Col>
//               </Row>
//               {/* <Image
//                 style={{ width: Dimensions.get('window').width - 30, height: 53,margin: 15,bottom:30}}
//                 source={require('../images/ad1.jpg')}
//               /> */}
//             </View>



//             <View style={this.state.watch == true ? styles.show : styles.hide}>

//               <View style={this.state.Warray != null ? styles.show : styles.hide}>

//                 <View style={{}}>
//                   <Row style={{ margin: 15, }}>
//                     <Col style={this.state.value == '0' ? styles.watchcollightup : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>Watchlist</Text></Col>
//                     <Col style={this.state.value == '0' ? styles.watchcollightup : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></Col>
//                   </Row>
//                 </View>

//                 <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', height: 120 }}>
//                   <Carousel
//                     layout={'default'}
//                     ref={ref => this.carousel = ref}
//                     data={this.state.Warray}
//                     sliderWidth={screenWidth}
//                     itemWidth={173}
//                     firstItem={1}
//                     renderItem={this.state.value == '0' ? this._renderItemlight : this._renderItemdark}
//                     inactiveSlideOpacity={this.state.value == '0' ? 10.95 : 0.9}
//                     inactiveSlideScale={0.9}
//                     inactiveSlideShift={4}

//                   />

//                 </View>

//               </View>

//             </View>
//             <View style={this.state.port == true ? styles.show : styles.hide}>

//               <View style={{ bottom: '1%' }}>
//                 <Row style={{ margin: 15, }}>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>Portfolio</Text></Col>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></Col>
//                 </Row>
//               </View>
//               <View style={{ bottom: '11%' }}>
//                 <Row style={{ margin: 10, }}>
//                   <Col style={{ width: '50%', height: 110 }}>
//                     <View style={this.state.value == '0' ? styles.portlightview : styles.portdarkview}>
//                       <Text style={this.state.value == '0' ? styles.lightgain : styles.darkgain}>Overall Gain</Text>
//                       {/* <Row style={{ marginTop: 10, }}>
//                         <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}><Image style={{ width: 55, height: 23, marginLeft: 20, }} source={this.state.value == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
//                         <Col style={{ width: '50%', height: 40 }}><Image style={{ width: 55, height: 23, marginLeft: 20, }} source={this.state.value == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
//                       </Row> */}
//                       <Row style={{ marginTop: 10, }}>
//                         <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 55, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
//                         <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
//                         <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 55, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
//                       </Row>
//                       <Row style={{}}>
//                         <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>113.65</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>in rupee</Text></Col>
//                         <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
//                         <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>566.68%</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>in percentage</Text></Col>
//                       </Row>
//                       <Text></Text>

//                     </View>
//                   </Col>
//                   <Col style={{ width: '50%', height: 110 }}>
//                     <View style={this.state.value == '0' ? styles.portlightview : styles.portdarkview}>
//                       <Text style={this.state.value == '0' ? styles.lightgain : styles.darkgain}>Today's Gain</Text>
//                       <Row style={{ marginTop: 10, }}>
//                         <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 70, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/down.jpg') : require('../images/rsz_1new_design_hompage_black-theme_code.jpg')} /></Col>
//                         <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
//                         <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 70, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/down.jpg') : require('../images/rsz_1new_design_hompage_black-theme_code.jpg')} /></Col>
//                       </Row>
//                       <Row style={{}}>
//                         <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>-6.00</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>in rupee</Text></Col>
//                         <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
//                         <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>-4.55%</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>in percentage</Text></Col>
//                       </Row>
//                       <Text></Text>


//                     </View>
//                   </Col>
//                 </Row>
//               </View>

//             </View>

//             <View style={this.state.port == false && this.state.watch == false ? styles.hide : styles.show}>
//               <View style={{ margin: 15, bottom: 10 }}>
//                 <Image
//                   style={{ width: Dimensions.get('window').width - 30, height: 53 }}
//                   source={require('../images/ad2.jpg')}
//                 />
//               </View>
//             </View>

//             <View style={this.state.indices == true ? styles.show : styles.hide}>

//               <View style={{ bottom: '2%' }}>
//                 <Row style={{ margin: 15, }}>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>Indices</Text></Col>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></Col>
//                 </Row>
//               </View>
//               <View style={this.state.indicesloader == true ? styles.showindloader : styles.hide}>
//                 <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
//               </View>
//               <View style={this.state.indicesloader == false ? styles.showindview : styles.hide}>
//                 <FlatList
//                   data={this.state.indicesArray}
//                   style={this.state.value == '0' ? styles.indiceslight : styles.indicesdark}
//                   renderItem={({ item }) =>
//                     <View>
//                       <Row style={this.state.value == '0' ? styles.showindices : styles.hide}>
//                         <Col style={{ width: '10%', height: 50, }}><Icon name={item.chg.charAt(0) != '-' ? "md-arrow-round-up" : "md-arrow-round-down"} style={item.chg.charAt(0) != '-' ? styles.iconpos : styles.iconneg} size={23} /></Col>
//                         <Col style={{ width: '4%', height: 50, }}></Col>
//                         <Col style={{ width: '61%', height: 50, }}><Text style={this.state.value == '0' ? styles.lightindtext : styles.darkindtext}>{item.indxnm}</Text><Text style={item.chg.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chg} {item.perchg}%</Text></Col>
//                         <Col style={{ width: '25%', height: 50, }}><Text style={this.state.value == '0' ? styles.lightvaluetext : styles.darkvaluetext}>{item.ltp}</Text></Col>
//                       </Row>
//                       <Row style={this.state.value == '1' ? styles.showindices : styles.hide}>
//                         <Col style={{ width: '10%', height: 50, }}><Icon name={item.chg.charAt(0) != '-' ? "md-arrow-round-up" : "md-arrow-round-down"} style={item.chg.charAt(0) != '-' ? styles.iconposdark : styles.iconnegdark} size={24} /></Col>
//                         <Col style={{ width: '1%', height: 50 }}></Col>
//                         <Col style={{ width: '64%', height: 50, }}><Text style={this.state.value == '0' ? styles.lightindtext : styles.darkindtext}>{item.indxnm}</Text><Text style={item.chg.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chg} {item.perchg}%</Text></Col>
//                         <Col style={{ width: '25%', height: 50, }}><Text style={this.state.value == '0' ? styles.lightvaluetext : styles.darkvaluetext}>{item.ltp}</Text></Col>
//                       </Row>
//                     </View>
//                   }
//                   keyExtractor={item => item.id}
//                 />
//               </View>

//             </View>

//             <View style={this.state.commodity == true ? styles.show : styles.hide}>

//               <View style={{ bottom: '4%' }}>
//                 <Row style={{ margin: 15, }}>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>Commodity</Text></Col>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></Col>
//                 </Row>
//               </View>

//               <View style={this.state.commodityloader == true ? styles.showcommodityloader : styles.hide}>
//                 <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
//               </View>
//               <View style={this.state.commodityloader == false ? styles.showcommodityview : styles.hide}>
//                 <View style={this.state.value == '0' ? styles.show : styles.hide}>
//                   {/* <FlatList
//                     data={this.state.commodityArray}
//                     horizontal={true}
//                     showsHorizontalScrollIndicator={false}
//                     style={{ marginLeft: 4, marginRight: 4 }}
//                     renderItem={({ item }) =>
//                       <View>
//                         <View style={styles.currlightview}>
//                           <Text style={styles.currnamelight} numberOfLines = { 1 }>{item.INSTRUMENTNAME}</Text>
//                           <Text style={styles.sensexltplight}>{item.LASTTRADERATE}</Text>

//                           <Text style={styles.currvaluelight}> <Text style={item.CHANGEPERC.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{item.CHANGE}   {item.CHANGEPERC}%</Text></Text>
//                           <Text style={{ marginTop: 5 }}></Text>
//                         </View>
//                         <Text></Text>
//                       </View>
//                     }
//                     keyExtractor={item => item.id}
//                   /> */}

//                   <IndicatorViewPager
//                     style={{ height: 100 }}
//                     indicator={this._renderDotIndicator()}
//                   >
//                     <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.one.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE}   {this.state.one.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview1}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.two.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE}   {this.state.two.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview2}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.three.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE}   {this.state.three.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                     </View>
//                     <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.three.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE}   {this.state.three.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview1}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.four.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE}   {this.state.four.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview2}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.five.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE}   {this.state.five.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                     </View>
//                     <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.five.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE}   {this.state.five.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview1}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.six.LASTTRADERATE}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE}   {this.state.six.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>

//                     </View>

//                   </IndicatorViewPager>

//                 </View>

//                 <View style={this.state.value == '1' ? styles.show : styles.hide}>
//                   <FlatList
//                     data={this.state.commodityArray}
//                     horizontal={true}
//                     showsHorizontalScrollIndicator={false}
//                     style={{ marginLeft: 4, marginRight: 4 }}
//                     renderItem={({ item }) =>
//                       <View>
//                         <View style={styles.currdarkview}>
//                           <Text style={styles.currnamedark} numberOfLines={1}>{item.INSTRUMENTNAME}</Text>
//                           <Text style={styles.sensexltpdark}>{item.LASTTRADERATE}</Text>
//                           <Text style={styles.currvaluedark}> <Text style={item.CHANGEPERC.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{item.CHANGE}  {item.CHANGEPERC}%</Text></Text>
//                           <Text style={{ marginTop: 5 }}></Text>
//                         </View>
//                         <Text></Text>
//                       </View>
//                     }
//                     keyExtractor={item => item.id}
//                   />
//                 </View>


//               </View>

//             </View>

//             <View style={this.state.heatmap == true ? styles.show : styles.hide}>

//               <View style={{ bottom: '4%' }}>
//                 <Row style={{ margin: 15, }}>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>Sensex Heat Map</Text></Col>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></Col>
//                 </Row>
//               </View>
//               <View style={this.state.heatmaploader == true ? styles.showheatloader : styles.hide}>
//                 <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
//               </View>
//               <View style={this.state.heatmaploader == false ? styles.showheatview : styles.hide}>
//                 <View style={this.state.value == '0' ? styles.show : styles.hide}>
//                   {/* <FlatList
//                     data={this.state.sensexHeatArray}
//                     horizontal={true}
//                     showsHorizontalScrollIndicator={false}
//                     style={{ marginLeft: 4, marginRight: 4 }}
//                     renderItem={({ item }) =>
//                       <TouchableOpacity activeOpacity={.9} onPress={() => this.detailsensex(item.scode)}>
//                         <View>
//                           <View style={styles.currlightview} >
//                             <Text style={styles.currnamelight} numberOfLines={1}>{item.sname}</Text>
//                             <Text style={styles.sensexltplight}>{item.ltp}</Text>
//                             <Text style={styles.currvaluelight}> <Text style={item.chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{item.chgval}  {item.chgper}%</Text></Text>
//                             <Text style={{ marginTop: 5 }}></Text>
//                           </View>
//                           <Text></Text>
//                         </View>
//                       </TouchableOpacity>
//                     }
//                     keyExtractor={item => item.id}
//                   /> */}

//                   <IndicatorViewPager
//                     style={{ height: 105 }}
//                     indicator={this._renderDotIndicator()}
//                   >
//                     <View style={{}}>
//                       {/* <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.scodeArray[0].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.scodeArray[0].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.scodeArray[0].chgval > 0 ? styles.currencypos : styles.currencyred}>{this.scodeArray[0].chgval}   {this.scodeArray[0].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View> */}
//                       {/* <View style={styles.currlightview1}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sensexHeatArray[1].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.sensexHeatArray[1].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.sensexHeatArray[1].chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{this.state.sensexHeatArray[1].chgval}   {this.state.sensexHeatArray[1].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview2}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sensexHeatArray[2].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.sensexHeatArray[2].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.sensexHeatArray[2].chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{this.state.sensexHeatArray[2].chgval}   {this.state.sensexHeatArray[2].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View> */}
//                     </View>
//                     {/* <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sensexHeatArray[2].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.sensexHeatArray[2].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.sensexHeatArray[2].chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{this.state.sensexHeatArray[2].chgval}   {this.state.sensexHeatArray[3].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview1}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sensexHeatArray[3].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.sensexHeatArray[3].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.sensexHeatArray[3].chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{this.state.sensexHeatArray[3].chgval}   {this.state.sensexHeatArray[3].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview2}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sensexHeatArray[4].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.sensexHeatArray[4].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.sensexHeatArray[4].chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{this.state.sensexHeatArray[4].chgval}   {this.state.sensexHeatArray[4].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                     </View> */}
//                     {/* <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sensexHeatArray[4].sname}</Text>
//                         <Text style={styles.sensexltplight}>{this.state.sensexHeatArray[4].ltp}</Text>

//                         <Text style={styles.currvaluelight}> <Text style={this.state.sensexHeatArray[4].chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{this.state.sensexHeatArray[4].chgval}   {this.state.sensexHeatArray[4].chgper}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
                     

//                     </View> */}

//                   </IndicatorViewPager>

//                 </View>

//                 {/* <View style={this.state.value == '1' ? styles.show : styles.hide}>
//                   <FlatList
//                     data={this.state.sensexHeatArray}
//                     horizontal={true}
//                     showsHorizontalScrollIndicator={false}
//                     style={{ marginLeft: 4, marginRight: 4 }}
//                     renderItem={({ item }) =>
//                       <TouchableOpacity onPress={() => this.detailsensex(item.scode)}>
//                         <View>
//                           <View style={styles.currdarkview}>
//                             <Text style={styles.currnamedark} numberOfLines={1}>{item.sname}</Text>
//                             <Text style={styles.sensexltpdark}>{item.ltp}</Text>
//                             <Text style={styles.currvaluedark}><Text style={item.chgval.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{item.chgval}   {item.chgper}%</Text></Text>
//                             <Text style={{ marginTop: 5 }}></Text>
//                           </View>
//                           <Text></Text>
//                         </View>
//                       </TouchableOpacity>
//                     }
//                     keyExtractor={item => item.id}
//                   />
//                 </View> */}
//               </View>
//             </View>

//             <View style={this.state.currency == true ? styles.show : styles.hide}>

//               <View style={{ bottom: '4%' }}>
//                 <Row style={{ margin: 15, }}>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>Currency</Text></Col>
//                   <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></Col>
//                 </Row>
//               </View>
//               <View style={this.state.currencyloader == true ? styles.showcurrloader : styles.hide}>
//                 <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
//               </View>
//               <View style={this.state.currencyloader == false ? styles.showcurrview : styles.hide}>
//                 <View style={this.state.value == '0' ? styles.show : styles.hide}>
//                   {/* <FlatList
//                     data={this.state.currencyArray}
//                     horizontal={true}
//                     showsHorizontalScrollIndicator={false}
//                     style={{ marginLeft: 4, marginRight: 4, }}
//                     renderItem={({ item }) =>
//                       <View>
//                         <View style={styles.currlightview}>
//                           <Text style={styles.currnamelight} numberOfLines={1}>{item.INSTRUMENTNAME}</Text>
//                           <Text style={styles.currdatelight}>{item.EXPIRYDATE}</Text>
//                           <Text style={styles.currvaluelight} >{item.LASTTRADERATE} <Text style={item.CHANGEPERC.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{item.CHANGE} {item.CHANGEPERC}%</Text></Text>
//                           <Text style={{ marginTop: 5 }}></Text>
//                         </View>
//                         <Text></Text>
//                       </View>

//                     }
//                     keyExtractor={item => item.id}
//                   /> */}

//                   <IndicatorViewPager
//                     style={{ height: 105 }}
//                     indicator={this._renderDotIndicator()}
//                   >
//                     <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.onecurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.onecurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.onecurr.LASTTRADERATE} <Text style={this.state.onecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.onecurr.CHANGE}   {this.state.onecurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview3}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.twocurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.twocurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.twocurr.LASTTRADERATE} <Text style={this.state.twocurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.twocurr.CHANGE}   {this.state.twocurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview4}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.threecurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.threecurr.LASTTRADERATE} <Text style={this.state.threecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE}   {this.state.threecurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                     </View>
//                     <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.threecurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.threecurr.LASTTRADERATE} <Text style={this.state.threecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE}   {this.state.threecurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview3}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fourcurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.fourcurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.fourcurr.LASTTRADERATE} <Text style={this.state.fourcurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fourcurr.CHANGE}   {this.state.fourcurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview4}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.fivecurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.fivecurr.LASTTRADERATE} <Text style={this.state.fivecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE}   {this.state.fivecurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                     </View>
//                     <View style={{}}>
//                       <View style={styles.currlightview}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.fivecurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.fivecurr.LASTTRADERATE} <Text style={this.state.fivecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE}   {this.state.fivecurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
//                       <View style={styles.currlightview3}>
//                         <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sixcurr.INSTRUMENTNAME}</Text>
//                         <Text style={styles.currdatelight}>{this.state.sixcurr.EXPIRYDATE}</Text>

//                         <Text style={styles.currvaluelight}>{this.state.sixcurr.LASTTRADERATE} <Text style={this.state.sixcurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.sixcurr.CHANGE}   {this.state.fourcurr.CHANGEPERC}%</Text></Text>
//                         <Text style={{ marginTop: 5 }}></Text>
//                       </View>
                     
//                     </View>
//                   </IndicatorViewPager>
//                   <Text></Text>
//                 </View>
//                     <View style={this.state.value == '1' ? styles.show : styles.hide}>
//                       <FlatList
//                         data={this.state.currencyArray}
//                         horizontal={true}
//                         showsHorizontalScrollIndicator={false}
//                         style={{ marginLeft: 4, marginRight: 4, }}
//                         renderItem={({ item }) =>
//                           <View>
//                             <View style={styles.currdarkview}>
//                               <Text style={styles.currnamedark} numberOfLines={1}>{item.INSTRUMENTNAME}</Text>
//                               <Text style={styles.currdatedark}>{item.EXPIRYDATE}</Text>
//                               <Text style={styles.currvaluedark}>{item.LASTTRADERATE} <Text style={item.CHANGEPERC.charAt(0) != '-' ? styles.currencypos : styles.currencyred}>{item.CHANGE} {item.CHANGEPERC}%</Text></Text>
//                               <Text style={{ marginTop: 5 }}></Text>
//                             </View>
//                             <Text></Text>
//                           </View>
//                         }
//                         keyExtractor={item => item.id}
//                       />
//                     </View>
//               </View>
//                 </View>

//                 <Popover
//                   isVisible={this.state.isVisible}
//                   fromView={this.touchable}
//                   arrowStyle={true}
//                   showArrow={true}
//                   onRequestClose={() => this.closePopover()}>
//                   <View style={{ backgroundColor: 'white', height: '100%' }}>

//                     <Text></Text>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}><IconEnt name="setting" style={{ textAlign: 'center' }} color={'black'} size={23}></IconEnt></Col>
//                       <Col style={{ width: '80%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 20, bottom: 3, color: 'black', }}>Settings</Text></Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, bottom: 2, color: 'black' }}> {this.state.themename} Theme </Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <Switch
//                           trackColor="black"
//                           trackColor="grey"
//                           value={this.state.switchval}
//                           style={{ bottom: 3, alignSelf: 'center' }}
//                           onValueChange={(e) => this.toggleSwitch(e)}
//                         />
//                       </Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, bottom: 2, color: 'black' }}> Edit HomePage </Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Icon name="ios-arrow-dropright-circle" onPress={() => this.edit()} style={{ textAlign: 'center' }} color={'black'} size={25} /></Col>
//                     </Row>




//                   </View>

//                 </Popover>
//                 <Popover
//                   isVisible={this.state.edit}
//                   fromView={this.touchable}>
//                   <View style={{}}>
//                     <Text></Text>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>Watchlist</Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <CheckBox value={this.state.watch} onValueChange={() => this.oncheckWatch()} style={{ alignSelf: "center", }} />

//                       </Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>Portfolio</Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <CheckBox value={this.state.port} onValueChange={() => this.oncheckPort()} style={{ alignSelf: "center", }} />

//                       </Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>Indices</Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <CheckBox value={this.state.indices} onValueChange={() => this.oncheckIndices()} style={{ alignSelf: "center", }} />

//                       </Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>Commodity</Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <CheckBox value={this.state.commodity} onValueChange={() => this.oncheckCommodit()} style={{ alignSelf: "center", }} />

//                       </Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>Sensex Heat Map</Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <CheckBox value={this.state.heatmap} onValueChange={() => this.oncheckSensex()} style={{ alignSelf: "center", }} />

//                       </Col>
//                     </Row>
//                     <Row>
//                       <Col style={{ width: '20%', height: 50, }}></Col>
//                       <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>Currency</Text></Col>
//                       <Col style={{ width: '40%', height: 50, }}>
//                         <CheckBox value={this.state.currency} onValueChange={() => this.oncheckCurrency()} style={{ alignSelf: "center", }} />

//                       </Col>
//                     </Row>


//                     <Row>
//                       <Col style={{ width: '100%', height: 40 }}><Button style={{ marginLeft: 130, marginRight: 130, backgroundColor: '#2087c9', height: 35 }} onPress={() => this.ok()}><Text style={{ marginLeft: 25, fontSize: 20 }}>OK</Text></Button></Col>
//                     </Row>
//                     <Text></Text>

//                   </View>
//                 </Popover>


//                 <Modal
//                   animationType="slide"
//                   transparent={false}
//                   visible={this.state.modvis}
//                   // onDismiss={() => {
//                   //   alert('dismiss');
//                   // }}
//                   style={{ height: '100%' }}
//                   onRequestClose={() => {
//                     this.setState({ modvis: false });
//                   }}
//                 >
//                   <View style={{ height: '100%' }}>

//                     <View style={this.state.sensexDetailloader == true ? styles.show : styles.hide}>
//                       <View style={{ marginTop: '30%', }}>
//                         <ActivityIndicator size="large" color={'#72a3bf'} />
//                       </View>
//                     </View>

//                     <View style={this.state.sensexDetailloader == false ? styles.show : styles.hide}>
//                       <View style={{ borderColor: 'black', borderWidth: 1, marginTop: '10%', margin: 10, height: '40%' }}>
//                         <Row style={{ margin: 10, }}>
//                           <Col style={{ width: '100%', height: 40 }}><Icon style={{ textAlign: 'right' }} name="ios-add" onPress={() => this.add(this.Data)} size={30} /></Col>
//                         </Row>
//                         <Row>
//                           <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 19 }}>{this.Data.sname}</Text></Col>
//                           <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 15 }}>Value:- {this.Data.ltp}</Text></Col>

//                         </Row>
//                         <Row>
//                           <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 15 }}>{this.Data.chgval} {this.Data.chgper}%</Text></Col>
//                           <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 15 }}>Scode:- {this.Data.scode}</Text></Col>

//                         </Row>

//                       </View>
//                     </View>
//                   </View>
//                 </Modal>


//           </ScrollView>

//               <Footer >

//                 <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
//                   <Button style={styles.inactive} onPress={() => this.tab('watch')}>
//                     <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
//                     <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive}>Watchlist </Text>
//                   </Button>


//                   <Button style={styles.inactive} onPress={() => this.tab('port')}>
//                     <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

//                     <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive}>Portfolio</Text>
//                   </Button>
//                   {/* <Button></Button> */}

//                   <Button style={this.activetab == 'home' ? styles.active : styles.inactive} onPress={() => this.tab('home')}>
//                     <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, }} />
//                     <Text style={this.state.value == '0' ? styles.tabnamelighthome : styles.tabnamedark}>Home</Text>
//                   </Button>
//                   {/* <Button></Button> */}
//                   <Button style={styles.inactive} onPress={() => this.tab('search')}>
//                     <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
//                     <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive}>Search</Text>
//                   </Button>
//                   {/* <Button></Button> */}
//                   <Button style={styles.inactive} onPress={() => this.tab('more')}>
//                     <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

//                     <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive}>More</Text>
//                   </Button>


//                 </FooterTab>
//               </Footer>
//             </View >
//       </View >
//           );
//         }
//       }
// const styles = StyleSheet.create({
//             iconpos: {
//             textAlign: "center", color: '#19cf3e', top: 3, padding: 5, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 10
//         },
      
//   iconneg: {
//             textAlign: "center", top: 3, color: '#f54845', padding: 5, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 10
//         },
//   iconposdark: {
//             textAlign: "center", top: 3, color: '#19cf3e', padding: 5, backgroundColor: '#1a1f1f', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 14
      
//         },
//   iconnegdark: {
//             textAlign: "center", top: 3, color: '#f54845', padding: 5, backgroundColor: '#1a1f1f', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 14
      
//         },
      
//   chnagepos: {
//             left: 5, color: '#19cf3e', fontSize: 13, top: 2, fontFamily: 'SegoeProDisplay-Regular'
//         },
//   changeneg: {
//             left: 5, color: '#f54845', fontSize: 13, top: 2, fontFamily: 'SegoeProDisplay-Regular'
//         },
//   currencypos: {
//             fontWeight: 'normal', fontSize: 10.5, color: '#00cc00', fontFamily: 'SegoeProDisplay-Regular'
//         },
//   currencyred: {
//             fontWeight: 'normal', fontSize: 10.5, color: 'red', fontFamily: 'SegoeProDisplay-Regular'
//         },
//   light: {
//             backgroundColor: '#f6f7f9'
//         },
//   dark: {
//             backgroundColor: 'black'
//         },
//   mainviewlight: {
//             backgroundColor: '#edeef2', height: '100%'
//         },
//   mainviewdark: {
//             backgroundColor: '#333333', height: '100%'
//         },
//   headermaxcollight: {
//             width: '70%', height: 30, marginTop: 15, backgroundColor: '#f6f7f9'
//         },
//   headermaxcoldark: {
//             width: '70%', height: 30, marginTop: 15, backgroundColor: 'black'
//         },
//   headermincollight: {
//             width: '15%', height: 30, marginTop: 15, backgroundColor: '#f6f7f9'
//         },
//   headermincoldark: {
//             width: '15%', height: 30, marginTop: 15, backgroundColor: 'black'
//         },
//   snpviewlight: {backgroundColor: '#f6f7f9', borderBottomRightRadius: 240, },
//   snpviewdark: {
//             backgroundColor: '#000000', borderBottomRightRadius: 240,
//         },
//   valuelight: {
//             textAlign: 'center', marginLeft: '9%', fontSize: 19, fontFamily: 'SegoePro-Bold', color: '#132243',
//         },
//   valuedark: {
//             textAlign: 'center', marginLeft: '9%', fontSize: 19, fontFamily: 'SegoePro-Bold', color: '#72a3bf',
//         },
//   datelight: {
//             textAlign: 'center', bottom: 31, fontSize: 17, color: '#aaafba', fontFamily: 'SegoeProDisplay-Regular'
//         },
//   datedark: {
//             textAlign: 'center', bottom: 31, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular'
//         },
//   scrollviewlight: {backgroundColor: '#edeef2', },
//   scrollviewdark: {backgroundColor: '#0b0b0b', },
//   watchcollight: {width: '50%', height: 30 },
//   watchcollightup: {width: '50%', height: 30, top: 8 },
//   watchcoldark: {width: '50%', height: 30, },
//   watchlighttext: {fontSize: 17, fontFamily: 'SegoePro-Bold', color: 'black' },
//   watchdarktext: {fontSize: 17, fontFamily: 'SegoePro-Bold', color: '#72a3bf' },
//   portlightview: {margin: 5, backgroundColor: 'white', height: 110, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
//   portdarkview: {margin: 5, backgroundColor: '#1a1f1f', height: 110, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
//   lightgain: {textAlign: 'center', top: 5, color: 'black', fontFamily: 'SegoeProDisplay-Regular' },
//   darkgain: {textAlign: 'center', top: 5, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular' },
//   lightcoloverall: {width: '1%', height: 40, backgroundColor: '#f4f4f4' },
//   darkcoloverall: {width: '1%', height: 40, backgroundColor: '#353a3a' },
//   gainlightprice: {textAlign: 'center', fontFamily: 'SegoePro-Bold', color: "black" },
//   gaindarkprice: {textAlign: 'center', fontFamily: 'SegoePro-Bold', color: "#72a3bf" },
//   lightcurrency: {textAlign: 'center', color: '#0089d0', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
//   darkcurrency: {textAlign: 'center', color: '#72a3bf', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
//   indiceslight: {backgroundColor: '#edeef2', marginTop: 10, },
//   indicesdark: {backgroundColor: '#0b0b0b', marginTop: 10, },
//   lightindtext: {left: 5, color: 'black', fontSize: 14, fontFamily: 'SegoeProDisplay-Regular' },
//   darkindtext: {left: 5, color: '#72a3bf', fontSize: 14, fontFamily: 'SegoeProDisplay-Regular' },
//   lightvaluetext: {textAlign: 'right', top: 7, fontFamily: 'SegoePro-Bold', fontSize: 15, color: 'black' },
//   darkvaluetext: {textAlign: 'right', top: 7, fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
//   currlightview: {width: 135, backgroundColor: 'white', marginLeft: 11, marginTop: 7, marginRight: 10, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
//   currlightview1: {bottom: 78, marginLeft: 160, width: 135, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
//   currlightview3: {bottom: 76, marginLeft: 160, width: 135, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
//   currlightview2: {bottom: 155.5, marginLeft: 310, width: 135, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
//   currlightview4: {bottom: 151, marginLeft: 310, width: 135, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
//   currdarkview: {width: 135, backgroundColor: '#1a1f1f', marginLeft: 11, marginTop: 7, marginRight: 10, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },
//   tablight: {backgroundColor: 'white' },
//   tabdark: {backgroundColor: '#1a1f1f' },
//   tabnamelight: {color: '#79868e', fontSize: 12, fontFamily: 'SegoeProDisplay-Regular' },
//   tabnamelighthome: {color: '#f6f7f9', fontSize: 12, fontFamily: 'SegoeProDisplay-Regular' },
//   tabnamedark: {color: '#f6f7f9', fontSize: 11, marginTop: 3, fontFamily: 'SegoeProDisplay-Regular' },
//   tabnamedarkdeactive: {color: '#79868e', fontSize: 11, marginTop: 3, fontFamily: 'SegoeProDisplay-Regular' },
//   show: {display: 'flex', },
//   showimg: {display: 'flex', marginTop: 15 },
//   showltp: {display: 'flex', bottom: 35 },
//   showdate: {display: 'flex', bottom: 70 },
//   hide: {display: 'none' },
//   showloader: {display: 'flex', bottom: '10%' },
//   showindices: {display: 'flex', marginLeft: 15, marginRight: 15 },
//   showindview: {display: 'flex', bottom: '5%' },
//   showindloader: {display: 'flex', bottom: '4%' },
//   showcommodityview: {display: 'flex', bottom: '11%' },
//   showcommodityloader: {display: 'flex', bottom: '4%' },
//   showheatloader: {display: 'flex', bottom: '4%' },
//   showheatview: {display: 'flex', bottom: '11%' },
//   showcurrloader: {display: 'flex', bottom: '4%' },
//   showcurrview: {display: 'flex', bottom: '11%' },
//   watchnamelight: {top: 5, left: 5, color: 'black', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
//   watchnamedark: {top: 5, left: 5, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
//   watchvallight: {top: 15, left: 5, color: 'black', fontSize: 13, fontFamily: 'SegoePro-Bold', },
//   watchvaldark: {top: 15, left: 5, color: '#72a3bf', fontSize: 14, fontFamily: 'SegoePro-Bold', },
//   watchwhite: {width: 160, marginTop: 15, backgroundColor: 'white', left: 5, margin: 5, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },
//   watchdark: {width: 160, marginTop: 15, backgroundColor: '#1a1f1f', left: 5, margin: 5, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: {width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
//   watchtrans: {width: 160, backgroundColor: 'transparent', left: 5, margin: 5, borderRadius: 10, },
//   currnamelight: {top: 5, left: 2, color: 'black', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
//   currnamedark: {top: 5, left: 2, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
        
//   currdatelight: {top: 10, left: 4, color: '#aaafba', fontSize: 12, fontFamily: 'SegoeProDisplay-Regular' },
//   currdatedark: {top: 10, left: 4, color: '#72a3bf', fontSize: 12, fontFamily: 'SegoeProDisplay-Regular' },
//   currvaluelight: {top: 18, left: 4, color: 'black', fontSize: 13, fontFamily: 'SegoePro-Bold', },
//   currvaluedark: {top: 18, left: 4, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoePro-Bold', },
//   sensexltplight: {top: 12, left: 4, color: 'black', fontSize: 13, fontFamily: 'SegoePro-Bold', },
//   sensexltpdark: {top: 11, left: 4, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoePro-Bold', },
//   active: {backgroundColor: '#2087c9', height: 65, marginLeft: 13, marginRight: 13, borderRadius: 3 },
//   inactive: {padding: 5 },
//   nowatchlistlight: {textAlign: 'center', fontSize: 20, },
//   nowatchlistdark: {textAlign: 'center', fontSize: 20, color: 'white' }
        
//         });
        
//         export default Test;
