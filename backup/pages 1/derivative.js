import React, { Component } from 'react';
import { View, Image, Modal, ToastAndroid, Linking, CheckBox, Dimensions, Switch, Picker, Text, BackHandler, RefreshControl, NetInfo, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view';
import IconEvil from 'react-native-vector-icons/EvilIcons';

import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';

const height = Dimensions.get('window').height;
class Derivative extends Component {
    static navigationOptions = {

        header: null,
    };
    watch = [];
    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            WatchArray: [],
            Summary: [],
            refreshing: false,
            sidemenu: false,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        setTimeout(() => { SplashScreen.hide() }, 3000);

    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        AsyncStorage.removeItem('indicesname');
        AsyncStorage.removeItem('indicesId')
        this.props.navigation.goBack(null);
        return true;

    }
    back() {
        AsyncStorage.removeItem('indicesname');
        AsyncStorage.removeItem('indicesId')

        this.props.navigation.goBack(null);
        return true;
    }
    onRefresh() {
        this.sensexfun();
        this.watchFun();
        this.summaryFun();
    }
    googleassistant() {
        Linking.openURL('https://assistant.google.com/explore');

    }
    componentDidMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        });
        this.themename = this.props.navigation.getParam('theme', '');
        if (this.themename == null || this.themename == 'dark') {
            this.setState({ value: '1' });
        }
        else {
            this.setState({ value: '0' });
        }
        this.sensexfun();
        this.watchFun();
        this.summaryFun();
    }
    sensexfun() {
        this.setState({ loader: true });
        fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

            responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';
        })
    }

    watchFun() {
        fetch('https://api.bseindia.com/msource/getDerivative.aspx?id=1&ln=en').then((response) => response.text()).then((responsejson) => {


            this.setState({ WatchArray: responsejson.split('#'), });
        });

    }

    summaryFun() {
        fetch('https://api.bseindia.com/msource/frmDerivatives.aspx?id=2&ln=en').then((response) => response.text()).then((responsejson) => {
            this.setState({ Summary: responsejson.split('#'), })
            console.log(this.state.contri);

            setTimeout(() => { this.setState({ refreshing: false, loader: false }) }, 4000);

        })
    }

    closesidemenu() {
        this.setState({ sidemenu: false });
    }
    equity() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('gainer', { 'theme': dt });
        });

    }
    indices() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('indices', { 'theme': dt });
        });
    }
    sensex() {
        // AsyncStorage.getItem('themecolor').then((dt) => {
        //     this.setState({ sidemenu: false });
        //     this.props.navigation.navigate('sensex', { 'theme': dt });
        // });
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('setting', { 'theme': dt, 'current': 0 });

        });
    }
    home() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('maindark', { 'theme': dt });
        });

    }
    derivative() {
        this.setState({ sidemenu: false });
    }
    currency() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('currency', { 'theme': dt });
        });

    }
    commodity() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('commodity', { 'theme': dt });
        });

    }
    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }

    render() {
        return (
            <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
                <StatusBar backgroundColor={this.state.value == '0' ? '#f6f7f9' : '#0b0b0b'} />
                <ViewPager style={{ height: height }} >
                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.maintitle : styles.maintitledark}>Market Watch</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                        </Row>


                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>

                        <Row style={{ marginLeft: 16, marginTop: '12%' }}>
                            <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Contract</Text><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Trd Qty</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Chnage(%)</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Buy Price(Qty)</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Sell Price(Qty)</Text></Col>
                        </Row>

                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.WatchArray}
                            style={{ marginTop: 55 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={item.split('@')[0] == '' ? styles.hide : styles.show} >
                                    <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                        <Col style={{ width: '100%', height: 58 }}>
                                            <Row>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.split('@')[6] > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.split('@')[0]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{item.split('@')[2]}</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={item.split('@')[6] > 0 ? styles.pos : styles.neg} numberOfLines={1}>{item.split('@')[1]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightwatchper : styles.darkwatchper}>{item.split('@')[6]} {item.split('@')[4]}%</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split('@')[15]} ({item.split('@')[16]})</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensex : styles.darksensex}>{item.split('@')[17]} ({item.split('@')[18]})</Text></Col>

                                            </Row>
                                        </Col>

                                    </Row>
                                    <Text></Text>

                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.maintitle : styles.maintitledark}>Market Summary</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: 10 }}>
                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                        </Row>

                        <Row style={{ marginLeft: 16, marginTop: 35 }}>
                            <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Instrument</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Contracts</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O (Cr.)</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Prem(Cr.)</Text></Col>
                        </Row>


                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.Summary}
                            style={{ marginTop: 40 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={item.split('@')[0] == '' ? styles.hide : styles.show} >
                                    <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                        <Col style={{ width: '100%', height: 58 }}>
                                            <Row style={item.split('@')[0] != 'Total' ? styles.show : styles.hide}>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split('@')[0]}</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split('@')[1]}</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split('@')[2]}</Text></Col>

                                            </Row>
                                            <Row style={item.split('@')[0] == 'Total' ? styles.show : styles.hide}>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.state.value == '0' ? styles.lightsensex1 : styles.darksensex1} numberOfLines={1}>Total</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split('@')[1]}</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split('@')[2]}</Text></Col>

                                            </Row>
                                        </Col>

                                    </Row>
                                    <Text></Text>

                                </View>

                            }
                            keyExtractor={item => item.id}
                        />



                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>


                </ViewPager>


                <Modal
                    animationType="slide"
                    transparent={false}
                    style={{ height: '100%' }}
                    visible={this.state.sidemenu}
                    onRequestClose={() => {
                        this.setState({ sidemenu: false });
                    }}
                >
                    <View style={this.state.value == '0' ? styles.lightmenu : styles.darkmenu}>
                        <Row style={{ marginLeft: 20, marginTop: 10 }}>
                            <Col style={{ width: '50%', height: 40 }}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={{ marginLeft: 10 }} size={40} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>

                        </Row>

                        <ScrollView style={{ top: '8%' }}>
                            <Image style={{ width: 150, height: 150, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/lightprofile.png') : require('../images/darkprofile.png')} />
                            {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
                            <Text></Text>
                            <View style={{}}>
                                <Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Home</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.equity()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Equity</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GAINERS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>LOSERS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TOP TURNOVER</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK HIGH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK LOW</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Indices</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.sensex()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Sensex</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>SECURITY</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>OVERVIEW</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TURNOVER</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CONTRIBUTION</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >SME</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET STATISTICS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.derivative()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Derivatives</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.currency()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Currency</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.commodity()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Commadity</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IRD</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>ETF</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ETF WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Debt</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>DEBT MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BONDS-OTC TRADES</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL CORP. DEBT</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GSEC</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL GOV BONDS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BOND-NDS-RST</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>EBP</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Corporates</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ANNOUNCEMENTS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ACTIONS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RESULTS CALENDAR</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>BOARD MEETINGS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Statistics</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Turnover</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPO/OFS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Listings</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Notices</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Watchlist</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Portfolio</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPF</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', }}></Text>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>

            </View >
        );
    }
}

const styles = StyleSheet.create({

    mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
    mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
    inactive: { padding: 12, },
    titlelight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    titledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    pos: { alignSelf: 'flex-end', marginRight: 10, marginTop: 9, fontFamily: 'SegoeProDisplay-Regular', color: '#19cf3e' },
    neg: { alignSelf: 'flex-end', marginRight: 10, marginTop: 9, fontFamily: 'SegoeProDisplay-Regular', color: '#f54845' },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamedark: { color: '#79868e', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
    lightdate: { color: '#132144', textAlign: 'right', marginTop: 14, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    darkdate: { color: '#72a3bf', textAlign: 'right', marginTop: 14, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    statuslight: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
    statusdark: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
    listlightcol1: { width: '65%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcol2: { width: '35%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcol1: { width: '65%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcol2: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', },
    listlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', },
    show: { display: 'flex' },
    hide: { display: 'none' },
    possensex: { marginLeft: 15, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    lighttext1: { marginLeft: 15, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightwatchper: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkwatchper: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex: { alignSelf: 'flex-end', marginRight: 10, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex1: { marginLeft: 20, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex: { alignSelf: 'flex-end', marginRight: 10, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex1: { marginLeft: 20, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    negsensex: { marginLeft: 15, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightltp: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darkltp: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    darkltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    darktext1: { marginLeft: 15, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    perlight: { alignSelf: 'flex-end', marginRight: 8, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perdark: { alignSelf: 'flex-end', marginRight: 8, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perturnlight: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    perturndark: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    ltpviewpos: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    ltpviewneg: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    lightheader1: { width: '32%', height: 40 },
    darkheader1: { width: '32%', height: 40 },
    lightheader2: { width: '30%', height: 40 },
    darkheader2: { width: '30%', height: 40 },
    lightheader3: { width: '33%', height: 40 },
    darkheader3: { width: '33%', height: 40 },
    lightheader4: { width: '57%', height: 40 },
    darkheader4: { width: '57%', height: 40 },
    lightPicker: { color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkPicker: { color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightheadertext: { marginLeft: 15, fontSize: 15, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext: { marginLeft: 15, fontSize: 15, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext1: { alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lighttext: { marginLeft: 15, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darktext: { marginLeft: 15, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    firstcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 65, },
    firstcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 65, },
    secondcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 95, },
    secondcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 95, },
    thirdcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 210, },
    thirdcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 210, },
    fourthcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 182, },
    fourthcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 182, },
    fivecol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 125, },
    fivecoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 125, },
    sixcol: { borderRadius: 4, width: '100%', height: 60, },
    sixcoldark: { borderRadius: 4, width: '100%', height: 60, },
    prvlight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    prvdark: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    titlelight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoePro-Bold', },
    titledark: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    maintitle: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    maintitledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    dropicon: { textAlign: 'center', marginLeft: 125, bottom: 35 },
    ltplight: { marginLeft: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    ltpdark: { marginLeft: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 30, fontSize: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkmenutext: { marginLeft: 30, fontSize: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: '#72a3bf', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    contricol: { width: '100%', height: 58 },
})

export default Derivative;
