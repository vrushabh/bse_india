import { Dimensions,PixelRatio } from 'react-native';
const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = width / guidelineBaseWidth;;
const verticalScale =  width * 350;
const moderateScale = width * 350;

function normalize(size) {
    const newSize = size * scale 
    
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    
  }

export {scale, verticalScale, moderateScale,normalize};