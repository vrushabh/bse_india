import React, { Component } from 'react';
import { View, Text, Picker, FlatList, Switch,NetInfo, BackHandler, CheckBox, RefreshControl, AsyncStorage, ToastAndroid, Modal, StatusBar, ActivityIndicator, ScrollView, SafeAreaView, Linking, StyleSheet, Alert, Image, Dimensions } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import { LineChart } from "react-native-chart-kit";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Popover from 'react-native-popover-view'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
import I18n from '../i18';


const { width: screenWidth } = Dimensions.get('window')
const chartConfig = {
  backgroundGradientFrom: "white",
  // backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "white",
  // backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false // optional
};
const data = {
  datasets: [
    {
      data: [50, 15, 75, 19, 20, 10],
      color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`, // optional
      strokeWidth: 2 // optional
    }
  ],

};

class DymanicMain extends Component {
  watchArray = [];
  // indicesArray = [];
  currencyArray = [];
  radio_props = [];
  color;
  activetab = 'home';
  ltp = '';
  chng = '';
  per = '';
  date = '';
  status = 'Close';
  sensexData = [];
  scodeArray;
  watch = [];
  Data = '';
  code = [];
  lan = "hn";


  // watcharray=[];
  static navigationOptions = {

    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      theme: 'dark',
      value: 0,
      sensexData: [],
      ltp: '',
      chng: '',

      per: '',
      date: '',
      status: 'Close',
      indicesArray: [],
      currencyArray: [],
      sensexHeatArray: [],
      commodityArray: [],
      switchval: false,
      themename: 'Light',
      loader: true,
      indicesloader: true,
      commodityloader: true,
      heatmaploader: true,
      currencyloader: true,
      sensexDetailloader: true,
      modvis: false,
      Warray: [],
      editIcon: 'ios-arrow-dropright-circle',
      edit: false,
      refreshing: false,
      watch: true,
      port: true,
      indices: true,
      commodity: true,
      heatmap: true,
      currency: true,
      sname1: '', ltp1: '', changeval1: '', changeper1: '', scode1: '',
      sname2: '', ltp2: '', changeval2: '', changeper2: '', scode2: '',
      sname3: '', ltp3: '', changeval3: '', changeper3: '', scode3: '',
      sname4: '', ltp4: '', changeval4: '', changeper4: '', scode4: '',
      sname5: '', ltp5: '', changeval5: '', changeper5: '', scode5: '',
      sname6: '', ltp6: '', changeval6: '', changeper6: '', scode6: '',
      one: {}, two: {}, three: {}, four: {}, five: {}, six: {},
      onecurr: {}, twocurr: {}, threecurr: {}, fourcurr: {}, fivecurr: {}, sixcurr: {},
      chnglan: false,
      selectlan: 'English',
      langauge: '',
      currencypager: '',
      lan: "hn",
      watchloader:false
    }
    setTimeout(() => { SplashScreen.hide() }, 4000);


    this.radio_props = [
      { label: 'Light Mode Theme', value: 0 },
      { label: 'Dark Mode Theme', value: 1 }
    ];

  }

  showview(val, name) {
    alert(val + '  ' + name);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
  }

  backPressed = () => {
    Alert.alert(
      'Exit App',
      'Do you want to exit?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false });
    return true;
  }

  more() {
    this.setState({ isVisible: true });
  }
  edit() {
    this.setState({ isVisible: false });

    this.setState({ edit: true });
  }

  toggleSwitch(val) {
    //alert(''+val);

    if (val == true) {
      this.setState({ value: 1 });
      this.setState({ isVisible: false });
      this.setState({ themename: 'Dark' });

    }
    else {
      this.setState({ value: 0 });
      this.setState({ isVisible: false });
      this.setState({ themename: 'Light' });

    }
    this.setState({ switchval: val });
  }

  closePopover() {
    this.setState({ isVisible: false });
  }

  changetheme(thmval) {
    // Alert.alert('change');
    this.setState({ value: thmval })
    this.setState({ isVisible: false });
    console.log(this.state.value);

  }


  _renderItemlight({ item, index }) {
    this.scodeArray = [20, 25, 10, 40, 10];
    return (
      <View>
        <View style={styles.watchwhite} >
          <Text style={styles.watchnamelight}>{item.sname}</Text>
          <Text style={styles.watchvallight} numberOfLines={1}>{item.ltp}<Text style={item.chgper > 0 ? styles.currencypos : styles.currencyred}>  {item.chgval}  {item.chgper}%</Text></Text>
          <Image style={{ width: 140, height: 33, marginLeft: '10%', marginTop: 20, bottom: 5 }} source={require('../images/pulse.jpg')} />
          {/* <LineChart
            data={
              {
                datasets: [
                  {
                    data: [this.scodeArray[0], this.scodeArray[1], this.scodeArray[2], this.scodeArray[3], this.scodeArray[4]],
                    color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                  }
                ],

              }
            }
            withDots={false}
            withInnerLines={false}
            withOuterLines={false}
            withHorizontalLabels={false}
            style={{ top: 5, left: 3 }}
            width={150}
            height={55}
            verticalLabelRotation={30}
            chartConfig={chartConfig}
            bezier
          /> */}
        </View>
        <Text></Text>
      </View>
    )
  }
  _renderItemdark({ item, index }) {
    return (

      <View>
        <View style={styles.watchdark} >
          <Text style={styles.watchnamedark}>{item.sname}</Text>
          <Text style={styles.watchvaldark} numberOfLines={1}>{item.ltp} <Text style={item.chgper > 0 ? styles.currencypos : styles.currencyred}>  {item.chgval}%  {item.chgper}%</Text></Text>
          <Image style={{ width: 140, height: 33, marginLeft: '10%', marginTop: 20, bottom: 5 }} source={require('../images/rsz_new_design_hompage_black-theme_code.jpg')} />
        </View>
        <Text></Text>
      </View>
    )
  }



  tab(val) {
    if (val == 'watch') { }
    if (val == 'port') { }
    if (val == 'home') { }
    if (val == 'search') { }
    if (val == 'more') { }
  }
  lanfun() {
  
    AsyncStorage.getItem('language').then((lan) => {
      if (lan == null) {
        this.setState({ langauge: 'en',selectlan:'English'});
        I18n.locale = "en"
      }
      else {
        //mr,hn,bn,gj,ml,or,tml,en

        lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
        lan == 'English' ? this.setState({ langauge: "en",selectlan:'English' }) : lan == 'Hindi' ? this.setState({ langauge: "hn",selectlan:'Hindi' }) : lan == 'Marathi' ? this.setState({ langauge: "mr",selectlan:'Marathi' }) : lan == 'Gujrati' ? this.setState({ langauge: "gj",selectlan:'Gujrati' }) : lan == 'Bengali' ? this.setState({ langauge: "bn",selectlan:'Bengali' }) : lan == 'Malayalam' ? this.setState({ langauge: "ml",selectlan:'Malayalam' }) : lan == 'Oriya' ? this.setState({ langauge: "or",selectlan:'Oriya' }) : lan == 'Tamil' ? this.setState({ langauge: "tml",selectlan:'Tamil' }) : this.setState({ langauge: "en",selectlan:'English' });
      }
      this.watchListFun();

      this.indicesFun();
      this.sensexHeatMapFun();

    });

  }

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' :  ToastAndroid.showWithGravityAndOffset(
        "You are Offline.Please check your connection.",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    });

    this.lanfun();
    setTimeout(()=> { this.watchListFun()},2000);
    // this.sensexHeatMapFun();

    //this.watchListFun();
    this.edithome();
    this.sensexFun();
    this.commodityFun();
    this.currencyFun();


  }
 
 


  edithome() {

    AsyncStorage.getItem('watchval').then((dt) => {
      if (dt == null || dt == undefined) {
        this.setState({ watch: true });
      }
      else {
        this.setState({ watch: false });
      }
    });
    AsyncStorage.getItem('portval').then((dt) => {

      if (dt == null || dt == undefined) {
        this.setState({ port: true });
      }
      else {
        this.setState({ port: false });
      }

    });
    AsyncStorage.getItem('indval').then((dt) => {

      if (dt == null || dt == undefined) {
        this.setState({ indices: true });
      }
      else {
        this.setState({ indices: false });
      }

    });
    AsyncStorage.getItem('commodt').then((dt) => {

      if (dt == null || dt == undefined) {
        this.setState({ commodity: true });
      }
      else {
        this.setState({ commodity: false });
      }

    });
    AsyncStorage.getItem('heatmap').then((dt) => {

      if (dt == null || dt == undefined) {
        this.setState({ heatmap: true });
      }
      else {
        this.setState({ heatmap: false });
      }

    });
    AsyncStorage.getItem('curr').then((dt) => {

      if (dt == null || dt == undefined) {
        this.setState({ currency: true });
      }
      else {
        this.setState({ currency: false });
      }

    });
  }
  ok() {
    AsyncStorage.getItem('watchval').then((dt) => {
      if (dt == null) { this.setState({ watch: true }) }
      else { this.setState({ watch: false }); }
    });
    AsyncStorage.getItem('portval').then((dt) => {
      if (dt == null) { this.setState({ port: true }) }
      else { this.setState({ port: false }); }

    });
    AsyncStorage.getItem('indval').then((dt) => {
      if (dt == null) { this.setState({ indices: true }) }
      else { this.setState({ indices: false }); }

    });
    AsyncStorage.getItem('commodt').then((dt) => {
      if (dt == null) { this.setState({ commodity: true }) }
      else { this.setState({ commodity: false }); }

    });
    AsyncStorage.getItem('heatmap').then((dt) => {
      if (dt == null) { this.setState({ heatmap: true }) }
      else { this.setState({ heatmap: false }); }

    });
    AsyncStorage.getItem('curr').then((dt) => {
      if (dt == null) { this.setState({ currency: true }) }
      else { this.setState({ currency: false }); }

    });
    this.setState({ edit: false });
  }
  sensexFun() {
    this.setState({ loader: true });
    fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

      this.sensexData = responsejson;
      console.log(this.sensexData);

      this.setState({ ltp: this.sensexData[0].ltp, chng: this.sensexData[0].chg, per: this.sensexData[0].perchg, date: this.sensexData[0].dttm, });
      switch (this.sensexData[0].F) {
        case '0':
          this.setState({ status: 'Open' });
          break;
        case '1':
          this.setState({ status: 'Pre-Open' });

          break;
        case '2':
          this.setState({ status: 'Close' });

          break;
        case '3':
          this.setState({ status: 'Open' });

          break;
      }
      this.setState({ loader: false });

    })
  }
  format(val) {
    //alert(val);
    var no = val.split('.');
    if (no[0].length > 5 || no[0].length == 5) {
      return val.substring(0, 2) + ',' + val.substring(2);
    }
    else if (no[0].length == 4) {
      return val.substring(0, 1) + ',' + val.substring(1);
    }
    else if (no[0].length == 3 || no[0].length < 3) {
      return val;
    }

  }

  indicesFun() {
    //alert(val);
    this.setState({ indicesloader: true });
    switch (this.state.langauge) {
      case 'en':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"en","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });
        break;
      case 'hn':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"hn","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });

        break;
      case 'mr':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"mr","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });

        break;
      case 'gj':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"gj","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });
        break;
      case 'bn':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"bn","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });
        break;
      case 'ml':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"ml","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });
        break;
      case 'or':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"or","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });
        break;
      case 'tml':
        fetch('https://api.bseindia.com/bseindia/api/Indexmasternew/GetData?json={"flag":"","ln":"tml","pg":"1","cnt":"6","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
          this.setState({ indicesArray: responsejson });

          this.state.indicesArray.map((dt) => {
            if (dt.chg.charAt(0) != '-') {
              dt.chg = '+' + dt.chg;
              dt.perchg = '+' + dt.perchg;
            }

          })
          this.setState({ indicesloader: false });
        });

        break;
    }


  }
  currencyFun() {
    this.setState({ currencyloader: true });



    fetch('https://api.bseindia.com/BseIndiaAPI/api/CurrHeatMap/m?Flag=9&Records=1000&scripcode=0&pg=1&cnt=6&field=8&ln=en').then((response) => response.json()).then((responsejson) => {
      this.setState({ currencypager: responsejson[0].PageCount });
      // alert('currcount  '+this.state.currencypager);
    });




    fetch('https://api.bseindia.com/BseIndiaAPI/api/CurrHeatMap/m?Flag=9&Records=1000&scripcode=0&pg=2&cnt=5&field=CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM&ln=en').then((response) => response.json()).then((responsejson) => {
      this.setState({ currencyArray: responsejson });
      this.state.currencyArray.map((dt) => {
        if (dt.CHANGEPERC.charAt(0) != '-') {
          dt.CHANGEPERC = '+' + dt.CHANGEPERC;
          dt.CHANGE = '+' + dt.CHANGE;

        }
      })
      this.setState({ onecurr: this.state.currencyArray[0], twocurr: this.state.currencyArray[1], threecurr: this.state.currencyArray[2], fourcurr: this.state.currencyArray[3], fivecurr: this.state.currencyArray[4], sixcurr: this.state.currencyArray[5] });
      this.setState({ currencyloader: false });

    });

  }
  sensexHeatMapFun() {
    this.setState({ heatmaploader: true });



    switch (this.state.langauge) {
      case 'en':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });
        break;
      case 'hn':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });

        break;
      case 'mr':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });
        break;
      case 'gj':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });
        break;
      case 'bn':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });
        break;
      case 'ml':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });
        break;
      case 'or':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });
        break;
      case 'tml':
        fetch('https://api.bseindia.com/bseindia/api/IndexConstituentsnew/GetData?json={"icode":"16","order":"","pg":"1","cnt":"6","fields":"1"}').then((response) => response.json()).then((responsejson) => {
          this.code = responsejson[0].scode.split('|');
          console.log('code', this.code);
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[0] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname1: responsejson[0].sname, ltp1: responsejson[0].ltp, changeval1: responsejson[0].chgval, changeper1: responsejson[0].chgper, scode1: responsejson[0].scode });

          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[1] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname2: responsejson[0].sname, ltp2: responsejson[0].ltp, changeval2: responsejson[0].chgval, changeper2: responsejson[0].chgper, scode2: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[2] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname3: responsejson[0].sname, ltp3: responsejson[0].ltp, changeval3: responsejson[0].chgval, changeper3: responsejson[0].chgper, scode3: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[3] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname4: responsejson[0].sname, ltp4: responsejson[0].ltp, changeval4: responsejson[0].chgval, changeper4: responsejson[0].chgper, scode4: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[4] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname5: responsejson[0].sname, ltp5: responsejson[0].ltp, changeval5: responsejson[0].chgval, changeper5: responsejson[0].chgper, scode5: responsejson[0].scode });
          });
          fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + this.code[5] + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            responsejson.map((dt) => {
              if (dt.chgval.charAt(0) != '-') {
                dt.chgval = '+' + dt.chgval;
                dt.chgper = '+' + dt.chgper;

              }
            })
            this.setState({ sname6: responsejson[0].sname, ltp6: responsejson[0].ltp, changeval6: responsejson[0].chgval, changeper6: responsejson[0].chgper, scode6: responsejson[0].scode });
          });
          setTimeout(() => { this.setState({ heatmaploader: false }) }, 2000)

        });

        break;
    }

  }
  commodityFun() {
    this.setState({ commodityloader: true });

    fetch('https://api.bseindia.com/BseIndiaAPI/api/CommHeatMap/m?Flag=8&Records=1000&scripcode=0&pg=1&cnt=6&field=CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM&ln=en').then((response) => response.json()).then((responsejson) => {
      this.setState({ commodityArray: responsejson });
      this.state.commodityArray.map((dt) => {
        if (dt.CHANGEPERC.charAt(0) != '-') {
          dt.CHANGEPERC = '+' + dt.CHANGEPERC;
          dt.CHANGE = '+' + dt.CHANGE;

        }
      })
      console.log(this.state.commodityArray);

      this.setState({ one: this.state.commodityArray[0], two: this.state.commodityArray[1], three: this.state.commodityArray[2], four: this.state.commodityArray[3], five: this.state.commodityArray[4], six: this.state.commodityArray[5] });
      //alert(this.state.one)
      this.setState({ commodityloader: false });

    });
  }

 

  googleassistant() {

    // Linking.openURL('https://assistant.google.com/explore');
    AsyncStorage.clear();
  }
  refresh() {
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' :  ToastAndroid.showWithGravityAndOffset(
        "You are Offline.Please check your connection.",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    });
    this.sensexHeatMapFun();
    this.watchListFun();

    this.sensexFun();
    this.indicesFun();

    this.commodityFun();
    this.currencyFun();
    // AsyncStorage.clear();


  }
  
  detailsensex(cd) {
    AsyncStorage.getItem('array').then((data) => {
        const val = JSON.parse(data);
        alert(''+val);
    });
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' :  ToastAndroid.showWithGravityAndOffset(
        "You are Offline.Please check your connection.",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    });

    this.setState({ modvis: true });
    this.setState({ sensexDetailloader: true });

    switch (this.state.langauge) {
      case 'en':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });
        break;
      case 'hn':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });

        break;
      case 'mr':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });

        break;
      case 'gj':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });
        break;
      case 'bn':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });
        break;
      case 'ml':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });
        break;
      case 'or':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });
        break;
      case 'tml':
        fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
          responsejson.map((dt) => {
            if (dt.chgval > 0) {
              dt.chgper = '+' + dt.chgper;
              dt.chgval = '+' + dt.chgval;
            }
          });
          this.Data = responsejson[0];

          this.setState({ sensexDetailloader: false });

        });

        break;
    }


  }

//   add(dt) {



//     AsyncStorage.getItem('array').then((data) => {
//       const val = JSON.parse(data);
//       console.log('val', val);
//       if (val == null) {
//         console.log('1');
//         this.setState({watchloader:true});
//         this.watchArray.push(dt);
//         console.log(this.watchArray);


//         console.log('val', this.watchArray);
//         var ary = this.watchArray.filter((obj, pos, arr) => {
//           return arr.map(mapObj =>
//             mapObj.scode).indexOf(obj.scode) == pos;
//         });
//         ary.map((val) => {
//           val.ltp = this.format(val.ltp);

//         })
//         console.log('ary', ary);
//         //alert(ary[0].ltp);
//         this.setState({ Warray: ary });

//         AsyncStorage.setItem('array', JSON.stringify(ary));
//         ToastAndroid.showWithGravityAndOffset(
//           "Added to Watchlist Successfully.",
//           ToastAndroid.LONG,
//           ToastAndroid.BOTTOM,
//           25,
//           50
//         );
//         this.watchListFun();
//       }
//       else if (val != '') {
//         // AsyncStorage.setItem('watchval', "false");
//         // this.ok();
//         this.setState({watchloader:true});

//         console.log('2');
//         val.push(dt);
//         console.log('val', val);
//         var ary = val.filter((obj, pos, arr) => {
//           return arr.map(mapObj =>
//             mapObj.scode).indexOf(obj.scode) == pos;
//         });
//         ary.map((val) => {
//           if (val.ltp.includes(",") == true) { }
//           else if (val.ltp.includes(",") == false) {
//             val.ltp = this.format(val.ltp)
//           }



//         })
//         console.log('ary', ary);
//         // alert(ary[0].ltp);
//         this.setState({ Warray: ary });
       

//         AsyncStorage.setItem('array', JSON.stringify(ary));
//         this.watchListFun();
//         ToastAndroid.showWithGravityAndOffset(
//           "Added to Watchlist Successfully.",
//           ToastAndroid.LONG,
//           ToastAndroid.BOTTOM,
//           25,
//           50
//         );
//         // AsyncStorage.removeItem('watchval');
//         // this.ok();
//       }


//     })


//     this.setState({ modvis: false });

//   }

add(dt)
{

    AsyncStorage.getItem('array').then((data) => {
        const val = JSON.parse(data);

        if(val==null)
        {
            this.watchArray.push(dt.scode);
            AsyncStorage.setItem('array', JSON.stringify(this.watchArray));

        }
        else if (val != '')
        {
            val.push(dt.scode);
            var ary = val.filter((item,index) => {
                if(val.indexOf(item) === index)
                {
                    return val.indexOf(item) === index;
                }
                else
                {
                    alert('Scode Already Present');
                }
            });
            AsyncStorage.setItem('array', JSON.stringify(ary));
        }

    });

    this.setState({ modvis: false });
  
}
dynamicwatch()
{
    
}
  watchListFun() {
    AsyncStorage.setItem('watchval', "false");
    this.ok();
    AsyncStorage.getItem('array').then((data) => {
      console.log('data', JSON.parse(data));
      let watchdata = JSON.parse(data);
     // this.setState({ Warray: JSON.parse(data) });
     
     
      switch (this.state.langauge) {
        case 'en':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata,});
            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          
          })
          
          break;
        case 'hn':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});

            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
            
          })
          
          break;
        case 'mr':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});

            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          })
          
          break;
        case 'gj':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});

            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          })
         
          break;
        case 'bn':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});
            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          })
          break;
        case 'ml':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});
            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          })
          break;
        case 'or':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});
            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          })
          
          break;
        case 'tml':
          watchdata.map((dt) => {
            fetch('https://api.bseindia.com/bseindia/api/GetQuotescripnew/GetData?json={"scode":' + dt.scode + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
              dt.sname = responsejson[0].sname;
            })
            this.setState({ Warray: watchdata});
            if(this.state.indicesArray=='')
            {
              AsyncStorage.removeItem('watchval');
                this.ok();
            }
            else
            {
              setTimeout(()=> {
                AsyncStorage.removeItem('watchval');
                this.ok();
                this.setState({watchloader:false});
              },3000);
            }
          })
          break;
      }
     
    })
  }

  onRefresh() {
    this.refresh();
  }

  oncheckWatch() {
    //this.setState({ watch: !val });
    this.state.watch == true ? AsyncStorage.setItem('watchval', "false") : AsyncStorage.removeItem('watchval');
  }
  oncheckPort() {
    // this.setState({ port: !val });
    this.state.port == true ? AsyncStorage.setItem('portval', "false") : AsyncStorage.removeItem('portval');
  }
  oncheckIndices() {
    // this.setState({ indices: !val });
    this.state.indices == true ? AsyncStorage.setItem('indval', "false") : AsyncStorage.removeItem('indval');
  }
  oncheckCommodit() {
    // this.setState({ commodity: !val });
    this.state.commodity == true ? AsyncStorage.setItem('commodt', "false") : AsyncStorage.removeItem('commodt');
  }
  oncheckSensex() {
    // this.setState({ heatmap: !val });
    this.state.heatmap == true ? AsyncStorage.setItem('heatmap', "false") : AsyncStorage.removeItem('heatmap');
  }
  oncheckCurrency() {
    // this.setState({ currency: !val });
    this.state.currency == true ? AsyncStorage.setItem('curr', "false") : AsyncStorage.removeItem('curr');
  }

  _renderDotIndicatorlight() {
    return <PagerDotIndicator pageCount={3} dotStyle={{ backgroundColor: '#d5d6d9', top: 25, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#0089d0', top: 25, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _renderDotIndicatordark() {
    return <PagerDotIndicator pageCount={3} dotStyle={{ backgroundColor: '#2e414a', top: 25, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#72a3bf', top: 25, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;
  }
  _rndrdot() {
    return <PagerDotIndicator pageCount={this.state.currencypager} dotStyle={{ backgroundColor: '#2e414a', top: 25, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} selectedDotStyle={{ backgroundColor: '#72a3bf', top: 25, marginLeft: 5, marginRight: 5, width: 8, height: 8, borderRadius: 5 }} />;

  }
  chnglan(lan) {
    this.setState({ selectlan: lan,watchloader:true });
    lan == 'English' ? I18n.locale = "en" : lan == 'Hindi' ? I18n.locale = "hi" : lan == 'Marathi' ? I18n.locale = "mr" : lan == 'Gujrati' ? I18n.locale = "gj" : lan == 'Bengali' ? I18n.locale = "bn" : lan == 'Malayalam' ? I18n.locale = "ml" : lan == 'Oriya' ? I18n.locale = "or" : lan == 'Tamil' ? I18n.locale = "tml" : I18n.locale = "en";
    this.closePopover();
    AsyncStorage.setItem('language', lan);
    this.lanfun();


  }


  render() {

    return (
      <View>

        <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
          <StatusBar hidden={true} />
          <ScrollView style={this.state.value == '0' ? styles.scrollviewlight : styles.scrollviewdark}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
            showsVerticalScrollIndicator={false}

          >

            <Header style={this.state.value == '0' ? styles.light : styles.dark} >

              {/* <Row style={{bottom:15}}>
                <Col style={this.state.value == '0' ? styles.headermaxcollight : styles.headermaxcoldark}><TouchableOpacity></TouchableOpacity></Col>
                <Col style={this.state.value == '0' ? styles.headermincollight : styles.headermincoldark}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 4, }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                <Col style={this.state.value == '0' ? styles.headermincollight : styles.headermincoldark}><TouchableOpacity activeOpacity={.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'right',marginRight:15 }} size={30} color={this.state.value == '0' ? 'grey' : '#72a3bf'} /></TouchableOpacity></Col>
              </Row> */}
              <Left>
                <TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, marginLeft: 270 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity>

              </Left>
              <Right>
                <TouchableOpacity activeOpacity={.5} onPress={() => this.more()}><Icon name="md-more" style={{ marginTop: 5, marginRight: 13 }} size={30} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></TouchableOpacity>

              </Right>

            </Header>

            <View style={this.state.value == '0' ? styles.snpviewlight : styles.snpviewdark}>
              <View style={this.state.loader == false ? styles.show : styles.hide}>
                {/* <Text></Text> */}
                <Row>
                  <Col style={{ width: '17%', height: 65, }}></Col>
                  <Col style={{ width: '73%', height: 65, }}><Thumbnail square style={{ width: 200, height: 53, marginTop: 5, marginLeft: '7%', }} source={this.state.value == '0' ? require('../images/sensex-removebg-preview.png') : require('../images/sen.jpg')} /></Col>
                  <Col style={{ width: '10%', height: 65, }}></Col>
                </Row>



                <Row>
                  <Col style={{ width: '17%', height: 65, }}></Col>
                  <Col style={{ width: '31%', height: 40, }}><Text style={this.state.value == '0' ? styles.valuelight : styles.valuedark}>{this.state.ltp}</Text></Col>
                  <Col style={{ width: '42%', height: 40, }}><Text style={this.state.chng > 0 ? styles.headervalpos : styles.headervalneg}>{this.state.chng}  {this.state.per}%</Text></Col>
                  <Col style={{ width: '10%', height: 65, }}></Col>
                  {/* redval="#f54845" */}
                </Row>
                <Row>
                  <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.datelight : styles.datedark}>{this.state.date} | {I18n.t(this.state.status)}</Text></Col>
                </Row>
              </View>
              <View style={this.state.loader == true ? styles.show : styles.hide}>
                <ActivityIndicator size="large" style={{ bottom: 20 }} color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
              </View>
              <Row style={{ bottom: 35 }}>
                <Col style={{ width: '100%', height: 53, }}><Image
                  style={{ width: Dimensions.get('window').width - 30, height: 53, margin: 15, }}
                  source={require('../images/ad1.jpg')}
                /></Col>
              </Row>

            </View>



            <View style={this.state.watch == true ? styles.show : styles.hide}>

              <View style={this.state.Warray != null ? styles.show : styles.hide}>

                <View style={{}}>
                  <Row style={{ margin: 15, }}>
                    <Col style={this.state.value == '0' ? styles.watchcollightup : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext} numberOfLines={1}>{I18n.t('watchlist')}</Text></Col>
                    <Col style={this.state.value == '0' ? styles.watchcollightup : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></Col>
                  </Row>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', height: 120, bottom: '7%' }}>
                  <Carousel
                    layout={'default'}
                    ref={ref => this.carousel = ref}
                    data={this.state.Warray}
                    sliderWidth={screenWidth}
                    itemWidth={173}
                    firstItem={1}
                    renderItem={this.state.value == '0' ? this._renderItemlight : this._renderItemdark}
                    inactiveSlideOpacity={this.state.value == '0' ? 1 : 0.9}
                    inactiveSlideScale={0.9}
                    inactiveSlideShift={4}

                  />

                </View>

              </View>

            </View>
            <View style={this.state.watchloader == false ? styles.hide : styles.show}>
            <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />

            </View>
            <View style={this.state.port == true ? styles.show : styles.hide}>

              <View style={this.state.Warray != null && this.state.watch == true ? styles.withwatch : this.state.watch == false ? styles.wowatch : styles.wowatch}>
                <Row style={{ margin: 15, }}>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('portfolio')}</Text></Col>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></Col>
                </Row>
              </View>
              <View style={this.state.Warray != null && this.state.watch == true ? styles.withwatcport : this.state.watch == false ? styles.wowatchport : styles.wowatchport}>
                <Row style={{ margin: 10, }}>
                  <Col style={{ width: '48%', height: 110 }}>
                    <View style={this.state.value == '0' ? styles.portlightview : styles.portdarkview}>
                      <Text style={this.state.value == '0' ? styles.lightgain : styles.darkgain}>{I18n.t('overallgain')}</Text>

                      <Row style={{ marginTop: 10, }}>
                        <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 55, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
                        <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 55, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/up.png') : require('../images/rsz_new_design_hompage_black-theme_codeup.jpg')} /></Col>
                      </Row>
                      <Row style={{}}>
                        <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>113.65</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inrupee')}</Text></Col>
                        <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>566.68%</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inpercentage')}</Text></Col>
                      </Row>
                      <Text></Text>

                    </View>
                  </Col>
                  <Col style={{ width: '3%', height: 110 }}></Col>
                  <Col style={{ width: '48%', height: 110 }}>
                    <View style={this.state.value == '0' ? styles.portlightview : styles.portdarkview}>
                      <Text style={this.state.value == '0' ? styles.lightgain : styles.darkgain}>{I18n.t('todaygain')}</Text>
                      <Row style={{ marginTop: 10, }}>
                        <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 70, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/down.jpg') : require('../images/rsz_1new_design_hompage_black-theme_code.jpg')} /></Col>
                        <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={{ width: '49.5%', height: 40 }}><Image style={{ width: 70, height: 23, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/down.jpg') : require('../images/rsz_1new_design_hompage_black-theme_code.jpg')} /></Col>
                      </Row>
                      <Row style={{}}>
                        <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>-6.00</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inrupee')}</Text></Col>
                        <Col style={this.state.value == '0' ? styles.lightcoloverall : styles.darkcoloverall}></Col>
                        <Col style={{ width: '49.5%', height: 40 }}><Text style={this.state.value == '0' ? styles.gainlightprice : styles.gaindarkprice}>-4.55%</Text><Text style={this.state.value == '0' ? styles.lightcurrency : styles.darkcurrency}>{I18n.t('inpercentage')}</Text></Col>
                      </Row>
                      <Text></Text>


                    </View>
                  </Col>
                </Row>
              </View>

            </View>

            <View style={this.state.port == false && this.state.watch == false ? styles.hide : styles.show}>
              <View style={this.state.Warray != null ? styles.withwatchimage : styles.wowatchimage}>
                <Image
                  style={{ width: Dimensions.get('window').width - 30, height: 53 }}
                  source={require('../images/ad2.jpg')}
                />
              </View>
            </View>

            <View style={this.state.indices == true ? styles.show : styles.hide}>

              <View style={{ bottom: '3%' }}>
                <Row style={{ margin: 15, }}>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('indices')}</Text></Col>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></Col>
                </Row>
              </View>
              <View style={this.state.indicesloader == true ? styles.showindloader : styles.hide}>
                <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
              </View>
              <View style={this.state.indicesloader == false ? styles.showindview : styles.hide}>
                <FlatList
                  data={this.state.indicesArray}
                  style={this.state.value == '0' ? styles.indiceslight : styles.indicesdark}
                  renderItem={({ item }) =>
                    <View>
                      <Row style={this.state.value == '0' ? styles.showindices : styles.hide}>
                        <Col style={{ width: '10%', height: 50, }}><Icon name={item.chg.charAt(0) != '-' ? "md-arrow-round-up" : "md-arrow-round-down"} style={item.chg.charAt(0) != '-' ? styles.iconpos : styles.iconneg} size={23} /></Col>
                        <Col style={{ width: '4%', height: 50, }}></Col>
                        <Col style={{ width: '61%', height: 50, }}><Text style={styles.lightindtext} numberOfLines={1}>{item.indxnm}</Text><Text style={item.chg.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chg}   {item.perchg}%</Text></Col>
                        <Col style={{ width: '25%', height: 50, }}><Text style={styles.lightvaluetext}>{this.format(item.ltp)}</Text></Col>
                      </Row>
                      <Row style={this.state.value == '1' ? styles.showindices : styles.hide}>
                        <Col style={{ width: '10%', height: 50, }}><Icon name={item.chg.charAt(0) != '-' ? "md-arrow-round-up" : "md-arrow-round-down"} style={item.chg.charAt(0) != '-' ? styles.iconposdark : styles.iconnegdark} size={24} /></Col>
                        <Col style={{ width: '1%', height: 50 }}></Col>
                        <Col style={{ width: '64%', height: 50, }}><Text style={styles.darkindtext} numberOfLines={1}>{item.indxnm}</Text><Text style={item.chg.charAt(0) != '-' ? styles.chnagepos : styles.changeneg}>{item.chg}   {item.perchg}%</Text></Col>
                        <Col style={{ width: '25%', height: 50, }}><Text style={styles.darkvaluetext}>{this.format(item.ltp)}</Text></Col>
                      </Row>
                    </View>
                  }
                  keyExtractor={item => item.id}
                />
              </View>

            </View>

            <View style={this.state.commodity == true ? styles.show : styles.hide}>

              <View style={{ bottom: '4%' }}>
                <Row style={{ margin: 15, }}>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('commodity')}</Text></Col>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></Col>
                </Row>
              </View>

              <View style={this.state.commodityloader == true ? styles.showcommodityloader : styles.hide}>
                <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
              </View>
              <View style={this.state.commodityloader == false ? styles.showcommodityview : styles.hide}>
                <View style={this.state.value == '0' ? styles.show : styles.hide}>


                  <IndicatorViewPager
                    style={{ height: 107 }}
                    indicator={this._renderDotIndicatorlight()}
                  >
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.one.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.one.EXPIRYDATE}</Text>
                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text> </Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.two.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.two.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.three.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.three.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>

                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.three.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.three.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.four.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.four.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.four.LASTTRADERATE)} <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE} {this.state.four.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.five.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.five.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.five.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.five.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.six.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.six.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.six.LASTTRADERATE)} <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE} {this.state.six.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>

                      </Row>


                    </View>

                  </IndicatorViewPager>

                </View>

                <View style={this.state.value == '1' ? styles.show : styles.hide}>

                  <IndicatorViewPager
                    style={{ height: 107 }}
                    indicator={this._renderDotIndicatordark()}
                  >
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.one.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.one.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.one.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.one.LASTTRADERATE)} <Text style={this.state.one.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.one.CHANGE} {this.state.one.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.two.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.two.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.two.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.two.LASTTRADERATE)} <Text style={this.state.two.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.two.CHANGE} {this.state.two.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.three.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.three.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>

                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.three.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.three.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.three.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.three.LASTTRADERATE)} <Text style={this.state.three.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.three.CHANGE} {this.state.three.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.four.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.four.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.four.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.four.LASTTRADERATE)} <Text style={this.state.four.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.four.CHANGE} {this.state.four.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.five.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.five.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.five.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.five.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.five.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.five.LASTTRADERATE)} <Text style={this.state.five.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.five.CHANGE} {this.state.five.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.six.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.six.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.six.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.six.LASTTRADERATE)} <Text style={this.state.six.CHANGEPERC > 0 ? styles.currencypos : styles.currencyred}>{this.state.six.CHANGE} {this.state.six.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>

                      </Row>

                    </View>

                  </IndicatorViewPager>
                </View>


              </View>

            </View>

            <View style={this.state.heatmap == true ? styles.show : styles.hide}>

              <View style={{ bottom: '4%' }}>
                <Row style={{ margin: 15, }}>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('sensexheat')}</Text></Col>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></Col>
                </Row>
              </View>
              <View style={this.state.heatmaploader == true ? styles.showheatloader : styles.hide}>
                <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
              </View>
              <View style={this.state.heatmaploader == false ? styles.showheatview : styles.hide}>
                <View style={this.state.value == '0' ? styles.show : styles.hide}>


                  <IndicatorViewPager
                    style={{ height: 107 }}
                    indicator={this._renderDotIndicatorlight()}
                  >
                    <View style={{}}>

                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.sname1 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview} >
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode1)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname1}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp1)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval1 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval1}   {this.state.changeper1}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.sname2 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode2)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname2}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp2)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval2 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval2}   {this.state.changeper2}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.sname3 == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode3)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname3}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp3)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>

                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.scode3 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode3)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname3}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp3)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.sname4 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode4)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname4}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp4)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval4 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval4}   {this.state.changeper4}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.sname5 == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode5)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname5}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp5)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                      </Row>


                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.scode5 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode5)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname5}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp5)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.scode6 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode6)}>

                              <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sname6}</Text>
                              <Text style={styles.sensexltplight}>{this.format('' + this.state.ltp6)}</Text>

                              <Text style={styles.currvaluelight}><Text style={this.state.changeval6 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval6}   {this.state.changeper6}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>


                      </Row>

                    </View>

                  </IndicatorViewPager>

                </View>

                <View style={this.state.value == '1' ? styles.show : styles.hide}>


                  <IndicatorViewPager
                    style={{ height: 105 }}
                    indicator={this._renderDotIndicatordark()}
                  >
                    <View style={{}}>

                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.sname1 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview} >
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode1)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname1}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp1)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval1 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval1}   {this.state.changeper1}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.sname2 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode2)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname2}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp2)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval2 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval2}   {this.state.changeper2}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.sname3 == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode3)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname3}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp3)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.sname3 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode3)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname3}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp3)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval3 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval3}   {this.state.changeper3}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.sname4 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode4)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname4}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp4)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval4 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval4}   {this.state.changeper4}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.scode5 == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode5)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname5}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp5)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.sname5 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode5)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname5}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp5)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval5 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval5}   {this.state.changeper5}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.sname6 == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <TouchableOpacity onPress={() => this.detailsensex(this.state.scode6)}>

                              <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sname6}</Text>
                              <Text style={styles.sensexltpdark}>{this.format('' + this.state.ltp6)}</Text>

                              <Text style={styles.currvaluedark}><Text style={this.state.changeval6 > 0 ? styles.currencypos : styles.currencyred}>{this.state.changeval6}   {this.state.changeper6}%</Text></Text>
                              <Text style={{ marginTop: 5 }}></Text>
                            </TouchableOpacity>

                          </View>
                        </Col>


                      </Row>


                    </View>

                  </IndicatorViewPager>
                </View>
              </View>
            </View>

            <View style={this.state.currency == true ? styles.show : styles.hide}>

              <View style={{ bottom: '4%' }}>
                <Row style={{ margin: 15, }}>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Text style={this.state.value == '0' ? styles.watchlighttext : styles.watchdarktext}>{I18n.t('currency')}</Text></Col>
                  <Col style={this.state.value == '0' ? styles.watchcollight : styles.watchcoldark}><Icon name="ios-more" style={{ textAlign: "right", top: 3 }} size={25} color={this.state.value == '0' ? '#aaafba' : '#72a3bf'} /></Col>
                </Row>
              </View>
              <View style={this.state.currencyloader == true ? styles.showcurrloader : styles.hide}>
                <ActivityIndicator size="large" color={this.state.value == '0' ? "#0000ff" : '#72a3bf'} />
              </View>
              <View style={this.state.currencyloader == false ? styles.showcurrview : styles.hide}>
                <View style={this.state.value == '0' ? styles.show : styles.hide}>


                  <IndicatorViewPager
                    style={{ height: 107 }}
                    indicator={this._renderDotIndicatorlight()}
                  >
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.onecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.onecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.onecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.onecurr.LASTTRADERATE)} <Text style={this.state.onecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.onecurr.CHANGE} {this.state.onecurr.CHANGEPERC}%</Text> </Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.twocurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.twocurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.twocurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.twocurr.LASTTRADERATE)} <Text style={this.state.twocurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.twocurr.CHANGE} {this.state.twocurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.threecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE}   {this.state.threecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.threecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE}   {this.state.threecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>

                        <Col style={this.state.fourcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fourcurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.fourcurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.fourcurr.LASTTRADERATE)} <Text style={this.state.fourcurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fourcurr.CHANGE}   {this.state.fourcurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.fivecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE}   {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.fivecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE}   {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.sixcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currlightview}>
                            <Text style={styles.currnamelight} numberOfLines={1}>{this.state.sixcurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatelight}>{this.state.sixcurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluelight} numberOfLines={1}>{this.format('' + this.state.sixcurr.LASTTRADERATE)} <Text style={this.state.sixcurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.sixcurr.CHANGE}   {this.state.sixcurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>

                      </Row>


                    </View>
                  </IndicatorViewPager>
                  <Text></Text>
                </View>
                <View style={this.state.value == '1' ? styles.show : styles.hide}>

                  <IndicatorViewPager
                    style={{ height: 107 }}
                    indicator={this._renderDotIndicatordark()}
                  >
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.onecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.onecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.onecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.onecurr.LASTTRADERATE)} <Text numberOfLines={1} style={this.state.onecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.onecurr.CHANGE} {this.state.onecurr.CHANGEPERC} %</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.twocurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.twocurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.twocurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.twocurr.LASTTRADERATE)} <Text style={this.state.twocurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.twocurr.CHANGE} {this.state.twocurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.threecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE} {this.state.threecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.threecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.threecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.threecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.threecurr.LASTTRADERATE)} <Text style={this.state.threecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.threecurr.CHANGE} {this.state.threecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.fourcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.fourcurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.fourcurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.fourcurr.LASTTRADERATE)} <Text style={this.state.fourcurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fourcurr.CHANGE} {this.state.fourcurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined2}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.fivecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE} {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                      </Row>

                    </View>
                    <View style={{}}>
                      <Row style={{ marginLeft: 15 }}>
                        <Col style={this.state.fivecurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.fivecurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.fivecurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.fivecurr.LASTTRADERATE)} <Text style={this.state.fivecurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.fivecurr.CHANGE} {this.state.fivecurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>
                        <Col style={{ width: '4%', height: 90 }}></Col>
                        <Col style={this.state.sixcurr.INSTRUMENTNAME == undefined ? styles.hide : styles.colnudefined}>
                          <View style={styles.currdarkview}>
                            <Text style={styles.currnamedark} numberOfLines={1}>{this.state.sixcurr.INSTRUMENTNAME}</Text>
                            <Text style={styles.currdatedark}>{this.state.sixcurr.EXPIRYDATE}</Text>

                            <Text style={styles.currvaluedark} numberOfLines={1}>{this.format('' + this.state.sixcurr.LASTTRADERATE)} <Text style={this.state.sixcurr.chgval > 0 ? styles.currencypos : styles.currencyred}>{this.state.sixcurr.CHANGE} {this.state.fourcurr.CHANGEPERC}%</Text></Text>
                            <Text style={{ marginTop: 5 }}></Text>
                          </View>
                        </Col>

                      </Row>


                    </View>
                  </IndicatorViewPager>
                  <Text></Text>
                </View>
              </View>
            </View>
            {/* <View>
            <IndicatorViewPager
                style={{ height: 107 }}
                indicator={this._rndrdot()}
              >
                <View>

                
              <FlatList
                data={this.state.currencypager}                
                renderItem={({ item }) =>
                
                 <View style={{backgroundColor:'white',margin:15,height:10}}>

                 </View>


                } />
                </View>
                </IndicatorViewPager>

                
            </View>
            <View><Text></Text></View>
            <View><Text></Text></View>
            <View><Text></Text></View>
            <View><Text></Text></View> */}

            <Popover
              isVisible={this.state.isVisible}
              fromView={this.touchable}
              arrowStyle={true}
              showArrow={true}
              onRequestClose={() => this.closePopover()}>
              <View style={{ backgroundColor: 'white', height: '100%' }}>

                <Text></Text>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}><IconEnt name="setting" style={{ textAlign: 'center' }} color={'black'} size={23}></IconEnt></Col>
                  <Col style={{ width: '80%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 20, bottom: 3, color: 'black', }}>{I18n.t('setting')}</Text></Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, bottom: 2, color: 'black' }}> {this.state.themename} {I18n.t('theme')} </Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <Switch
                      trackColor="black"
                      trackColor="grey"
                      value={this.state.switchval}
                      style={{ bottom: 3, alignSelf: 'center' }}
                      onValueChange={(e) => this.toggleSwitch(e)}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, bottom: 2, color: 'black' }}> {I18n.t('edithome')} </Text></Col>
                  <Col style={{ width: '40%', height: 50, }}><Icon name="ios-arrow-dropright-circle" onPress={() => this.edit()} style={{ textAlign: 'center' }} color={'black'} size={25} /></Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, bottom: 2, color: 'black' }}> {I18n.t('changelanguage')}</Text></Col>
                  <Col style={{ width: '5%', height: 50, }}></Col>
                  <Col style={{ width: '35%', height: 50 }}>
                    <Picker
                      selectedValue={this.state.selectlan}
                      style={{ height: 30, bottom: 5, width: '100%', }}
                      onValueChange={(itemValue) => this.chnglan(itemValue)}
                    >
                      <Picker.Item label="English" value="English" />
                      <Picker.Item label="Hindi" value="Hindi" />
                      <Picker.Item label="Marathi" value="Marathi" />
                      <Picker.Item label="Gujrati" value="Gujrati" />
                      <Picker.Item label="Bengali" value="Bengali" />
                      <Picker.Item label="Malayalam" value="Malayalam" />
                      <Picker.Item label="Oriya" value="Oriya" />
                      <Picker.Item label="Tamil" value="Tamil" />
                    </Picker>
                  </Col>
                </Row>




              </View>

            </Popover>
            <Popover
              isVisible={this.state.edit}
              fromView={this.touchable}>
              <View style={{}}>
                <Text></Text>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>{I18n.t('editwatch')}</Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <CheckBox value={this.state.watch} onValueChange={() => this.oncheckWatch()} style={{ alignSelf: "center", }} />

                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>{I18n.t('editport')}</Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <CheckBox value={this.state.port} onValueChange={() => this.oncheckPort()} style={{ alignSelf: "center", }} />

                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>{I18n.t('editindices')}</Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <CheckBox value={this.state.indices} onValueChange={() => this.oncheckIndices()} style={{ alignSelf: "center", }} />

                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>{I18n.t('editcommodity')}</Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <CheckBox value={this.state.commodity} onValueChange={() => this.oncheckCommodit()} style={{ alignSelf: "center", }} />

                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>{I18n.t('editsensex')}</Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <CheckBox value={this.state.heatmap} onValueChange={() => this.oncheckSensex()} style={{ alignSelf: "center", }} />

                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '20%', height: 50, }}></Col>
                  <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 17, color: 'black' }}>{I18n.t('editcurrency')}</Text></Col>
                  <Col style={{ width: '40%', height: 50, }}>
                    <CheckBox value={this.state.currency} onValueChange={() => this.oncheckCurrency()} style={{ alignSelf: "center", }} />

                  </Col>
                </Row>


                <Row>
                  <Col style={{ width: '100%', height: 40 }}><Button style={{ marginLeft: 130, marginRight: 130, backgroundColor: '#2087c9', height: 35 }} onPress={() => this.ok()}><Text style={{ marginLeft: 25, fontSize: 20 }}>{I18n.t('ok')}</Text></Button></Col>
                </Row>
                <Text></Text>

              </View>
            </Popover>

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modvis}
              // onDismiss={() => {
              //   alert('dismiss');
              // }}
              style={{ height: '100%' }}
              onRequestClose={() => {
                this.setState({ modvis: false });
              }}
            >
              <View style={{ height: '100%' }}>

                <View style={this.state.sensexDetailloader == true ? styles.show : styles.hide}>
                  <View style={{ marginTop: '30%', }}>
                    <ActivityIndicator size="large" color={'#72a3bf'} />
                  </View>
                </View>

                <View style={this.state.sensexDetailloader == false ? styles.show : styles.hide}>
                  <View style={{ borderColor: 'black', borderWidth: 1, marginTop: '10%', margin: 10, height: '40%' }}>
                    <Row style={{ margin: 10, }}>
                      <Col style={{ width: '100%', height: 40 }}><Icon style={{ textAlign: 'right' }} name="ios-add" onPress={() => this.add(this.Data)} size={30} /></Col>
                    </Row>
                    <Row>
                      <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 19 }}>{this.Data.sname}</Text></Col>
                      <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 15 }}>Value:- {this.Data.ltp}</Text></Col>

                    </Row>
                    <Row>
                      <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 15 }}>{this.Data.chgval} {this.Data.chgper}%</Text></Col>
                      <Col style={{ width: '50%', height: 40 }}><Text style={{ textAlign: 'center', fontSize: 15 }}>Scode:- {this.Data.scode}</Text></Col>

                    </Row>

                  </View>
                </View>
              </View>
            </Modal>


          </ScrollView>

          <Footer >

            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
              <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('watchtab')} </Text>
              </Button>


              <Button style={styles.inactive} onPress={() => this.tab('port')}>
                <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('porttab')}</Text>
              </Button>
              {/* <Button></Button> */}

              <Button style={this.activetab == 'home' ? styles.active : styles.inactive} onPress={() => this.tab('home')}>
                <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                <Text style={this.state.value == '0' ? styles.tabnamelighthome : styles.tabnamedark} numberOfLines={1}>{I18n.t('home')}</Text>
              </Button>
              {/* <Button></Button> */}
              <Button style={styles.inactive} onPress={() => this.tab('search')}>
                <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('search')}</Text>
              </Button>
              {/* <Button></Button> */}
              <Button style={styles.inactive} onPress={() => this.tab('more')}>
                <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedarkdeactive} numberOfLines={1}>{I18n.t('more')}</Text>
              </Button>


            </FooterTab>
          </Footer>
        </View >
      </View >
    );
  }
}
const styles = StyleSheet.create({
  iconpos: {
    textAlign: "center", color: '#19cf3e', top: 3, padding: 5, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10
  },

  iconneg: {
    textAlign: "center", top: 3, color: '#f54845', padding: 5, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10
  },
  iconposdark: {
    textAlign: "center", top: 3, color: '#19cf3e', padding: 5, backgroundColor: '#1a1f1f', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14

  },
  iconnegdark: {
    textAlign: "center", top: 3, color: '#f54845', padding: 5, backgroundColor: '#1a1f1f', borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14

  },

  chnagepos: {
    left: 5, color: '#19cf3e', fontSize: 13, top: 2, fontFamily: 'SegoeProDisplay-Regular'
  },
  changeneg: {
    left: 5, color: '#f54845', fontSize: 13, top: 2, fontFamily: 'SegoeProDisplay-Regular'
  },
  currencypos: {
    fontWeight: 'normal', fontSize: 11.5, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular'
  },
  currencyred: {
    fontWeight: 'normal', fontSize: 11.5, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular'
  },
  light: {
    backgroundColor: '#f6f7f9'
  },
  dark: {
    backgroundColor: 'black'
  },
  mainviewlight: {
    backgroundColor: '#f6f7f9', height: '100%'
  },
  mainviewdark: {
    backgroundColor: '#333333', height: '100%'
  },
  headermaxcollight: {
    width: '70%', height: 30, marginTop: 15, backgroundColor: '#f6f7f9'
  },
  headermaxcoldark: {
    width: '70%', height: 30, marginTop: 15, backgroundColor: 'black'
  },
  headermincollight: {
    width: '15%', height: 30, marginTop: 15, backgroundColor: '#f6f7f9'
  },
  headermincoldark: {
    width: '15%', height: 30, marginTop: 15, backgroundColor: 'black'
  },
  snpviewlight: { backgroundColor: '#f6f7f9', borderBottomRightRadius: 240, },
  snpviewdark: {
    backgroundColor: '#000000', borderBottomRightRadius: 240,
  },
  valuelight: {
    textAlign: 'center', marginLeft: '9%', fontSize: 19, fontFamily: 'SegoePro-Bold', color: '#132144',
  },
  valuedark: {
    textAlign: 'center', marginLeft: '9%', fontSize: 19, fontFamily: 'SegoePro-Bold', color: '#72a3bf',
  },
  datelight: {
    textAlign: 'center', bottom: 31, fontSize: 16, color: '#aaafba', fontFamily: 'SegoeProDisplay-Regular'
  },
  datedark: {
    textAlign: 'center', bottom: 31, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular'
  },
  scrollviewlight: { backgroundColor: '#f1f2f6', },
  scrollviewdark: { backgroundColor: '#0b0b0b', },
  watchcollight: { width: '50%', height: 30 },
  watchcollightup: { width: '50%', height: 30, top: 8 },
  watchcoldark: { width: '50%', height: 30, },
  watchlighttext: { fontSize: 17, fontFamily: 'SegoePro-Bold', color: '#132144' },
  watchdarktext: { fontSize: 17, fontFamily: 'SegoePro-Bold', color: '#72a3bf' },
  portlightview: { width: 160, margin: 5, backgroundColor: 'white', height: 95, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  portdarkview: { width: 160, margin: 5, backgroundColor: '#1a1f1f', height: 95, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  lightgain: { textAlign: 'center', top: 5, color: '#132144', fontFamily: 'SegoeProDisplay-Regular' },
  darkgain: { textAlign: 'center', top: 5, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular' },
  lightcoloverall: { width: '1%', height: 40, backgroundColor: '#f4f4f4' },
  darkcoloverall: { width: '1%', height: 40, backgroundColor: '#353a3a' },
  gainlightprice: { textAlign: 'center', fontFamily: 'SegoePro-Bold', color: "#132144" },
  gaindarkprice: { textAlign: 'center', fontFamily: 'SegoePro-Bold', color: "#72a3bf" },
  lightcurrency: { textAlign: 'center', color: '#0089d0', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
  darkcurrency: { textAlign: 'center', color: '#72a3bf', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
  indiceslight: { marginTop: 10, },
  indicesdark: { backgroundColor: '#0b0b0b', marginTop: 10, },
  lightindtext: { left: 5, color: '#132144', fontSize: 14, fontFamily: 'SegoeProDisplay-Regular' },
  darkindtext: { left: 5, color: '#72a3bf', fontSize: 14, fontFamily: 'SegoeProDisplay-Regular' },
  lightvaluetext: { textAlign: 'right', top: 7, fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
  darkvaluetext: { textAlign: 'right', top: 7, fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
  currlightview: { width: 145, backgroundColor: 'white', marginTop: 7, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11 },
  currdarkview: { width: 145, backgroundColor: '#1a1f1f', marginTop: 7, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },


  tablight: { backgroundColor: 'white' },
  tabdark: { backgroundColor: '#1a1f1f' },
  tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamedark: { color: '#f6f7f9', fontSize: 10.5, marginTop: 3, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamedarkdeactive: { color: '#7a878f', fontSize: 11, marginTop: 3, fontFamily: 'SegoeProDisplay-Regular' },
  show: { display: 'flex', },
  showimg: { display: 'flex', marginTop: 15 },
  showltp: { display: 'flex', bottom: 35 },
  showdate: { display: 'flex', bottom: 70 },
  hide: { display: 'none' },
  showloader: { display: 'flex', bottom: '10%' },
  showindices: { display: 'flex', marginLeft: 15, marginRight: 15 },
  showindview: { display: 'flex', bottom: '8%' },
  showindloader: { display: 'flex', bottom: '4%' },
  showcommodityview: { display: 'flex', bottom: '14%' },
  showcommodityloader: { display: 'flex', bottom: '4%' },
  showheatloader: { display: 'flex', bottom: '4%' },
  showheatview: { display: 'flex', bottom: '14%' },
  showcurrloader: { display: 'flex', bottom: '4%' },
  showcurrview: { display: 'flex', bottom: '14%' },
  watchnamelight: { top: 5, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
  watchnamedark: { top: 5, marginRight: 10, marginLeft: 10, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
  watchvallight: { top: 15, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 13, fontFamily: 'SegoePro-Bold', },
  watchvaldark: { top: 15, marginRight: 10, marginLeft: 10, color: '#72a3bf', fontSize: 14, fontFamily: 'SegoePro-Bold', },
  watchwhite: { width: 160, marginTop: 15, backgroundColor: 'white', left: 5, margin: 5, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 10 },
  watchdark: { width: 160, marginTop: 15, backgroundColor: '#1a1f1f', left: 5, margin: 5, borderRadius: 10, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 14 },
  watchtrans: { width: 160, backgroundColor: 'transparent', left: 5, margin: 5, borderRadius: 10, },
  currnamelight: { top: 5, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },
  currnamedark: { top: 5, marginRight: 10, marginLeft: 10, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoeProDisplay-Regular' },

  currdatelight: { top: 12, marginRight: 10, marginLeft: 10, color: '#aaafba', fontSize: 12, fontFamily: 'SegoeProDisplay-Regular' },
  currdatedark: { top: 12, marginRight: 10, marginLeft: 10, color: '#72a3bf', fontSize: 12, fontFamily: 'SegoeProDisplay-Regular' },
  currvaluelight: { top: 18, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 13, fontFamily: 'SegoePro-Bold', },
  currvaluedark: { top: 18, marginRight: 10, marginLeft: 10, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoePro-Bold', },
  sensexltplight: { top: 12, marginRight: 10, marginLeft: 10, color: '#132144', fontSize: 13, fontFamily: 'SegoePro-Bold', },
  sensexltpdark: { top: 12, marginRight: 10, marginLeft: 10, color: '#72a3bf', fontSize: 13, fontFamily: 'SegoePro-Bold', },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
  inactive: { padding: 12, },
  nowatchlistlight: { textAlign: 'center', fontSize: 20, },
  nowatchlistdark: { textAlign: 'center', fontSize: 20, color: 'white' },
  withwatch: { bottom: '7%' },
  wowatch: { bottom: '2%' },
  withwatcport: { bottom: '20%' },
  wowatchport: { bottom: '15%' },
  wowatchimage: { margin: 15, bottom: 20 },
  withwatchimage: { margin: 15, bottom: 25 },
  headervalpos: { fontSize: 15, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', marginTop: 5 },
  headervalneg: { fontSize: 15, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', marginTop: 5 },
  colnudefined:{width: '43%', height: 90,},
  colnudefined2:{width: '8%', height: 90,}
});

export default DymanicMain;
