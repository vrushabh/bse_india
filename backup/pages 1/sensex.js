import React, { Component } from 'react';
import { View, Image, Dimensions, Linking, ToastAndroid, CheckBox, Modal, NetInfo, RefreshControl, Alert, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import Iconoct from 'react-native-vector-icons/Octicons';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view'
import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';

const height = Dimensions.get('window').height;

class Sensex extends Component {
    static navigationOptions = {

        header: null,
    };
    Sensex = [];
    TurnArray = [];
    themename = '';
    indicesdt = [];
    IndName = '';
    IndId = '';
    per = '';
    per1 = '';
    per2 = '';
    per3 = '';
    indicesId = '16';
    count = 0;
    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            date: '',
            status: '',
            value: 1,
            loader: false,
            indicesLoader: false,
            isVisible: false,
            switchval: false,
            security: [],
            overview: [],
            Turn: [],
            contri: [],
            refreshing: false,
            sidemenu: false,
            settingshow: false,
            theme: '',
            showedit: false,
            iconname: 'ios-arrow-down',
            selectlan: 'English',
            indicesdata: [],
            indicesName: 'S&P BSE SENSEX',

            ltp: '',
            prvClose: '',
            chanage: '',
            changePer: '',
            indicesDropdown: false

        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setTimeout(() => { SplashScreen.hide() }, 3000);

    }

    componentDidMount() {
        this.TurnArray = [];
        this.setState({ Turn: [] });
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        });
        this.themename = this.props.navigation.getParam('theme', '');
        if (this.themename == null || this.themename == 'dark') {
            this.setState({ value: '1' });
        }
        else {
            this.setState({ value: '0' });
        }
        this.IndName = this.props.navigation.getParam('name', '');
        this.IndId = this.props.navigation.getParam('id', '');
        if (this.IndName == null || this.IndId == null || this.IndName == '' || this.IndId == '') {
            this.securitydatafun();
            this.overviewdatafun(this.indicesId);
            this.turndatafun(this.indicesId);
            this.contridatafun(this.indicesId);

        }
        else {

            this.setState({ indicesId: this.IndId });
            this.indicesSecurity();
            this.overviewdatafun(this.IndId);
            this.turndatafun(this.IndId);
            this.contridatafun(this.IndId);
        }
        this.sensexfun();


    }
    indicesSecurity() {
        this.setState({ indicesName: this.IndName });
        fetch('https://api.bseindia.com/msource/Index.aspx?code=' + this.IndId).then((response) => response.text()).then((responsejson) => {
            this.setState({ ltp: responsejson.split('#')[0].split(',')[0], prvClose: responsejson.split('#')[0].split(',')[1] });
            this.per = this.state.ltp - this.state.prvClose;
            this.per1 = this.per.toFixed(2);
            this.per2 = (this.per / this.state.prvClose) * 100;
            this.per3 = this.per2.toFixed(2);
            this.setState({ chanage: this.per1, changePer: this.per3 });


        })
        fetch('https://api.bseindia.com/msource/Indicesscriplist.aspx?flag=AP&scripcd=' + this.IndId + '&ln=en').then((response) => response.text()).then((responsejson) => {
            this.setState({ security: responsejson.split('@'), });

        })
        this.indicesfun();
        AsyncStorage.setItem('indicesname', this.IndName);
        AsyncStorage.setItem('indicesid', this.IndId);
    }
    sensexfun() {
        this.setState({ loader: true });
        fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

            responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';
        })
    }
    securitydatafun() {
        fetch('https://api.bseindia.com/msource/Index.aspx?code=16').then((response) => response.text()).then((responsejson) => {
            this.setState({ ltp: responsejson.split('#')[0].split(',')[0], prvClose: responsejson.split('#')[0].split(',')[1] });
            this.per = this.state.ltp - this.state.prvClose;
            this.per1 = this.per.toFixed(2);
            this.per2 = (this.per / this.state.prvClose) * 100;
            this.per3 = this.per2.toFixed(2);
            this.setState({ chanage: this.per1, changePer: this.per3 });


        })
        fetch('https://api.bseindia.com/msource/Indicesscriplist.aspx?flag=AP&scripcd=16&ln=en').then((response) => response.text()).then((responsejson) => {
            this.setState({ security: responsejson.split('@'), });
        })

        this.indicesfun();
    }
    indicesfun() {
        fetch('https://api.bseindia.com/msource/indexmoversAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson) => {

            for (let i = 0; i < responsejson.split('#').length; i++) {
                if (responsejson.split('#')[i].split(',')[0] == '') { }
                else {
                    this.indicesdt.push({ 'name': responsejson.split('#')[i].split(',')[0], 'id': responsejson.split('#')[i].split(',')[5] });

                }
            }
            this.setState({ indicesdata: this.indicesdt, });

        })
    }
    overviewdatafun(id) {
        fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=0&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {

            fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson1) => {

                this.Sensex = [
                    { 'no': '1', 'prv': responsejson.split('@')[1], 'open': responsejson.split('@')[3], 'high': responsejson.split('@')[5], 'low': responsejson.split('@')[7], },
                    { 'no': '2', 'full': responsejson.split('@')[9], 'free': responsejson.split('@')[11], },
                    { 'no': '3', 'advance': responsejson1.split('#')[0].split('@')[1], 'advanceTO': responsejson1.split('#')[0].split('@')[2], 'decline': responsejson1.split('#')[1].split('@')[1], 'declineTO': responsejson1.split('#')[1].split('@')[2], 'unchanged': responsejson1.split('#')[2].split('@')[1], 'unchangedTO': responsejson1.split('#')[2].split('@')[2], 'nottraded': responsejson1.split('#')[3].split('@')[1], 'nottradedTO': responsejson1.split('#')[3].split('@')[2], 'total': responsejson1.split('#')[4].split('@')[1], 'totalTO': responsejson1.split('#')[4].split('@')[2] },
                    { 'no': '4', 'high': responsejson.split('@')[13], 'highdate': responsejson.split('@')[14], 'low': responsejson.split('@')[16], 'lowdate': responsejson.split('@')[17], 'alltimehigh': responsejson.split('@')[19], 'alltimehighdate': responsejson.split('@')[20], 'alltimelow': responsejson.split('@')[22], 'alltimelowdate': responsejson.split('@')[23], },
                    { 'no': '5', 'pe': responsejson.split('@')[25], 'pb': responsejson.split('@')[27], 'divided': responsejson.split('@')[29].substring(0, 4), 'value': '57.45 +64.34', 'ltp': '2345.54', 'buy': '6545.00(4)', 'sell': '2131.00(4)', 'trd': '40,337', 'status': 'pos', 'name': 'INDUSDINBANK', },


                ]
                this.setState({ overview: this.Sensex, })

            })
        })
    }
    turndatafun(id) {
        this.TurnArray = [];
        this.setState({ Turn: [] });

        fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {

            fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=4&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson1) => {

                for (let i = 0; i < responsejson.split('#').length; i++) {
                    this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], 'name2': responsejson1.split('#')[i].split('@')[0], 'status1': responsejson1.split('#')[i].split('@')[4], });
                }
            });
            this.setState({ Turn: this.TurnArray, });
        });
    }
    contridatafun(id) {
        fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=4&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {
            this.setState({ contri: responsejson.split('#'), })
            console.log(this.state.contri);

            setTimeout(() => { this.setState({ refreshing: false, loader: false }) }, 4000);

        })
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        AsyncStorage.removeItem('indicesname');
        AsyncStorage.removeItem('indicesId')
        this.props.navigation.goBack(null);
        return true;

    }
    back() {
        AsyncStorage.removeItem('indicesname');
        AsyncStorage.removeItem('indicesId')

        this.props.navigation.goBack(null);
        return true;
    }
    graph() {
        alert('under development');
    }

    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }

    more() {
        this.setState({ isVisible: true });
    }
    toggleSwitch(val) {
        if (val == true) {
            this.setState({ value: 1, isVisible: false, themename: 'Dark', switchval: val });
            AsyncStorage.setItem("themecolor", "dark");
        }
        else {
            this.setState({ value: 0, isVisible: false, themename: 'Light', switchval: val });
            AsyncStorage.setItem("themecolor", "light");
        }

    }
    closePopover() {
        this.setState({ isVisible: false });
    }
    value(val) {
        var no = '' + val;
        if (no.length == 5) {
            return no.substring(0, 2) + ',' + no.substring(2);
        }
        else if (no.length == 4) {
            return no.substring(0, 1) + ',' + no.substring(1);
        }
        else if (no.length == 3) {
            return no;
        }
        else if (no.length == 6) {
            return no.substring(0, 1) + ',' + no.substring(1, 3) + ',' + no.substring(3);
        }
        else if (no.length == 7) {
            return no.substring(0, 2) + ',' + no.substring(2, 4) + ',' + no.substring(4);
        }
    }
    chng(per1, per2, per3) {

        if (per1 > 0) {
            return '+' + per1 + ' ' + '+' + per2 + ' %';
        }
        else if (per1 < 0) {
            return per1 + ' ' + per2 + ' %';
        }
        else if (per3 > 0) {
            return '+' + per3;
        }
        else if (per3 < 0) {
            return per3;
        }
        else if (per3 == 0.00) {
            return per3;
        }

    }
    closesidemenu() {
        this.setState({ sidemenu: false });
    }
    equity() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('gainer', { 'theme': dt });
        });

    }
    indices() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('indices', { 'theme': dt });
        });
    }
    sensex() {
        this.setState({ sidemenu: false });
    }
    home() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('maindark', { 'theme': dt });
        });

    }
    derivative() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('derivative', { 'theme': dt });
        });

    }
    currency() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('currency', { 'theme': dt });
        });

    }
    commodity() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('commodity', { 'theme': dt });
        });

    }

    refresh() {
        this.TurnArray = [];
        this.setState({ Turn: [] });
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
                "Application requires Network to proceed",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50
            );
        });
        this.sensexfun();

        AsyncStorage.getItem('indicesname').then((dt) => {
            AsyncStorage.getItem('indicesid').then((dtid) => {
                if (dt == 'S&P BSE SENSEX' || dt == null) {
                    this.securitydatafun();
                    this.overviewdatafun(this.indicesId);
                    this.turndatafun(this.indicesId);
                    this.contridatafun(this.indicesId);
                }
                else {
                    this.setState({ indicesName: dt });
                    fetch('https://api.bseindia.com/msource/Indicesscriplist.aspx?flag=AP&scripcd=' + dtid + '&ln=en').then((response) => response.text()).then((responsejson) => {
                        this.setState({ security: responsejson.split('@'), });
                    })
                    this.overviewdatafun(dtid);
                    this.turndatafun(dtid);
                    this.contridatafun(dtid);

                }

            });
        });



    }
    onRefresh() {
        this.setState({ refreshing: true });
        this.refresh();
    }
    setting() {
        this.setState({ settingshow: true });
        this.setState({ sidemenu: false });
        // this.props.navigation.navigate('setting');
    }

    editicon(val) {
        val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
        // this.setState({showedit:!val});
    }

    changeTheme(thm) {
        this.setState({ theme: thm, settingshow: false, sidemenu: false });
        AsyncStorage.setItem("themecolor", thm);
    }
    chnglanguage(lan) {
        this.setState({ settingshow: false, sidemenu: false });

        AsyncStorage.setItem('language', lan);
    }
    googleassistant() {
        Linking.openURL('https://assistant.google.com/explore');

    }
    selectedValue(name, id) {
        this.TurnArray = [];
        this.setState({ loader: true, Turn: [], indicesDropdown: false });

        AsyncStorage.setItem('indicesname', name);
        AsyncStorage.setItem('indicesid', id);
        this.setState({ indicesName: name });
        this.overviewdatafun(id);
        this.turndatafun(id);
        fetch('https://api.bseindia.com/msource/Index.aspx?code=' + id).then((response) => response.text()).then((responsejson) => {
            this.setState({ ltp: responsejson.split('#')[0].split(',')[0], prvClose: responsejson.split('#')[0].split(',')[1] });
            this.per = this.state.ltp - this.state.prvClose;
            this.per1 = this.per.toFixed(2);
            this.per2 = (this.per / this.state.prvClose) * 100;
            this.per3 = this.per2.toFixed(2);
            this.setState({ chanage: this.per1, changePer: this.per3 });


        })
        fetch('https://api.bseindia.com/msource/Indicesscriplist.aspx?flag=AP&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {
            this.setState({ security: responsejson.split('@') });
        })
        this.contridatafun(id);

    }




    render() {

        return (
            <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
                <StatusBar backgroundColor={this.state.value == '0' ? '#f6f7f9' : '#0b0b0b'} />
                <ViewPager style={{ height: height }} >

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.maintitle : styles.maintitledark}>Security</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '10%' }}>
                            <Col style={{ width: '37%', height: 40, }}>

                                <Text numberOfLines={1} onPress={() => this.setState({ indicesDropdown: true })} style={this.state.value == '0' ? styles.lightselectedText : styles.darkselectedText}>{this.state.indicesName}</Text>
                            </Col>
                            <Col style={{ width: '10%', height: 40, }} >
                                <TouchableOpacity activeOpacity={.5} onPress={() => this.setState({ indicesDropdown: true })} ><Icon name="ios-arrow-dropdown" style={styles.dropicon} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity>

                            </Col>
                            <Col style={{ width: '53%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


                        </Row>

                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '18%' }}>
                            <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.ltplight : styles.ltpdark}>{this.state.ltp} <Text style={this.state.chanage > 0 ? styles.possensex : styles.negsensex}> {this.chng(this.state.chanage, this.state.changePer, '')}</Text></Text></Col>
                            <Col style={{ width: '30%', height: 40, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.graph()} ><Iconoct name="graph" style={{ textAlign: 'right', marginRight: 5 }} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity></Col>

                        </Row>

                        <Row style={{ marginLeft: 16, marginTop: '12%' }}>
                            <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Trd Qty</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Chnage(%)</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Buy Price(Qty)</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Sell Price(Qty)</Text></Col>
                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.security}
                            style={{ marginTop: 55 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                        <Col style={{ width: '100%', height: 58 }}>
                                            <Row style={item.split(',')[0] == '' ? styles.hide : styles.show}>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.split(',')[3] > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.split(',')[1]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{this.value(item.split(',')[9])}</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><View style={item.split(',')[3] > 0 ? styles.show : styles.hide}><Text style={item.split(',')[3] > 0 ? styles.lightltp : styles.darkltp} numberOfLines={1}>{item.split(',')[2]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>+{item.split(',')[3]} +{item.split(',')[4]}%</Text></View><View style={item.split(',')[3] < 0 ? styles.show : styles.hide}><Text style={item.split(',')[3] > 0 ? styles.lightltp : styles.darkltp} numberOfLines={1}>{item.split(',')[2]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>{item.split(',')[3]} {item.split(',')[4]}%</Text></View></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split(',')[5]} {'(' + item.split(',')[6] + ')'}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.split(',')[7]} {'(' + item.split(',')[8] + ')'}</Text></Col>

                                            </Row>
                                        </Col>
                                        <Col style={{ width: '6.5%', height: 58 }}></Col>
                                        {/* <Col style={{ width: '100%', height: 58, }}>
                                            <Row >
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.split(',')[3] > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.split(',')[1]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{item.split(',')[10]}</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={item.status == 'pos' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.ltp}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.value} %</Text></Col>
                                                <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.split(',')[5]} {'(' + item.split(',')[6] + ')'}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensex1 : styles.darksensex1}>{item.split(',')[7]} {'(' + item.split(',')[8] + ')'}</Text></Col>

                                            </Row>
                                        </Col> */}
                                    </Row>
                                    <Text></Text>

                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.maintitle : styles.maintitledark}>Overview</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>
                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '10%' }}>
                            <Col style={{ width: '37%', height: 40, }}>

                                <Text numberOfLines={1} onPress={() => this.setState({ indicesDropdown: true })} style={this.state.value == '0' ? styles.lightselectedText : styles.darkselectedText}>{this.state.indicesName}</Text>
                            </Col>
                            <Col style={{ width: '10%', height: 40, }} >
                                <TouchableOpacity activeOpacity={.5} onPress={() => this.setState({ indicesDropdown: true })} ><Icon name="ios-arrow-dropdown" style={styles.dropicon} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity>

                            </Col>
                            <Col style={{ width: '53%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


                        </Row>

                        <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '18%' }}>
                            <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.ltplight : styles.ltpdark}>{this.state.ltp} <Text style={this.state.chanage > 0 ? styles.possensex : styles.negsensex}> {this.chng(this.state.chanage, this.state.changePer, '')}</Text></Text></Col>
                            <Col style={{ width: '30%', height: 40, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.graph()} ><Iconoct name="graph" style={{ textAlign: 'right', marginRight: 5 }} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity></Col>

                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>

                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.overview}
                            style={{ marginTop: 50 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <Row style={{ marginLeft: 17, marginRight: 17 }}>
                                        <Col style={item.no == '1' && this.state.value == '0' ? styles.firstcol : item.no == '1' && this.state.value == '1' ? styles.firstcoldark : styles.hide}>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>Prv Close  : {item.prv} <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>           Open  : {item.open}</Text> </Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>High          : {item.high} <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>           Low     : {item.low}</Text></Text>
                                        </Col>
                                        <Col style={item.no == '2' && this.state.value == '0' ? styles.secondcol : item.no == '2' && this.state.value == '1' ? styles.secondcoldark : styles.hide}>
                                            <Text style={this.state.value == '0' ? styles.titlelight : styles.titledark} >Market Capitalisation</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} >Full(Cr)             : {item.full}</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} >Free Float(Cr) : {item.free}</Text>
                                        </Col>
                                        <Col style={item.no == '3' && this.state.value == '0' ? styles.thirdcol : item.no == '3' && this.state.value == '1' ? styles.thirdcoldark : styles.hide}>
                                            <Text style={this.state.value == '0' ? styles.titlelight : styles.titledark} >Ups & Downs</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>                                        <Text>Scrips</Text>                      <Text>T/O Cr</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>Advances                       <Text>{item.advance}</Text><Text>                           {item.advanceTO}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>Declines                         <Text>{item.decline}</Text><Text>                           {item.declineTO}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>Unchanged                   <Text>{item.unchanged}</Text><Text>                              {item.unchangedTO}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>Not Traded                   <Text>{item.nottraded}</Text><Text>                              {item.nottradedTO}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark}>Total                               <Text>{item.total}</Text><Text>                           {item.totalTO}</Text></Text>
                                        </Col>
                                        <Col style={item.no == '4' && this.state.value == '0' ? styles.fourthcol : item.no == '4' && this.state.value == '1' ? styles.fourthcoldark : styles.hide}>
                                            <Text style={this.state.value == '0' ? styles.titlelight : styles.titledark} >High Lows</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>52 Wk  : {item.high} <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>           52 Wk  : {item.low}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>High    : {item.highdate} <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>           Low     : {item.lowdata}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>All         : {item.alltimehigh} <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>            All : {item.alltimelow}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>time       {item.alltimehighdate}<Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>           time {item.alltimelowdate}</Text></Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>High        <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} numberOfLines={1}>                              Low</Text></Text>

                                        </Col>
                                        <Col style={item.no == '5' && this.state.value == '0' ? styles.fivecol : item.no == '5' && this.state.value == '1' ? styles.fivecoldark : styles.hide}>
                                            <Text style={this.state.value == '0' ? styles.titlelight : styles.titledark} >Fundamental Data</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} >PE                           : {item.pe}</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} >PB                           : {item.pb}</Text>
                                            <Text style={this.state.value == '0' ? styles.prvlight : styles.prvdark} >Dividend Yield   : {item.divided}</Text>

                                        </Col>

                                    </Row>
                                    <Text></Text>

                                </View>

                            }
                            keyExtractor={item => item.id}
                        />


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.maintitle : styles.maintitledark}>Turnover</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>
                        <Row style={this.state.Turn.length == 1 || this.state.Turn.length == 3 ? styles.oneTurnData : styles.dateheight}>
                            <Col style={{ width: '37%', height: 40, }}>

                                <Text numberOfLines={1} onPress={() => this.setState({ indicesDropdown: true })} style={this.state.value == '0' ? styles.lightselectedText : styles.darkselectedText}>{this.state.indicesName}</Text>
                            </Col>
                            <Col style={{ width: '10%', height: 40, }} >
                                <TouchableOpacity activeOpacity={.5} onPress={() => this.setState({ indicesDropdown: true })} ><Icon name="ios-arrow-dropdown" style={styles.dropicon} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity>

                            </Col>
                            <Col style={{ width: '53%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


                        </Row>

                        <Row style={this.state.Turn.length == 1 || this.state.Turn.length == 3 ? styles.ltpTurnNoData : styles.ltpData}>
                            <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.ltplight : styles.ltpdark}>{this.state.ltp} <Text style={this.state.chanage > 0 ? styles.possensex : styles.negsensex}> {this.chng(this.state.chanage, this.state.changePer, '')}</Text></Text></Col>
                            <Col style={{ width: '30%', height: 40, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.graph()} ><Iconoct name="graph" style={{ textAlign: 'right', marginRight: 5 }} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity></Col>

                        </Row>
                        <Row style={this.state.Turn == '' ? styles.hide : styles.turnHeader}>
                            <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Traded Qty</Text></Col>
                            <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>T/O( <IconFonsito name="rupee" style={{}} color={this.state.value == '0' ? 'black' : '#72a3bf'} size={13}></IconFonsito> Lacs )</Text></Col>
                        </Row>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>
                        <View style={this.state.Turn != '' ? styles.show : styles.hide}>
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh.bind(this)}
                                />
                            }
                            data={this.state.Turn}
                            style={{marginTop:55}}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View>
                                    <View style={item.id != 0 ? styles.show : styles.hide}>
                                        <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                            <Col style={{ width: '90%', height: 58 }}>
                                                <Row>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.status > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.name1}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.trd}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.to}</Text></Col>

                                                </Row>
                                            </Col>
                                            <Col style={{ width: '6.5%', height: 58 }}></Col>
                                            <Col style={{ width: '100%', height: 58, }}>
                                                <Row>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.status1 > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.name2}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={item.status1 > 0 ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.trd}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.to}</Text></Col>

                                                </Row>
                                            </Col>
                                        </Row>
                                        <Text></Text>

                                    </View>
                                    <View style={item.name1 == 'Total' ? styles.show : styles.hide}>
                                        <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                            <Col style={{ width: '90%', height: 58 }}>
                                                <Row>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={this.state.value == '0' ? styles.totallight : styles.totaldark} numberOfLines={1}>{item.name1}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.trd}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.to}</Text></Col>

                                                </Row>
                                            </Col>
                                            <Col style={{ width: '6.5%', height: 58 }}></Col>
                                            <Col style={{ width: '100%', height: 58, }}>
                                                <Row>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.status1 > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.name2}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={item.status1 > 0 ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.trd}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={this.state.value == '0' ? styles.lightsensex : styles.darksensex} numberOfLines={1}>{item.to}</Text></Col>

                                                </Row>
                                            </Col>
                                        </Row>
                                        <Text></Text>

                                    </View>
                                </View>

                            }
                            keyExtractor={item => item.id}
                        />
                        </View>

                        <View style={this.state.Turn == '' ? styles.showNoData : styles.hide}>
                            <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                        </View>


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                    <View>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.maintitle : styles.maintitledark}>Contribution</Text></Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                        </Row>
                        <Row style={this.state.contri == '' ? styles.nodataDate : styles.dateheight}>
                            <Col style={{ width: '37%', height: 40, }}>

                                <Text numberOfLines={1} onPress={() => this.setState({ indicesDropdown: true })} style={this.state.value == '0' ? styles.lightselectedText : styles.darkselectedText}>{this.state.indicesName}</Text>
                            </Col>
                            <Col style={{ width: '10%', height: 40, }} >
                                <TouchableOpacity activeOpacity={.5} onPress={() => this.setState({ indicesDropdown: true })} ><Icon name="ios-arrow-dropdown" style={styles.dropicon} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity>

                            </Col>
                            <Col style={{ width: '53%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


                        </Row>

                        <Row style={this.state.contri == '' ? styles.ltpnoData : styles.ltpData}>
                            <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.ltplight : styles.ltpdark}>{this.state.ltp} <Text style={this.state.chanage > 0 ? styles.possensex : styles.negsensex}> {this.chng(this.state.chanage, this.state.changePer, '')}</Text></Text></Col>
                            <Col style={{ width: '30%', height: 40, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.graph()} ><Iconoct name="graph" style={{ textAlign: 'right', marginRight: 5 }} size={22} color={this.state.value == '0' ? "#132144" : "#72a3bf"} /></TouchableOpacity></Col>

                        </Row>
                        <View style={this.state.contri == '' ? styles.hide : styles.show}>
                            <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                                <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text></Col>
                                <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP( <IconFonsito name="rupee" style={{}} color={this.state.value == '0' ? 'black' : '#72a3bf'} size={13}></IconFonsito>)</Text></Col>
                                <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Contribution</Text></Col>
                            </Row>
                        </View>
                        <View style={this.state.loader == true ? styles.show : styles.hide}>
                            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                        </View>
                        <View style={this.state.contri != '' ? styles.show : styles.hide}>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.contri}
                                style={{ marginTop: 55 }}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <View style={item.split('@')[0] == '' ? styles.hide : styles.show}>
                                        <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                            <Col style={item.split('@')[4] == 0.00 || item.split('@')[5] == 0.00 ? styles.hide : styles.contricol}>
                                                <Row>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.split('@')[4] > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.split('@')[0]}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={item.split('@')[4] > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{item.split('@')[3]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>{this.chng(item.split('@')[4], item.split('@')[5], '')}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={item.split('@')[4] > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{this.chng('', '', item.split('@')[6])}</Text></Col>

                                                </Row>
                                            </Col>
                                            <Col style={item.split('@')[4] == 0.00 || item.split('@')[5] == 0.00 ? styles.contricol : styles.hide}>
                                                <Row>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.split('@')[4] > 0 ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.split('@')[0]}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}><Text style={item.split('@')[4] > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{item.split('@')[3]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensexvalue : styles.darksensexvalue}>{item.split('@')[4]}  {item.split('@')[5]}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={item.split('@')[4] > 0 ? styles.lightltpcontri : styles.darkltpcontri} numberOfLines={1}>{item.split('@')[6]}</Text></Col>

                                                </Row>
                                            </Col>
                                            <Col style={{ width: '6.5%', height: 58 }}></Col>

                                        </Row>
                                        <Text></Text>

                                    </View>

                                }
                                keyExtractor={item => item.id}

                            />
                        </View>
                        <View style={this.state.contri == '' ? styles.showNoData : styles.hide}>
                            <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                        </View>


                        <Footer>

                            <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                                <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                    <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                                </Button>


                                <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                    <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                                </Button>

                                <Button style={styles.active} onPress={() => this.tab('home')}>
                                    <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                    <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                    <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                                </Button>
                                <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                    <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                    <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                                </Button>


                            </FooterTab>
                        </Footer>
                    </View>

                </ViewPager>


                <Modal
                    animationType="slide"
                    transparent={false}
                    style={{ height: '100%' }}
                    visible={this.state.sidemenu}
                    onRequestClose={() => {
                        this.setState({ sidemenu: false });
                    }}
                >
                    <View style={this.state.value == '0' ? styles.lightmenu : styles.darkmenu}>
                        <Row style={{ marginLeft: 20, marginTop: 10 }}>
                            <Col style={{ width: '50%', height: 40 }}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={{ marginLeft: 10 }} size={40} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>

                        </Row>

                        <ScrollView style={{ top: '8%' }}>
                            <Image style={{ width: 150, height: 150, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/lightprofile.png') : require('../images/darkprofile.png')} />
                            {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
                            <Text></Text>
                            <View style={{}}>
                                <Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Home</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.equity()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Equity</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GAINERS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>LOSERS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TOP TURNOVER</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK HIGH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK LOW</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Indices</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.setState({ sidemenu: false })} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Sensex</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>SECURITY</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>OVERVIEW</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TURNOVER</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CONTRIBUTION</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >SME</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET STATISTICS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.derivative()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Derivatives</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.currency()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Currency</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.commodity()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Commadity</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IRD</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>ETF</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ETF WATCH</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Debt</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>DEBT MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BONDS-OTC TRADES</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL CORP. DEBT</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GSEC</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL GOV BONDS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BOND-NDS-RST</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>EBP</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Corporates</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ANNOUNCEMENTS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ACTIONS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RESULTS CALENDAR</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>BOARD MEETINGS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Statistics</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Turnover</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPO/OFS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Listings</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Notices</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Watchlist</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Portfolio</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPF</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', }}></Text>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>

                <Popover
                    isVisible={this.state.indicesDropdown}
                    fromView={this.touchable}
                    onRequestClose={() => this.setState({ indicesDropdown: false })} >
                    <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
                        <FlatList
                            data={this.state.indicesdata}
                            // style={{ marginTop: 55 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View style={this.state.value == '0' ? styles.lightdropdown : styles.darkdropdown}>
                                    <Text onPress={() => this.selectedValue(item.name, item.id)} style={this.state.value == '0' ? styles.lightdroptext : styles.darkdroptext}>{item.name}</Text>
                                </View>
                            }

                        />
                        <Text></Text>
                        <Text></Text>


                    </View>
                </Popover>

            </View>
        );
    }
}

const styles = StyleSheet.create({

    mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
    mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
    inactive: { padding: 12, },
    titlelight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    titledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    pos: { backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 5, color: 'white' },
    neg: { backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 5, color: 'white' },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamedark: { color: '#79868e', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
    lightdate: { color: '#132144', textAlign: 'right', marginTop: 14, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    darkdate: { color: '#72a3bf', textAlign: 'right', marginTop: 14, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    statuslight: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
    statusdark: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
    listlightcol1: { width: '65%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcol2: { width: '35%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcol1: { width: '65%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcol2: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex1: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcolsensex2: { width: '32%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex3: { width: '33%', height: 58, backgroundColor: '#1a1f1f', },
    listlightcolsensex1: { width: '35%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcolsensex2: { width: '32%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listlightcolsensex3: { width: '33%', height: 58, backgroundColor: 'white', },
    show: { display: 'flex' },
    hide: { display: 'none' },
    showNoData: { display: 'flex', bottom: '50%' },
    possensex: { marginLeft: 15, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    lighttext1: { marginLeft: 15, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex1: { alignSelf: 'flex-end', marginRight: 7, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex: { alignSelf: 'center', marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex1: { alignSelf: 'center', color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    negsensex: { marginLeft: 15, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightltp: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darkltp: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    darkltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    darktext1: { marginLeft: 15, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    perlight: { alignSelf: 'flex-end', marginRight: 8, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perdark: { alignSelf: 'flex-end', marginRight: 8, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perturnlight: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    perturndark: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    ltpviewpos: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    ltpviewneg: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    lightheader1: { width: '32%', height: 40 },
    darkheader1: { width: '32%', height: 40 },
    lightheader2: { width: '30%', height: 40 },
    darkheader2: { width: '30%', height: 40 },
    lightheader3: { width: '33%', height: 40 },
    darkheader3: { width: '33%', height: 40 },
    lightheader4: { width: '57%', height: 40 },
    darkheader4: { width: '57%', height: 40 },
    lightPicker: { color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkPicker: { color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightheadertext: { marginLeft: 15, fontSize: 15, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext: { marginLeft: 15, fontSize: 15, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext1: { alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lighttext: { marginLeft: 15, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darktext: { marginLeft: 15, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    firstcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 65, },
    firstcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 65, },
    secondcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 95, },
    secondcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 95, },
    thirdcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 210, },
    thirdcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 210, },
    fourthcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 182, },
    fourthcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 182, },
    fivecol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 125, },
    fivecoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 125, },
    sixcol: { borderRadius: 4, width: '100%', height: 60, },
    sixcoldark: { borderRadius: 4, width: '100%', height: 60, },
    prvlight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    prvdark: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    titlelight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoePro-Bold', },
    titledark: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    maintitle: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    maintitledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    dropicon: { textAlign: 'center', marginTop: 10 },
    ltplight: { marginLeft: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    ltpdark: { marginLeft: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 30, fontSize: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkmenutext: { marginLeft: 30, fontSize: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: '#72a3bf', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    contricol: { width: '100%', height: 58 },
    lightdropdown: { margin: 10, backgroundColor: '#f1f2f6' },
    darkdropdown: { margin: 10, backgroundColor: '#0b0b0b' },
    lightdroptext: { color: '#132144' },
    darkdroptext: { color: '#72a3bf' },
    lightselectedText: { color: '#132144', marginTop: 13, marginLeft: 5 },
    darkselectedText: { color: '#72a3bf', marginTop: 13, marginLeft: 5 },
    totallight: { marginLeft: 15, marginTop: 9, color: '#132144', fontFamily: 'SegoePro-Bold', },
    totaldark: { marginLeft: 15, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    dateheight: { marginLeft: 16, marginRight: 16, marginTop: '10%' },
    nodataDate: { marginLeft: 16, marginRight: 16, bottom: 130 },
    ltpData: { marginLeft: 16, marginRight: 16, marginTop: '18%' },
    ltpnoData: { marginLeft: 16, marginRight: 16, bottom: '70%' },
    nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    nodatadark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    oneTurnData: { marginLeft: 16, marginRight: 16, bottom: 53 },
    ltpTurnNoData: { marginLeft: 16, marginRight: 16, bottom: 90 },
    turnHeader:{marginLeft: 16, marginRight: 16,marginTop:50},
    turnData:{bottom:10},
    Singleturndata:{bottom:'300%'}
})

export default Sensex;
