import React, { Component } from 'react';

import { View, Image, Modal, Linking, ToastAndroid, CheckBox, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view';
import IconEvil from 'react-native-vector-icons/EvilIcons';

class Indices extends Component {
    static navigationOptions = {

        header: null,
    };
    Data = [];
    themename = '';

    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            date: '',
            status: '',
            value: 1,
            isVisible: false,
            switchval: false,
            indicesdata: [],
            loader: false,
            refreshing: false,
            sidemenu: false,
            settingshow: false,
            theme: '',
            showedit: false,
            iconname: 'ios-arrow-down',
            selectlan: 'English',
            nodata:false

        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setTimeout(() => { SplashScreen.hide() }, 3000);
        // setInterval(() => { this.main() }, 500);

    }
    main() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            dt == null || dt == 'dark' ? this.setState({ value: '1', switchval: true, theme: 'Dark' }) : this.setState({ value: '0', switchval: false, theme: 'Light' });

        });

    }
    componentDidMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        });
        this.themename = this.props.navigation.getParam('theme', '');
        if (this.themename == null || this.themename == 'dark') {
            this.setState({ value: '1' });
        }
        else {
            this.setState({ value: '0' });
        }
        this.sensexdatefun();
        this.indicesdatafun();
    }
    sensexdatefun() {
        this.setState({ loader: true });
        fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

            try {
                responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';

            } catch (error) {
                console.log(error);
            }
        })
    }
    indicesdatafun() {
        try {
            fetch('https://api.bseindia.com/msource/indexmoversAndroid.aspx?ln=en').then((response) => response.text()).then((responsejson) => {

                this.setState({ indicesdata: responsejson.split('#') });
                setTimeout(() => { this.setState({ refreshing: false, loader: false }) }, 4000);

            })
        } catch (error) {
            console.log(error);
            this.setState({nodata:true,loader: false});
        }

    }

    refresh() {
        NetInfo.isConnected.fetch().then(isConnected => {
            isConnected ? '' : ToastAndroid.showWithGravityAndOffset(
                "Application requires Network to proceed",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50
            );
        });
        this.sensexdatefun();
        this.indicesdatafun();
    }

    onRefresh() {
        this.setState({ refreshing: true });
        this.refresh();
    }

    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }

    more() {
        this.setState({ isVisible: true });
    }
    toggleSwitch(val) {
        if (val == true) {
            this.setState({ value: 1, isVisible: false, themename: 'Dark', switchval: val });
            AsyncStorage.setItem("themecolor", "dark");
        }
        else {
            this.setState({ value: 0, isVisible: false, themename: 'Light', switchval: val });
            AsyncStorage.setItem("themecolor", "light");
        }

    }
    closePopover() {
        this.setState({ isVisible: false });
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {

        this.props.navigation.goBack(null);
        return true;
    }
    back() {
        this.props.navigation.goBack(null);
        return true;
    }

    closesidemenu() {
        this.setState({ sidemenu: false });
    }
    // equity() {
    //     AsyncStorage.getItem('themecolor').then((dt) => {
    //         this.setState({ sidemenu: false });
    //         this.props.navigation.navigate('gainer', { 'theme': dt });
    //     });

    // }
    indices() {
        // this.setState({ sidemenu: false });
    }
    sensex() {

        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('setting', { 'theme': dt, 'current': 0 });

        });

    }
    home() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('maindark', { 'theme': dt });
        });

    }
    equitypage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('equity', { 'theme': dt, 'current': val });

        });
    }
    derivativepage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('derivativetab', { 'theme': dt, 'current': val });

        });
    }
    currencypage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('currencytab', { 'theme': dt, 'current': val });

        });
    }
    settingpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('setting', { 'theme': dt, 'current': val });

        });
    }
    commoditypage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('commoditytab', { 'theme': dt, 'current': val });

        });
    }
    irdpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('ird', { 'theme': dt, 'current': val });

        });
    }
    smepage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('sme', { 'theme': dt, 'current': val });

        });
    }
    etfpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('etf', { 'theme': dt, 'current': val });

        });
    }
    debtpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('debt', { 'theme': dt, 'current': val });

        });
    }

    setting() {
        this.setState({ settingshow: true });
        this.setState({ sidemenu: false });
        // this.props.navigation.navigate('setting');
    }

    editicon(val) {
        val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
        // this.setState({showedit:!val});
    }

    changeTheme(thm) {
        this.setState({ theme: thm, settingshow: false, sidemenu: false });
        AsyncStorage.setItem("themecolor", thm);
    }
    chnglanguage(lan) {
        this.setState({ settingshow: false, sidemenu: false });

        AsyncStorage.setItem('language', lan);
    }
    googleassistant() {
        Linking.openURL('https://assistant.google.com/explore');

    }
    detail(nameindices, id) {
        AsyncStorage.getItem('themecolor').then((dt) => {

            this.props.navigation.navigate('setting', { 'current': 0, 'theme': dt, 'name': nameindices, 'id': id });
        });
    }
    chng(per1, per2) {
        if (per1 > 0) {
            return '+' + per1 + ' ' + '+' + per2 + '%';
        }
        else if (per1 < 0) {
            return per1 + ' ' + per2 + '%';
        }

    }

    render() {

        return (
            <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}

            >
                <StatusBar backgroundColor={this.state.value == '0' ? '#f6f7f9' : '#0b0b0b'} />

                <Row style={{ margin: 5 }}>
                    <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                    <Col style={{ width: '65%', height: 30 }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>S&P BSE INDICES</Text></Col>
                    <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                    {/* <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.more()}><Icon name="md-more" style={{ textAlign: 'center', }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col> */}
                </Row>

                <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                    <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

                </Row>
                <Row style={{ marginLeft: 16, marginRight: 16, marginTop: '12%' }}>
                    <Col style={this.state.value == '0' ? styles.lightheader4 : styles.darkheader4}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Indices</Text></Col>
                    <Col style={this.state.value == '0' ? styles.lightheader4 : styles.darkheader4}></Col>
                </Row>
                <View style={this.state.loader == true ? styles.show : styles.hide}>
                    <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                </View>
                <View style={this.state.nodata == true ? styles.showNoData : styles.hide}>
                  <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                </View>

                <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                    data={this.state.indicesdata}
                    style={{ marginTop: 39 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                        <View style={item.split(',')[0] == '' ? styles.hide : styles.show}>
                            <Row style={{ marginLeft: 17, marginRight: 17 }}>
                                <Col style={{ width: '100%', height: 58, }}>
                                    <Row onPress={() => this.detail(item.split(',')[0], item.split(',')[5])}>
                                        <Col style={this.state.value == '0' ? styles.listlightcol1 : styles.listdarkcol1}><Text numberOfLines={1} style={item.split(',')[2] > 0 ? styles.namepos : styles.nameneg} >{item.split(',')[0]}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}></Text></Col>
                                        <Col style={this.state.value == '0' ? styles.listlightcol2 : styles.listdarkcol2}><Text numberOfLines={1} style={item.split(',')[2] > 0 ? styles.valuepos : styles.valueneg} >{item.split(',')[1]} </Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.perturnlight : styles.perturndark}>{this.chng(item.split(',')[2], item.split(',')[3])}</Text></Col>

                                    </Row>
                                </Col>

                            </Row>
                            <Text></Text>
                        </View>

                    }
                    keyExtractor={item => item.id}
                />

                <Modal
                    animationType="slide"
                    transparent={false}
                    style={{ height: '100%' }}
                    visible={this.state.sidemenu}
                    onRequestClose={() => {
                        this.setState({ sidemenu: false });
                    }}
                >
                    <View style={this.state.value == '0' ? styles.lightmenu : styles.darkmenu}>
                        <Row style={{ marginLeft: 20, marginTop: 10 }}>
                            <Col style={{ width: '50%', height: 40 }}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={{ marginLeft: 10 }} size={40} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>

                        </Row>

                        <ScrollView style={{ top: '8%', }}>
                            <Image style={{ width: 150, height: 150, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/lightprofile.png') : require('../images/darkprofile.png')} />
                            {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
                            <Text></Text>
                            <View style={{}}>
                                <Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Home</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.equity()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Equity</Text>
                                <Text onPress={() => this.equitypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GAINERS</Text>
                                <Text onPress={() => this.equitypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>LOSERS</Text>
                                <Text onPress={() => this.equitypage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TOP TURNOVER</Text>
                                <Text onPress={() => this.equitypage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK HIGH</Text>
                                <Text onPress={() => this.equitypage(4)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK LOW</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text onPress={() => this.setState({ sidemenu: false })} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Indices</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Sensex</Text>
                                <Text onPress={() => this.settingpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>SECURITY</Text>
                                <Text onPress={() => this.settingpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>OVERVIEW</Text>
                                <Text onPress={() => this.settingpage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TURNOVER</Text>
                                <Text onPress={() => this.settingpage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CONTRIBUTION</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >SME</Text>
                                <Text onPress={() => this.smepage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text onPress={() => this.smepage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET STATISTICS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Derivatives</Text>
                                <Text onPress={() => this.derivativepage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text onPress={() => this.derivativepage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Currency</Text>
                                <Text onPress={() => this.currencypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text onPress={() => this.currencypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Commadity</Text>
                                <Text onPress={() => this.commoditypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text onPress={() => this.commoditypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IRD</Text>
                                <Text onPress={() => this.irdpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                <Text onPress={() => this.irdpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>ETF</Text>
                                <Text onPress={() => this.etfpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ETF WATCH</Text>
                                <Text onPress={() => this.etfpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Debt</Text>
                                <Text onPress={() => this.debtpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>DEBT MARKET SUMMARY</Text>
                                <Text onPress={() => this.debtpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BONDS-OTC TRADES</Text>
                                <Text onPress={() => this.debtpage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL CORP. DEBT</Text>
                                <Text onPress={() => this.debtpage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GSEC</Text>
                                <Text onPress={() => this.debtpage(4)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL GOV BONDS</Text>
                                <Text onPress={() => this.debtpage(5)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>EBP</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Corporates</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ANNOUNCEMENTS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ACTIONS</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RESULTS CALENDAR</Text>
                                <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>BOARD MEETINGS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Statistics</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Turnover</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPO/OFS</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Listings</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Notices</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Watchlist</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Portfolio</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPF</Text>
                                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', }}></Text>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>


                <Footer>

                    <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                        <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                            <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                            <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                        </Button>


                        <Button style={styles.inactive} onPress={() => this.tab('port')}>
                            <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                            <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                        </Button>

                        <Button style={styles.active} onPress={() => this.tab('home')}>
                            <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                            <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                        </Button>
                        <Button style={styles.inactive} onPress={() => this.tab('search')}>
                            <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                            <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                        </Button>
                        <Button style={styles.inactive} onPress={() => this.tab('more')}>
                            <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                            <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                        </Button>


                    </FooterTab>
                </Footer>
                <Popover
                    isVisible={this.state.isVisible}
                    fromView={this.touchable}
                    arrowStyle={true}
                    showArrow={true}
                    onRequestClose={() => this.closePopover()}>
                    <View style={{ backgroundColor: 'white', height: '100%' }}>

                        <Text></Text>
                        <Row>
                            <Col style={{ width: '20%', height: 50, }}><IconEnt name="setting" style={{ textAlign: 'center' }} color={'black'} size={23}></IconEnt></Col>
                            <Col style={{ width: '80%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 20, bottom: 3, color: 'black', }}>{I18n.t('setting')}</Text></Col>
                        </Row>
                        <Row>
                            <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
                            <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' }}> {this.state.themename} {I18n.t('theme')} </Text></Col>
                            <Col style={{ width: '40%', height: 50, }}>
                                <Switch
                                    trackColor="black"
                                    trackColor="grey"
                                    value={this.state.switchval}
                                    style={{ bottom: 3, alignSelf: 'center' }}
                                    onValueChange={(e) => this.toggleSwitch(e)}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
                            <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' }}> {I18n.t('edithome')} </Text></Col>
                            <Col style={{ width: '40%', height: 50, }}><Icon name="ios-arrow-dropright-circle" onPress={() => this.edit()} style={{ textAlign: 'center' }} color={'black'} size={25} /></Col>
                        </Row>
                        <Row>
                            <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
                            <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' }}> {I18n.t('changelanguage')}</Text></Col>
                            <Col style={{ width: '5%', height: 50, }}></Col>
                            <Col style={{ width: '35%', height: 50 }}>
                                <Picker
                                    selectedValue={this.state.selectlan}
                                    style={{ height: 30, bottom: 5, width: '100%', }}
                                // onValueChange={(itemValue) => this.chnglan(itemValue)}
                                >
                                    <Picker.Item label="English" value="English" />
                                    <Picker.Item label="Hindi" value="Hindi" />
                                    <Picker.Item label="Marathi" value="Marathi" />
                                    <Picker.Item label="Gujrati" value="Gujrati" />
                                    <Picker.Item label="Bengali" value="Bengali" />
                                    <Picker.Item label="Malayalam" value="Malayalam" />
                                    <Picker.Item label="Oriya" value="Oriya" />
                                    <Picker.Item label="Tamil" value="Tamil" />
                                </Picker>
                            </Col>
                        </Row>




                    </View>

                </Popover>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    showNoData: { display: 'flex'},
    nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    nodatadark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
    mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
    lightdate: { color: '#132144', textAlign: 'right', marginTop: 10, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    darkdate: { color: '#72a3bf', textAlign: 'right', marginTop: 10, fontFamily: 'SegoeProDisplay-Regular', fontSize: 15, marginRight: 5 },
    statuslight: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
    statusdark: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
    lightheadertext: { marginLeft: 15, fontSize: 18, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext: { marginLeft: 15, fontSize: 18, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightheadertext1: { alignSelf: 'center', color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext1: { alignSelf: 'center', color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    show: { display: 'flex' },
    hide: { display: 'none' },
    namepos: { marginLeft: 15, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    nameneg: { marginLeft: 15, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightheader4: { width: '57%', height: 40 },
    darkheader4: { width: '57%', height: 40 },
    listlightcol1: { width: '50%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcol2: { width: '50%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcol1: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcol2: { width: '50%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    ltpviewneg: { width: 60, marginTop: 8, fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    perturnlight: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    perturndark: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    neg: { backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 5, color: 'white' },
    valuepos: { marginRight: 10, marginTop: 9, fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', color: '#72a3bf' },
    valueneg: { marginRight: 10, marginTop: 9, fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', color: '#f54845' },
    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
    inactive: { padding: 12, },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamedark: { color: '#79868e', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
    titlelight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    titledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 30, fontSize: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkmenutext: { marginLeft: 30, fontSize: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: '#72a3bf', borderBottomWidth: 1, marginRight: 25, bottom: 10 },

})

export default Indices;
