import React, { Component } from 'react';
import { View, Image, Modal, CheckBox, Linking, Dimensions, NetInfo, RefreshControl, Switch, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Tab, Tabs, Container, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconEvil from 'react-native-vector-icons/EvilIcons';
import Iconoct from 'react-native-vector-icons/Octicons';

import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import I18n from '../i18';
import Popover from 'react-native-popover-view'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { PagerTabIndicator, ViewPager, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
const height = Dimensions.get('window').height;

class SmeTabs extends Component {
    static navigationOptions = { header: null };
    per = '';
    per1 = '';
    per2 = '';
    per3 = '';
    indicesId = '16';
    watchdata = [];
    summary = [];
    turnlength = '';
    tabval = 0;
    themename = '';
    constructor(props) {
        super(props);
        if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            date: '',
            status: '',
            value: 1,
            loader: false,
            indicesLoader: false,
            isVisible: false,
            switchval: false,
            security: [],
            overview: [],
            Turn: [],
            contri: [],
            refreshing: false,
            sidemenu: false,
            settingshow: false,
            theme: '',
            showedit: false,
            selectlan: 'English',
            indicesdata: [],
            indicesName: 'S&P BSE SENSEX',
            ltp: '',
            change: '',
            changeper: '',
            indicesDropdown: false,
            tabsValue: 0,
            initial: '',
            iconname: 'ios-arrow-forward',
            iconnamesensex: 'ios-arrow-forward',
            iconnamesme: 'ios-arrow-forward',
            iconnamederivative: 'ios-arrow-forward',
            iconnamecurrency: 'ios-arrow-forward',
            iconnamecommodity: 'ios-arrow-forward',
            iconnameird: 'ios-arrow-forward',
            iconnameetf: 'ios-arrow-forward',
            iconnamedebt: 'ios-arrow-forward',
            iconnamecorporate: 'ios-arrow-forward',
            equity: false,
            sensex: false,
            sme: false,
            derivative: false,
            currency: false,
            commodity: false,
            ird: false,
            etf: false,
            debt: false,
            corporate: false,
            Watch: [],
            Summary: [],
            summaryturn: '',
            nowatchdata: false,
            nosummarydata: false

        };
        setTimeout(() => { SplashScreen.hide() }, 4000);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }
    componentDidMount() {

        this.themename = this.props.navigation.getParam('theme', '');
        if (this.themename == null || this.themename == 'dark') {
            this.setState({ value: '1' });
        }
        else {
            this.setState({ value: '0' });
        }
        this.sensexfun();
        this.watchfun()
        this.summaryfun();

    }

    tab(val) {

        if (val == 'watch') { }
        else if (val == 'port') { }
        else if (val == 'home') {
            this.props.navigation.navigate('maindark');

        }
        else if (val == 'search') {
            AsyncStorage.getItem('themecolor').then((dt) => {
                this.setState({ sidemenu: false });
                this.props.navigation.navigate('search', { 'theme': dt });
            });
        }
        else if (val == 'more') {
            this.setState({ sidemenu: true })
        }

    }
    componentWillMount() {
        this.tabval = this.props.navigation.getParam('current', '');

        this.setState({ tabsValue: this.tabval });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;

    }
    back() {
        this.props.navigation.goBack(null);
        return true;
    }
    sensexfun() {
        this.setState({ loader: true });
        fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

            try {
                responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';

            } catch (error) {
                console.log(error);
            }
        })

    }
    watchfun() {
        try {
            fetch('https://api.bseindia.com/msource/SMEMarketWatch.aspx?flag=2&ln=en').then((response) => response.text()).then((responsejson) => {
                for (let i = 0; i < responsejson.split('#').length; i++) {
                    this.watchdata.push({ 'id': responsejson.split('#')[i].split('@')[0], 'trd': responsejson.split('#')[i].split('@')[7], 'ltp': responsejson.split('#')[i].split('@')[2], 'change': responsejson.split('#')[i].split('@')[5], 'percent': responsejson.split('#')[i].split('@')[6], 'name1': responsejson.split('#')[i].split('@')[1], });
                }

                this.setState({ Watch: this.watchdata });
                console.log(this.state.Watch);
            });
        } catch (error) {
            this.setState({ nowatchdata: true });
        }

    }
    summaryfun() {
        try {
            fetch('https://api.bseindia.com/msource/SMEMarketWatch.aspx?flag=3&ln=en').then((response) => response.text()).then((responsejson) => {
                this.setState({ Summary: responsejson.split('#'), loader: false, refreshing: false });

            });
        } catch (error) {
            this.setState({ nosummarydata: true, loader: false });

        }

    }
    value(val) {
        var no = '' + val;
        if (no.length == 5) {
            return no.substring(0, 2) + ',' + no.substring(2);
        }
        else if (no.length == 4) {
            return no.substring(0, 1) + ',' + no.substring(1);
        }
        else if (no.length == 3) {
            return no;
        }
        else if (no.length == 2) {
            return no;
        } else if (no.length == 1) {
            return no;
        }
        else if (no.length == 6) {
            return no.substring(0, 1) + ',' + no.substring(1, 3) + ',' + no.substring(3);
        }
        else if (no.length == 7) {
            return no.substring(0, 1) + ',' + no.substring(1, 4) + ',' + no.substring(4);
        }
    }
    chng(per1, per2) {
        if (per1 > 0 || per1 == '0.00') {
            return '+' + per1 + ' ' + '+' + per2 + ' %';
        }
        else if (per1 < 0) {
            return per1 + '  ' + per2 + ' %';
        }


    }
    fixed(val) {
        var turn = parseFloat(val).toFixed(2);
        return turn;

    }



    onRefresh() {
        this.setState({ refreshing: true });
        this.refresh();
    }
    refresh() {
        this.sensexfun();
        this.watchfun()
        this.summaryfun();

    }

    googleassistant() {
        Linking.openURL('https://assistant.google.com/explore');

    }


    closesidemenu() {
        this.setState({ sidemenu: false });
    }

    indices() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('indices', { 'theme': dt });
        });
    }

    home() {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('maindark', { 'theme': dt });
        });

    }
    equitypage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('equity', { 'theme': dt, 'current': val });

        });
    }
    settingpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('setting', { 'theme': dt, 'current': val });

        });
    }
    derivativepage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('derivativetab', { 'theme': dt, 'current': val });

        });
    }
    irdpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('ird', { 'theme': dt, 'current': val });

        });
    }

    currencypage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('currencytab', { 'theme': dt, 'current': val });

        });
    }
    commoditypage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('commoditytab', { 'theme': dt, 'current': val });

        });
    }


    etfpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('etf', { 'theme': dt, 'current': val });

        });
    }

    debtpage(val) {
        AsyncStorage.getItem('themecolor').then((dt) => {
            this.setState({ sidemenu: false });
            this.props.navigation.navigate('debt', { 'theme': dt, 'current': val });

        });
    }



    equityView() {
        this.state.equity == false ? this.setState({ equity: true, iconname: 'ios-arrow-dropdown' }) : this.setState({ equity: false, iconname: 'ios-arrow-forward' });
    }
    sensexView() {
        this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-dropdown' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
    }
    smeView() {
        this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-dropdown' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
    }
    derivativeView() {
        this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-dropdown' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
    }
    currencyView() {
        this.state.currency == false ? this.setState({ currency: true, iconnamecurrency: 'ios-arrow-dropdown' }) : this.setState({ currency: false, iconnamecurrency: 'ios-arrow-forward' });
    }
    commadityView() {
        this.state.commodity == false ? this.setState({ commodity: true, iconnamecommodity: 'ios-arrow-dropdown' }) : this.setState({ commodity: false, iconnamecommodity: 'ios-arrow-forward' });
    }
    irdView() {
        this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-dropdown' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
    }
    etfView() {
        this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-dropdown' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
    }
    debtView() {
        this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-dropdown' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
    }
    corporateView() {
        this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-dropdown' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
    }



    render() {
        return (
            <StyleProvider style={getTheme(material)}>

                <Container>


                    <Header hasTabs androidStatusBarColor={this.state.value == '0' ? '#f6f7f9' : '#0b0b0b'} style={this.state.value == '0' ? styles.headerLight : styles.headerDark} >


                    </Header>

                    <View style={this.state.value == '0' ? styles.viewLight : styles.viewDark}>
                        <Row style={{ margin: 5 }}>
                            <Col style={{ width: '15%', height: 30 }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                            <Col style={{ width: '55%', height: 30, }}>
                                <Text style={this.state.tabsValue == 0 && this.state.value == 0 ? styles.lightgainertabs : this.state.tabsValue == 0 && this.state.value == 1 ? styles.darkgainertabs : styles.hide}>Market Watch</Text>
                                <Text style={this.state.tabsValue == 1 && this.state.value == 0 ? styles.lightgainertabs : this.state.tabsValue == 1 && this.state.value == 1 ? styles.darkgainertabs : styles.hide}>Market Summary</Text>

                            </Col>
                            <Col style={{ width: '20%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, alignSelf: 'flex-end', marginTop: 3, marginRight: 15 }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
                            <Col style={{ width: '10%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => alert('under develoment')}><IconEvil name="user" style={{ textAlign: 'center', }} size={35} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
                        </Row>
                        <Row style={{ margin: 15, marginTop: '3%' }}>

                            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>


                        </Row>

                    </View>

                    {/* <Tabs initialPage={this.state.tabsValue} tabContainerStyle={{ elevation: 0, height: 10 }} tabBarUnderlineStyle={this.state.value == 0 ? styles.lightunderline : styles.darkunderline} onChangeTab={({ i }) => this.setState({ tabsValue: i })}>
                        <Tab heading={<TabHeading style={this.state.value == 0 ? styles.lighttab : styles.darktab}></TabHeading>} >
                            <View style={this.state.value == 0 ? styles.lightview : styles.darkview}>
                                <Row style={{ marginLeft: 16, }}>
                                    <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Trd Qty</Text></Col>
                                    <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}></Col>
                                    <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Change(%)</Text></Col>
                                </Row>
                                <View style={this.state.loader == true ? styles.show : styles.hide}>
                                    <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                                </View>
                                <View style={this.state.nowatchdata == true ? styles.showNoData : styles.hide}>
                                    <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                                </View>
                                <FlatList
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={this.state.refreshing}
                                            onRefresh={this.onRefresh.bind(this)}
                                        />
                                    }
                                    data={this.state.Watch}
                                    style={{ marginTop: 60, height: '100%' }}
                                    onEndReachedThreshold={0.5}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item }) =>
                                        <View style={item.id == '' ? styles.hide : styles.show}>
                                            <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                                <Col style={{ width: '100%', height: 58 }}>
                                                    <Row style={{}}>
                                                        <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.change > 0 || item.change == '0.00' ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.name1}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{this.value(item.trd)}</Text></Col>
                                                        <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}></Col>
                                                        <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={item.change > 0 || item.change == '0.00' ? styles.possensexltp : styles.negsensexltp} numberOfLines={1}>{item.ltp}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensex1 : styles.darksensex1}>{this.chng(item.change, item.percent)}</Text></Col>

                                                    </Row>

                                                </Col>
                                               
                                            </Row>
                                            <Text></Text>

                                        </View>

                                    }
                                    keyExtractor={item => item.id}
                                />
                            </View>
                        </Tab>
                        <Tab heading={<TabHeading style={this.state.value == 0 ? styles.lighttab : styles.darktab}></TabHeading>}>
                            <View style={this.state.value == 0 ? styles.lightview : styles.darkview}>
                                <Row style={{ marginLeft: 16, marginTop: 10, }}>
                                    <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Statistics</Text></Col>
                                    <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}></Col>
                                    <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Value</Text></Col>
                                </Row>

                                <View style={this.state.loader == true ? styles.show : styles.hide}>
                                    <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                                </View>
                                <View style={this.state.nosummarydata == true ? styles.showNoData : styles.hide}>
                                    <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                                </View>

                                <FlatList
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={this.state.refreshing}
                                            onRefresh={this.onRefresh.bind(this)}
                                        />
                                    }
                                    data={this.state.Summary}
                                    style={{ marginTop: 40, height: '100%' }}
                                    onEndReachedThreshold={0.5}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item }) =>
                                        <View style={item.split('@')[0] == '' ? styles.hide : styles.show} >
                                            <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                                <Col style={{ width: '100%', height: 58 }}>
                                                    <Row style={{}}>
                                                        <Col style={this.state.value == '0' ? styles.listlightcolsensexsummary1 : styles.listdarkcolsensexsummary1}><Text style={this.state.value == '0' ? styles.lightsensexname : styles.darksensexname} numberOfLines={1}>{item.split('@')[0]}</Text></Col>
                                                        <Col style={this.state.value == '0' ? styles.listlightcolsensexsummary2 : styles.listdarkcolsensexsummary2}><Text style={this.state.value == '0' ? styles.lightLtp : styles.darkLtp} numberOfLines={1}>{item.split('@')[1]}</Text></Col>

                                                    </Row>

                                                </Col>

                                            </Row>
                                            <Text></Text>

                                        </View>

                                    }
                                    keyExtractor={item => item.id}
                                />

                            </View>

                        </Tab>



                    </Tabs> */}

                    <ViewPager
                        style={{ height: height - 135 }}
                        initialPage={this.tabval}
                        onPageScroll={(i) => this.setState({ tabsValue: i.position })}
                    >
                        <View style={this.state.value == 0 ? styles.lightview : styles.darkview}>
                            <Row style={{ marginLeft: 16, }}>
                                <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Security</Text><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Trd Qty</Text></Col>
                                <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}></Col>
                                <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>LTP</Text><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Change(%)</Text></Col>
                            </Row>
                            <View style={this.state.loader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                            </View>
                            <View style={this.state.nowatchdata == true ? styles.showNoData : styles.hide}>
                                <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Watch}
                                style={{ marginTop: 60, height: '100%' }}
                                onEndReachedThreshold={0.5}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <View style={item.id == '' ? styles.hide : styles.show}>
                                        <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                            <Col style={{ width: '100%', height: 58 }}>
                                                <Row style={{}}>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex1 : styles.listdarkcolsensex1}><Text style={item.change > 0 || item.change == '0.00' ? styles.possensex : styles.negsensex} numberOfLines={1}>{item.name1}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{this.value(item.trd)}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex3 : styles.listdarkcolsensex3}></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensex2 : styles.listdarkcolsensex2}><Text style={item.change > 0 || item.change == '0.00' ? styles.possensexltp : styles.negsensexltp} numberOfLines={1}>{item.ltp}</Text><Text numberOfLines={1} style={this.state.value == '0' ? styles.lightsensex1 : styles.darksensex1}>{this.chng(item.change, item.percent)}</Text></Col>

                                                </Row>

                                            </Col>

                                        </Row>
                                        <Text></Text>

                                    </View>

                                }
                                keyExtractor={item => item.id}
                            />
                        </View>

                        <View style={this.state.value == 0 ? styles.lightview : styles.darkview}>
                            <Row style={{ marginLeft: 16, marginTop: 10, }}>
                                <Col style={this.state.value == '0' ? styles.lightheader1 : styles.darkheader1}><Text style={this.state.value == '0' ? styles.lightheadertext : styles.darkheadertext}>Statistics</Text></Col>
                                <Col style={this.state.value == '0' ? styles.lightheader2 : styles.darkheader2}></Col>
                                <Col style={this.state.value == '0' ? styles.lightheader3 : styles.darkheader3}><Text style={this.state.value == '0' ? styles.lightheadertext1 : styles.darkheadertext1}>Value</Text></Col>
                            </Row>

                            <View style={this.state.loader == true ? styles.show : styles.hide}>
                                <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />

                            </View>
                            <View style={this.state.nosummarydata == true ? styles.showNoData : styles.hide}>
                                <Text style={this.state.value == '0' ? styles.nodataLight : styles.nodatadark}>No Data Found</Text>
                            </View>

                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                data={this.state.Summary}
                                style={{ marginTop: 40, height: '100%' }}
                                onEndReachedThreshold={0.5}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) =>
                                    <View style={item.split('@')[0] == '' ? styles.hide : styles.show} >
                                        <Row style={{ marginLeft: 16, marginRight: 16, }}>
                                            <Col style={{ width: '100%', height: 58 }}>
                                                <Row style={{}}>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensexsummary1 : styles.listdarkcolsensexsummary1}><Text style={this.state.value == '0' ? styles.lightsensexname : styles.darksensexname} numberOfLines={1}>{item.split('@')[0]}</Text></Col>
                                                    <Col style={this.state.value == '0' ? styles.listlightcolsensexsummary2 : styles.listdarkcolsensexsummary2}><Text style={this.state.value == '0' ? styles.lightLtp : styles.darkLtp} numberOfLines={1}>{item.split('@')[1]}</Text></Col>

                                                </Row>

                                            </Col>

                                        </Row>
                                        <Text></Text>

                                    </View>

                                }
                                keyExtractor={item => item.id}
                            />

                        </View>
                    </ViewPager>


                    <Modal
                        animationType="slide"
                        transparent={false}
                        style={{ height: '100%' }}
                        visible={this.state.sidemenu}
                        onRequestClose={() => {
                            this.setState({ sidemenu: false });
                        }}
                    >
                        <View style={this.state.value == '0' ? styles.lightmenu : styles.darkmenu}>
                            <Row style={{ marginLeft: 20, marginTop: 10 }}>
                                <Col style={{ width: '67%', height: 40 }}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={{ marginLeft: 10 }} size={40} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>
                                <Col style={{ width: '15%', height: 40 }}><IconEvil name="user" style={{ marginTop: 3, textAlign: 'right' }} size={35} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>
                                <Col style={{ width: '15%', height: 40, }}><Icon name="ios-settings" style={{ marginRight: 5, marginTop: 2, textAlign: 'right' }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>
                            </Row>

                            <ScrollView style={{ top: '8%', }}>
                                <Image style={{ width: 150, height: 150, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/lightprofile.png') : require('../images/darkprofile.png')} />
                                {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
                                <Text></Text>
                                <View style={{}}>
                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Home</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.equityView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Equity</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconname} onPress={() => this.equityView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.equity == false ? styles.hide : styles.show}>
                                        <Text onPress={() => this.equitypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GAINERS</Text>
                                        <Text onPress={() => this.equitypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>LOSERS</Text>
                                        <Text onPress={() => this.equitypage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TOP TURNOVER</Text>
                                        <Text onPress={() => this.equitypage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK HIGH</Text>
                                        <Text onPress={() => this.equitypage(4)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK LOW</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Indices</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.sensexView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Sensex</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamesensex} onPress={() => this.sensexView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.sensex == false ? styles.hide : styles.show}>
                                        <Text onPress={() => this.settingpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>SECURITY</Text>
                                        <Text onPress={() => this.settingpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>OVERVIEW</Text>
                                        <Text onPress={() => this.settingpage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TURNOVER</Text>
                                        <Text onPress={() => this.settingpage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CONTRIBUTION</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.smeView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >SME</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamesme} onPress={() => this.smeView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.sme == false ? styles.hide : styles.show}>
                                        <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                        <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET STATISTICS</Text>
                                        <Text></Text>
                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.derivativeView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Derivatives</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamederivative} onPress={() => this.derivativeView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.derivative == false ? styles.hide : styles.show}>

                                        <Text onPress={() => this.derivativepage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                        <Text onPress={() => this.derivativepage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.currencyView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Currency</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecurrency} onPress={() => this.currencyView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.currency == false ? styles.hide : styles.show}>
                                        <Text onPress={() => this.currencypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                        <Text onPress={() => this.currencypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.commadityView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Commadity</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecommodity} onPress={() => this.commadityView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.commodity == false ? styles.hide : styles.show}>

                                        <Text onPress={() => this.commoditypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                        <Text onPress={() => this.commoditypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.irdView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IRD</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnameird} onPress={() => this.irdView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.ird == false ? styles.hide : styles.show}>
                                        <Text onPress={() => this.irdpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                                        <Text onPress={() => this.irdpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.etfView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>ETF</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnameetf} onPress={() => this.etfView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.etf == false ? styles.hide : styles.show}>
                                        <Text onPress={() => this.etfpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ETF WATCH</Text>
                                        <Text onPress={() => this.etfpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.debtView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Debt</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamedebt} onPress={() => this.debtView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.debt == false ? styles.hide : styles.show}>

                                        <Text onPress={() => this.debtpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>DEBT MARKET SUMMARY</Text>
                                        <Text onPress={() => this.debtpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BONDS-OTC TRADES</Text>
                                        <Text onPress={() => this.debtpage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL CORP. DEBT</Text>
                                        <Text onPress={() => this.debtpage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GSEC</Text>
                                        <Text onPress={() => this.debtpage(4)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL GOV BONDS</Text>
                                        <Text onPress={() => this.debtpage(5)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>EBP</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.corporateView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Corporates</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecorporate} onPress={() => this.corporateView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                                    </Row>
                                    <View style={this.state.corporate == false ? styles.hide : styles.show}>
                                        <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ANNOUNCEMENTS</Text>
                                        <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ACTIONS</Text>
                                        <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RESULTS CALENDAR</Text>
                                        <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>BOARD MEETINGS</Text>
                                        <Text></Text>

                                    </View>
                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Statistics</Text></Col>
                                        <Col style={{ width: '30%', height: 40 }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Turnover</Text></Col>
                                        <Col style={{ width: '30%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPO/OFS</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Listings</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Notices</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Watchlist</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Portfolio</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                                    <Row style={{ marginLeft: 16, marginRight: 16 }}>
                                        <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPF</Text></Col>
                                        <Col style={{ width: '50%', height: 40, }}></Col>
                                    </Row>

                                    <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>
                                    <Text></Text>
                                    <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', }}></Text>
                                </View>
                            </ScrollView>
                        </View>
                    </Modal>

                    <Footer>

                        <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
                            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
                                <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
                                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
                            </Button>


                            <Button style={styles.inactive} onPress={() => this.tab('port')}>
                                <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
                            </Button>

                            <Button style={styles.active} onPress={() => this.tab('home')}>
                                <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
                                <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
                            </Button>
                            <Button style={styles.inactive} onPress={() => this.tab('search')}>
                                <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
                                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
                            </Button>
                            <Button style={styles.inactive} onPress={() => this.tab('more')}>
                                <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

                                <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
                            </Button>


                        </FooterTab>
                    </Footer>
                </Container>
            </StyleProvider>
        );
    }
}

const styles = StyleSheet.create({
    showNoData: { display: 'flex' },
    nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    nodatadark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    viewDark: { backgroundColor: '#0b0b0b', height: 80 },
    viewLight: { backgroundColor: '#f6f7f9', height: 80 },
    darkgainertabs: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold' },
    lightgainertabs: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold' },
    darkIndName: { color: '#72a3bf', bottom: 17 },
    lightIndName: { color: '#132144', bottom: 17 },
    lightLtp: { alignSelf: 'flex-end', marginTop: 20, marginRight: 5, color: '#132144', fontSize: 14, },
    darkLtp: { alignSelf: 'flex-end', marginTop: 20, marginRight: 5, color: '#72a3bf', fontSize: 14, },
    mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
    mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
    posChange: { color: '#19cf3e', fontSize: 13, bottom: 30, marginLeft: 70 },
    negChange: { color: '#f54845', fontSize: 13, bottom: 30, marginLeft: 70 },
    headerDark: { backgroundColor: '#0b0b0b', height: 1 },
    headerLight: { backgroundColor: '#f6f7f9', height: 1 },
    lightdate: { marginTop: 10, color: '#132144', fontSize: 14, textAlign: 'right' },
    darkdate: { marginTop: 10, color: '#72a3bf', fontSize: 14, textAlign: 'right' },
    darkview: { backgroundColor: '#0b0b0b', height: '100%' },
    lightview: { backgroundColor: '#f6f7f9', height: '100%' },
    lighttab: { backgroundColor: '#f6f7f9', },
    darktab: { backgroundColor: '#0b0b0b', },
    lightunderline: { borderBottomWidth: 4, borderBottomColor: '#f6f7f9' },
    darkunderline: { borderBottomWidth: 4, borderBottomColor: '#0b0b0b' },

    active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
    inactive: { padding: 12, },
    titlelight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    titledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    pos: { alignSelf: 'flex-end', marginRight: 10, marginTop: 9, fontFamily: 'SegoeProDisplay-Regular', color: '#19cf3e' },
    neg: { alignSelf: 'flex-end', marginRight: 10, marginTop: 9, fontFamily: 'SegoeProDisplay-Regular', color: '#f54845' },
    tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
    tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamedark: { color: '#79868e', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
    tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },

    statuslight: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
    statusdark: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
    listlightcol1: { width: '65%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcol2: { width: '35%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcol1: { width: '65%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcol2: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex1: { width: '45%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcolsensex2: { width: '45%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listdarkcolsensex3: { width: '10%', height: 58, backgroundColor: '#1a1f1f', },
    listlightcolsensex1: { width: '45%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcolsensex2: { width: '45%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listlightcolsensex3: { width: '10%', height: 58, backgroundColor: 'white', },

    listdarkcolsensexsummary1: { width: '65%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listdarkcolsensexsummary2: { width: '35%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
    listlightcolsensexsummary1: { width: '65%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
    listlightcolsensexsummary2: { width: '35%', height: 58, backgroundColor: 'white', borderBottomRightRadius: 5, borderTopRightRadius: 5 },


    show: { display: 'flex' },
    hide: { display: 'none' },
    possensex: { marginLeft: 5, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    lighttext1: { marginLeft: 5, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexturn: { alignSelf: 'flex-end', marginRight: 7, marginTop: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensex1: { alignSelf: 'flex-end', marginRight: 7, marginTop: 1, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex: { alignSelf: 'flex-end', marginRight: 7, marginTop: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexturn: { alignSelf: 'flex-end', marginRight: 7, marginTop: 20, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexname: { alignSelf: 'flex-start', marginLeft: 5, marginTop: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexcontract: { alignSelf: 'center', marginTop: 20, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexcontract: { alignSelf: 'center', marginTop: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexvol: { alignSelf: 'center', marginRight: 10, marginTop: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexvol: { alignSelf: 'center', marginRight: 10, marginTop: 20, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexnametotal: { alignSelf: 'flex-start', marginLeft: 5, marginTop: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darksensexnametotal: { alignSelf: 'flex-start', marginLeft: 5, marginTop: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightsensexsell: { alignSelf: 'flex-end', marginRight: 5, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexsell: { alignSelf: 'flex-end', marginRight: 5, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexname: { alignSelf: 'flex-start', marginLeft: 5, marginTop: 20, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    darksensex1: { alignSelf: 'flex-end', marginRight: 7, marginTop: 1, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexvalue: { marginRight: 13, fontSize: 12.5, alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    negsensex: { marginLeft: 5, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },

    lightltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    darkltpcontri: { alignSelf: 'flex-end', marginRight: 16, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lastlightcontri: { alignSelf: 'flex-end', marginRight: 6, marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    lastdarkcontri: { alignSelf: 'flex-end', marginRight: 6, marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    darktext1: { marginLeft: 5, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    perlight: { alignSelf: 'flex-end', marginRight: 8, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perdark: { alignSelf: 'flex-end', marginRight: 8, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13 },
    perturnlight: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    perturndark: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', marginTop: 2, fontSize: 13, },
    ltpviewpos: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    ltpviewneg: { width: 60, marginTop: 8, color: 'white', backgroundColor: '#f54845', fontFamily: 'SegoeProDisplay-Regular', borderRadius: 4, alignSelf: 'flex-end', marginRight: 8 },
    lightheader1: { width: '32%', height: 40 },
    darkheader1: { width: '32%', height: 40 },
    lightheader2: { width: '30%', height: 40 },
    darkheader2: { width: '30%', height: 40 },
    lightheader3: { width: '33%', height: 40 },
    darkheader3: { width: '33%', height: 40 },

    lightPicker: { color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkPicker: { color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightheadertext: { marginLeft: 5, fontSize: 15, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext: { marginLeft: 5, fontSize: 15, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightheadertext1: { alignSelf: 'flex-end', color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkheadertext1: { alignSelf: 'flex-end', color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lighttext: { marginLeft: 15, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darktext: { marginLeft: 15, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    firstcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 65, },
    firstcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 65, },
    secondcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 95, },
    secondcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 95, },
    thirdcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 210, },
    thirdcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 210, },
    fourthcol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 155, },
    fourthcoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 155, },
    fivecol: { borderRadius: 4, backgroundColor: 'white', width: '100%', height: 125, },
    fivecoldark: { borderRadius: 4, backgroundColor: '#1a1f1f', width: '100%', height: 125, },
    sixcol: { borderRadius: 4, width: '100%', height: 60, },
    sixcoldark: { borderRadius: 4, width: '100%', height: 60, },
    prvlight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    prvdark: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    titlelight: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoePro-Bold', },
    titledark: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    maintitle: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    maintitledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
    dropicon: { textAlign: 'center', marginTop: 10 },
    ltplight: { marginLeft: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    ltpdark: { marginLeft: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
    darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
    lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darkmenutext: { marginLeft: 15, fontSize: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    darkline: { marginLeft: 30, borderBottomColor: '#72a3bf', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
    contricol: { width: '100%', height: 58 },
    lightdropdown: { margin: 10, backgroundColor: '#f6f7f9' },
    darkdropdown: { margin: 10, backgroundColor: '#0b0b0b' },
    lightdroptext: { color: '#132144' },
    darkdroptext: { color: '#72a3bf' },
    lightselectedText: { color: '#132144', marginTop: 13, marginLeft: 5 },
    darkselectedText: { color: '#72a3bf', marginTop: 13, marginLeft: 5 },
    totallight: { marginLeft: 5, marginTop: 9, color: '#132144', fontFamily: 'SegoePro-Bold', },
    totaldark: { marginLeft: 5, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
    dateheight: { marginLeft: 16, marginRight: 16, marginTop: '10%' },
    nodataDate: { marginLeft: 16, marginRight: 16, bottom: 130 },
    ltpData: { marginLeft: 16, marginRight: 16, marginTop: '18%' },
    ltpnoData: { marginLeft: 16, marginRight: 16, bottom: '70%' },
    nodataLight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    nodatadark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', alignSelf: 'center' },
    oneTurnData: { marginLeft: 16, marginRight: 16, bottom: 53 },
    ltpTurnNoData: { marginLeft: 16, marginRight: 16, bottom: 90 },
    turnHeader: { marginLeft: 16, marginRight: 16, marginTop: 50 },
    turnData: { bottom: 10 },
    Singleturndata: { bottom: '300%' },
    securitylengthone: { bottom: 125 },
    turnlengthone: { bottom: 170 },
    securitylength: { marginTop: 57 },
    turnlengthlist: { marginTop: 40, },
    lightwatchper: { alignSelf: 'flex-end', marginRight: 10, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darkwatchper: { alignSelf: 'flex-end', marginRight: 10, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    possensexltp: { marginRight: 5, alignSelf: 'flex-end', marginTop: 9, color: '#19cf3e', fontFamily: 'SegoeProDisplay-Regular', },
    negsensexltp: { marginRight: 5, alignSelf: 'flex-end', marginTop: 9, color: '#f54845', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensexsummary: { marginLeft: 16, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
    darksensexSummary: { marginLeft: 16, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
    lightsensextotal: { marginLeft: 5, marginTop: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
    darksensextotal: { marginLeft: 5, marginTop: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },

})

export default SmeTabs;
