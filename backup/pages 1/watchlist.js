import React, { Component } from 'react';
import { View, Image, Dimensions, Switch,PixelRatio, Picker, Text, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, TouchableHighlight, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Ripple from 'react-native-material-ripple';
import { Tab, Tabs, Container, StyleProvider, TabHeading, Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import { normalize } from 'react-native-elements';
const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');
const scale = SCREEN_WIDTH / 350;

class Watchlist extends Component {
  static navigationOptions = {

    header: null,
  };
  Gainer = [];
  Loser = [];
  Turnover = [];
  name = '';
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    this.state = {
      selecttheme: 'dark',

    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    setTimeout(() => { SplashScreen.hide() }, 4000);

  }
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    this.setState({ selecttheme: 'light' });
    this.props.navigation.goBack(null);
    return true;
  }

  


  render() {
    return (

      <View>
       

          <Text style={styles.watch}>Watch it</Text>


      </View>

    );
  }
}
const styles = StyleSheet.create({
  watch: {
    fontSize: normalize(25),
    textAlign: 'center',
    margin: 10,

  },
  listlightcol1: { width: '70%', height: 58, backgroundColor: 'white', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listlightcol2: { width: '30%', height: 58, backgroundColor: 'white', textAlign: 'left', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
  listdarkcol1: { width: '70%', height: 58, backgroundColor: '#1a1f1f', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 },
  listdarkcol2: { width: '30%', height: 58, backgroundColor: '#1a1f1f', borderBottomRightRadius: 5, borderTopRightRadius: 5 },
});

export default Watchlist;

