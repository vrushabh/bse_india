import React, { Component } from 'react';
import { View,TouchableOpacity, Text, Picker, FlatList, Switch, NetInfo, BackHandler, CheckBox, RefreshControl, AsyncStorage, ToastAndroid, Modal, StatusBar, ActivityIndicator, ScrollView, SafeAreaView, Linking, StyleSheet, Alert, Image, Dimensions } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';

class Sidemenu extends Component {
  static navigationOptions = {

    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    // setInterval(() => { this.main() }, 2000);
    setTimeout(() => { SplashScreen.hide() }, 3000);

  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  handleBackButtonClick() {

    this.props.navigation.goBack(null);
    return true;
  }
  close()
  {
    this.props.navigation.goBack(null);
    return true;
  }
  home()
  {
    AsyncStorage.getItem('themecolor').then((dt) => {
      if(dt == null || dt == 'dark')
      {
        this.props.navigation.navigate('maindark');

      }
      else if(dt == 'light')
      {
        this.props.navigation.navigate('mainlight');

      }
  });
  }
  gain()
  {
    // AsyncStorage.setItem("pageshow", "gain");
    // this.props.navigation.navigate('gainer')
  }
  lose()
  {
    // AsyncStorage.setItem("pageshow", "lose");
    // this.props.navigation.navigate('gainer')
  }
  turn()
  {
    // AsyncStorage.setItem("pageshow", "turn");
    // this.props.navigation.navigate('gainer')
  }
  high()
  {
    // AsyncStorage.setItem("pageshow", "high");
    // this.props.navigation.navigate('gainer')
  }
  low()
  {
    // AsyncStorage.setItem("pageshow", "low");
    // this.props.navigation.navigate('gainer')
  }




  render() {
    return (
      <View>
        <StatusBar backgroundColor={'#f1f2f6'} />
          <Row style={{marginLeft:20,marginTop:10}}>
            <Col style={{width:'100%',height:40}}><TouchableOpacity activeOpacity={0.5} onPress={() => this.close()}><Icon name="ios-close" style={{marginLeft:10}} size={40} color="#132144" /></TouchableOpacity></Col>
          </Row>

          <ScrollView style={{marginTop:40,}}>
          <Image style={{ width: 120, height: 120, marginTop: 6, alignSelf: 'center' }} source={require('../images/user.png')} />
          {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
          <Text></Text>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.home()}><Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Home</Text></TouchableOpacity>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate('gainer')}><Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Equity</Text></TouchableOpacity>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.gain()}><Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>GAINERS</Text></TouchableOpacity>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.lose()}><Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>LOSERS</Text></TouchableOpacity>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.turn()}><Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>TOP TURNOVER</Text></TouchableOpacity>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.high()}><Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>52 WK HIGH</Text></TouchableOpacity>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.low()}><Text  style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>52 WK LOW</Text></TouchableOpacity>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate('indices')}><Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Indices</Text></TouchableOpacity>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate('sensex')}><Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Sensex</Text></TouchableOpacity>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>SECURITY</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>OVERVIEW</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>TURNOVER</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>CONTRIBUTION</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>SME</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET WATCH</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET STATISTICS</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Derivatives</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET WATCH</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET SUMMARY</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Currency</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET WATCH</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET SUMMARY</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Commadity</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET WATCH</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET SUMMARY</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>IRD</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET WATCH</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET SUMMARY</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>ETF</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>ETF WATCH</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>MARKET SUMMARY</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Debt</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>DEBT MARKET SUMMARY</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>CORPORATE BONDS-OTC TRADES</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>RETAIL CORP. DEBT</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>GSEC</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>RETAIL GOV BONDS</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>CORPORATE BOND-NDS-RST</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>EBP</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Corporates</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>ANNOUNCEMENTS</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>ACTIONS</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>RESULTS CALENDAR</Text>
          <Text style={{marginLeft:100,marginTop:13,fontSize:16,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>BOARD MEETINGS</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Market Statistics</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Market Turnover</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>IPO/OFS</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Listings</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Notices</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Watchlist</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Portfolio</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>IPF</Text>
          <Text style={{marginLeft:80,borderBottomColor:'#132144',borderBottomWidth:1,marginRight:25,bottom:10}}></Text>

          <Text style={{marginLeft:80,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}></Text>

          </ScrollView>
      </View>
    );
  }
}

export default Sidemenu;
