import React, { Component } from 'react';
import { NetInfo, View, KeyboardAvoidingView, Image, Text, Linking, CheckBox, ToastAndroid, Modal, Switch, Picker, BackHandler, ActivityIndicator, FlatList, StatusBar, TextInput, TouchableOpacity, AsyncStorage, StyleSheet, ScrollView } from 'react-native';
import { Card, CardItem, Col, Body, Button, Thumbnail, Left, Header, Footer, Right, FooterTab, Toast, } from 'native-base';
import { Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import IconSimple from 'react-native-vector-icons/SimpleLineIcons';
import IconEvil from 'react-native-vector-icons/EvilIcons';

import IconEnt from 'react-native-vector-icons/AntDesign';
import IconFonsito from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import * as Animatable from 'react-native-animatable';
import Voice from '@react-native-community/voice';
import I18n from '../i18';
import Popover from 'react-native-popover-view'

class Search extends Component {

  static navigationOptions = {

    header: null,
  };
  themename = '';

  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps.allowFontScaling = false;


    this.state = {
      date: '',
      status: '',
      Equity: '',
      Derivative: '',
      mf: '',
      debt: '',
      showloader: false,
      showloaderderivative: false,
      showloadermf: false,
      showloaderdebt: false,
      val: '',
      valderivative: '',
      valmf: '',
      valdebt: '',
      closeicon: false,
      closeiconderivative: false,
      closeiconmf: false,
      closeicondebt: false,
      name: 'equity',
      value: 1,
      isVisible: false,
      switchval: false,
      sidemenu: false,
      settingshow: false,
      theme: '',
      showedit: false,
      selectlan: 'English',
      iconname: 'ios-arrow-forward',
      iconnamesensex: 'ios-arrow-forward',
      iconnamesme: 'ios-arrow-forward',
      iconnamederivative: 'ios-arrow-forward',
      iconnamecurrency: 'ios-arrow-forward',
      iconnamecommodity: 'ios-arrow-forward',
      iconnameird: 'ios-arrow-forward',
      iconnameetf: 'ios-arrow-forward',
      iconnamedebt: 'ios-arrow-forward',
      iconnamecorporate: 'ios-arrow-forward',
      equity: false,
      sensex: false,
      sme: false,
      derivative: false,
      currencytab: false,
      commoditytab: false,
      ird: false,
      etf: false,
      debt: false,
      corporate: false
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    setTimeout(() => { SplashScreen.hide() }, 1000);

    Voice.onSpeechStart = this.onSpeechStart.bind(this);
    Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
  }

  setting() {
    this.setState({ settingshow: true });
    this.setState({ sidemenu: false });
    // this.props.navigation.navigate('setting');
  }
  sensexfun() {
    fetch('https://api.bseindia.com/bseindia/api/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {

      try {
        responsejson[0].F == '0' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : responsejson[0].F == '1' ? this.setState({ date: responsejson[0].dttm, status: 'Pre-Open' }) : responsejson[0].F == '2' ? this.setState({ date: responsejson[0].dttm, status: 'Close' }) : responsejson[0].F == '3' ? this.setState({ date: responsejson[0].dttm, status: 'Open' }) : '';

      } catch (error) {
        console.log(error);
      }
    })

  }

  editicon(val) {
    val == false ? this.setState({ iconname: 'ios-arrow-up', showedit: true }) : this.setState({ iconname: 'ios-arrow-down', showedit: false });
    // this.setState({showedit:!val});
  }

  changeTheme(thm) {
    this.setState({ theme: thm, settingshow: false, sidemenu: false });
    AsyncStorage.setItem("themecolor", thm);
  }
  chnglanguage(lan) {
    this.setState({ settingshow: false, sidemenu: false });

    AsyncStorage.setItem('language', lan);
  }
  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      isConnected ? '' : ToastAndroid.showWithGravityAndOffset("Application requires Network to proceed", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    });
    this.themename = this.props.navigation.getParam('theme', '');
    if (this.themename == null || this.themename == 'dark') {
      this.setState({ value: '1' });
    }
    else {
      this.setState({ value: '0' });
    }
    this.sensexfun();
    // I18n.locale = "en";
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    Voice.destroy().then(Voice.removeAllListeners);
  }

  onSpeechStart(e) {
    console.log(e);
  };

  onSpeechRecognized(e) {
    console.log(e);
  };

  onSpeechResults(e) {
    let res = JSON.stringify(e.value[0]).slice(1, -1);
    this.setState({ val: res, valderivative: res, valmf: res, valdebt: res });
    this.chng(res);
    this.chngderivative(res);
    this.chngmf(res);
    this.chngdebt(res);
  }

  voice() {
    Voice.start('en-US');
  }


  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  back() {
    this.props.navigation.goBack(null);
    return true;
  }
  tab(val) {

    if (val == 'watch') { }
    else if (val == 'port') { }
    else if (val == 'home') {
      this.props.navigation.navigate('maindark');
    }
    else if (val == 'search') { }
    else if (val == 'more') {
      this.setState({ sidemenu: true })
    }

  }

  chng(event) {
    if (event != '') {
      if (event.length < 3) {
        this.setState({ val: event, valderivative: event, valmf: event, valdebt: event, closeicon: true });
      }
      else {
        this.setState({ showloader: true, val: event, valderivative: event, valmf: event, valdebt: event, closeicon: true });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Equity: responsejson.split('#'), showloader: false });
        });
        this.chngderivative(event);
        this.chngmf(event);
        this.chngdebt(event);
      }

    }
    else { this.setState({ val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false }); }
  }

  close() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
  }

  chngderivative(event) {

    if (event != '') {
      if (event.length < 3) {
        this.setState({ valderivative: event, val: event, valmf: event, valdebt: event, closeiconderivative: true, closeicon: true, closeiconmf: true, closeicondebt: true });
      }
      else {
        this.setState({ showloaderderivative: true, showloader: true, showloadermf: true, showloaderdebt: true, valderivative: event, val: event, valmf: event, valdebt: event, closeiconderivative: true, closeicon: true, closeiconmf: true, closeicondebt: true });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Derivative: responsejson.split('#'), showloaderderivative: false });

        });

        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Equity: responsejson.split('#'), showloader: false });

        });

        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ mf: responsejson.split('#'), showloadermf: false });

        });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ debt: responsejson.split('#'), showloaderdebt: false });

        });

      }

    }
    else {

      this.setState({ valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    }

  }
  closederivative() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
  }

  chngmf(event) {
    if (event != '') {
      if (event.length < 3) {
        this.setState({ valmf: event, val: event, valderivative: event, valdebt: event, closeiconmf: true, closeicon: true, closeiconderivative: true, closeicondebt: true });
      }
      else {
        this.setState({ showloadermf: true, showloaderderivative: true, showloader: true, showloaderdebt: true, valmf: event, val: event, valderivative: event, valdebt: event, closeiconmf: true, closeicon: true, closeiconderivative: true, closeicondebt: true });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ mf: responsejson.split('#'), showloadermf: false });

        });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Derivative: responsejson.split('#'), showloaderderivative: false });

        });

        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ Equity: responsejson.split('#'), showloader: false });

        });
        fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

          this.setState({ debt: responsejson.split('#'), showloaderdebt: false });

        });

      }

    }
    else {

      this.setState({ valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    }
  }
  closemf() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
  }

  chngdebt(event) {
    if (event != '') {

      if (event != '') {
        if (event.length < 3) {
          this.setState({ valdebt: event, val: event, valderivative: event, valmf: event, closeicondebt: true, closeicon: true, closeiconderivative: true, closeiconmf: true });
        }
        else {
          this.setState({ showloaderdebt: true, showloadermf: true, showloaderderivative: true, showloader: true, valdebt: event, val: event, valderivative: event, valmf: event, closeicondebt: true, closeicon: true, closeiconderivative: true, closeiconmf: true });
          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ debt: responsejson.split('#'), showloaderdebt: false });

          });
          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ Derivative: responsejson.split('#'), showloaderderivative: false });

          });

          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ Equity: responsejson.split('#'), showloader: false });

          });
          fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=' + event + '&ln=en').then((response) => response.text()).then((responsejson) => {

            this.setState({ mf: responsejson.split('#'), showloadermf: false });

          });

        }

      }

    }
    else {

      this.setState({ valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    }
  }
  closedebt() {
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=debt&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ debt: responsejson.split('#'), valdebt: '', val: '', valderivative: '', valmf: '', closeicondebt: false, closeicon: false, closeiconderivative: false, closeiconmf: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=mf&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ mf: responsejson.split('#'), valmf: '', val: '', valderivative: '', valdebt: '', closeiconmf: false, closeicon: false, closeiconderivative: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=deri&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Derivative: responsejson.split('#'), valderivative: '', val: '', valmf: '', valdebt: '', closeiconderivative: false, closeicon: false, closeiconmf: false, closeicondebt: false });

    });
    fetch('https://api.bseindia.com/msource/1D/getQouteSearch.aspx?t=EQ&s=&ln=en').then((response) => response.text()).then((responsejson) => {

      this.setState({ Equity: responsejson.split('#'), val: '', valderivative: '', valmf: '', valdebt: '', closeicon: false, closeiconderivative: false, closeiconmf: false, closeicondebt: false });

    });
  }

  equity() { this.setState({ name: 'equity' }) }
  derivative() { this.setState({ name: 'derivative' }) }
  mf() { this.setState({ name: 'mf' }) }
  debt() { this.setState({ name: 'debt' }) }

  more() {
    this.setState({ isVisible: true });
  }

  closePopover() {
    this.setState({ isVisible: false });
  }
  closesidemenu() {
    this.setState({ sidemenu: false });
  }

  indices() {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('indices', { 'theme': dt });
    });
  }

  home() {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('maindark', { 'theme': dt });
    });

  }

  settingpage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('setting', { 'theme': dt, 'current': val });

    });
  }
  equitypage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('equity', { 'theme': dt, 'current': val });

    });
  }
  derivativepage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('derivativetab', { 'theme': dt, 'current': val });

    });
  }
  currencypage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('currencytab', { 'theme': dt, 'current': val });

    });
  }
  commoditypage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('commoditytab', { 'theme': dt, 'current': val });

    });
  }
  irdpage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('ird', { 'theme': dt, 'current': val });

    });
  }
  smepage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('sme', { 'theme': dt, 'current': val });

    });
  }
  etfpage(val) {
    AsyncStorage.getItem('themecolor').then((dt) => {
      this.setState({ sidemenu: false });
      this.props.navigation.navigate('etf', { 'theme': dt, 'current': val });

    });
  }

  googleassistant() {
    Linking.openURL('https://assistant.google.com/explore');

  }
  equityView() {
    this.state.equity == false ? this.setState({ equity: true, iconname: 'ios-arrow-dropdown' }) : this.setState({ equity: false, iconname: 'ios-arrow-forward' });
  }
  sensexView() {
    this.state.sensex == false ? this.setState({ sensex: true, iconnamesensex: 'ios-arrow-dropdown' }) : this.setState({ sensex: false, iconnamesensex: 'ios-arrow-forward' });
  }
  smeView() {
    this.state.sme == false ? this.setState({ sme: true, iconnamesme: 'ios-arrow-dropdown' }) : this.setState({ sme: false, iconnamesme: 'ios-arrow-forward' });
  }
  derivativeView() {
    this.state.derivative == false ? this.setState({ derivative: true, iconnamederivative: 'ios-arrow-dropdown' }) : this.setState({ derivative: false, iconnamederivative: 'ios-arrow-forward' });
  }
  currencyView() {
    this.state.currencytab == false ? this.setState({ currencytab: true, iconnamecurrency: 'ios-arrow-dropdown' }) : this.setState({ currencytab: false, iconnamecurrency: 'ios-arrow-forward' });
  }
  commadityView() {
    this.state.commoditytab == false ? this.setState({ commoditytab: true, iconnamecommodity: 'ios-arrow-dropdown' }) : this.setState({ commoditytab: false, iconnamecommodity: 'ios-arrow-forward' });
  }
  irdView() {
    this.state.ird == false ? this.setState({ ird: true, iconnameird: 'ios-arrow-dropdown' }) : this.setState({ ird: false, iconnameird: 'ios-arrow-forward' });
  }
  etfView() {
    this.state.etf == false ? this.setState({ etf: true, iconnameetf: 'ios-arrow-dropdown' }) : this.setState({ etf: false, iconnameetf: 'ios-arrow-forward' });
  }
  debtView() {
    this.state.debt == false ? this.setState({ debt: true, iconnamedebt: 'ios-arrow-dropdown' }) : this.setState({ debt: false, iconnamedebt: 'ios-arrow-forward' });
  }
  corporateView() {
    this.state.corporate == false ? this.setState({ corporate: true, iconnamecorporate: 'ios-arrow-dropdown' }) : this.setState({ corporate: false, iconnamecorporate: 'ios-arrow-forward' });
  }


  render() {

    return (
      <View style={this.state.value == '0' ? styles.mainviewlight : styles.mainviewdark}>
        <StatusBar backgroundColor={this.state.value == '0' ? '#f6f7f9' : '#0b0b0b'} />

        <View style={{ height: '32%', }}>
          <Row style={{ margin: 5, marginTop: 10, }}>
            <Col style={{ width: '15%', height: 30,  }}><TouchableOpacity activeOpacity={0.5} onPress={() => this.back()}><Icon name="md-arrow-back" style={{ textAlign: 'left', marginLeft: 13 }} size={27} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>
            <Col style={{ width: '55%', height: 30,  }}><Text style={this.state.value == '0' ? styles.titlelight : styles.titledark}>Get Quote</Text></Col>
            <Col style={{ width: '15%', height: 30, }}><TouchableOpacity activeOpacity={.5} onPress={() => this.googleassistant()}><Image style={{ width: 23, height: 22, marginTop: 3, alignSelf: 'flex-end' }} source={this.state.value == '0' ? require('../images/google-removebg-preview.png') : require('../images/googledark.png')} /></TouchableOpacity></Col>
            <Col style={{ width: '15%', height: 30, }}><TouchableOpacity activeOpacity={.5} ><IconEvil name="user" style={{ marginTop: 1, textAlign: 'right' }} size={35} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></TouchableOpacity></Col>  
          </Row>

          <Row style={{ margin: 5, marginRight: 19, }}>
            <Col style={{ width: '100%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightdate : styles.darkdate}>{this.state.date} <Text style={this.state.value == '0' ? styles.statuslight : styles.statusdark}>   {this.state.status}</Text></Text></Col>

          </Row>

          <ScrollView horizontal style={this.state.value == '0' ? styles.buttonlight : styles.buttondark} showsHorizontalScrollIndicator={false}>

            <View style={{ marginLeft: 15 }}></View>
            <View style={this.state.name == 'equity' ? styles.btnViewActive : this.state.value == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.equity()}><Text style={this.state.name == 'equity' ? styles.textActive : this.state.value == '0' ? styles.textinActive : styles.textinActiveDark}> Equity </Text></TouchableOpacity></View>
            <View style={this.state.value == '0' ? styles.gaplight : styles.gapdark}></View>
            <View style={this.state.name == 'derivative' ? styles.btnViewActive : this.state.value == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.derivative()}><Text style={this.state.name == 'derivative' ? styles.textActive : this.state.value == '0' ? styles.textinActive : styles.textinActiveDark}> Derivatives </Text></TouchableOpacity></View>
            <View style={this.state.value == '0' ? styles.gaplight : styles.gapdark}></View>

            <View style={this.state.name == 'mf' ? styles.btnViewActive : this.state.value == '0' ? styles.btnViewinactive : styles.btnViewinactivedark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.mf()}><Text style={this.state.name == 'mf' ? styles.textActive : this.state.value == '0' ? styles.textinActive : styles.textinActiveDark}> MF/ETFS </Text></TouchableOpacity></View>
            <View style={this.state.value == '0' ? styles.gaplight : styles.gapdark}></View>

            <View style={this.state.name == 'debt' ? styles.btnViewactiveLast : this.state.value == '0' ? styles.btnViewinactiveLast : styles.btnViewinactiveLastdark}><TouchableOpacity activeOpacity={0.5} onPress={() => this.debt()}><Text style={this.state.name == 'debt' ? styles.textActivelast : this.state.value == '0' ? styles.textinActivelast : styles.textinActivelasDark}> Debt/Others </Text></TouchableOpacity></View>
            <View style={this.state.value == '0' ? styles.gaplight : styles.gapdark}></View>

          </ScrollView>

          <View style={this.state.name == 'equity' ? styles.showequity : styles.hide} >
            <Row style={{ marginLeft: 16, marginRight: 16, }}>
              <Col style={this.state.value == '0' ? styles.lightsearch : styles.darksearch}><Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 22, height: 23, margin: 12 }} /></Col>
              <Col style={this.state.value == '0' ? styles.lightsearch1 : styles.darksearch1}><TextInput placeholder="Security Name / Id / Code" value={this.state.val} placeholderTextColor={this.state.value == '0' ? "black" : "#72a3bf"} onChangeText={(e) => this.chng(e)} style={this.state.value == '0' ? styles.lightsearchtext : styles.darksearchtext} /></Col>
              <Col style={this.state.closeicon == false ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voice()}><Icon name={'ios-mic'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
              <Col style={this.state.closeicon == true ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.close()}><Icon name={'ios-close'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>

            </Row>
          </View>
          <View style={this.state.name == 'derivative' ? styles.showequity : styles.hide} >
            <Row style={{ marginLeft: 16, marginRight: 16, }}>
              <Col style={this.state.value == '0' ? styles.lightsearch : styles.darksearch}><Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 22, height: 23, margin: 12 }} /></Col>
              <Col style={this.state.value == '0' ? styles.lightsearch1 : styles.darksearch1}><TextInput placeholder="Enter Underlying Index Name" value={this.state.valderivative} placeholderTextColor={this.state.value == '0' ? "black" : "#72a3bf"} style={this.state.value == '0' ? styles.lightsearchtext : styles.darksearchtext} onChangeText={(e) => this.chngderivative(e)} /></Col>
              <Col style={this.state.closeiconderivative == false ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voice()}><Icon name={'ios-mic'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
              <Col style={this.state.closeiconderivative == true ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.closederivative()}><Icon name={'ios-close'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
            </Row>

          </View>
          <View style={this.state.name == 'mf' ? styles.showequity : styles.hide} >
            <Row style={{ marginLeft: 16, marginRight: 16, }}>
              <Col style={this.state.value == '0' ? styles.lightsearch : styles.darksearch}><Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 22, height: 23, margin: 12 }} /></Col>
              <Col style={this.state.value == '0' ? styles.lightsearch1 : styles.darksearch1}><TextInput placeholder="Enter Scrip Name/Id/Code" value={this.state.valmf} placeholderTextColor={this.state.value == '0' ? "black" : "#72a3bf"} style={this.state.value == '0' ? styles.lightsearchtext : styles.darksearchtext} onChangeText={(e) => this.chngmf(e)} /></Col>
              <Col style={this.state.closeiconmf == false ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voice()}><Icon name={'ios-mic'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
              <Col style={this.state.closeiconmf == true ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.closemf()}><Icon name={'ios-close'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
            </Row>
          </View>
          <View style={this.state.name == 'debt' ? styles.showequity : styles.hide} >
            <Row style={{ marginLeft: 16, marginRight: 16, }}>
              <Col style={this.state.value == '0' ? styles.lightsearch : styles.darksearch}><Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 22, height: 23, margin: 12 }} /></Col>
              <Col style={this.state.value == '0' ? styles.lightsearch1 : styles.darksearch1}><TextInput placeholder="Enter Scrip Name/Id/Code" value={this.state.valdebt} placeholderTextColor={this.state.value == '0' ? "black" : "#72a3bf"} style={this.state.value == '0' ? styles.lightsearchtext : styles.darksearchtext} onChangeText={(e) => this.chngdebt(e)} /></Col>
              <Col style={this.state.closeicondebt == false ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.voice()}><Icon name={'ios-mic'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
              <Col style={this.state.closeicondebt == true ? this.state.value == '0' ? styles.close : styles.closedark : styles.hide}><TouchableOpacity activeOpacity={0.5} onPress={() => this.closedebt()}><Icon name={'ios-close'} style={styles.closeiconclass} size={30} color={'#aaafba'} /></TouchableOpacity></Col>
            </Row>
          </View>
        </View>

        <ScrollView style={this.state.name == 'equity' ? styles.show : styles.hide}>

          <View style={this.state.showloader == true ? styles.show : styles.hide}>
            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />
          </View>

          <View style={this.state.Equity == '' ? styles.hide : styles.show}>

            <FlatList
              data={this.state.Equity}
              style={{ height: '100%', bottom: 12 }}
              renderItem={({ item }) =>
                <Animatable.View animation="fadeInUp">
                  <Row style={{ marginLeft: 16, marginRight: 16, marginTop: 20, }}>
                    <Col style={item.split(',')[0] != '' ? this.state.value == '0' ? styles.list : styles.listdark : styles.hide}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split(',')[2]}</Text><Text style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{item.split(',')[0]} | {item.split(',')[1]}</Text></Col>
                  </Row>
                </Animatable.View>

              }
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>

        <ScrollView style={this.state.name == 'derivative' ? styles.show : styles.hide}>

          <View style={this.state.showloaderderivative == true ? styles.show : styles.hide}>
            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />
          </View>


          <View style={this.state.Derivative == '' ? styles.hide : styles.show}>
            <FlatList
              data={this.state.Derivative}
              style={{ height: '100%', bottom: 12 }}
              renderItem={({ item }) =>
                <Animatable.View animation="fadeInUp">
                  <Row style={{ marginLeft: 16, marginRight: 16, marginTop: 20, }}>
                    <Col style={item.split(',')[0] != '' ? this.state.value == '0' ? styles.list : styles.listdark : styles.hide}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split(',')[2]}</Text><Text style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1} >{item.split(',')[0]} | {item.split(',')[1]}</Text></Col>
                  </Row>
                </Animatable.View>

              }
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>

        <ScrollView style={this.state.name == 'mf' ? styles.show : styles.hide}>

          <View style={this.state.showloadermf == true ? styles.show : styles.hide}>
            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />
          </View>


          <View style={this.state.mf == '' ? styles.hide : styles.show}>
            <FlatList
              data={this.state.mf}
              style={{ height: '100%', bottom: 12 }}
              renderItem={({ item }) =>
                <Animatable.View animation="fadeInUp">
                  <Row style={{ marginLeft: 16, marginRight: 16, marginTop: 20, }}>
                    <Col style={item.split(',')[0] != '' ? this.state.value == '0' ? styles.list : styles.listdark : styles.hide}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split(',')[2]}</Text><Text style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1}>{item.split(',')[0]} | {item.split(',')[1]}</Text></Col>
                  </Row>
                </Animatable.View>

              }
              keyExtractor={item => item.id}
            />
          </View>

        </ScrollView>

        <ScrollView style={this.state.name == 'debt' ? styles.show : styles.hide}>

          <View style={this.state.showloaderdebt == true ? styles.show : styles.hide}>
            <ActivityIndicator size="large" style={{ marginTop: 20 }} color={this.state.value == '0' ? "#0000ff" : "#72a3bf"} />
          </View>


          <View style={this.state.debt == '' ? styles.hide : styles.show}>
            <FlatList
              data={this.state.debt}
              style={{ height: '100%', bottom: 12 }}
              renderItem={({ item }) =>
                <Animatable.View animation="fadeInUp">
                  <Row style={{ marginLeft: 16, marginRight: 16, marginTop: 20, }}>
                    <Col style={item.split(',')[0] != '' ? this.state.value == '0' ? styles.list : styles.listdark : styles.hide}><Text style={this.state.value == '0' ? styles.lighttext : styles.darktext} numberOfLines={1}>{item.split(',')[2]}</Text><Text style={this.state.value == '0' ? styles.lighttext1 : styles.darktext1} >{item.split(',')[0]} | {item.split(',')[1]}</Text></Col>
                  </Row>
                </Animatable.View>

              }
              keyExtractor={item => item.id}
            />
          </View>

        </ScrollView>

        <Popover
          isVisible={this.state.isVisible}
          fromView={this.touchable}
          arrowStyle={true}
          showArrow={true}
          onRequestClose={() => this.closePopover()}>
          <View style={{ backgroundColor: 'white', height: '100%' }}>

            <Text></Text>
            <Row>
              <Col style={{ width: '20%', height: 50, }}><IconEnt name="setting" style={{ textAlign: 'center' }} color={'black'} size={23}></IconEnt></Col>
              <Col style={{ width: '80%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 20, bottom: 3, color: 'black', }}>{I18n.t('setting')}</Text></Col>
            </Row>
            <Row>
              <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
              <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' }}> {this.state.themename} {I18n.t('theme')} </Text></Col>
              <Col style={{ width: '40%', height: 50, }}>
                <Switch
                  trackColor="black"
                  trackColor="grey"
                  value={this.state.switchval}
                  style={{ bottom: 3, alignSelf: 'center' }}
                  onValueChange={(e) => this.toggleSwitch(e)}
                />
              </Col>
            </Row>
            <Row>
              <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
              <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' }}> {I18n.t('edithome')} </Text></Col>
              <Col style={{ width: '40%', height: 50, }}><Icon name="ios-arrow-dropright-circle" onPress={() => this.edit()} style={{ textAlign: 'center' }} color={'black'} size={25} /></Col>
            </Row>
            <Row>
              <Col style={{ width: '20%', height: 50, }}><Icon name="ios-arrow-forward" style={{ textAlign: 'center' }} color={'black'} size={20} ></Icon></Col>
              <Col style={{ width: '40%', height: 50, }}><Text style={{ textAlign: 'left', fontSize: 16, bottom: 2, color: 'black' }}> {I18n.t('changelanguage')}</Text></Col>
              <Col style={{ width: '5%', height: 50, }}></Col>
              <Col style={{ width: '35%', height: 50 }}>
                <Picker
                  selectedValue={this.state.selectlan}
                  style={{ height: 30, bottom: 5, width: '100%', }}
                // onValueChange={(itemValue) => this.chnglan(itemValue)}
                >
                  <Picker.Item label="English" value="English" />
                  <Picker.Item label="Hindi" value="Hindi" />
                  <Picker.Item label="Marathi" value="Marathi" />
                  <Picker.Item label="Gujrati" value="Gujrati" />
                  <Picker.Item label="Bengali" value="Bengali" />
                  <Picker.Item label="Malayalam" value="Malayalam" />
                  <Picker.Item label="Oriya" value="Oriya" />
                  <Picker.Item label="Tamil" value="Tamil" />
                </Picker>
              </Col>
            </Row>

          </View>

        </Popover>
        <Modal
          animationType="slide"
          transparent={false}
          style={{ height: '100%' }}
          visible={this.state.sidemenu}
          onRequestClose={() => {
            this.setState({ sidemenu: false });
          }}
        >
          <View style={this.state.value == '0' ? styles.lightmenu : styles.darkmenu}>
            <Row style={{ marginLeft: 20, marginTop: 10 }}>
              <Col style={{ width: '67%', height: 40 }}><Icon name="ios-close" onPress={() => this.closesidemenu()} style={{ marginLeft: 10 }} size={40} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>
              <Col style={{ width: '15%', height: 40 }}><IconEvil name="user" style={{ marginTop: 3, textAlign: 'right' }} size={35} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>
              <Col style={{ width: '15%', height: 40, }}><Icon name="ios-settings" style={{ marginRight: 5, marginTop: 2, textAlign: 'right' }} size={30} color={this.state.value == '0' ? '#132144' : '#72a3bf'} /></Col>
            </Row>

            <ScrollView style={{ top: '8%', }}>
              <Image style={{ width: 150, height: 150, alignSelf: 'center' }} source={this.state.value == '0' ? require('../images/lightprofile.png') : require('../images/darkprofile.png')} />
              {/* <Text style={{textAlign:'center',marginTop: 15,fontSize:20,color: '#132144', fontFamily: 'SegoeProDisplay-Regular',}}>Vrushabh Mendhe</Text> */}
              <Text></Text>
              <View style={{}}>
                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Home</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.home()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.equityView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Equity</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconname} onPress={() => this.equityView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.equity == false ? styles.hide : styles.show}>
                  <Text onPress={() => this.equitypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GAINERS</Text>
                  <Text onPress={() => this.equitypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>LOSERS</Text>
                  <Text onPress={() => this.equitypage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TOP TURNOVER</Text>
                  <Text onPress={() => this.equitypage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK HIGH</Text>
                  <Text onPress={() => this.equitypage(4)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>52 WK LOW</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Indices</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.indices()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}></Text></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.sensexView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Sensex</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamesensex} onPress={() => this.sensexView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.sensex == false ? styles.hide : styles.show}>
                  <Text onPress={() => this.settingpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>SECURITY</Text>
                  <Text onPress={() => this.settingpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>OVERVIEW</Text>
                  <Text onPress={() => this.settingpage(2)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>TURNOVER</Text>
                  <Text onPress={() => this.settingpage(3)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CONTRIBUTION</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.smeView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >SME</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamesme} onPress={() => this.smeView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.sme == false ? styles.hide : styles.show}>
                  <Text onPress={() => this.smepage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                  <Text onPress={() => this.smepage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET STATISTICS</Text>
                  <Text></Text>
                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.derivativeView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext} >Derivatives</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamederivative} onPress={() => this.derivativeView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.derivative == false ? styles.hide : styles.show}>

                  <Text onPress={() => this.derivativepage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                  <Text onPress={() => this.derivativepage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.currencyView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Currency</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecurrency} onPress={() => this.currencyView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.currencytab == false ? styles.hide : styles.show}>
                  <Text onPress={() => this.currencypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                  <Text onPress={() => this.currencypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.commadityView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Commadity</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecommodity} onPress={() => this.commadityView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.commoditytab == false ? styles.hide : styles.show}>

                  <Text onPress={() => this.commoditypage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                  <Text onPress={() => this.commoditypage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.irdView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IRD</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnameird} onPress={() => this.irdView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.ird == false ? styles.hide : styles.show}>
                  <Text onPress={() => this.irdpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET WATCH</Text>
                  <Text onPress={() => this.irdpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.etfView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>ETF</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnameetf} onPress={() => this.etfView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.etf == false ? styles.hide : styles.show}>
                  <Text onPress={() => this.etfpage(0)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ETF WATCH</Text>
                  <Text onPress={() => this.etfpage(1)} style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>MARKET SUMMARY</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.debtView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Debt</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamedebt} onPress={() => this.debtView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.debt == false ? styles.hide : styles.show}>

                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>DEBT MARKET SUMMARY</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BONDS-OTC TRADES</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL CORP. DEBT</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>GSEC</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RETAIL GOV BONDS</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>CORPORATE BOND-NDS-RST</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>EBP</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text onPress={() => this.corporateView()} style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Corporates</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}><Icon name={this.state.iconnamecorporate} onPress={() => this.corporateView()} style={{ textAlign: 'right', marginRight: 20, marginTop: 6 }} color={this.state.value == '0' ? '#132144' : '#72a3bf'} size={25} /></Col>
                </Row>
                <View style={this.state.corporate == false ? styles.hide : styles.show}>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ANNOUNCEMENTS</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>ACTIONS</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>RESULTS CALENDAR</Text>
                  <Text style={this.state.value == '0' ? styles.lightmenusubtext : styles.darkmenusubtext}>BOARD MEETINGS</Text>
                  <Text></Text>

                </View>
                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Statistics</Text></Col>
                  <Col style={{ width: '30%', height: 40 }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '70%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Market Turnover</Text></Col>
                  <Col style={{ width: '30%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPO/OFS</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Listings</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Notices</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Watchlist</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>Portfolio</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>

                <Row style={{ marginLeft: 16, marginRight: 16 }}>
                  <Col style={{ width: '50%', height: 40, }}><Text style={this.state.value == '0' ? styles.lightmenutext : styles.darkmenutext}>IPF</Text></Col>
                  <Col style={{ width: '50%', height: 40, }}></Col>
                </Row>

                <Text style={this.state.value == '0' ? styles.lightline : styles.darkline}></Text>
                <Text></Text>
                <Text style={{ marginLeft: 80, fontSize: 20, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', }}></Text>
              </View>
            </ScrollView>
          </View>
        </Modal>


        <Footer>

          <FooterTab style={this.state.value == '0' ? styles.tablight : styles.tabdark}>
            <Button style={styles.inactive} onPress={() => this.tab('watch')}>
              <Image source={this.state.value == '0' ? require('../images/watchlisticon.png') : require('../images/watchlisticon_forblack.png')} style={{ width: 26, height: 28, }} />
              <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('watchtab')} </Text>
            </Button>


            <Button style={styles.inactive} onPress={() => this.tab('port')}>
              <Image source={this.state.value == '0' ? require('../images/portfolioicon.png') : require('../images/portfolioicon_forblack.png')} style={{ width: 26, height: 28, }} />

              <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('porttab')}</Text>
            </Button>

            <Button style={styles.active} onPress={() => this.tab('home')}>
              <Image source={require('../images/bsemenuicon.png')} style={{ width: 23, height: 24, bottom: 2 }} />
              <Text style={styles.tabnamelighthome} numberOfLines={1}>{I18n.t('home')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('search')}>
              <Image source={this.state.value == '0' ? require('../images/searchicon.png') : require('../images/searchicon_for-black.png')} style={{ width: 26, height: 27, }} />
              <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('search')}</Text>
            </Button>
            <Button style={styles.inactive} onPress={() => this.tab('more')}>
              <Image source={this.state.value == '0' ? require('../images/moreicon.png') : require('../images/moreicon_forblack.png')} style={{ width: 26, height: 28, }} />

              <Text style={this.state.value == '0' ? styles.tabnamelight : styles.tabnamedark} numberOfLines={1}>{I18n.t('menu')}</Text>
            </Button>


          </FooterTab>
        </Footer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  lightdate: { bottom: 28, color: '#132144', fontSize: 14, textAlign: 'right', marginRight: 5, },
  darkdate: { bottom: 28, color: '#72a3bf', fontSize: 14, textAlign: 'right', marginRight: 5, },
  statuslight: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#132144' },
  statusdark: { fontFamily: 'SegoePro-Bold', fontSize: 15, color: '#72a3bf' },
  mainviewlight: { backgroundColor: '#f6f7f9', height: '100%' },
  mainviewdark: { backgroundColor: '#0b0b0b', height: '100%' },
  btnViewActive: { borderRadius: 5, backgroundColor: '#2087c9', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactive: { borderRadius: 5, backgroundColor: 'white', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactiveLast: { borderRadius: 5, backgroundColor: 'white', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactivedark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 97, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewinactiveLastdark: { borderRadius: 5, backgroundColor: '#1a1f1f', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  btnViewactiveLast: { borderRadius: 5, backgroundColor: '#2087c9', width: 120, height: 40, shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 7, marginTop: 5, },
  textActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'SegoePro-Semibold', color: 'white', fontSize: 13 },
  textinActive: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'SegoePro-Semibold', color: '#132144', fontSize: 13 },
  textinActiveDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'SegoePro-Semibold', color: '#72a3bf', fontSize: 13 },
  textActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'SegoePro-Semibold', color: 'white', fontSize: 13 },
  textinActivelast: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'SegoePro-Semibold', color: '#132144', fontSize: 13 },
  textinActivelasDark: { margin: 9, top: 2, textAlign: 'center', fontFamily: 'SegoePro-Semibold', color: '#72a3bf', fontSize: 13 },
  show: { display: 'flex' },
  showequity: { display: 'flex', bottom: 60 },
  hide: { display: 'none' },
  list: { width: '100%', height: 58, backgroundColor: 'white', borderRadius: 5, },
  listdark: { width: '100%', height: 58, backgroundColor: '#1a1f1f', borderRadius: 5, },
  closeiconclass: { textAlign: 'center', marginTop: 9 },
  close: { width: '15%', height: 45, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  closedark: { width: '15%', height: 45, backgroundColor: '#1a1f1f', borderTopRightRadius: 5, borderBottomRightRadius: 5 },
  active: { backgroundColor: '#2087c9', height: 68, marginLeft: 19, marginRight: 19, borderRadius: 3 },
  inactive: { padding: 12, },
  tablight: { backgroundColor: 'white', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabdark: { backgroundColor: '#1a1f1f', shadowOpacity: 0.9, shadowOffset: { width: 0, height: 0, }, shadowRadius: 15, elevation: 11, },
  tabnamelight: { color: '#7a878f', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamedark: { color: '#79868e', fontSize: 10.5, fontFamily: 'SegoeProDisplay-Regular' },
  tabnamelighthome: { color: '#f6f7f9', fontSize: 11, fontFamily: 'SegoeProDisplay-Regular' },
  buttonlight: { bottom: 74, height: 0, marginLeft: 4, backgroundColor: '#f6f7f9' },
  buttondark: { bottom: 74, height: 0, marginLeft: 4, backgroundColor: '#0b0b0b' },
  gaplight: { backgroundColor: '#f6f7f9', width: 10, height: 70, bottom: 10 },
  gapdark: { backgroundColor: '#0b0b0b', width: 10, height: 70, bottom: 10 },
  titlelight: { color: '#132144', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
  titledark: { color: '#72a3bf', fontSize: 19, fontFamily: 'SegoePro-Bold', marginLeft: 15 },
  lightsearch: { width: '15%', height: 45, backgroundColor: 'white', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  darksearch: { width: '15%', height: 45, backgroundColor: '#1a1f1f', borderTopLeftRadius: 5, borderBottomLeftRadius: 5 },
  lightsearch1: { width: '70%', height: 45, backgroundColor: 'white', },
  darksearch1: { width: '70%', height: 45, backgroundColor: '#1a1f1f', },
  lightsearchtext: { fontSize: 15, top: 2, fontFamily: 'SegoeProDisplay-Regular', color: '#132144' },
  darksearchtext: { fontSize: 15, top: 2, fontFamily: 'SegoeProDisplay-Regular', color: '#72a3bf' },
  lighttext: { marginLeft: 9, marginTop: 9, color: '#132144', fontFamily: 'SegoeProDisplay-Regular' },
  darktext: { marginLeft: 9, marginTop: 9, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular' },
  lighttext1: { marginLeft: 9, marginTop: 1, color: '#aaafba', fontFamily: 'SegoeProDisplay-Regular' },
  darktext1: { marginLeft: 9, marginTop: 1, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular' },
  lightmenu: { backgroundColor: '#f1f2f6', height: '100%' },
  darkmenu: { backgroundColor: '#0b0b0b', height: '100%' },
  lightmenutext: { marginLeft: 15, fontSize: 20, color: '#132144', fontFamily: 'SegoePro-Bold', },
  darkmenutext: { marginLeft: 15, fontSize: 20, color: '#72a3bf', fontFamily: 'SegoePro-Bold', },
  lightmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#132144', fontFamily: 'SegoeProDisplay-Regular', },
  darkmenusubtext: { marginLeft: 40, marginTop: 13, fontSize: 16, color: '#72a3bf', fontFamily: 'SegoeProDisplay-Regular', },
  lightline: { marginLeft: 30, borderBottomColor: '#132144', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
  darkline: { marginLeft: 30, borderBottomColor: '#72a3bf', borderBottomWidth: 1, marginRight: 25, bottom: 10 },
})

export default Search;
