

class API {
    gain = [];
    lose = [];
    turn = [];
    high = [];
    low = [];
    marketstatic = [];
    indicesData = [];
    watchdata = [];
    smewatchdata = [];
    marketturn = [];
    ipo = [];
    TurnArray = [];
    domain1='https://api.bseindia.com/bseindia/api';

    //Home ---------------
    date() {
        return fetch(this.domain1+'/Sensex/getSensexData?json={"name":"AppSensex","fields":"2,3,4,5,6,7"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }

    indices(lan) {
        switch (lan) {
            case 'en':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"en","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'hn':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"hn","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'mr':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"mr","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'gj':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"gj","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'bn':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"bn","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'ml':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"ml","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'or':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"or","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'tml':
                return fetch(this.domain1+'/Indexmasternew/GetData?json={"flag":"","ln":"tml","pg":"1","cnt":"15","fields":"1,2,3,4,5,6","hmpg":"1"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
        }

    }

    currency() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CurrHeatMap/m?Flag=9&Records=1000&scripcode=0&pg=2&cnt=5&field=CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        });
    }

    commodity() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CommHeatMap/m?Flag=8&Records=1000&scripcode=0&pg=1&cnt=6&field=CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        });

    }

    sensexdetail(cd, lan) {

        switch (lan) {
            case 'en':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'hn':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'mr':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'gj':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'bn':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'ml':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'or':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'tml':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
        }
    }

    watchlist(cd, lan) {

        switch (lan) {
            case 'en':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'hn':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"hn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'mr':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"mr","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'gj':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"gj","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'bn':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"bn","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'ml':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"ml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'or':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"or","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
            case 'tml':
                return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + cd + ',"ln":"tml","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
                    return responsejson;
                });
                break;
        }
    }

    detailwatch(scode, lan) {
        return fetch('https://api.bseindia.com/msource/watchlistandroid.aspx?scripsid=' + scode + '&flag=1&ln=' + lan).then((response) => response.text()).then((responsejson) => {
            return responsejson;
        });
    }
    //----------------------------------------

    // Equity------------------------------------

    equityDate(encrypted) {
        // return fetch('https://api.bseindia.com/msource/SensexData.aspx').then((response) => response.text()).then((responsejson) => {
        //     return responsejson;
        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndicesSensexData/w', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data ;
            })
    }

    gainer(lan, encrypted) {
        this.gain = [];

        // return fetch('https://api.bseindia.com/msource/gainersAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     return fetch('https://api.bseindia.com/msource/losersAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson1) => {
        //         for (let i = 0; i < responsejson.split('#').length; i++) {
        //             this.gain.push({ 'id': responsejson.split('#')[i].split(',')[0], 'change': responsejson.split('#')[i].split(',')[1], 'value1': responsejson.split('#')[i].split(',')[2], 'value2': responsejson.split('#')[i].split(',')[3], 'name1': responsejson.split('#')[i].split(',')[4], 'name2': responsejson1.split('#')[i].split(',')[4] });
        //         }
        //         return this.gain;

        //     });
        // });
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/NewGainersAndroid/w?ln=' + lan, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
    }
    loser(lan, encrypted) {
        this.lose = [];

        // return fetch('https://api.bseindia.com/msource/losersAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     return fetch('https://api.bseindia.com/msource/turnoverAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson1) => {
        //         for (let i = 0; i < responsejson.split('#').length; i++) {

        //             this.lose.push({ 'id': responsejson.split('#')[i].split(',')[0], 'change': responsejson.split('#')[i].split(',')[1], 'value1': responsejson.split('#')[i].split(',')[2], 'value2': responsejson.split('#')[i].split(',')[3], 'name1': responsejson.split('#')[i].split(',')[4], 'name2': '' });


        //         }
        //         return this.lose;

        //     });

        // });
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/NewLosersAndroid/w?ln=' + lan, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
    }

    turnover(lan, encrypted) {
        this.turn = [];

        // return fetch('https://api.bseindia.com/msource/turnoverAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     return fetch('https://api.bseindia.com/msource/get52highlow.aspx?flag=0&ln=' + lan).then((response) => response.text()).then((responsejson1) => {
        //         for (let i = 0; i < responsejson.split('#').length; i++) {
        //             this.turn.push({ 'per': responsejson.split('#')[i].split(',')[2], 'id': responsejson.split('#')[i].split(',')[0], 'change': responsejson.split('#')[i].split(',')[1], 'value': responsejson.split('#')[i].split(',')[5], 'name1': responsejson.split('#')[i].split(',')[6], 'name2': responsejson1.split('#')[i].split('@')[1] });
        //         }
        //         return this.turn;

        //     });

        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/NewTurnoverAndroid/w?ln=' + lan, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
    }

    highapi(lan, encrypted) {
        this.high = [];
        // return fetch('https://api.bseindia.com/msource/get52highlow.aspx?flag=0&ln=' + lan).then((response) => response.text()).then((responsejson) => {
        //     for (let i = 0; i < responsejson.split('#').length; i++) {
        //         this.high.push({ 'id': responsejson.split('#')[i].split('@')[0], 'name': responsejson.split('#')[i].split('@')[1], 'high': responsejson.split('#')[i].split('@')[2], 'ltp': responsejson.split('#')[i].split('@')[3] });
        //     }
        //     return this.high;

        // });
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/NewHighLow52Week/w?ln=' + lan + '&flag=0', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
    }

    lowapi(lan, encrypted) {
        this.low = [];

        // return fetch('https://api.bseindia.com/msource/get52highlow.aspx?flag=1&ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     for (let i = 0; i < responsejson.split('#').length; i++) {
        //         this.low.push({ 'id': responsejson.split('#')[i].split('@')[0], 'change': responsejson.split('#')[i].split('@')[2], 'ltp': responsejson.split('#')[i].split('@')[3], 'name': responsejson.split('#')[i].split('@')[1], });
        //     }
        //     return this.low;;

        // });
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/NewHighLow52Week/w?ln=' + lan + '&flag=1', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })

    }

    //---------------------------------------------

    //Indices-------------------------------------

    sideMenuIndices(lan, encrypted) {
        this.indicesData = [];

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndexmoversAndroid/w?ln=' + lan, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })

        // return fetch('https://api.bseindia.com/msource/indexmoversAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     for (let i = 0; i < responsejson.split('#').length; i++) {
        //         this.indicesData.push({ 'name': responsejson.split('#')[i].split(',')[0], 'value': responsejson.split('#')[i].split(',')[1], 'change': responsejson.split('#')[i].split(',')[2], 'changeper': responsejson.split('#')[i].split(',')[3], 'id': responsejson.split('#')[i].split(',')[5] });
        //     }
        //     return this.indicesData;
        // })
    }
    //-------------------------------------------

    //Market Statistic------------------------

    statistic(lan, encrypted) {
        this.marketstatic = [];

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/MarketStatAP/w?ln=' + lan, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
        // return fetch('https://api.bseindia.com/msource/getMarketStatAP.aspx?ln=' + lan).then((response) => response.text()).then((responsejson) => {
        //     for (let i = 0; i < responsejson.split('#').length; i++) {
        //         this.marketstatic.push({ 'static': responsejson.split('#')[i].split('@')[0], 'value': responsejson.split('#')[i].split('@')[1] });
        //     }
        //     return this.marketstatic;

        // })
    }
    //

    //Commodity-----------------------------------
    commodityDate() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/COMMMarketSTATUS/m').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        });
    }
    watch() {
        this.watchdata = [];
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CommMarketWatch/m?Flag=1&Records=10000&scripcode=0&ln=en').then((response) => response.json()).then((responsejson) => {


            for (let i = 0; i < responsejson.length; i++) {
                this.watchdata.push({ 'name': responsejson[i].Contract, 'trd': responsejson[i].TrdContracts, 'ltp': responsejson[i].LTP, 'change': responsejson[i].CHANGE, 'changeper': responsejson[i].ChangePerc, 'buy': responsejson[i].BuyPrice, 'buyquantity': responsejson[i].BuyQty, 'sell': responsejson[i].sellPrice, 'sellquantity': responsejson[i].sellQty, });
            }
            return this.watchdata;

        });
    }

    summary() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CommMarketSummary/m').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        });
    }
    //

    //Corporate----------------------------------
    announcement(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/getAnnAp.aspx?flag=3').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/AnnAp/w?ln=' + lan + '&flag=3', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })


    }
    action(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/getAnnAp.aspx?flag=0&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');
        // })
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/AnnAp/w?ln=' + lan + '&flag=0', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })

    }
    result(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/getAnnAp.aspx?flag=2&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');
        // })
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/AnnAp/w?ln=' + lan + '&flag=1', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }
    board(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/getAnnAp.aspx?flag=2&ln=' + lan).then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');
        // })
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/AnnAp/w?ln=' + lan + '&flag=2', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    //----------------------------------------------

    //currency------------------------------
    currencyDate() {
        return fetch('https://api.bseindia.com/msource/currencymostActive.aspx?flag=1&ln=en').then((response) => response.text()).then((responsejson) => {
            return responsejson;
        });
    }
    currencywatch(encrypted) {
        // return fetch('https://api.bseindia.com/msource/currencymostActive.aspx').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('@');
        // });
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/CurrencyMostActive/w?', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
    }
    currencysummary(encrypted) {
        // return fetch('https://api.bseindia.com/msource/currderivativewatch.aspx?flag=1&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('@');
        // });
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/CurrDerivativeWatch/w?ln=en&flag=1', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })
    }
    //--------------------------------------

    //Debt----------------------------------

    debtSummary() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/DebtMarketSummary/w?RCount=10&ln=en').then(response => response.json()).then((responsejson) => {
            return responsejson._ObjMktSummary
        });
    }
    debtCorpbond() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CorpBonds/w?flag=1&RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson._CorporateBonds
        });
    }
    debtReailcorp() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CorpEOD/w?value=ALL&flags=D&RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson._ObjCorporateEODDataList
        });
    }
    debtgsec() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/DebtWholeSale/w?RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson._objDebt_wholesale
        });
    }
    debtRetailgov() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/DebtRetail/w?group=ALL&flag=C&RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson.lstDebtRetails
        });
    }
    debtCorpbondnds() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/CorpBondsNDS-RST/w?RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson._CorporateBonds
        });
    }
    debtebp() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/EBPDebt/w?RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson._ObjEBP
        });
    }
    debtdetail(name, id, oeid, issuename) {
        if (name == 'corp') {
            return fetch('https://api.bseindia.com/BseIndiaAPI/api/CorpBondScriptWiseTradeReport/w?Scrip_ID=' + id + '&OEID=&Flag=1&RCount=1&ln=en').then((response) => response.json()).then((responsejson) => {
                return responsejson._objTradeDetails;
            });
        }
        else if (name == 'gsec') {
            return fetch('https://api.bseindia.com/BseIndiaAPI/api/GSecTradeReport/w?scripcode=' + id + '&RCount=1&ln=en').then((response) => response.json()).then((responsejson) => {
                return responsejson;

            });
        }
        else {
            return fetch('https://api.bseindia.com/BseIndiaAPI/api/ScriptTradeReportNDS-RST/w?Scrip_ID=' + id + '&isin_no=' + issuename + '&cmpbid=' + oeid + '&RCount=10&ln=en').then((response) => response.json()).then((responsejson) => {
                return responsejson;

            });
        }
    }
    //--------------------------------------

    //Derivative------------------------------

    derivativeDate() {
        return fetch('https://api.bseindia.com/msource/getderivative.aspx?id=3&ln=en').then((response) => response.text()).then((responsejson) => {
            return responsejson;
        });
    }
    derivativewatch(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/getDerivative.aspx?id=1&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');

        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/Derivative/w?ln=' + lan + '&id=1', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
                // console.log('equity data', data);
            })

    }
    derivativesummary(lan, encrypted) {
        return fetch('https://api.bseindia.com/msource/frmDerivatives.aspx?id=2&ln=' + lan).then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');

        });

        // return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/Derivative/w?ln=en&id=2', {
        //     method: 'GET',
        //     headers: {
        //         'Sdt': encrypted
        //     }
        // })
        //     .then(response => response.json())
        //     .then(data => {
        //         return data;
        //         // console.log('equity data', data);
        //     })
    }

    //--------------------------------------

    //ETF---------------------------------

    etfwatch() {
        return fetch(this.domain1+'/ETFmarketwatch?$format=json&cnt=15&fields=1,2,4,7,10,22,24,25,26,27,28&ln=en').then((response) => response.json()).then((responsejson) => {

            return responsejson;

        });
    }
    etfsummary() {
        return fetch(this.domain1+'/ETFsummary?$format=json&cnt=4&fields=2,4,5&ln=en').then((response) => response.json()).then((responsejson) => {
            return responsejson;

        });
    }
    //------------------------------------

    //IRD---------------------------------

    irdwatch(encrypted) {
        // return fetch('https://api.bseindia.com/msource/currderivativewatch.aspx?flag=2&ln=en').then((response) => response.text()).then((responsejson) => {

        //     return responsejson.split('@');

        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/CurrDerivativeWatch/w?ln=en&flag=2', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }
    irdsummary(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/currderivativewatch.aspx?flag=3&ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     return responsejson.split('@');

        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/CurrDerivativeWatch/w?ln=' + lan + '&flag=3', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }
    //------------------------------------

    //SME-----------------------

    smewatch(encrypted) {
        // return fetch('https://api.bseindia.com/msource/SMEMarketWatch.aspx?flag=2&ln=en').then((response) => response.text()).then((responsejson) => {
        //     for (let i = 0; i < responsejson.split('#').length; i++) {
        //         this.smewatchdata.push({ 'id': responsejson.split('#')[i].split('@')[0], 'trd': responsejson.split('#')[i].split('@')[7], 'ltp': responsejson.split('#')[i].split('@')[2], 'change': responsejson.split('#')[i].split('@')[5], 'percent': responsejson.split('#')[i].split('@')[6], 'name1': responsejson.split('#')[i].split('@')[1], });
        //     }
        //     return this.smewatchdata;
        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/SMEMarketWatch/w?ln=en&flag=2', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }
    smesummary(lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/SMEMarketWatch.aspx?flag=3&ln=' + lan).then((response) => response.text()).then((responsejson) => {

        //     return responsejson.split('#');

        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/SMEMarketWatch/w?ln=' + lan + '&flag=3', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }
    //--------------------------

    //Market Turn----------------------

    marketturnfun(lan) {
        this.marketturn = [];
        return fetch('https://api.bseindia.com/msource/WSMarketTurnover.aspx?flag=3&ln=' + lan).then((response) => response.text()).then((responsejson) => {
            for (let i = 0; i < responsejson.split('#').length; i++) {
                this.marketturn.push({ 'turnname': responsejson.split('#')[i].split('@')[0], 'volume': responsejson.split('#')[i].split('@')[1], 'oi': responsejson.split('#')[i].split('@')[2], 'to': responsejson.split('#')[i].split('@')[3] });
            }
            return this.marketturn;

        })
    }
    //---------------------------------

    //Listing-------------------------
    listdata(encrypted) {
        // return fetch('https://api.bseindia.com/msource/IOP_OFC_Listing_Json.aspx?flag=list&ln=en').then((response) => response.text()).then((responsejson) => {

        //     return responsejson;

        // });

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IpoOfcListing/w?ln=en&flag=list', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    //--------------------------------

    //ipo----------------------------
    ipodata(lan, encrypted) {
        this.ipo = [];
        // return fetch('https://api.bseindia.com/msource/IOP_OFC_Listing_Json.aspx?flag=OFSIPO&ln=' + lan).then((response) => response.text()).then((responsejson) => {
        //     for (let i = 0; i < responsejson.split('#').length; i++) {
        //         this.ipo.push({ 'mainname': responsejson.split('#')[i].split('@')[0], 'name': responsejson.split('#')[i].split('@')[0] == 'comp' ? responsejson.split('#')[i].split('@')[1] : '', 'pricevalue': responsejson.split('#')[i].split('@')[1], });
        //     }
        //     return this.ipo;

        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IpoOfcListing/w?ln=' + lan + '&flag=OFSIPO', {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {

                for (let i = 0; i < data.data.split('#').length; i++) {
                    this.ipo.push({ 'mainname': data.data.split('#')[i].split('@')[0], 'name': data.data.split('#')[i].split('@')[0] == 'comp' ? data.data.split('#')[i].split('@')[1] : '', 'pricevalue': data.data.split('#')[i].split('@')[1], });
                }
                return this.ipo;
            })
    }
    //-------------------------------

    //sensex--------------------------

    indicesDropDown(lan) {
        return fetch('https://api.bseindia.com/msource/indexmoversAndroid.aspx?ln=' + lan).then((response) => response.text()).then((responsejson) => {

            return responsejson.split('#')
        })
    }
    sensexLtp(id) {
        return fetch('https://api.bseindia.com/msource/Index.aspx?code=' + id).then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');
        })
    }
    security(id, lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/Indicesscriplist.aspx?flag=AP&scripcd=' + id + '&ln=' + lan).then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('@');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndicesScripList/w?ln=' + lan + '&flag=AP&scode=' + id, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {


                return data;
            })
    }
    overview1(id, encrypted) {
        // return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=0&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('@');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndicesScripFilter/w?ln=en&flag=0&scode=' + id, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                console.log(data.Indexdata.split('@'));
                return data.Indexdata.split('@');
            })
    }
    overview2(id, lan, encrypted) {

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndicesScripFilter/w?ln=' + lan + '&flag=3&scode=' + id, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);

                console.log(data.AdvDeclData.split('#'));

                return data.AdvDeclData.split('#');
            })
        // switch (lan) {
        //     case 'en':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'hn':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=hn').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'mr':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=mr').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'gj':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=gj').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'bn':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=bn').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'ml':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=ml').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'or':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=or').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        //     case 'tml':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=3&scripcd=' + id + '&ln=tml').then((response) => response.text()).then((responsejson1) => {
        //             return responsejson1.split('#');
        //         })
        //         break;
        // }
    }
    sensexTurnover(id, lan, encrypted) {
        this.TurnArray = [];
        // switch (lan) {
        //     case 'en':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'hn':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=hn').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'mr':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=mr').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'gj':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=gj').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'bn':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=bn').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'ml':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=ml').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'or':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=or').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        //     case 'tml':
        //         return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=2&scripcd=' + id + '&ln=tml').then((response) => response.text()).then((responsejson) => {
        //             for (let i = 0; i < responsejson.split('#').length; i++) {
        //                 this.TurnArray.push({ 'id': responsejson.split('#')[i].split('@')[0], 'status': responsejson.split('#')[i].split('@')[4], 'to': responsejson.split('#')[i].split('@')[2], 'trd': responsejson.split('#')[i].split('@')[3], 'name1': responsejson.split('#')[i].split('@')[1], });
        //             }
        //             return this.TurnArray;
        //         });
        //         break;
        // }
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndicesScripFilter/w?ln=' + lan + '&flag=2&scode=' + id, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);

                for (let i = 0; i < data.Turnoverwisedata.split('#').length; i++) {
                    this.TurnArray.push({ 'id': data.Turnoverwisedata.split('#')[i].split('@')[0], 'status': data.Turnoverwisedata.split('#')[i].split('@')[4], 'to': data.Turnoverwisedata.split('#')[i].split('@')[2], 'trd': data.Turnoverwisedata.split('#')[i].split('@')[3], 'name1': data.Turnoverwisedata.split('#')[i].split('@')[1], });
                }
                return this.TurnArray;

                //return data;
            })

    }
    contribution(id, lan, encrypted) {
        // return fetch('https://api.bseindia.com/msource/Indicesscripfilter.aspx?flag=4&scripcd=' + id + '&ln=' + lan).then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/IndicesScripFilter/w?ln=' + lan + '&flag=4&scode=' + id, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {


                return data;
            })
    }
    //--------------------------------

    //IPF----------------------------

    service() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/IPFInvestorSubCategory/w?flag=1').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    guide() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/IPFInvestorSubCategory/w?flag=2').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    complaint() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/IPFInvestorSubCategory/w?flag=3').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    arbitration() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/IPFInvestorSubCategory/w?flag=4').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    regulatory() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/IPFInvestorSubCategory/w?flag=5').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    dissemination() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/IPFInvestorSubCategory/w?flag=6').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    //-------------------------------
    //Graph API --------------

    grp() {
        return fetch('https://api.bseindia.com/msource/GetSensexData.aspx?index=16&flag=1').then((response) => response.text()).then((responsejson) => {
            return responsejson;
        })
    }
    detailgrp(value) {
        return fetch('https://api.bseindia.com/msource/GetStockReachVolPriceDatav1.aspx?index=' + value).then((response) => response.text()).then((responsejson) => {
            return responsejson;
        })
    }

    // Notice -------------

    noticeData(encrypted, no, dt) {
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/NewNotices/w?NoticesNo=' + no + '&Date=' + dt, {
            method: 'GET',
            headers: {
                'Sdt': encrypted
            }
        })
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }
    getquoteseries(id, dt,encrypted) {
        // return fetch('https://api.bseindia.com/msource/CurrMktGetScripcodedepth.aspx?t=d&scrip=' + id + '&date=' + dt).then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('@')[0];
        // })
        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/CurrMktGetScripcodeDepth/w?ln=mr&scode='+id+'&dt='+dt+'&flag=d', {
            method: 'GET',
            headers: {
              'Sdt': encrypted
            }
          })
            .then(response => response.json())
            .then(data => {
                return data ;
            })

    }
    getquotemarket(id, dt,encrypted) {
        // return fetch('https://api.bseindia.com/msource/CurrMktGetScripcodedepth.aspx?t=md&scrip=' + id + '&date=' + dt + '&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#')[0].split('|');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/CurrMktGetScripcodeDepth/w?ln=en&scode=' + id + '&dt='+ dt + '&flag=md', {
            method: 'GET',
            headers: {
              'Sdt': encrypted
            }
          })
            .then(response => response.json())
            .then(data => {
                return data ;
            })
    }

    note() {
        return fetch('https://api.bseindia.com/BseIndiaAPI/api/MKTOINote/w?Flag=C').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    getquoteEquity1(id,encrypted) {
        // return fetch('https://api.bseindia.com/msource/getquote.aspx?scode=' + id + '&ln=en&flag=2').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#')[0].split(',');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/GetQuote/w?ln=en&flag=2&scode=' + id + '&rcount', {
            method: 'GET',
            headers: {
              'Sdt': encrypted
            }
          })
            .then(response => response.json())
            .then(data => {
                return data ;
            })

    }
    getquoteEquity2(id) {
        return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + id + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    getquoteEquity3(id,encrypted) {
        // return fetch('https://api.bseindia.com/msource/getdepth.aspx?scode=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#')[1].split('|');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/GetDepth/w?ln=en&scode='+id, {
        method: 'GET',
        headers: {
          'Sdt': encrypted
        }
      })
        .then(response => response.json())
        .then(data => {
            return data ; 
        })
    }

    getquoteEquity4(id) {
        return fetch('https://api.bseindia.com/msource/getGouteWeekhighlow.aspx?scrip=' + id + '&ln=en&flag=1').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#')[0].split('@');
        })
    }

    getquoteEquity5(id) {
        return fetch(this.domain1+'/ScripPriceBandGSM/GetData?json={"scode":' + id + ',"ln":"en","fields":"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }

    equitygetquoteAnnounce(id) {
        return fetch('https://api.bseindia.com/msource/getann.aspx?scode=' + id + '&flag=1').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');
        })
    }

    equitygetquoteResult(id,encrypted) {
        // return fetch('https://api.bseindia.com/msource/StockreachResult.aspx?scrip=' + id + '&ln=en&type=RES').then((response) => response.text()).then((responsejson) => {
        //     return responsejson.split('#');
        // })

        return fetch('https://mockapi.bseindia.com/BseIndiaAPI/api/StockreachResult/w?ln=en&type=RES&scode='+id, {
            method: 'GET',
            headers: {
              'Sdt': encrypted
            }
          })
            .then(response => response.json())
            .then(data => {
                return data.data.split('#') ; 
            })

    }

    equitygetquoteShare(id) {
        return fetch(this.domain1+'/shp?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }

    equitygetquoteCorp(id) {
        return fetch('https://api.bseindia.com/msource/getGouteWeekhighlow.aspx?Scrip=' + id + '&ln=en&Flag=ca').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');
        })
    }
    equitygetquoteCorp1(id) {
        return fetch(this.domain1+'/DivBonus?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }

    equitygetquoteVoting(id) {
        return fetch(this.domain1+'/VotingRes?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    equitygetquoteBulk(id) {
        return fetch(this.domain1+'/BulkBlock?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }

    sensexgetquote1(id) {
        return fetch('https://api.bseindia.com/msource/getquote.aspx?scode=' + id + '&ln=en&flag=2').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#')[0].split(',');
        })
    }
    sensexgetquote2(id) {
        return fetch(this.domain1+'/GetQuotescripnew/GetData?json={"scode":' + id + ',"ln":"en","fields":"1,2,3,4,11,12"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    sensexgetquote3(id) {
        return fetch(this.domain1+'/ScripPriceBandGSM/GetData?json={"scode":' + id + ',"ln":"en","fields":"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    sensexgetquote4(id) {
        return fetch('https://api.bseindia.com/msource/getdepth.aspx?scode=' + id + '&ln=en').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#')[1].split('|');
        })
    }
    sensexgetquote5(id) {
        return fetch('https://api.bseindia.com/msource/getGouteWeekhighlow.aspx?scrip=' + id + '&ln=en&flag=1').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#')[0].split('@');
        })
    }
    sensexgetquoteAnnounce(id) {
        return fetch('https://api.bseindia.com/msource/getann.aspx?scode=' + id + '&flag=1').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');
        })
    }

    sensexgetquoteResult(id) {
        return fetch('https://api.bseindia.com/msource/StockreachResult.aspx?scrip=' + id + '&ln=en&type=RES').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');
        })
    }
    sensexgetquoteShare(id) {
        return fetch(this.domain1+'/shp?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    sensexgetquoteCorp(id) {
        return fetch('https://api.bseindia.com/msource/getGouteWeekhighlow.aspx?Scrip=' + id + '&ln=en&Flag=ca').then((response) => response.text()).then((responsejson) => {
            return responsejson.split('#');
        })
    }
    sensexgetquoteCorp1(id) {
        return fetch(this.domain1+'/DivBonus?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    sensexgetquoteVoting(id) {
        return fetch(this.domain1+'/VotingRes?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }
    sensexgetquoteBulk(id) {
        return fetch(this.domain1+'/BulkBlock?json={"sc":' + id + ',"ln":"en"}').then((response) => response.json()).then((responsejson) => {
            return responsejson;
        })
    }

}

const api = new API();
export default api;
