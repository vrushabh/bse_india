/**
 * @format
 */
import React from 'react';

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { createStore } from 'redux';
import { Provider } from 'react-redux';



const initialState = {
  theme: 1,
  themename: 'dark',

}
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'light':
      return { theme: 0, themename: 'light' }

    case 'dark':
      return { theme: 1, themename: 'dark' }

  }

  return state

}
const store = createStore(reducer);

const reduxtutorial = () =>
  <Provider store={store}>
    <App />
  </Provider>
AppRegistry.registerComponent(appName, () => reduxtutorial);
