

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './pages/main';
import SplashScreen from 'react-native-splash-screen';

import Icon from 'react-native-vector-icons/Ionicons';
import Search from './pages/search';
import Watchlist from './pages/watchlist';
import Indices from './pages/indices';
import Sensex from './pages/sensex';
import Equity from './pages/equitytab';
import DerivativeTab from './pages/derivativetab';
import CurrencyTab from './pages/currencytab';
import CommadityTab from './pages/commoditytab';
import IrdTabs from './pages/irdtabs';
import SmeTabs from './pages/smetab';
import EtfTabs from './pages/etftabs';
import DebtTabs from './pages/debttabs';
import Corporate from './pages/corporate';
import MarketStatic from './pages/marketstatistics';
import MarketTurn from './pages/marketturn';
import Listing from './pages/listing';
import IPO from './pages/ipo';
import IPF from './pages/ipf';
import Second from './pages/secondhome';
import Graph from './pages/graph';
import DetailGraph from './pages/detailgraph';
import Notice from './pages/notices';
import Getquote from './pages/getquote';
import NewEquity from './pages/equitynewtab';
import NewGetQuote from './pages/getquotenew';
import Demo from './pages/demo';
import SensexAgain from './pages/sensexnew';
import NewGet from './pages/getquotenewagain';
import EditWatch from './pages/editwatch';
import EditPort from './pages/editportfolio';


class App extends Component {

  
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    maindark: Home,
    search: Search,
    watch: Watchlist,
    indices: Indices,
    setting:Sensex,
    equity:Equity,
    derivativetab:DerivativeTab,
    currencytab:CurrencyTab,
    commoditytab:CommadityTab,
    ird:IrdTabs,
    sme:SmeTabs,
    etf:EtfTabs,
    debt:DebtTabs,
    corporate:Corporate,
    marketstatic:MarketStatic,
    marketturn:MarketTurn,
    listing:Listing,
    ipo:IPO,
    ipf:IPF,
    second:Second,
    graph:Graph,
    detailgraph:DetailGraph,
    notice:Notice,
    getquote:Getquote,
    equitynew:NewEquity,
    newgetquote:NewGetQuote,
    demo:Demo,
    sensexagain:SensexAgain,
    newget:NewGet,
    editwatch:EditWatch,
    editport:EditPort
    
    
  },
  {
    initialRouteName: "maindark",
  }
);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default createAppContainer(AppNavigator);

